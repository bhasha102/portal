//
// Watch task
//

'use strict';

module.exports = function(gulp, config, $) {

	gulp.task('watch', function(cb) {

		return require('run-sequence')(
			//['clean'],
			['build'],
			function() {
				gulp.watch(config.iconfont.src, ['iconfont', 'iconfont:svg']);
				gulp.watch(config.styles.src, ['styles']);
				gulp.watch(config.scripts.src, ['scripts']);
				return cb();
			}
		);
	});

	gulp.task('default', ['build']);
};
