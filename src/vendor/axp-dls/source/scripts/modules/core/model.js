import Obj from './obj';

export default class Model extends Obj {
	constructor (opts) {
		super();

		this.attributes = Object.assign({}, this.defaults, opts);
	}

	get (attribute) {
		return attribute ? this.attributes[attribute] : this.attributes;
	}

	set (attribute, value) {
		//Previous values to compare against
		this.previousAttributes = {};

		Object.keys(this.attributes)
			.forEach(attr => {
				this.previousAttributes[attr] = this.attributes[attr];
			})

		//If Multi key/values
		if (typeof attribute === 'object')
			this.attributes = this.merge(
				this.attributes, attribute
			);

		//If setting one attr
		else if (attribute && value!==undefined) {
			this.attributes[attribute] = value;
		}

		//Check for changes
		var changed_attrs = {};

		Object.keys(this.attributes)
			.forEach(attr => {
				if (this.previousAttributes[attr] != this.attributes[attr])
					changed_attrs[attr] = this.attributes[attr];
			})

		//Return changes
		if(attribute)
			if (!attribute.silent)
				this.emit('change', changed_attrs);
			else
				delete attribute.silent;

		return this;
	}

	escape (attribute) {
		return attribute ? this.attributes[attribute] : this;
	}

	has (attribute) {
		return attribute && this.attributes[attribute];
	}

	clear (opts) {
		this.attributes = {};
		if(opts && !opts.silent)
			this.emit('change', null);
	}
}
