//
// Stepper Model
//

import Model from '../../core/model';

export default class StepperModel extends Model {}

StepperModel.prototype.defaults = {
	stepInterval: 50,
	stepDelay: 500,
	min: 0,
	max: 10,
	step: 1,
	value: 5,
	prefix:'',
	suffix:'',
	render: true
};
