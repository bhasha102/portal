//
// Tabs Component
//

import View from '../../core/view';
import Component from '../classes/component';
import TabsModel from '../models/tabs.js';

//
// Configuration
//
const Name = 'Tabs';
const Selector = '[data-toggle="tabs"][role="tablist"]';
const SelectorButton = '[role="tab"]';
const DataKey = 'dls.tabs';
const EventKey = `.${DataKey}`;

//
// Initiator
//
export default class Tabs extends Component {

	constructor (opts) {
		super(opts);
		this.component = TabsView;

		this.render();
		this.onClickListen();
	}
}

//
// Tabs View
//
class TabsView extends View {

	constructor (opts) {
		super(opts);
	}

	//
	// Getters
	//
	static get Name() {return Name;}
	static get Selector() {
		return Selector + ':not([data-ignore]) ' + SelectorButton;
	}
	static get EventKey() {return EventKey;}

	toggle(tab) {
		let controls = tab.attr('aria-controls'),
			panels = [],
			$panels,
			panel = $(`#${controls}`);

		if(!panel.length || !this.target.length)
			return;

		this.target
			.find(SelectorButton)
			.each( (idx, el) => {
				el = $(el);

				if(el.closest(Selector)[0] === this.target[0]) {
					let controls = el.attr('aria-controls'),
						current = $(`#${controls}`)[0];

					if($(current).length)
						if(current !== panel[0])
							panels.push(current);

				}
			})
			.not(tab)
			.attr('aria-selected', false);
		$panels = $(panels);

		tab.attr('aria-selected', true);
		$panels.attr('aria-hidden', true);
		panel.attr('aria-hidden', false);
	}


	//
	// Interface
	//
	static _interface(opts) {
		let $tab = this,
			$el = $tab.closest(Selector),
			data = $el.data(DataKey),
			cmd = opts.invoke ? 'toggle' : null,
			config = Object.assign(
				{},
				$el.data(),
				typeof opts === 'object' && opts
			);

		if(!data) {

			data = new TabsView({
				target: $el,
				model: new TabsModel(config)
			});

			$el.data(DataKey, data);
		}else {
			// Component instance already exists
		}

		if(typeof cmd === 'string')
			if(data[cmd] !== undefined) {
				data[cmd]($tab);
			}

		return data;
	}

	//
	// Destroy
	//
	destroy() {
		$.removeData(this.target, DataKey);
	}
}
