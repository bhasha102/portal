//
// DLS
//
// Create the main application and pass across our
// base config / options

import DLSApp from './modules/app';
window.DLS = new DLSApp({
	scope:$('body'),
	modules: {}
});

DLS.start();
