!function(e){function r(e,r,o){return 4===arguments.length?t.apply(this,arguments):void n(e,{declarative:!0,deps:r,declare:o})}function t(e,r,t,o){n(e,{declarative:!1,deps:r,executingRequire:t,execute:o})}function n(e,r){r.name=e,e in v||(v[e]=r),r.normalizedDeps=r.deps}function o(e,r){if(r[e.groupIndex]=r[e.groupIndex]||[],-1==g.call(r[e.groupIndex],e)){r[e.groupIndex].push(e);for(var t=0,n=e.normalizedDeps.length;n>t;t++){var a=e.normalizedDeps[t],u=v[a];if(u&&!u.evaluated){var d=e.groupIndex+(u.declarative!=e.declarative);if(void 0===u.groupIndex||u.groupIndex<d){if(void 0!==u.groupIndex&&(r[u.groupIndex].splice(g.call(r[u.groupIndex],u),1),0==r[u.groupIndex].length))throw new TypeError("Mixed dependency cycle detected");u.groupIndex=d}o(u,r)}}}}function a(e){var r=v[e];r.groupIndex=0;var t=[];o(r,t);for(var n=!!r.declarative==t.length%2,a=t.length-1;a>=0;a--){for(var u=t[a],i=0;i<u.length;i++){var s=u[i];n?d(s):l(s)}n=!n}}function u(e){return y[e]||(y[e]={name:e,dependencies:[],exports:{},importers:[]})}function d(r){if(!r.module){var t=r.module=u(r.name),n=r.module.exports,o=r.declare.call(e,function(e,r){if(t.locked=!0,"object"==typeof e)for(var o in e)n[o]=e[o];else n[e]=r;for(var a=0,u=t.importers.length;u>a;a++){var d=t.importers[a];if(!d.locked)for(var i=0;i<d.dependencies.length;++i)d.dependencies[i]===t&&d.setters[i](n)}return t.locked=!1,r},r.name);t.setters=o.setters,t.execute=o.execute;for(var a=0,i=r.normalizedDeps.length;i>a;a++){var l,s=r.normalizedDeps[a],c=v[s],f=y[s];f?l=f.exports:c&&!c.declarative?l=c.esModule:c?(d(c),f=c.module,l=f.exports):l=p(s),f&&f.importers?(f.importers.push(t),t.dependencies.push(f)):t.dependencies.push(null),t.setters[a]&&t.setters[a](l)}}}function i(e){var r,t=v[e];if(t)t.declarative?f(e,[]):t.evaluated||l(t),r=t.module.exports;else if(r=p(e),!r)throw new Error("Unable to load dependency "+e+".");return(!t||t.declarative)&&r&&r.__useDefault?r["default"]:r}function l(r){if(!r.module){var t={},n=r.module={exports:t,id:r.name};if(!r.executingRequire)for(var o=0,a=r.normalizedDeps.length;a>o;o++){var u=r.normalizedDeps[o],d=v[u];d&&l(d)}r.evaluated=!0;var c=r.execute.call(e,function(e){for(var t=0,n=r.deps.length;n>t;t++)if(r.deps[t]==e)return i(r.normalizedDeps[t]);throw new TypeError("Module "+e+" not declared as a dependency.")},t,n);c&&(n.exports=c),t=n.exports,t&&t.__esModule?r.esModule=t:r.esModule=s(t)}}function s(r){var t={};if(("object"==typeof r||"function"==typeof r)&&r!==e)if(m)for(var n in r)"default"!==n&&c(t,r,n);else{var o=r&&r.hasOwnProperty;for(var n in r)"default"===n||o&&!r.hasOwnProperty(n)||(t[n]=r[n])}return t["default"]=r,x(t,"__useDefault",{value:!0}),t}function c(e,r,t){try{var n;(n=Object.getOwnPropertyDescriptor(r,t))&&x(e,t,n)}catch(o){return e[t]=r[t],!1}}function f(r,t){var n=v[r];if(n&&!n.evaluated&&n.declarative){t.push(r);for(var o=0,a=n.normalizedDeps.length;a>o;o++){var u=n.normalizedDeps[o];-1==g.call(t,u)&&(v[u]?f(u,t):p(u))}n.evaluated||(n.evaluated=!0,n.module.execute.call(e))}}function p(e){if(I[e])return I[e];if("@node/"==e.substr(0,6))return D(e.substr(6));var r=v[e];if(!r)throw"Module "+e+" not present.";return a(e),f(e,[]),v[e]=void 0,r.declarative&&x(r.module.exports,"__esModule",{value:!0}),I[e]=r.declarative?r.module.exports:r.esModule}var v={},g=Array.prototype.indexOf||function(e){for(var r=0,t=this.length;t>r;r++)if(this[r]===e)return r;return-1},m=!0;try{Object.getOwnPropertyDescriptor({a:0},"a")}catch(h){m=!1}var x;!function(){try{Object.defineProperty({},"a",{})&&(x=Object.defineProperty)}catch(e){x=function(e,r,t){try{e[r]=t.value||t.get.call(e)}catch(n){}}}}();var y={},D="undefined"!=typeof System&&System._nodeRequire||"undefined"!=typeof require&&require.resolve&&"undefined"!=typeof process&&require,I={"@empty":{}};return function(e,n,o,a){return function(u){u(function(u){for(var d={_nodeRequire:D,register:r,registerDynamic:t,get:p,set:function(e,r){I[e]=r},newModule:function(e){return e}},i=0;i<n.length;i++)(function(e,r){r&&r.__esModule?I[e]=r:I[e]=s(r)})(n[i],arguments[i]);a(d);var l=p(e[0]);if(e.length>1)for(var i=1;i<e.length;i++)p(e[i]);return o?l["default"]:l})}}}("undefined"!=typeof self?self:global)

(["1"], [], false, function($__System) {
var require = this.require, exports = this.exports, module = this.module;
!function(e){function n(e,n){e=e.replace(l,"");var r=e.match(u),t=(r[1].split(",")[n]||"require").replace(s,""),i=p[t]||(p[t]=new RegExp(a+t+f,"g"));i.lastIndex=0;for(var o,c=[];o=i.exec(e);)c.push(o[2]||o[3]);return c}function r(e,n,t,o){if("object"==typeof e&&!(e instanceof Array))return r.apply(null,Array.prototype.splice.call(arguments,1,arguments.length-1));if("string"==typeof e&&"function"==typeof n&&(e=[e]),!(e instanceof Array)){if("string"==typeof e){var l=i.get(e);return l.__useDefault?l["default"]:l}throw new TypeError("Invalid require")}for(var a=[],f=0;f<e.length;f++)a.push(i["import"](e[f],o));Promise.all(a).then(function(e){n&&n.apply(null,e)},t)}function t(t,l,a){"string"!=typeof t&&(a=l,l=t,t=null),l instanceof Array||(a=l,l=["require","exports","module"].splice(0,a.length)),"function"!=typeof a&&(a=function(e){return function(){return e}}(a)),void 0===l[l.length-1]&&l.pop();var f,u,s;-1!=(f=o.call(l,"require"))&&(l.splice(f,1),t||(l=l.concat(n(a.toString(),f)))),-1!=(u=o.call(l,"exports"))&&l.splice(u,1),-1!=(s=o.call(l,"module"))&&l.splice(s,1);var p={name:t,deps:l,execute:function(n,t,o){for(var p=[],c=0;c<l.length;c++)p.push(n(l[c]));o.uri=o.id,o.config=function(){},-1!=s&&p.splice(s,0,o),-1!=u&&p.splice(u,0,t),-1!=f&&p.splice(f,0,function(e,t,l){return"string"==typeof e&&"function"!=typeof t?n(e):r.call(i,e,t,l,o.id)});var d=a.apply(-1==u?e:t,p);return"undefined"==typeof d&&o&&(d=o.exports),"undefined"!=typeof d?d:void 0}};if(t)c.anonDefine||c.isBundle?c.anonDefine&&c.anonDefine.name&&(c.anonDefine=null):c.anonDefine=p,c.isBundle=!0,i.registerDynamic(p.name,p.deps,!1,p.execute);else{if(c.anonDefine&&!c.anonDefine.name)throw new Error("Multiple anonymous defines in module "+t);c.anonDefine=p}}var i=$__System,o=Array.prototype.indexOf||function(e){for(var n=0,r=this.length;r>n;n++)if(this[n]===e)return n;return-1},l=/(\/\*([\s\S]*?)\*\/|([^:]|^)\/\/(.*)$)/gm,a="(?:^|[^$_a-zA-Z\\xA0-\\uFFFF.])",f="\\s*\\(\\s*(\"([^\"]+)\"|'([^']+)')\\s*\\)",u=/\(([^\)]*)\)/,s=/^\s+|\s+$/g,p={};t.amd={};var c={isBundle:!1,anonDefine:null};i.amdDefine=t,i.amdRequire=r}("undefined"!=typeof self?self:global);
$__System.register('2', ['3', '4', '5', '6', '7'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Templates;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// Templates Handler
			//
			// On-the-fly templating

			'use strict';

			Templates = (function (_Obj) {
				_inherits(Templates, _Obj);

				function Templates(opts) {
					_classCallCheck(this, Templates);

					_get(Object.getPrototypeOf(Templates.prototype), 'constructor', this).call(this, opts);

					this.escaper = /\\|'|\r|\n|\u2028|\u2029/g;
				}

				_createClass(Templates, [{
					key: 'render',
					value: function render(html, opts) {
						opts = opts || {};

						var template = html,
						    data = opts || {};

						//Must have template passed to render
						if (!template) return;

						//If No Opts fallback to defaults
						var settings = this.settings,
						    escaper = this.escaper,
						    escapes = this.escapes,
						    escapeChar = function escapeChar(match) {
							return '\\' + escapes[match];
						};

						//Compile Vars & Fn
						var index = 0,
						    source = "__p+='",
						    matcher = RegExp([(settings.escape || noMatch).source, (settings.interpolate || noMatch).source, (settings.evaluate || noMatch).source].join('|') + '|$', 'g');

						// Compile the template source, escaping string literals appropriately.
						template.replace(matcher, function (match, escape, interpolate, evaluate, offset) {
							source += template.slice(index, offset).replace(escaper, escapeChar);
							index = offset + match.length;

							if (escape) source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";else if (interpolate) source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";else if (evaluate) source += "';\n" + evaluate + "\n__p+='";

							// Adobe VMs need the match returned to produce the correct offest.
							return match;
						});

						source += "';\n";

						settings.variable = 'obj';
						source = 'with(obj||{}){\n' + source + '}\n';

						source = "var __t,__p='',__j=Array.prototype.join," + "print=function(){__p+=__j.call(arguments,'');};\n" + source + 'return __p;\n';

						var _this = this;

						this.source = 'function(' + settings.variable + '){\n' + source + '}';

						var output = new Function(settings.variable, '_', source).call(this, data, _this);
						output = output.trim().replace(/\r/g, '').replace(/\n/g, '').replace(/\s+/g, ' ').replace(/(">\s)/g, '">').replace(/('>\s)/g, "'>").replace(/(\s<)/g, '<');

						return output;
					}
				}]);

				return Templates;
			})(Obj);

			Templates.prototype.settings = {
				evaluate: /<%([\s\S]+?)%>/g,
				interpolate: /<%=([\s\S]+?)%>/g,
				escape: /<%-([\s\S]+?)%>/g
			};

			Templates.prototype.escapes = {
				"'": "'",
				'\\': '\\',
				'\r': 'r',
				'\n': 'n',
				'\u2028': 'u2028',
				'\u2029': 'u2029'
			};

			_export('default', Templates);
		}
	};
});

$__System.register('8', ['2', '3', '4', '5', '6', '7'], function (_export) {
	var Templates, Obj, _get, _inherits, _createClass, _classCallCheck, uid, Util;

	return {
		setters: [function (_6) {
			Templates = _6['default'];
		}, function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// Utility Functions
			//

			'use strict';

			uid = 0;

			Util = (function (_Obj) {
				_inherits(Util, _Obj);

				function Util(opts) {
					_classCallCheck(this, Util);

					_get(Object.getPrototypeOf(Util.prototype), 'constructor', this).call(this, opts);
					this.setup();
				}

				_createClass(Util, [{
					key: 'setup',
					value: function setup() {
						this.template = new Templates();
					}

					//
					// Get unique ID
					//
				}, {
					key: 'getUID',
					value: function getUID(prefix) {
						uid++;
						return prefix ? prefix + uid : 'el' + uid;
					}

					//
					// Get Query string parameters
					//
				}, {
					key: 'getQueryParameters',
					value: function getQueryParameters(str) {
						return (str || document.location.search).replace(/(^\?)/, '').split("&").map((function (n) {
							return n = n.split("="), this[n[0]] = n[1], this;
						}).bind({}))[0];
					}

					// Throttle
				}, {
					key: 'throttle',
					value: function throttle(func) {
						var ms = arguments.length <= 1 || arguments[1] === undefined ? 500 : arguments[1];
						var context = arguments.length <= 2 || arguments[2] === undefined ? window : arguments[2];

						var to = undefined,
						    pass = undefined,
						    wait = false;

						return function () {
							for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
								args[_key] = arguments[_key];
							}

							var later = function later() {
								func.apply(context, args);
							};

							if (!wait) {
								later();
								wait = true;
								clearTimeout(pass);
								to = setTimeout(function () {
									wait = false;
								}, ms);
							} else {
								clearTimeout(pass);
								pass = setTimeout(function () {
									later();
								}, ms);
							}
						};
					}
				}]);

				return Util;
			})(Obj);

			_export('default', Util);
		}
	};
});

$__System.register('9', ['3', '4', '5', '7', 'a'], function (_export) {
	var Obj, _get, _inherits, _classCallCheck, _Object$assign, AppRouter;

	return {
		setters: [function (_4) {
			Obj = _4['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_a) {
			_Object$assign = _a['default'];
		}],
		execute: function () {
			//
			// Router
			//

			'use strict';

			AppRouter = (function (_Obj) {
				_inherits(AppRouter, _Obj);

				function AppRouter(opts) {
					var _this = this;

					_classCallCheck(this, AppRouter);

					_get(Object.getPrototypeOf(AppRouter.prototype), 'constructor', this).call(this, opts);

					_Object$assign(this, {
						routes: [],
						mode: null,
						root: '/',
						fragment: '',

						config: function config(options) {
							_this.mode = options && options.mode && options.mode == 'history' && !!history.pushState ? 'history' : 'hash';
							_this.root = options && options.root ? '/' + _this.clearSlashes(options.root) + '/' : '/';
							return _this;
						},

						getFragment: function getFragment() {
							var fragment = '';
							if (_this.mode === 'history') {
								fragment = _this.clearSlashes(decodeURI(location.pathname + location.search));

								fragment = fragment.replace(/\?(.*)$/, '');
								fragment = _this.root != '/' ? fragment.replace(_this.root, '') : fragment;
							} else {
								var match = window.location.href.match(/#(.*)$/);
								fragment = match ? match[1] : '';
							}

							return _this.clearSlashes(fragment);
						},

						clearSlashes: function clearSlashes(path) {
							return path.toString().replace(/\/$/, '').replace(/^\//, '');
						},

						add: function add(path, handler) {
							if (typeof path == 'function') {
								handler = path;
								path = '';
							}
							_this.routes.push({ path: path, handler: handler });
							return _this;
						},

						remove: function remove(param) {
							for (var i = 0, r; i < _this.routes.length, r = _this.routes[i]; i++) {
								if (r.handler === param || r.path.toString() === param.toString()) {
									_this.routes.splice(i, 1);
									return _this;
								}
							}
							return _this;
						},

						flush: function flush() {
							_this.routes = [];
							_this.mode = null;
							_this.root = '/';
							return _this;
						},

						check: function check(hash) {
							var _loop = function () {
								var routeParams = {},
								    keys = _this.routes[i].path.match(/:([^\/]+)/g),
								    match = hash.match(new RegExp(_this.routes[i].path.replace(/:([^\/]+)/g, "([^\/]*)")));
								if (match) {
									match.shift();
									match.forEach(function (value, i) {
										routeParams[keys[i].replace(':', '')] = value;
									});

									var fragment = _this.getFragment();

									if (_this.fragment !== fragment) _this.fragment = _this.getFragment();
									_this.routes[i].handler.call({}, routeParams);

									return {
										v: _this
									};
								}
							};

							for (var i = 0, il = _this.routes.length; i < il; i++) {
								var _ret = _loop();

								if (typeof _ret === 'object') return _ret.v;
							}
							return _this;
						},

						bindListen: function bindListen() {
							var _this2 = this;

							var self = this,
							    current = this.root + self.getFragment(),
							    fn = function fn() {
								var fragment = _this2.root + self.getFragment();
								if (current !== fragment && _this2.fragment !== fragment) {
									current = fragment;
									self.check(current);
								}
							};

							clearInterval(this.interval);
							this.interval = setInterval(fn, 50);
							return this;
						},

						navigate: function navigate(path, options) {
							path = path ? path : '';
							if (!options || options === true) options = { trigger: !!options };

							if (_this.mode === 'history') {
								var route = _this.root + _this.clearSlashes(path);
								route = path.replace('//', '/');
								if (history.current === route) return;

								history.pushState(null, null, route);
								history.current = route;
							} else {
								window.location.href = window.location.href.replace(/#(.*)$/, '') + '#' + path;
							}

							if (options.trigger) return _this.check(path);
							return _this;
						}
					});
				}

				return AppRouter;
			})(Obj);

			_export('default', AppRouter);
		}
	};
});

$__System.register('b', ['4', '5', '6', '7', '9'], function (_export) {
	var _get, _inherits, _createClass, _classCallCheck, AppRouter, Router;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_5) {
			AppRouter = _5['default'];
		}],
		execute: function () {
			//
			// The Router
			//

			'use strict';

			Router = (function (_AppRouter) {
				_inherits(Router, _AppRouter);

				function Router(opts) {
					_classCallCheck(this, Router);

					_get(Object.getPrototypeOf(Router.prototype), 'constructor', this).call(this, opts);
					this.listen();
				}

				//
				// Bind the listeners
				//

				_createClass(Router, [{
					key: 'listen',
					value: function listen() {
						$(this.scope).on('click', 'a[href]:not([target="_blank"]):not([data-ignore])', $.proxy(this.linkSelect, this));
					}

					//
					// Link seletected
					// TODO: Pick up more accurately whether link is an internal URL
					//
				}, {
					key: 'linkSelect',
					value: function linkSelect(e) {
						var href = $(e.currentTarget).attr('href');
						href = href.split('../').join('').split('./').join('');
						href = activeRelPath + '/' + href;

						this.navigate(href, { trigger: true });
						e.preventDefault();
					}

					//
					// Start routing
					//
				}, {
					key: 'start',
					value: function start() {
						this.config({ mode: 'history', root: this.calculateRoot() });
						this.bindListen();
						this.app.vent.trigger('router:started');
					}

					//
					// Calculate the root directory
					//
				}, {
					key: 'calculateRoot',
					value: function calculateRoot() {
						var paths = location.pathname.split('/'),
						    relPath = window.relPath.match(/../g),
						    subDirs = 0,
						    dirs = [],
						    rootArr = undefined;

						if (relPath != null) subDirs = relPath.length;

						for (var i = 0, il = paths.length; i < il; ++i) {
							var path = paths[i];
							if (path !== '') dirs.push(path);
						}

						return '/' + dirs.slice(0, -subDirs).join('/') + '/';
					}
				}]);

				return Router;
			})(AppRouter);

			_export('default', Router);
		}
	};
});

$__System.register('c', ['3', '4', '5', '6', '7'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Dimensions;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// Dimensions Handler
			//
			// The dimensions handler manages all dimension
			// related events for the application.
			//
			// It utilizes the â€˜ventâ€˜ for triggering its events.

			'use strict';

			Dimensions = (function (_Obj) {
				_inherits(Dimensions, _Obj);

				function Dimensions(opts) {
					_classCallCheck(this, Dimensions);

					_get(Object.getPrototypeOf(Dimensions.prototype), 'constructor', this).call(this, opts);

					this.debounce = null;
					this.width = 0;
					this.height = 0;

					this.listen();
				}

				// Bind the resize handlers

				_createClass(Dimensions, [{
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.vent.on('resize:trigger', function (e) {
							_this.refresh();
						});

						$(window).on('resize.app', function (e) {
							_this.refresh();
						});
					}

					// Refresh the dimensions
				}, {
					key: 'refresh',
					value: function refresh() {
						this.relayout();
						this.debounceRelayout();
					}

					// Refresh the layout on debounce
				}, {
					key: 'debounceRelayout',
					value: function debounceRelayout() {
						var _this2 = this;

						clearTimeout(this.debounce);
						this.debounce = setTimeout(function (e) {
							_this2.relayout();
						}, 300);
					}

					// Refresh the layout values
				}, {
					key: 'relayout',
					value: function relayout() {
						this.width = $(window).width();
						this.height = $(window).height();
						this.vent.trigger('resize');
					}
				}]);

				return Dimensions;
			})(Obj);

			_export('default', Dimensions);
		}
	};
});

$__System.register('d', ['3', '4', '5', '6', '7', 'e'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Model, Scroller;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_e) {
			Model = _e['default'];
		}],
		execute: function () {
			//
			// Scroller
			//
			// Manages and records the scrolling status of a specified $view.

			'use strict';

			Scroller = (function (_Model) {
				_inherits(Scroller, _Model);

				function Scroller(opts) {
					var _this2 = this;

					_classCallCheck(this, Scroller);

					_get(Object.getPrototypeOf(Scroller.prototype), 'constructor', this).call(this, {
						scrollY: 0,
						scrollX: 0,
						lastScrollY: 0,
						deltaY: 0,
						directionY: null,
						directionX: null,
						scrollRegionHeight: 0,
						scrollBottom: 0,
						scrollHeight: 0,
						percentage: 0
					});

					this.$el = opts.$el;
					this.el = this.$el[0];

					//Used to check raf
					this.ticking = false;

					//Listen
					opts.vent.on('resize', function (e) {
						_this2.listen();
					});
					this.el.addEventListener('scroll', function (e) {
						_this2.listen();
					});
					this.el.addEventListener('touchstart', function (e) {
						_this2.listen();
					});
				}

				_createClass(Scroller, [{
					key: 'listen',
					value: function listen() {
						var _this3 = this;

						var now = Date.now(),
						    timeOffset = undefined;

						if (!this.ticking) {
							(function () {
								var _this = _this3;

								window.requestAnimationFrame(function (e) {
									_this.ticking = false;
								});

								var scrollY = _this3.getScroll('scrollY'),
								    scrollX = _this3.getScroll('scrollX'),
								    scrollRegionHeight = _this3.el.scrollHeight || document.body.scrollHeight,
								    scrollHeight = _this3.$el.height(),
								    scrollBottom = scrollRegionHeight - scrollY - scrollHeight,
								    scrollPercentage = Math.max(0, Math.min(100, (1 - scrollBottom / (scrollRegionHeight - scrollHeight)) * 100));

								_this3.attributes.deltaY = _this3.attributes.lastScrollY - scrollY;

								if (now - _this3.last > 100) _this3.attributes.deltaY = 0;

								_this3.set({
									scrollY: scrollY,
									scrollX: scrollX,
									scrollHeight: scrollHeight,
									scrollRegionHeight: scrollRegionHeight,
									scrollBottom: scrollBottom,
									percentage: scrollPercentage,
									directionY: (function () {
										var noPrevY = !_this.previousAttributes || _this.previousAttributes && _this.previousAttributes.scrollY === 0;
										if (scrollY === 0 && noPrevY) return null;else return scrollY < _this.get('scrollY') ? 'up' : 'down';
									})(),
									directionX: (function () {
										var noPrevX = !_this.previousAttributes || _this.previousAttributes && _this.previousAttributes.scrollX === 0;
										if (scrollX === 0 && noPrevX) return null;else return scrollX < _this.get('scrollX') ? 'left' : 'right';
									})()
								});
								_this3.last = Date.now();
								_this3.attributes.lastScrollY = _this3.attributes.scrollY;
								_this3.ticking = true;
							})();
						}
					}
				}, {
					key: 'getScroll',
					value: function getScroll(dir) {
						//ScrollY, only on window
						if (this.el[dir]) return this.el[dir];

						//ScrollY fallback, only on window
						var pageOffset = dir == 'scrollY' ? this.el.pageYOffset : this.el.pageXOffset;
						if (pageOffset !== undefined) return pageOffset;

						//Overflow elements or newer legacy browser
						var legacyDir = dir == 'scrollY' ? 'scrollTop' : 'scrollLeft';
						if (this.el[legacyDir]) return this.el[legacyDir];

						//CSS1 browsers, only for window
						var isCSS1Compat = (document.compatMode || "") === "CSS1Compat";
						return isCSS1Compat ? document.documentElement[legacyDir] : document.body[legacyDir];
					}
				}]);

				return Scroller;
			})(Model);

			_export('default', Scroller);
		}
	};
});

$__System.register('f', ['3', '4', '5', '6', '7'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Pages;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// Pages
			//

			'use strict';

			Pages = (function (_Obj) {
				_inherits(Pages, _Obj);

				function Pages(opts) {
					_classCallCheck(this, Pages);

					_get(Object.getPrototypeOf(Pages.prototype), 'constructor', this).call(this, opts);
					this.pages = [];
					this.currentPage = null;
					this.previousPage = null;
					this.root = '/';

					this.transitionIn();
					this.listen();
					this.registerRoute();
				}

				//
				// Bind the listeners
				//

				_createClass(Pages, [{
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.app.vent.on('router:started', function (e) {
							_this.currentPage = _this.root + _this.app.router.getFragment();
						});
					}

					//
					// Register the top level route
					//
				}, {
					key: 'registerRoute',
					value: function registerRoute() {
						var _this2 = this;

						this.router.add(this.root, function () {
							_this2.setPage('/' + _this2.router.getFragment());
						});
					}

					//
					// Set the page route
					//
				}, {
					key: 'setPage',
					value: function setPage(path) {
						var _this3 = this;

						if (!this.previousPage) this.previousPage = '/' + this.router.getFragment();
						if (this.currentPage === path) return;

						this.currentPage = path;

						this.app.vent.trigger('page:changing');

						if (this.previousPage) {
							this.close(this.previousPage, function () {
								if (path === _this3.currentPage) _this3.open(path);
							});
						} else {
							this.open(path);
						}

						this.previousPage = this.currentPage;
					}

					//
					// Close Route
					//
				}, {
					key: 'close',
					value: function close(path, cb) {
						var _this4 = this;

						//this.app.regions.page.removeClass('page-active');
						this.app.regions.page.find('.dls-panel-row, .dls-pagination').each(function () {
							$(this).removeClass('row-active');
						});

						clearTimeout(this.openTimeout);
						clearTimeout(this.closeTimeout);
						this.closeTimeout = setTimeout(function () {
							_this4.app.regions.page.empty();
							if (typeof cb === 'function') cb();
						}, 300);
					}

					//
					// Open route
					//
				}, {
					key: 'open',
					value: function open(path) {
						if (!this.pages[path]) {
							this.pages[path] = this.createPage(path);
						} else {
							this.show(this.pages[path]);
						}
					}

					//
					// Create the page
					//
				}, {
					key: 'createPage',
					value: function createPage(path) {
						var page = {
							path: path,
							loaded: false,
							html: ''
						};

						this.loadPage(page, true);

						return page;
					}

					//
					// Show the Page
					//
				}, {
					key: 'show',
					value: function show(page) {
						if (!page.loaded || this.currentPage !== page.path) return;

						window.activeRelPath = page.rel;

						this.updateLinks();

						var $page = $(page.html);

						this.app.regions.page.empty().append($page);

						this.app.regions.scroll.scrollTop(0);
						this.app.scroll.set('scrollY', 1);

						this.transitionIn();
						$(window).trigger('resize');
						this.app.vent.trigger('page:changed', page);

						DLS.render();
						this.app.render();

						window.prevRelPath = window.activeRelPath;
					}
				}, {
					key: 'updateLinks',
					value: function updateLinks() {
						var prev = undefined;
						if (window.prevRelPath != null) {
							prev = window.prevRelPath;
						} else {
							prev = window.relPath;
						}

						$('a').each(function (idx, el) {
							var $el = $(el),
							    href = $el.attr('href');

							if (href) if (href.indexOf(prev) > -1) {
								$el.attr('href', href.replace(prev, window.activeRelPath));
							}
						});
					}

					//
					// Transition in
					//
				}, {
					key: 'transitionIn',
					value: function transitionIn() {
						this.app.regions.content.find('.dls-panel-row, .dls-pagination').each(function (index, el) {
							var $row = $(el),
							    time = Math.min(1000, (index + 1) * 100);

							setTimeout(function () {
								$row.addClass('row-active');
							}, time);
						});
					}

					//
					// Load Page
					//
				}, {
					key: 'loadPage',
					value: function loadPage(page) {
						var _this5 = this;

						var ajax = $.ajax({
							url: page.path,
							data: {},
							dataType: 'html',
							complete: function complete(data) {
								var html = data.responseText.split('<!-- [START] -->')[1].split('<!-- [END] -->')[0],
								    splitRel = data.responseText.split('data-rel="')[1],
								    rel = splitRel.substr(0, splitRel.indexOf('"'));

								window.activeRelPath = rel;
								page.rel = rel;
								page.html = html;
								page.loaded = true;

								_this5.show(page);
							},
							error: function error(e) {}
						});
					}
				}]);

				return Pages;
			})(Obj);

			_export('default', Pages);
		}
	};
});

$__System.register('10', ['3', '4', '5', '6', '7', '8', '11', 'b', 'c', 'd', 'f'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Util, EventAggregator, Router, Dimensions, Scroller, Pages, DLSApplication;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_7) {
			Util = _7['default'];
		}, function (_6) {
			EventAggregator = _6['default'];
		}, function (_b) {
			Router = _b['default'];
		}, function (_c) {
			Dimensions = _c['default'];
		}, function (_d) {
			Scroller = _d['default'];
		}, function (_f) {
			Pages = _f['default'];
		}],
		execute: function () {
			//
			// DLS App
			//
			// It all starts here.

			'use strict';

			DLSApplication = (function (_Obj) {
				_inherits(DLSApplication, _Obj);

				function DLSApplication(opts) {
					_classCallCheck(this, DLSApplication);

					_get(Object.getPrototypeOf(DLSApplication.prototype), 'constructor', this).call(this, opts);
					Obj.prototype.scope = this.$el;
					Obj.prototype.app = this;

					this.prepare();
					this.createVent();
					this.addRegions();
					this.createUtils();
					this.createRouter();
					this.createDimensionsManager();
					this.createScrollManager();
					this.createPageManager();
					this.listen();
				}

				// Prepare

				_createClass(DLSApplication, [{
					key: 'prepare',
					value: function prepare() {
						if (!this.$el) this.$el = $('body');
					}

					// Create the vent event aggregator that we can
					// subscribe and publish events to. A la Marionette
				}, {
					key: 'createVent',
					value: function createVent() {
						this.vent = new EventAggregator();
					}

					// Add the core application regions
				}, {
					key: 'addRegions',
					value: function addRegions() {
						this.regions = {
							header: $('.dls-navbar', this.$el),
							scroll: $(window),
							menu: $('.dls-menu', this.$el),
							breadcrumbs: $('.dls-breadcrumbs', this.$el),
							content: $('.dls-container', this.$el),
							page: $('#dls-page-container', this.$el)
						};
					}

					// Create the utility helpers
				}, {
					key: 'createUtils',
					value: function createUtils() {
						this.util = new Util({
							vent: this.vent
						});

						Obj.prototype.util = this.util;
					}
				}, {
					key: 'createRouter',
					value: function createRouter() {
						Obj.prototype.router = new Router();
					}

					// Create the dimensions handler
				}, {
					key: 'createDimensionsManager',
					value: function createDimensionsManager() {
						this.dimensions = new Dimensions({
							vent: this.vent
						});
					}
				}, {
					key: 'createScrollManager',

					// Create the scroll manager that records and stores
					// scrolling behaviour
					value: function createScrollManager() {
						this.scroll = new Scroller({
							vent: this.vent,
							$el: this.regions.scroll
						});
					}

					//
					// Create the page manager
					//
				}, {
					key: 'createPageManager',
					value: function createPageManager() {
						this.pages = new Pages();
					}

					// Bind our key events
				}, {
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.on('before:start', function (e) {
							_this.beforeStart();
						}).on('after:start', function (e) {
							_this.afterStart();
						});
					}

					// Before Start tasks
				}, {
					key: 'beforeStart',
					value: function beforeStart() {}
				}, {
					key: 'afterStart',
					value: function afterStart() {
						this.vent.trigger('resize:trigger');
					}

					// Start the application
				}, {
					key: 'start',
					value: function start() {
						this.emit('before:start');
						this.initialize();
						this.emit('after:start');
					}

					// Initializer
				}, {
					key: 'initialize',
					value: function initialize() {
						this.startModules();
						this.router.start();
					}

					// Render the application
				}, {
					key: 'render',
					value: function render() {
						this.trigger('render');
						return this;
					}

					// Start all the supplied app modules
				}, {
					key: 'startModules',
					value: function startModules() {
						var _this2 = this;

						$.each(this.modules, function (key, Module) {
							_this2.modules[key] = new Module();
						});
					}
				}]);

				return DLSApplication;
			})(Obj);

			_export('default', DLSApplication);
		}
	};
});

$__System.register('12', ['3', '4', '5', '6', '7'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Header;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// Header Module
			//
			// Create the header module that manages the top header

			'use strict';

			Header = (function (_Obj) {
				_inherits(Header, _Obj);

				function Header(opts) {
					_classCallCheck(this, Header);

					_get(Object.getPrototypeOf(Header.prototype), 'constructor', this).call(this, opts);
					this.render();
				}

				// Render the application

				_createClass(Header, [{
					key: 'render',
					value: function render() {}
				}]);

				return Header;
			})(Obj);

			_export('default', Header);
		}
	};
});

$__System.register('13', ['3', '4', '5', '6', '7'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, MenuSearch;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// Menu Search
			//

			'use strict';

			MenuSearch = (function (_Obj) {
				_inherits(MenuSearch, _Obj);

				function MenuSearch(opts) {
					_classCallCheck(this, MenuSearch);

					_get(Object.getPrototypeOf(MenuSearch.prototype), 'constructor', this).call(this, opts);

					this.render();
					this.listen();
				}

				//
				// Render
				//

				_createClass(MenuSearch, [{
					key: 'render',
					value: function render() {
						var _this = this;

						this.searchField = DLS.create('search', {
							target: this.target,
							config: {
								live: false
							}
						});

						this.app.vent.on('search:change', function (value) {
							_this.searchField.set('value', value);
						}).on('search:state', function (state) {
							_this.searchField.set('state', state);
						}).on('page:changed', function () {
							if (document.location.pathname.indexOf('/search/') > -1) {} else {
								_this.searchField.set('value', '');
								_this.searchField.set('state', 'default');
							}
						});
					}

					//
					// Listen
					//
				}, {
					key: 'listen',
					value: function listen() {
						var _this2 = this;

						this.searchField.on('change', function (value) {
							_this2.app.router.navigate(activeRelPath + '/search/?q=' + value);
							_this2.app.vent.trigger('search', value);
							//this.searchField.set('state', 'searching');
						});
					}
				}]);

				return MenuSearch;
			})(Obj);

			_export('default', MenuSearch);
		}
	};
});

$__System.register('14', ['3', '4', '5', '6', '7'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, MenuBreadcrumbs;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// Menu Breadcrumbs
			//
			// Manages the menu menu breadcrumbs

			'use strict';

			MenuBreadcrumbs = (function (_Obj) {
				_inherits(MenuBreadcrumbs, _Obj);

				function MenuBreadcrumbs(opts) {
					_classCallCheck(this, MenuBreadcrumbs);

					_get(Object.getPrototypeOf(MenuBreadcrumbs.prototype), 'constructor', this).call(this, opts);
					this.enabled = true;
					this.toggled = false;

					this.setProps();
					this.listen();
					this.update();
				}

				//
				// Store the properties
				//

				_createClass(MenuBreadcrumbs, [{
					key: 'setProps',
					value: function setProps() {
						this.$sel = {};
						this.$sel.trail = this.$el.find('.breadcrumb');
					}

					// Bind the events
				}, {
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.app.vent.on('page:changing', $.proxy(this.disable, this)).on('page:changed', $.proxy(this.enable, this));

						this.$el.find('.btn-dls-burger').on('click', function (e) {
							_this.toggled = !_this.toggled;
							_this.emit('menu:toggle', _this.toggled);
							e.preventDefault();
						});

						this.$sel.trail.on('click', 'a', function (e) {
							_this.emit('menu:open');
							e.preventDefault();
						});
					}

					//
					// Disable (occurs when the page is changing)
					//
				}, {
					key: 'disable',
					value: function disable() {
						this.enabled = false;
					}

					//
					// Enable (occurs when the page has changed)
					//
				}, {
					key: 'enable',
					value: function enable() {
						this.enabled = true;
					}

					//
					// Update the breadcrumb trail based on the
					// currently active menu trail
					//
				}, {
					key: 'update',
					value: function update() {
						if (!this.enabled) return;

						var trail = this.getTrail(),
						    trailString = this.generateTrailString(trail);

						this.$sel.trail.empty().append(trailString);
					}

					//
					// Get the breadcrumb trail
					//
				}, {
					key: 'getTrail',
					value: function getTrail() {
						var trail = [],
						    $trails = this.$nav.find('.nav-item.open > a, .nav-item.active > a');

						$trails.each(function (index, el) {
							var $el = $(el);

							trail.push({
								title: $el.text(),
								active: $el.closest('.nav-item').is('.active')
							});
						});

						return trail;
					}

					//
					// Generate the trail string from the array
					//
				}, {
					key: 'generateTrailString',
					value: function generateTrailString(trail) {
						var str = '',
						    item = undefined;

						for (var i = 0, il = trail.length; i < il; ++i) {
							item = trail[i];
							if (!item.active || il === 1) str += '<li><a>' + item.title + '</a></li>';else str += '<li>' + item.title + '</li>';
						}

						return str;
					}
				}]);

				return MenuBreadcrumbs;
			})(Obj);

			_export('default', MenuBreadcrumbs);
		}
	};
});

$__System.register('15', ['3', '4', '5', '6', '7'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, MenuNav;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// Menu Nav
			//
			// Manages the menu navigation options

			'use strict';

			MenuNav = (function (_Obj) {
				_inherits(MenuNav, _Obj);

				function MenuNav(opts) {
					_classCallCheck(this, MenuNav);

					_get(Object.getPrototypeOf(MenuNav.prototype), 'constructor', this).call(this, opts);
					this.offset = 0;
					this.anchors = [];
					this.enabled = true;

					this.setProps();
					this.listen();
					this.pageChanged();
				}

				//
				// Set the properties
				//

				_createClass(MenuNav, [{
					key: 'setProps',
					value: function setProps() {
						this.$sel = {};
						this.$sel.additional = this.$el.find('.menu-additional');
					}

					//
					// Bind the listeners
					//
				}, {
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.app.vent.on('resize', $.proxy(this.updateDimensions, this)).on('page:changing', $.proxy(this.disable, this)).on('page:changed', $.proxy(this.pageChanged, this)).on('router:started', $.proxy(this.checkExistingHash, this));

						this.updateDimensions();

						this.scroll.on('change', this.app.util.throttle($.proxy(this.update, this), 30)).on('change', this.app.util.throttle($.proxy(this.updateBreadcrumbs, this)), 300);

						this.$content.on('click', 'a', function (e) {
							var $el = $(e.currentTarget);

							if ($el.attr('href') != null) if ($el.attr('href').indexOf('#') > -1) {
								var hash = $el.attr('href').split('#')[1];
								_this.hashChange(hash, true);
							}
						});

						this.$el.find('.nav-link[href]').on('click', function (e) {
							var $el = $(e.currentTarget);

							if ($el.attr('href').indexOf('#') > -1) {
								var hash = $el.attr('href').split('#')[1];
								_this.hashChange(hash, true);
							} else {
								if ($el.parent().is('.open')) _this.$scroll.scrollTop(0);
								_this.currentHash = null;
							}

							if (!$el.is('[data-closable="false"]') && _this.app.dimensions.width < _this.app.breakpoints.md) {
								_this.trigger('menu:close');
							}
						});

						this.$sel.additional.find('a').on('click', function (e) {
							_this.$el.find('.nav-item.accordion.open > [data-toggle="accordion"]').trigger('menu:close');
						});
					}
				}, {
					key: 'updateDimensions',
					value: function updateDimensions() {
						this.offset = this.app.dimensions.width <= this.app.breakpoints.xl ? 100 : 45;
					}
				}, {
					key: 'checkExistingHash',
					value: function checkExistingHash() {
						var hash = document.location.hash;
						if (hash.length) {
							var _name = hash.split('#')[1];
							this.hashChange(_name, true);
						}
					}

					//
					// Disable (occurs when the page is changing)
					//
				}, {
					key: 'disable',
					value: function disable() {
						this.enabled = false;
					}

					//
					// pageChanged (occurs when the page has changed)
					//
				}, {
					key: 'pageChanged',
					value: function pageChanged() {
						var _this2 = this;

						var hash = document.location.hash.split('#')[1];
						this.compile();
						this.refreshMenu();
						this.update({ scrollY: 0 });
						this.enabled = true;
						this.checkExistingHash();
						setTimeout(function () {
							_this2.update(_this2.scroll.attributes);
						}, 300);
					}

					//
					// Refresh the menu when the page route is changed.
					// We need to ensure it is kept in sync with how it should appear for the
					// currently active route.
					//
				}, {
					key: 'refreshMenu',
					value: function refreshMenu() {
						var _this3 = this;

						this.$el.find('.nav-item > .nav-link[href]').each(function (index, el) {
							var $el = $(el),
							    href = $(el).attr('href').split(window.activeRelPath)[1].split('#')[0];

							if (href === document.location.pathname.replace(_this3.router.root, '/')) {

								if ($el.is('[data-toggle="accordion"]') && !$el.parent().is('.open')) {
									var $parentAccordion = $el.parent().parent().closest('.nav-item.accordion');

									if ($parentAccordion.length) {

										$el.trigger('data', {
											expand: true,
											instant: $parentAccordion.is('.open') ? false : true
										});

										$parentAccordion.find('> .nav-link[data-toggle="accordion"]').trigger('data', {
											expand: true,
											openAutoExpanded: false
										});
									} else {
										$el.trigger('data', { expand: true });
									}
								} else if (!$el.is('[data-toggle="accordion"]') && !$el.closest('.accordion').length) {
									$el.closest('.nav-item').addClass('open');
									_this3.$el.find('.nav-item.accordion.open > [data-toggle="accordion"]').trigger('data', { expand: false });
								} else {}
							} else {
								if (document.location.pathname.indexOf('/search/') > -1) {
									$el.trigger('data', { expand: false });
									$el.closest('.nav-item').removeClass('open');
								}

								if (!$el.is('[data-toggle="accordion"]')) {
									$el.trigger('data', { expand: false });
									$el.closest('.nav-item').removeClass('open');
								}
							}
						});
					}

					//
					// Recompile the page anchors and reset the active anchors
					// in the nav menu.
					//
				}, {
					key: 'compile',
					value: function compile() {
						var _this4 = this;

						this.anchors = [];
						this.$content.find('a[name]').each(function (index, el) {
							var $el = $(el),
							    id = $el.attr('name');

							$el.removeAttr('name');

							_this4.anchors.push({
								name: id,
								id: '#' + id,
								$target: $el,
								y: 0
							});
						});
					}

					//
					// Update the scroll location
					//
				}, {
					key: 'update',
					value: function update(e, hashTrigger) {
						if (!this.enabled || e.scrollY === undefined) return;

						var scrollReal = this.scroll.attributes.percentage * 0.01 * this.scroll.attributes.scrollRegionHeight,
						    activeAnchor = this.getAnchor(scrollReal);

						if (this.hashTrigger || hashTrigger) {
							if (!hashTrigger) this.hashTrigger = false;
							activeAnchor = this.hashTarget;
						}

						this.$el.find('.nav-item.active').removeClass('active');

						if (activeAnchor) {
							this.$el.find('.nav-item > .nav-link[href$="' + activeAnchor.id + '"]').parent().addClass('active');
							this.hashChange(activeAnchor.name);
						} else {
							this.$el.find('.nav-item.accordion.open:last').addClass('active');
							this.hashChange('!');
						}
					}

					//
					// Update the breadcrumbs on an extended throttle
				}, {
					key: 'updateBreadcrumbs',
					value: function updateBreadcrumbs() {
						this.breadcrumbs.update();
					}

					//
					// Get closest anchor
					//
				}, {
					key: 'getAnchor',
					value: function getAnchor(scrollReal) {
						var anchor = null;
						this.calculateAnchors();

						for (var i = 0, il = this.anchors.length; i < il; ++i) {
							if (this.anchors[i].y <= scrollReal) {
								anchor = this.anchors[i];
							}
						}

						return anchor;
					}

					//
					// Calculate the anchor positions
					//
				}, {
					key: 'calculateAnchors',
					value: function calculateAnchors() {
						var _this5 = this;

						this.anchors.forEach(function (obj, index) {
							var y = obj.$target.offset().top,
							    real = y - _this5.offset; //y - this.$scroll.offset().top;
							_this5.anchors[index].y = real;
							_this5.anchors[index].worldY = _this5.anchors[index].y / _this5.scroll.attributes.scrollRegionHeight;
						});
					}

					//
					// Hash Change
					//
				}, {
					key: 'hashChange',
					value: function hashChange(hash, trigger) {
						var _this6 = this;

						if (this.currentHash === hash || this.hashChanging) return;

						var hashTarget = this.getAnchorByName(hash);

						if (hashTarget === null) return;

						if (document.location.hash !== '#' + hash) document.location.hash = hash;

						this.currentHash = hash;
						this.updateBreadcrumbs();

						if (!trigger || !this.enabled) return;

						this.hashTrigger = true;
						this.hashTarget = this.getAnchorByName(hash);

						this.hashChanging = true;
						setTimeout(function () {
							_this6.hashChanging = false;
						}, 100);

						$(window).trigger('resize');

						var target = this.getAnchorByName(hash),
						    scrollReal = this.scroll.attributes.percentage * 0.01 * this.scroll.attributes.scrollRegionHeight;

						if (!target) return;

						this.calculateAnchors();
						this.$scroll.scrollTop(target.y);

						this.update(this.scroll.attributes, true);
					}

					//
					// Get an anchor by its hash name, returns null if not exist
					//
				}, {
					key: 'getAnchorByName',
					value: function getAnchorByName(hash) {
						var target = null;

						for (var i = 0, il = this.anchors.length; i < il; ++i) {
							if (this.anchors[i].name === hash) {
								target = this.anchors[i];
								break;
							}
						}

						return target;
					}
				}]);

				return MenuNav;
			})(Obj);

			_export('default', MenuNav);
		}
	};
});

$__System.register('16', ['3', '4', '5', '6', '7', 'e'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Model, Mousewheel;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_e) {
			Model = _e['default'];
		}],
		execute: function () {
			//
			// Mousewheel
			//
			// Manages and records mousewheel or touch events

			'use strict';

			Mousewheel = (function (_Obj) {
				_inherits(Mousewheel, _Obj);

				function Mousewheel(opts) {
					_classCallCheck(this, Mousewheel);

					_get(Object.getPrototypeOf(Mousewheel.prototype), 'constructor', this).call(this, opts);
					this.slice = Array.prototype.slice, this.el = this.$el[0] || window;
					this.touch = false;
					this.setup();
				}

				_createClass(Mousewheel, [{
					key: 'setup',
					value: function setup() {
						if (this.touch) this.setupTouch();else this.setupMousewheel();
					}
				}, {
					key: 'setupTouch',
					value: function setupTouch() {}
				}, {
					key: 'setupMousewheel',
					value: function setupMousewheel() {
						var _this = this;

						this.nullLowestDeltaTimeout;
						this.lowestDelta;

						var bindings = 'onwheel' in document || document.documentMode >= 9 ? ['wheel'] : ['mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'];

						for (var i = 0, il = bindings.length; i < il; ++i) {
							this.el.addEventListener(bindings[i], function () {
								_this.mousewheelHandler.apply(_this, arguments);
								if (_this.preventDefault) arguments[0].preventDefault();
							}, false);
						}
					}
				}, {
					key: 'mousewheelHandler',
					value: function mousewheelHandler(e) {
						var _this2 = this;

						var orgEvent = e || window.event,
						    args = this.slice.call(arguments, 1),
						    delta = 0,
						    deltaX = 0,
						    deltaY = 0,
						    absDelta = 0,
						    offsetX = 0,
						    offsetY = 0,
						    boundingRect = undefined;

						if ('delta' in orgEvent) deltaY = orgEvent.detail * -1;
						if ('wheelDelta' in orgEvent) deltaY = orgEvent.wheelDelta;
						if ('wheelDataY' in orgEvent) deltaY = orgEvent.wheelDelta;
						if ('wheelDeltaX' in orgEvent) deltaX = orgEvent.wheelDeltaX * -1;

						if ('axis' in orgEvent && orgEvent.axis == orgEvent.HORIZONTAL_AXIS) {
							deltaX = deltaY * 1;
							deltaY = 0;
						}

						delta = deltaY === 0 ? deltaX : deltaY;

						if ('deltaY' in orgEvent) {
							deltaY = orgEvent.deltaY * -1;
							delta = deltaY;
						}

						if ('deltaX' in orgEvent) {
							deltaX = orgEvent.deltaX;
							if (deltaY == 0) delta = deltaX * -1;
						}

						if (deltaY === 0 && deltaX === 0) return;

						absDelta = Math.max(Math.abs(deltaY), Math.abs(deltaX));

						if (!this.lowestDelta || absDelta < this.lowestDelta) {
							this.lowestDelta = absDelta;

							if (this.shouldAdjustOldDeltas(orgEvent, absDelta)) {
								this.lowestDelta /= 40;
							}
						}

						if (this.shouldAdjustOldDeltas(orgEvent, absDelta)) {
							delta /= 40;
							deltaX /= 40;
							deltaY /= 40;
						}

						delta = Math[delta >= 1 ? 'floor' : 'ceil'](delta / this.lowestDelta);
						deltaX = Math[deltaX >= 1 ? 'floor' : 'ceil'](deltaX / this.lowestDelta);
						deltaY = Math[deltaY >= 1 ? 'floor' : 'ceil'](deltaY / this.lowestDelta);

						boundingRect = this.el.getBoundingClientRect();
						offsetX = e.clientX - boundingRect.left;
						offsetY = event.clientY - boundingRect.top;;

						args.unshift(e, delta, deltaX, deltaY);

						if (this.nullLowestDeltaTimeout) clearTimeout(this.nullLowestDeltaTimeout);
						this.nullLowestDeltaTimeout = setTimeout(function (e) {
							_this2.lowestDelta = null;
						}, 100);

						this.emit('scroll', {
							x: deltaX,
							y: deltaY
						});
					}
				}, {
					key: 'shouldAdjustOldDeltas',
					value: function shouldAdjustOldDeltas(orgEvent, absDelta) {
						return orgEvent.type === 'mousewheel' && absDelta % 120 === 0;
					}
				}]);

				return Mousewheel;
			})(Obj);

			_export('default', Mousewheel);
		}
	};
});

$__System.register('17', ['3', '4', '5', '6', '7', '16'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Mousewheel, MenuNavScroll;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_6) {
			Mousewheel = _6['default'];
		}],
		execute: function () {
			//
			// Menu Scroll
			//
			// Manages the menu scroll position

			'use strict';

			MenuNavScroll = (function (_Obj) {
				_inherits(MenuNavScroll, _Obj);

				function MenuNavScroll(opts) {
					_classCallCheck(this, MenuNavScroll);

					_get(Object.getPrototypeOf(MenuNavScroll.prototype), 'constructor', this).call(this, opts);

					this.pos = {
						y: 0
					};

					this.createScroll();
					this.listen();
				}

				_createClass(MenuNavScroll, [{
					key: 'createScroll',
					value: function createScroll() {
						if (!DLS.platform.touch) this.scroll = new Mousewheel({
							$el: this.$el,
							preventDefault: true
						});
					}

					// Bind the required listeners
				}, {
					key: 'listen',
					value: function listen() {
						var _this = this;

						if (!DLS.platform.touch) {
							this.contentScroll.on('change', function (e) {
								_this.scrollContent(e);
							});

							this.scroll.on('scroll', function (e) {
								_this.updateScrollPosition(e);
							});
						}

						this.breadcrumbs.on('menu:toggle', function (active) {
							if (active) _this.resetPosition();
						});
					}

					//
					// Reset the position on command
					//
				}, {
					key: 'resetPosition',
					value: function resetPosition() {
						this.pos.y = 0;
						this.relayout();
					}

					//
					// Update the scroll position
					//
				}, {
					key: 'updateScrollPosition',
					value: function updateScrollPosition(e) {
						this.pos.y += e.y;
						this.relayout();
					}

					// The primary content view was scrolled
				}, {
					key: 'scrollContent',
					value: function scrollContent(e) {
						this.pos.y += this.contentScroll.attributes.deltaY;
						this.relayout();
					}

					// Relayout the position of the menu based on the co-ordinates.
					// Ensure it is clamped into its region.
				}, {
					key: 'relayout',
					value: function relayout() {
						this.pos.y = Math.min(0, Math.max(-this.$el[0].scrollHeight + this.contentScroll.attributes.scrollHeight, this.pos.y));
						this.$el.scrollTop(-this.pos.y);
					}
				}]);

				return MenuNavScroll;
			})(Obj);

			_export('default', MenuNavScroll);
		}
	};
});

$__System.register('18', ['3', '4', '5', '6', '7', '13', '14', '15', '17'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, MenuSearch, MenuBreadcrumbs, MenuNav, MenuNavScroll, Menu;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_6) {
			MenuSearch = _6['default'];
		}, function (_7) {
			MenuBreadcrumbs = _7['default'];
		}, function (_8) {
			MenuNav = _8['default'];
		}, function (_9) {
			MenuNavScroll = _9['default'];
		}],
		execute: function () {
			//
			// Menu Module
			//
			// Create the menu module

			'use strict';

			Menu = (function (_Obj) {
				_inherits(Menu, _Obj);

				function Menu(opts) {
					_classCallCheck(this, Menu);

					_get(Object.getPrototypeOf(Menu.prototype), 'constructor', this).call(this, opts);
					this.render();
				}

				//
				// Render the application
				//

				_createClass(Menu, [{
					key: 'render',
					value: function render() {
						this.setProps();
						this.createSearch();
						this.createBreadcrumbs();
						this.createScroll();
						this.createList();

						this.listen();
					}

					//
					// Store the selectors
					//
				}, {
					key: 'setProps',
					value: function setProps() {
						this.$sel = {};
						this.$sel.nav = this.$el.find('nav');
						this.$sel.navViewport = this.$el.find('.menu-viewport');
					}

					//
					// Create the search
					//
				}, {
					key: 'createSearch',
					value: function createSearch() {
						this.search = new MenuSearch({
							target: this.$sel.nav.find('[data-toggle="search"]')
						});
					}

					//
					// Create the breadcrumbs
					//
				}, {
					key: 'createBreadcrumbs',
					value: function createBreadcrumbs() {
						this.breadcrumbs = new MenuBreadcrumbs({
							$el: this.$breadcrumbs,
							$nav: this.$sel.nav.find('> .nav, .menu-additional .menu-additional-inner > .nav')
						});
					}

					//
					// Create scroll
					//
				}, {
					key: 'createScroll',
					value: function createScroll() {
						this.navScroll = new MenuNavScroll({
							$el: this.$sel.navViewport,
							$contentScroll: this.$scroll,
							contentScroll: this.scroll,
							breadcrumbs: this.breadcrumbs
						});
					}

					// Create the menu nav list
				}, {
					key: 'createList',
					value: function createList() {
						this.nav = new MenuNav({
							$el: this.$el,
							$scroll: this.$scroll,
							$content: this.$content,
							scroll: this.scroll,
							breadcrumbs: this.breadcrumbs
						});
					}

					//
					// Bind the listeners
					//
				}, {
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.$el.find('.btn-nav-close').on('click', function (e) {
							_this.closeMenu();
							e.preventDefault();
						});

						this.$el.on('transitionend', function (e) {
							$(window).trigger('resize');
						});

						this.breadcrumbs.on('menu:toggle', function (e) {
							_this.toggleMenu();
						}).on('menu:open', function () {
							_this.openMenu();
						});

						this.nav.on('menu:close', function (e) {
							_this.closeMenu();
						});
					}

					//
					// Toggle the menu on command
					//
				}, {
					key: 'toggleMenu',
					value: function toggleMenu() {
						this.$el.toggleClass('toggled');
						this.$breadcrumbs.toggleClass('toggled');
					}

					//
					// Close the menu on command
					//
				}, {
					key: 'closeMenu',
					value: function closeMenu() {
						this.$el.removeClass('toggled');
						this.$breadcrumbs.removeClass('toggled');
					}

					//
					// Open the menu on command
					//
				}, {
					key: 'openMenu',
					value: function openMenu() {
						this.$el.addClass('toggled');
						this.$breadcrumbs.addClass('toggled');
					}
				}]);

				return Menu;
			})(Obj);

			_export('default', Menu);
		}
	};
});

$__System.register('19', ['3', '4', '5', '6', '7', '12', '18'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Header, Menu, UI;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_6) {
			Header = _6['default'];
		}, function (_7) {
			Menu = _7['default'];
		}],
		execute: function () {
			//
			// User Interface Module
			//
			// User interface handling system.
			// Create and manages the core UI elements
			// E.g. - Header and menu

			'use strict';

			UI = (function (_Obj) {
				_inherits(UI, _Obj);

				function UI(opts) {
					_classCallCheck(this, UI);

					_get(Object.getPrototypeOf(UI.prototype), 'constructor', this).call(this, opts);
					this.render();
				}

				// Create the UI Regions (supplied from the app)

				_createClass(UI, [{
					key: 'createUIRegions',
					value: function createUIRegions() {
						this.header = new Header({
							$el: this.app.regions.header
						});

						this.menu = new Menu({
							$el: this.app.regions.menu,
							$breadcrumbs: this.app.regions.breadcrumbs,
							$scroll: this.app.regions.scroll,
							$content: this.app.regions.content,
							scroll: this.app.scroll
						});
					}

					// Render the application
				}, {
					key: 'render',
					value: function render() {
						this.createUIRegions();
					}
				}]);

				return UI;
			})(Obj);

			_export('default', UI);
		}
	};
});

(function() {
var define = $__System.amdDefine;
define("1a", [], function() {
  return "\n<!doctype html>\n<html>\n\t<head>\n\n\t\t<link rel=\"stylesheet\" href=\"<%= rel %>/assets/vendor/dls/styles/dls-exclude-iconfont.css\">\n\t\t<link rel=\"stylesheet\" href=\"<%= rel %>/assets/styles/dls-docs.css\"/>\n\t</head>\n\t<body class=\"mode-desktop\" style=\"background-color: #FFF; height: auto; overflow-y:hidden;\">\n\t\t<div class=\"dls-iframe-content\">\n\t\t\t<%= content %>\n\t\t</div>\n\n\t\t<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js\"></script>\n\t\t<script>window.jQuery || document.write('<script src=\"<%= rel %>/assets/scripts/vendor/jquery/jquery.min.js\"><\\/script>')</script>\n\t\t<script src=\"<%= rel %>/assets/scripts/vendor/misc/clipboard.min.js\"></script>\n\t\t<script src=\"<%= rel %>/assets/vendor/dls/scripts/dls.js\"></script>\n\t</body>\n</html>\n";
});

})();
$__System.register('1b', ['3', '4', '5', '6', '7', '1a'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, TmplIFrame, CodeExample;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_a) {
			TmplIFrame = _a['default'];
		}],
		execute: function () {
			//
			// Code Example Component
			//

			'use strict';

			CodeExample = (function (_Obj) {
				_inherits(CodeExample, _Obj);

				function CodeExample(opts) {
					_classCallCheck(this, CodeExample);

					_get(Object.getPrototypeOf(CodeExample.prototype), 'constructor', this).call(this, opts);
					this.listen();
					this.render();
				}

				// htmlEntities(str) {
				//     return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
				// }

				//
				// Bind the events
				//

				_createClass(CodeExample, [{
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.app.on('render', function (e) {
							_this.render();
						});
					}

					// Render the component asynchronously
				}, {
					key: 'render',
					value: function render() {
						var _this2 = this;

						$(this.selector + ':not([data-rendered])', this.scope).each(function (idx, el) {

							var $el = $(el);
							_this2.createElement($el);
						});
					}
				}, {
					key: 'createElement',
					value: function createElement($el) {
						var _this3 = this;

						if (!$.contains(document.body, $el[0])) return;

						$el.css({ visibility: 'visible' });

						$el.attr('data-rendered', '');

						var $sourceToggle = $el.find('.dls-example-source-toggle'),
						    $viewToggles = $el.find('.dls-example-view-toggle'),
						    $exampleMode = $el.find('.dls-example-mode'),
						    $output = $el.find('.dls-example-output'),
						    $codeView = $el.find('.code-highlight'),
						    $pre = $el.find('pre'),
						    $copyBtn = $el.find('a.dls-example-source-copy-btn'),
						    $iframe = undefined,
						    timeout = undefined,
						    offset = undefined;

						var clipper = new Clipboard($copyBtn[0], {
							target: function target(trigger) {
								return $pre[0];
							}
						});

						var updateDimensions = function updateDimensions() {
							//if(!$.contains(document.body, $iframe[0])) {
							_this3.app.vent.off('resize', _this3.util.throttle(updateDimensions, 50));
							//destroy();
							//	return;
							//}
							if ($iframe) if ($iframe[0].contentWindow) $iframe.height($('.dls-iframe-content', $iframe[0].contentWindow.document.body).outerHeight(true) + 50);
						};

						if ($el.hasClass('breakpoints-true')) {
							$('[data-rendered]', $output).removeAttr('data-rendered');
							$iframe = $('<iframe class="dls-iframe" />');
							$output.after($iframe);
							$iframe[0].contentWindow.document.write(this.util.template.render(TmplIFrame, {
								rel: window.activeRelPath || '.',
								content: $output.html()
							}));

							$iframe[0].contentWindow.document.close();
							$output.remove();

							//if($($viewToggles[0]).hasClass('icon-active')) $iframe.css('height', $output.css('max-height'));
							//else $iframe.css('height', $output.css('min-height'));

							this.app.vent.on('resize', this.util.throttle(updateDimensions, 50));
						}

						clipper.on('success', function (e) {
							$copyBtn.text($copyBtn.attr('data-copied'));
							clearTimeout(timeout);
							timeout = setTimeout(function (e) {
								$copyBtn.text($copyBtn.attr('data-default'));
							}, 2000);
						});

						clipper.on('error', function (e) {
							$copyBtn.text($copyBtn.attr('data-selected'));
							clearTimeout(timeout);
							timeout = setTimeout(function (e) {
								$copyBtn.text($copyBtn.attr('data-default'));
							}, 2000);
						});

						$viewToggles.on('click', function (e) {

							var $toggle = $(e.target);

							if ($toggle.hasClass('dls-example-mobile-toggle')) {
								$($viewToggles[1]).addClass('icon-active');
								$($viewToggles[0]).removeClass('icon-active');
								$exampleMode.addClass('mode-mobile').addClass('reduced-mode');

								$exampleMode.removeClass('mode-desktop');
							} else {
								$($viewToggles[1]).removeClass('icon-active');
								$($viewToggles[0]).addClass('icon-active');
								$exampleMode.removeClass('mode-mobile').removeClass('reduced-mode');

								$exampleMode.addClass('mode-desktop');
							}
							updateDimensions();
							$(window).trigger('resize');
						});

						$sourceToggle.on('click', function (e) {
							var active = !$el.is('.toggled');
							$el.toggleClass('toggled');
							e.preventDefault();
						});

						var destroy = function destroy() {
							$sourceToggle.off('click');
							$viewToggles.off('click');
							clipper.destroy();
						};
					}
				}]);

				return CodeExample;
			})(Obj);

			_export('default', CodeExample);
		}
	};
});

$__System.register('1c', ['3', '4', '5', '6', '7'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Swatch;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// Swatch Component
			//

			'use strict';

			Swatch = (function (_Obj) {
				_inherits(Swatch, _Obj);

				function Swatch(opts) {
					_classCallCheck(this, Swatch);

					_get(Object.getPrototypeOf(Swatch.prototype), 'constructor', this).call(this, opts);
					this.listen();
					this.render();
				}

				//
				// Bind the events
				//

				_createClass(Swatch, [{
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.app.on('render', function (e) {
							_this.render();
						});
					}

					// Render the component asynchronously
				}, {
					key: 'render',
					value: function render() {
						$(this.selector + ':not([data-rendered])', this.scope).each(function (idx, el) {

							var $copy = $(el).find('a.swatch-color'),
							    $btn = $(el).find('.swatch-copy-btn'),
							    $code = $(el).find('.swatch-code'),
							    timeout = undefined;

							var clipper = new Clipboard($copy[0], {
								target: function target(trigger) {
									return $code[0];
								}
							});

							$(el).attr('data-rendered', '');

							clipper.on('success', function (e) {
								$btn.text($btn.attr('data-copied'));
								clearTimeout(timeout);
								timeout = setTimeout(function (e) {
									$btn.text($btn.attr('data-default'));
								}, 2000);
							});

							clipper.on('error', function (e) {
								$btn.text($btn.attr('data-selected'));
								clearTimeout(timeout);
								timeout = setTimeout(function (e) {
									$btn.text($btn.attr('data-default'));
								}, 2000);
							});
						});
					}
				}]);

				return Swatch;
			})(Obj);

			_export('default', Swatch);
		}
	};
});

$__System.register('1d', ['3', '4', '5', '6', '7'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Scaler;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// Scaler Component
			//

			'use strict';

			Scaler = (function (_Obj) {
				_inherits(Scaler, _Obj);

				function Scaler(opts) {
					_classCallCheck(this, Scaler);

					_get(Object.getPrototypeOf(Scaler.prototype), 'constructor', this).call(this, opts);
					this.listen();
					this.render();
				}

				//
				// Bind the events
				//

				_createClass(Scaler, [{
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.app.on('render', function (e) {
							_this.render();
						});
					}

					// Render the component asynchronously
				}, {
					key: 'render',
					value: function render() {
						$(this.selector + ':not([data-rendered])', this.scope).each(function (idx, el) {
							var $scalar = $('.scaler-el', el),
							    $slider = $('[data-toggle="slider"]', el),
							    slider = undefined;

							if (!$slider.length || !$scalar.length) return;

							$(el).attr('data-rendered', '');

							// Create the DLS UI slider. We don't need add a config
							// as it already exists on our $slider element, but we can modify
							// values here if we needed
							slider = DLS.create('slider', {
								target: $slider,
								config: { min: 22, max: 82, value: 47, tooltip: true }
							});

							// Bind the listeners
							var listen = function listen() {
								slider.on('change', function (args) {
									sliderChange(args);
								});
							},
							    sliderChange = function sliderChange(val) {
								//console.log(val);
								$scalar.css({ width: val, height: val });
							},
							   

							// Start
							start = function start() {};

							listen();
							start();
						});
					}
				}]);

				return Scaler;
			})(Obj);

			_export('default', Scaler);
		}
	};
});

$__System.register('1e', ['3', '4', '5', '6', '7'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, SpeedSlider, debounce;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// Scaler Component
			//

			'use strict';

			SpeedSlider = (function (_Obj) {
				_inherits(SpeedSlider, _Obj);

				function SpeedSlider(opts) {
					_classCallCheck(this, SpeedSlider);

					_get(Object.getPrototypeOf(SpeedSlider.prototype), 'constructor', this).call(this, opts);
					this.listen();
					this.render();
				}

				//
				// Bind the events
				//

				_createClass(SpeedSlider, [{
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.app.on('render', function (e) {
							_this.render();
						});
					}

					// Render the component asynchronously
				}, {
					key: 'render',
					value: function render() {

						$(this.selector + ':not([data-rendered])', this.scope).each(function (idx, el) {

							var $slider = $('[data-toggle="slider"]', el);
							var $ball = $(el).find('.ball');
							var slider;

							if (!$slider.length) return;

							$(el).attr('data-rendered', '');

							// Create the DLS slider. We don't need add a config
							// as it already exists on our $slider element, but we can modify
							// values here if we needed
							slider = DLS.create('slider', {
								target: $slider,
								config: { min: Number($slider.attr("data-min")), max: Number($slider.attr("data-max")), step: 0.01, value: (Number($slider.attr("data-max")) + Number($slider.attr("data-min"))) / 2, tooltip: true, suffix: 's' }
							});

							// Bind the listeners
							var timeout = null,
							    listen = function listen() {
								slider.on('change', function (args) {
									sliderChangeDeboucned(args);
								});
							};

							var sliderChange = function sliderChange(val) {

								//var percent = val/100;
								//var clipPercent = (percent * 0.8) + 0.1;
								//var duration = clipPercent * 2;
								$ball.removeClass('el-animate');
								$ball.css({
									'animation-duration': val + 's',
									'-ms-animation-duration': val + 's',
									'-moz-animation-duration': val + 's',
									'-webkit-animation-duration': val + 's'
								});

								clearTimeout(timeout);

								timeout = setTimeout(function () {
									$ball.addClass('el-animate');
								}, 10);
							};

							var sliderChangeDeboucned = debounce(sliderChange, 500);

							// Start
							var start = function start() {};

							listen();
							start();
						});
					}
				}]);

				return SpeedSlider;
			})(Obj);

			_export('default', SpeedSlider);

			debounce = function debounce(func, wait) {

				var timeout, args, context, timestamp;

				return function () {

					context = this;
					args = [].slice.call(arguments, 0);
					timestamp = new Date();

					var later = function later() {

						var last = new Date() - timestamp;

						if (last < wait) {
							timeout = setTimeout(later, wait - last);
						} else {
							timeout = null;
							func.apply(context, args);
						}
					};

					if (!timeout) {
						timeout = setTimeout(later, wait);
					}
				};
			};
		}
	};
});

$__System.register('1f', ['3', '4', '5', '6', '7'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, ColorSlider;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// Scaler Component
			//

			'use strict';

			ColorSlider = (function (_Obj) {
				_inherits(ColorSlider, _Obj);

				function ColorSlider(opts) {
					_classCallCheck(this, ColorSlider);

					_get(Object.getPrototypeOf(ColorSlider.prototype), 'constructor', this).call(this, opts);
					this.listen();
					this.render();
				}

				//
				// Bind the events
				//

				_createClass(ColorSlider, [{
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.app.on('render', function (e) {
							_this.render();
						});
					}

					// Render the component asynchronously
				}, {
					key: 'render',
					value: function render() {
						var _this2 = this;

						$(this.selector + ':not([data-rendered])', this.scope).each(function (idx, el) {
							var defaultHex = '#64bfd3';
							var $hex = $(el).find("#hex");
							var $rgb = $(el).find("#rgb");
							var $icon = $(el).find('.icon');

							var listen = function listen() {
								$hex.on('keyup', hexChange);
								$rgb.on('keyup', rgbChange);
							};

							var setDefaultColorValue = function setDefaultColorValue() {
								$hex.val(defaultHex);
								$rgb.val(_this2.hexToRgb(defaultHex));
								$icon.css('color', defaultHex);
							};

							var hexChange = function hexChange(e) {
								var val = e.currentTarget.value;
								$rgb.val(_this2.hexToRgb(val));
								$icon.css('color', val);
							};

							var rgbChange = function rgbChange(e) {
								var val = e.currentTarget.value;
								$hex.val(_this2.rgbToHex(val));
								$icon.css('color', val);
							};

							listen();
							setDefaultColorValue();
						});
					}
				}, {
					key: 'hexToRgb',
					value: function hexToRgb(hex) {
						var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
						return result ? 'rgba(' + parseInt(result[1], 16) + ', ' + parseInt(result[2], 16) + ', ' + parseInt(result[3], 16) + ', 1)' : null;
					}
				}, {
					key: 'rgbToHex',
					value: function rgbToHex(rgb) {
						rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
						return rgb && rgb.length === 4 ? "#" + ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) + ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) + ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';
					}
				}]);

				return ColorSlider;
			})(Obj);

			_export('default', ColorSlider);
		}
	};
});

(function() {
var define = $__System.amdDefine;
define("20", [], function() {
  return "<div class=\"tiltcard-container\">\n\t<div class=\"dls-tiltcard\">\n\t\t<%= content %>\n\t\t<div class=\"tiltcard-shadow\">\n\t\t\t<img src=\"<%= rel %>/assets/img/visuals/elements/image/card-shadow.png\" alt=\"Card Shadow\" role=\"presentation\" />\n\t\t</div>\n\t\t<div class=\"tiltcard-gloss\">\n\t\t\t<img src=\"<%= rel %>/assets/img/visuals/elements/image/card-gloss.png\" alt=\"Card Gloss\" role=\"presentation\" />\n\t\t</div>\n\t</div>\n\n</div>\n";
});

})();
$__System.register('21', ['3', '4', '5', '6', '7', '20'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, TmplTiltCard, TiltCard;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_6) {
			TmplTiltCard = _6['default'];
		}],
		execute: function () {
			//
			// Code Example Component
			//

			'use strict';

			TiltCard = (function (_Obj) {
				_inherits(TiltCard, _Obj);

				function TiltCard(opts) {
					_classCallCheck(this, TiltCard);

					_get(Object.getPrototypeOf(TiltCard.prototype), 'constructor', this).call(this, opts);
					this.listen();
					this.render();
				}

				//
				// Bind the events
				//

				_createClass(TiltCard, [{
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.app.on('render', function (e) {
							_this.render();
						});
					}

					// Render the component asynchronously
				}, {
					key: 'render',
					value: function render() {
						var _this2 = this;

						$(this.selector + ':not([data-rendered])', this.scope).each(function (idx, el) {

							var $el = $(el);
							$el.attr('data-rendered', '');

							var $card = $(_this2.util.template.render(TmplTiltCard, {
								content: $el.html(),
								rel: window.activeRelPath
							})),
							    $sel = {
								tilt: $card.find(_this2.selector),
								gloss: $card.find('.tiltcard-gloss')
							};

							$el.after($card);
							$el.remove();

							$card.on(DLS.platform.event.over + ' ' + DLS.platform.event.move, function (e) {

								$sel.tilt.css('transform', e.offsetX / $card.width() < 0.5 ? 'rotateY(-30deg)' : 'rotateY(30deg)');
								$sel.gloss.css('transform', e.offsetX / $card.width() < 0.5 ? 'translateX(25%)' : 'translate(-25%)');
							});
							$card.on(DLS.platform.event.out, function (e) {
								$sel.tilt.css('transform', 'rotateY(0deg)');
								$sel.gloss.css('transform', 'translateX(0%)');
							});
						});
					}
				}]);

				return TiltCard;
			})(Obj);

			_export('default', TiltCard);
		}
	};
});

$__System.register('22', ['3', '4', '5', '6', '7'], function (_export) {
    var Obj, _get, _inherits, _createClass, _classCallCheck, ProgressAnimator;

    return {
        setters: [function (_5) {
            Obj = _5['default'];
        }, function (_) {
            _get = _['default'];
        }, function (_2) {
            _inherits = _2['default'];
        }, function (_3) {
            _createClass = _3['default'];
        }, function (_4) {
            _classCallCheck = _4['default'];
        }],
        execute: function () {
            //
            // Swatch Component
            //

            'use strict';

            ProgressAnimator = (function (_Obj) {
                _inherits(ProgressAnimator, _Obj);

                function ProgressAnimator(opts) {
                    _classCallCheck(this, ProgressAnimator);

                    _get(Object.getPrototypeOf(ProgressAnimator.prototype), 'constructor', this).call(this, opts);

                    // Current step (int)
                    this.currStep = 0;
                    // Main selector for the progress bar.
                    this.selector = opts.selector;
                    // Steps of the progres bars (el[])
                    this.steps = null;
                    // Leverage the progress bars
                    this.progressSelector = opts.progressSelector;
                    // Progress bar (if there is one)
                    this.bar = null;
                    // Percent Complete
                    this.percent = 0;
                    // Timer Interval
                    this.timerInt = null;

                    this.listen();
                    this.setup();
                }

                //
                // Bind the events
                //

                _createClass(ProgressAnimator, [{
                    key: 'listen',
                    value: function listen() {
                        var _this = this;

                        this.app.on('render', function (e) {
                            _this.setup();
                        });
                    }

                    /**
                     * Grab our progress bar element here, and then reset everything to default.
                     *
                     */
                }, {
                    key: 'setup',
                    value: function setup() {
                        /*$('iframe').each((idx, el) => {
                        	if(el.contentWindow) {
                        		let $body = $(el.contentWindow.document.body);
                        		}
                        });*/

                        //TODO: Figure out if this is the best way to select.
                        this.steps = $(this.selector + ':not([data-rendered])', this.scope).find('.nav-progress-step');

                        if (typeof this.progressSelector != 'undefined') {
                            this.bar = $(this.selector + ':not([data-rendered])', this.scope).find('.nav-progress-bar');
                        }

                        // Reset and start the cycle here.
                        this.reset();
                    }

                    /**
                     * Resets the progress bar to its default state
                     *
                     * @return;
                     */
                }, {
                    key: 'reset',
                    value: function reset() {
                        this.stopTimer();
                        this.currStep = 0;
                        //TODO: Fix this. It's a bit of a hack. Bad logic.
                        //this.percent    = 5;
                        this.percent = 0; // added padding to the bar.
                        this.render();

                        this.startTimer();
                    }

                    /**
                     * Increments the step and calls the render function.
                     * When it hits the last step, it pauses the timer, and resets
                     * after 5 seconds.
                     *
                     * @return;
                     */
                }, {
                    key: 'nextStep',
                    value: function nextStep() {
                        var _this2 = this;

                        this.currStep++;

                        if (this.currStep == this.steps.length) {
                            this.stopTimer();

                            setTimeout(function () {
                                return _this2.reset();
                            }, 5000);
                        } else {
                            this.percent = this.currStep * (100 / (this.steps.length - 1));
                            this.render();
                        }
                    }

                    /**
                     * Starts the timer to animate to the next steps.
                     */
                }, {
                    key: 'startTimer',
                    value: function startTimer() {
                        var _this3 = this;

                        this.timerInt = setInterval(function () {
                            return _this3.nextStep();
                        }, 2000);
                    }

                    /**
                     * Stops the interval to animate to the next steps
                     */
                }, {
                    key: 'stopTimer',
                    value: function stopTimer() {
                        clearInterval(this.timerInt);
                    }

                    /**
                     * Render based on our current model.
                     *
                     */
                }, {
                    key: 'render',
                    value: function render() {
                        for (var i = 0; i < this.steps.length; i++) {
                            var $el = $(this.steps[i]);

                            $el.attr('data-rendered', '');

                            if (i == this.currStep) {
                                $el.addClass('active');
                                $el.addClass('current');
                                $el.removeClass('disabled');
                            } else if (i < this.currStep) {
                                $el.addClass('active');
                                $el.removeClass('current');
                                $el.removeClass('disabled');
                            } else {
                                $el.removeClass('active');
                                $el.removeClass('current');
                                $el.addClass('disabled');
                            }
                        }

                        // If we have a progress bar. Set the width.
                        if (this.bar != null) {
                            this.bar.css("width", this.percent + "%");
                        }
                    }
                }]);

                return ProgressAnimator;
            })(Obj);

            _export('default', ProgressAnimator);
        }
    };
});

$__System.register('23', ['3', '4', '5', '6', '7'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, InputValidation;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// Swatch Component
			//
			'use strict';

			InputValidation = (function (_Obj) {
				_inherits(InputValidation, _Obj);

				function InputValidation(opts) {
					_classCallCheck(this, InputValidation);

					_get(Object.getPrototypeOf(InputValidation.prototype), 'constructor', this).call(this, opts);
					this.listen();
					this.render();
				}

				//
				// Bind the events
				//

				_createClass(InputValidation, [{
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.app.on('render', function (e) {
							_this.render();
						});
					}

					// Render the component asynchronously
				}, {
					key: 'render',
					value: function render() {

						var $el;

						$(this.selector + ':not([data-rendered])', this.scope).each(function (idx, el) {

							$el = $(el);
							reset($el);
							$el.attr('data-rendered', '');
							$el.data('original-text', $el.attr('value'));
						});

						function reset($el) {
							$el.data('index', 0);
							$el.val('');
							setTimeout(typeIntoForm, 1500, $el);
							$el.parent().removeClass('has-success');
						}

						function typeIntoForm($el) {
							if ($el.data('original-text')) {
								$el.val($el.val() + $el.data('original-text').charAt($el.data('index')));
								$el.data('index', parseInt($el.data('index')) + 1);
								if ($el.data('index') < $el.data('original-text').length) setTimeout(typeIntoForm, Math.random() * 50 + 25, $el);else setTimeout(complete, 250, $el);
							}
						}

						function complete($el) {
							$el.parent().addClass('has-success');
							setTimeout(reset, 2500, $el);
						}
					}
				}]);

				return InputValidation;
			})(Obj);

			_export('default', InputValidation);
		}
	};
});

$__System.register('24', ['3', '4', '5', '6', '7'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Loaders;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// Code Example Component
			//

			//import TmplTiltCard from './templates/tmpl-tiltcard.html!text';

			'use strict';

			Loaders = (function (_Obj) {
				_inherits(Loaders, _Obj);

				function Loaders(opts) {
					_classCallCheck(this, Loaders);

					_get(Object.getPrototypeOf(Loaders.prototype), 'constructor', this).call(this, opts);
					this.listen();
					this.render();
				}

				//
				// Bind the events
				//

				_createClass(Loaders, [{
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.app.on('render', function (e) {
							_this.render();
						});
					}

					// Render the component asynchronously
				}, {
					key: 'render',
					value: function render() {

						$(this.selector + ':not([data-rendered])', this.scope).each(function (idx, el) {

							var $el = $(el);
							$el.attr('data-rendered', '');

							var randomize = function randomize() {

								var rand = Math.floor(Math.random() * 100);
								if (Math.abs($el.attr('data-count') - rand) > 10) $el.attr('data-count', rand);else randomize();
							};

							randomize();
							setInterval(randomize, 3000);
						});
					}
				}]);

				return Loaders;
			})(Obj);

			_export('default', Loaders);
		}
	};
});

$__System.register('25', ['3', '4', '5', '6', '7', 'a'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, _Object$assign, Component;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_a) {
			_Object$assign = _a['default'];
		}],
		execute: function () {
			//
			// Component Base Class
			//

			'use strict';

			Component = (function (_Obj) {
				_inherits(Component, _Obj);

				function Component(opts) {
					_classCallCheck(this, Component);

					_get(Object.getPrototypeOf(Component.prototype), 'constructor', this).call(this, opts);
					this.listenCore();
				}

				_createClass(Component, [{
					key: 'throwError',
					value: function throwError(message) {
						var name = 'Component';
						if (this.component.Name) name = this.component.Name;
						throw new Error('[DLS > ' + name + ' Error]: ' + message);
						return this;
					}
				}, {
					key: 'listenCore',
					value: function listenCore() {
						var _this = this;

						this.app.on('render', function () {
							_this.render();
						});
					}

					//
					// Default onClick listen
					//
				}, {
					key: 'onClickListen',
					value: function onClickListen() {
						var _this2 = this;

						if (!this.component) return;

						this.scope.on('click' + this.component.EventKey, this.component.Selector, function (e) {
							e.preventDefault();
							_this2.create({
								target: e.currentTarget,
								config: {
									invoke: true
								}
							});
						});
					}

					//
					// Default Render
					//
				}, {
					key: 'render',
					value: function render() {
						var _this3 = this;

						if (!this.component) return;

						this.scope.find(this.component.Selector).each(function (idx, el) {
							_this3.create({
								target: el,
								config: {}
							});
						});
					}

					//
					// Default `Create`
					//
				}, {
					key: 'create',
					value: function create(options) {
						var _this4 = this;

						if (!this.component) return;

						if (!options.target) {
							this.throwError('A `target` must be specified!');
							return this;
						}

						var elements = [];
						options.config = _Object$assign({}, options.config);
						options.target = $(options.target);
						options.target.each(function (idx, el) {
							var element = _this4.component._interface.call($(el), options.config);

							if (element) elements.push(element);
						});

						return this.created(elements);
					}

					//
					// Return the created elements in flexible format
					//
				}, {
					key: 'created',
					value: function created(elements) {
						if (elements.length === 1) return elements[0];else if (elements.length) return elements;else return null;
					}
				}]);

				return Component;
			})(Obj);

			_export('default', Component);
		}
	};
});

$__System.registerDynamic("26", ["27", "28", "29", "2a"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var $ = $__require('27'),
      toObject = $__require('28'),
      IObject = $__require('29');
  module.exports = $__require('2a')(function() {
    var a = Object.assign,
        A = {},
        B = {},
        S = Symbol(),
        K = 'abcdefghijklmnopqrst';
    A[S] = 7;
    K.split('').forEach(function(k) {
      B[k] = k;
    });
    return a({}, A)[S] != 7 || Object.keys(a({}, B)).join('') != K;
  }) ? function assign(target, source) {
    var T = toObject(target),
        $$ = arguments,
        $$len = $$.length,
        index = 1,
        getKeys = $.getKeys,
        getSymbols = $.getSymbols,
        isEnum = $.isEnum;
    while ($$len > index) {
      var S = IObject($$[index++]),
          keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S),
          length = keys.length,
          j = 0,
          key;
      while (length > j)
        if (isEnum.call(S, key = keys[j++]))
          T[key] = S[key];
    }
    return T;
  } : Object.assign;
  return module.exports;
});

$__System.registerDynamic("2b", ["2c", "26"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var $export = $__require('2c');
  $export($export.S + $export.F, 'Object', {assign: $__require('26')});
  return module.exports;
});

$__System.registerDynamic("2d", ["2b", "2e"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  $__require('2b');
  module.exports = $__require('2e').Object.assign;
  return module.exports;
});

$__System.registerDynamic("a", ["2d"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = {
    "default": $__require('2d'),
    __esModule: true
  };
  return module.exports;
});

$__System.register('2f', ['3', '4', '5', '6', '7', 'e'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Model, View;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_e) {
			Model = _e['default'];
		}],
		execute: function () {
			//
			// View
			//

			'use strict';

			View = (function (_Obj) {
				_inherits(View, _Obj);

				function View(opts) {
					_classCallCheck(this, View);

					_get(Object.getPrototypeOf(View.prototype), 'constructor', this).call(this, opts);

					this.uid = this.util.getUID();

					if (!this.target) console.warn('[DLS > View]: No target specified for view');
					if (!this.model) this.model = new Model(opts);

					/*this.model.on('change', (...args) => {
     	this.emit('change', args);
     });*/
				}

				_createClass(View, [{
					key: 'render',
					value: function render() {
						console.warn('[DLS > View]: No render available.');
					}

					//
					// Pass the 'get' to the model
					//
				}, {
					key: 'get',
					value: function get() {
						var _model;

						return (_model = this.model).get.apply(_model, arguments);
					}

					//
					// Pass the 'set' to the model
					//
				}, {
					key: 'set',
					value: function set() {
						var _model2;

						return (_model2 = this.model).set.apply(_model2, arguments);
					}
				}]);

				return View;
			})(Obj);

			_export('default', View);
		}
	};
});

(function() {
var define = $__System.amdDefine;
define("30", [], function() {
  return "<div>\n\t<form class=\"search\" data-toggle=\"search\" data-state=\"default\" role=\"search\">\n\t\t<input type=\"search\" name=\"egInputSearch\" placeholder=\"Search\" value=\"<%= value %>\">\n\t\t<button type=\"submit\">\n\t\t\t<span class=\"search-state\"></span>\n\t\t</button>\n\t</form>\n\t<div class=\"dls-search\">\n\t\t<div class=\"dls-search-info\">\n\t\t\t<% if(value !== '') { %>Results for \"<%= value %>\"<% } %>\n\t\t</div>\n\t\t<div class=\"dls-search-results\">\n\n\t\t</div>\n\t</div>\n</div>\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("31", [], function() {
  return "<div class=\"dls-search-result\">\n\t<div class=\"dls-search-result-title\">\n\t\t<a href=\"<%= relPath %><%= url %>\"><%= title %></a>\n\t</div>\n\t<div class=\"dls-search-result-info\">\n\t\t<p><%= description %></p>\n\t</div>\n</div>\n";
});

})();
$__System.registerDynamic("32", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var toString = {}.toString;
  module.exports = function(it) {
    return toString.call(it).slice(8, -1);
  };
  return module.exports;
});

$__System.registerDynamic("29", ["32"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var cof = $__require('32');
  module.exports = Object('z').propertyIsEnumerable(0) ? Object : function(it) {
    return cof(it) == 'String' ? it.split('') : Object(it);
  };
  return module.exports;
});

$__System.registerDynamic("33", ["29", "34"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var IObject = $__require('29'),
      defined = $__require('34');
  module.exports = function(it) {
    return IObject(defined(it));
  };
  return module.exports;
});

$__System.registerDynamic("35", ["33", "36"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var toIObject = $__require('33');
  $__require('36')('getOwnPropertyDescriptor', function($getOwnPropertyDescriptor) {
    return function getOwnPropertyDescriptor(it, key) {
      return $getOwnPropertyDescriptor(toIObject(it), key);
    };
  });
  return module.exports;
});

$__System.registerDynamic("37", ["27", "35"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var $ = $__require('27');
  $__require('35');
  module.exports = function getOwnPropertyDescriptor(it, key) {
    return $.getDesc(it, key);
  };
  return module.exports;
});

$__System.registerDynamic("38", ["37"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = {
    "default": $__require('37'),
    __esModule: true
  };
  return module.exports;
});

$__System.registerDynamic("4", ["38"], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  var _Object$getOwnPropertyDescriptor = $__require('38')["default"];
  exports["default"] = function get(_x, _x2, _x3) {
    var _again = true;
    _function: while (_again) {
      var object = _x,
          property = _x2,
          receiver = _x3;
      _again = false;
      if (object === null)
        object = Function.prototype;
      var desc = _Object$getOwnPropertyDescriptor(object, property);
      if (desc === undefined) {
        var parent = Object.getPrototypeOf(object);
        if (parent === null) {
          return undefined;
        } else {
          _x = parent;
          _x2 = property;
          _x3 = receiver;
          _again = true;
          desc = parent = undefined;
          continue _function;
        }
      } else if ("value" in desc) {
        return desc.value;
      } else {
        var getter = desc.get;
        if (getter === undefined) {
          return undefined;
        }
        return getter.call(receiver);
      }
    }
  };
  exports.__esModule = true;
  return module.exports;
});

$__System.registerDynamic("39", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = function(it) {
    return typeof it === 'object' ? it !== null : typeof it === 'function';
  };
  return module.exports;
});

$__System.registerDynamic("3a", ["39"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var isObject = $__require('39');
  module.exports = function(it) {
    if (!isObject(it))
      throw TypeError(it + ' is not an object!');
    return it;
  };
  return module.exports;
});

$__System.registerDynamic("3b", ["27", "39", "3a", "3c"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var getDesc = $__require('27').getDesc,
      isObject = $__require('39'),
      anObject = $__require('3a');
  var check = function(O, proto) {
    anObject(O);
    if (!isObject(proto) && proto !== null)
      throw TypeError(proto + ": can't set as prototype!");
  };
  module.exports = {
    set: Object.setPrototypeOf || ('__proto__' in {} ? function(test, buggy, set) {
      try {
        set = $__require('3c')(Function.call, getDesc(Object.prototype, '__proto__').set, 2);
        set(test, []);
        buggy = !(test instanceof Array);
      } catch (e) {
        buggy = true;
      }
      return function setPrototypeOf(O, proto) {
        check(O, proto);
        if (buggy)
          O.__proto__ = proto;
        else
          set(O, proto);
        return O;
      };
    }({}, false) : undefined),
    check: check
  };
  return module.exports;
});

$__System.registerDynamic("3d", ["2c", "3b"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var $export = $__require('2c');
  $export($export.S, 'Object', {setPrototypeOf: $__require('3b').set});
  return module.exports;
});

$__System.registerDynamic("3e", ["3d", "2e"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  $__require('3d');
  module.exports = $__require('2e').Object.setPrototypeOf;
  return module.exports;
});

$__System.registerDynamic("3f", ["3e"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = {
    "default": $__require('3e'),
    __esModule: true
  };
  return module.exports;
});

$__System.registerDynamic("5", ["40", "3f"], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  var _Object$create = $__require('40')["default"];
  var _Object$setPrototypeOf = $__require('3f')["default"];
  exports["default"] = function(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }
    subClass.prototype = _Object$create(superClass && superClass.prototype, {constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }});
    if (superClass)
      _Object$setPrototypeOf ? _Object$setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  };
  exports.__esModule = true;
  return module.exports;
});

$__System.registerDynamic("34", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = function(it) {
    if (it == undefined)
      throw TypeError("Can't call method on  " + it);
    return it;
  };
  return module.exports;
});

$__System.registerDynamic("28", ["34"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var defined = $__require('34');
  module.exports = function(it) {
    return Object(defined(it));
  };
  return module.exports;
});

$__System.registerDynamic("41", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var global = module.exports = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();
  if (typeof __g == 'number')
    __g = global;
  return module.exports;
});

$__System.registerDynamic("42", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = function(it) {
    if (typeof it != 'function')
      throw TypeError(it + ' is not a function!');
    return it;
  };
  return module.exports;
});

$__System.registerDynamic("3c", ["42"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var aFunction = $__require('42');
  module.exports = function(fn, that, length) {
    aFunction(fn);
    if (that === undefined)
      return fn;
    switch (length) {
      case 1:
        return function(a) {
          return fn.call(that, a);
        };
      case 2:
        return function(a, b) {
          return fn.call(that, a, b);
        };
      case 3:
        return function(a, b, c) {
          return fn.call(that, a, b, c);
        };
    }
    return function() {
      return fn.apply(that, arguments);
    };
  };
  return module.exports;
});

$__System.registerDynamic("2c", ["41", "2e", "3c"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var global = $__require('41'),
      core = $__require('2e'),
      ctx = $__require('3c'),
      PROTOTYPE = 'prototype';
  var $export = function(type, name, source) {
    var IS_FORCED = type & $export.F,
        IS_GLOBAL = type & $export.G,
        IS_STATIC = type & $export.S,
        IS_PROTO = type & $export.P,
        IS_BIND = type & $export.B,
        IS_WRAP = type & $export.W,
        exports = IS_GLOBAL ? core : core[name] || (core[name] = {}),
        target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE],
        key,
        own,
        out;
    if (IS_GLOBAL)
      source = name;
    for (key in source) {
      own = !IS_FORCED && target && key in target;
      if (own && key in exports)
        continue;
      out = own ? target[key] : source[key];
      exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key] : IS_BIND && own ? ctx(out, global) : IS_WRAP && target[key] == out ? (function(C) {
        var F = function(param) {
          return this instanceof C ? new C(param) : C(param);
        };
        F[PROTOTYPE] = C[PROTOTYPE];
        return F;
      })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
      if (IS_PROTO)
        (exports[PROTOTYPE] || (exports[PROTOTYPE] = {}))[key] = out;
    }
  };
  $export.F = 1;
  $export.G = 2;
  $export.S = 4;
  $export.P = 8;
  $export.B = 16;
  $export.W = 32;
  module.exports = $export;
  return module.exports;
});

$__System.registerDynamic("2a", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = function(exec) {
    try {
      return !!exec();
    } catch (e) {
      return true;
    }
  };
  return module.exports;
});

$__System.registerDynamic("36", ["2c", "2e", "2a"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var $export = $__require('2c'),
      core = $__require('2e'),
      fails = $__require('2a');
  module.exports = function(KEY, exec) {
    var fn = (core.Object || {})[KEY] || Object[KEY],
        exp = {};
    exp[KEY] = exec(fn);
    $export($export.S + $export.F * fails(function() {
      fn(1);
    }), 'Object', exp);
  };
  return module.exports;
});

$__System.registerDynamic("43", ["28", "36"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var toObject = $__require('28');
  $__require('36')('keys', function($keys) {
    return function keys(it) {
      return $keys(toObject(it));
    };
  });
  return module.exports;
});

$__System.registerDynamic("2e", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var core = module.exports = {version: '1.2.6'};
  if (typeof __e == 'number')
    __e = core;
  return module.exports;
});

$__System.registerDynamic("44", ["43", "2e"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  $__require('43');
  module.exports = $__require('2e').Object.keys;
  return module.exports;
});

$__System.registerDynamic("45", ["44"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = {
    "default": $__require('44'),
    __esModule: true
  };
  return module.exports;
});

$__System.registerDynamic("46", ["27"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var $ = $__require('27');
  module.exports = function defineProperty(it, key, desc) {
    return $.setDesc(it, key, desc);
  };
  return module.exports;
});

$__System.registerDynamic("47", ["46"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = {
    "default": $__require('46'),
    __esModule: true
  };
  return module.exports;
});

$__System.registerDynamic("6", ["47"], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  var _Object$defineProperty = $__require('47')["default"];
  exports["default"] = (function() {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor)
          descriptor.writable = true;
        _Object$defineProperty(target, descriptor.key, descriptor);
      }
    }
    return function(Constructor, protoProps, staticProps) {
      if (protoProps)
        defineProperties(Constructor.prototype, protoProps);
      if (staticProps)
        defineProperties(Constructor, staticProps);
      return Constructor;
    };
  })();
  exports.__esModule = true;
  return module.exports;
});

$__System.registerDynamic("7", [], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  exports["default"] = function(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  };
  exports.__esModule = true;
  return module.exports;
});

$__System.registerDynamic("27", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var $Object = Object;
  module.exports = {
    create: $Object.create,
    getProto: $Object.getPrototypeOf,
    isEnum: {}.propertyIsEnumerable,
    getDesc: $Object.getOwnPropertyDescriptor,
    setDesc: $Object.defineProperty,
    setDescs: $Object.defineProperties,
    getKeys: $Object.keys,
    getNames: $Object.getOwnPropertyNames,
    getSymbols: $Object.getOwnPropertySymbols,
    each: [].forEach
  };
  return module.exports;
});

$__System.registerDynamic("48", ["27"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var $ = $__require('27');
  module.exports = function create(P, D) {
    return $.create(P, D);
  };
  return module.exports;
});

$__System.registerDynamic("40", ["48"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = {
    "default": $__require('48'),
    __esModule: true
  };
  return module.exports;
});

$__System.register('11', ['6', '7', '40'], function (_export) {
	var _createClass, _classCallCheck, _Object$create, Events;

	return {
		setters: [function (_) {
			_createClass = _['default'];
		}, function (_2) {
			_classCallCheck = _2['default'];
		}, function (_3) {
			_Object$create = _3['default'];
		}],
		execute: function () {
			//
			// Events Class
			//
			// Event Publisher/Subscriber handling

			'use strict';

			Events = (function () {
				function Events() {
					_classCallCheck(this, Events);

					this._callbacks = {};
					this._queue = {};
				}

				_createClass(Events, [{
					key: 'trigger',
					value: function trigger() {
						return this.emit.apply(this, arguments);
					}
				}, {
					key: 'emit',
					value: function emit(event) {

						var args = [].slice.call(arguments, 1),
						    callbacks = this._callbacks['$' + event];

						if (callbacks) {
							callbacks = callbacks.slice(0);

							for (var i = 0, len = callbacks.length; i < len; ++i) {
								if (callbacks[i].callback !== undefined) {
									callbacks[i].callback.apply(callbacks[i].context, args);
								}
							}
						} else {
							this._queue['$' + event] = arguments;
						}

						return this;
					}
				}, {
					key: 'refireQueue',
					value: function refireQueue(event) {
						if (this._queue['$' + event]) {
							var _queuedEvent = _Object$create(this._queue['$' + event]);

							delete this._queue['$' + event];
							this.emit(_queuedEvent[0], _queuedEvent[1]);
						}
					}
				}, {
					key: 'on',
					value: function on(eventname, callback, context) {
						if (!this._callbacks['$' + eventname]) this._callbacks['$' + eventname] = [];

						this._callbacks['$' + eventname].push({ callback: callback, context: context });
						this.refireQueue(eventname);

						return this;
					}
				}, {
					key: 'once',
					value: function once(eventname, callback, context) {
						var on = function on() {
							this.off(eventname, on);
							calback.apply(context, arguments);
						};

						on.fn = callback;
						this.on(eventname, on);
						return this;
					}
				}, {
					key: 'off',
					value: function off(event, fn) {
						this._callbacks = this._callbacks || {};

						// all
						if (0 == arguments.length) {
							this._callbacks = {};
							return this;
						}

						// specific event
						var callbacks = this._callbacks['$' + event];
						if (!callbacks) return this;

						// remove all handlers
						if (1 == arguments.length) {
							delete this._callbacks['$' + event];
							return this;
						}

						// remove specific handler
						var cb;
						for (var i = 0; i < callbacks.length; i++) {
							cb = callbacks[i];
							if (cb.callback === fn || cb.fn === fn) {
								callbacks.splice(i, 1);
								break;
							}
						}
						return this;
					}
				}, {
					key: 'addEventListener',
					value: function addEventListener(event, fn) {
						this.on(event, fn);
					}
				}, {
					key: 'listeners',
					value: function listeners(event) {
						return this._callbacks['$' + event] || [];
					}
				}, {
					key: 'hasListeners',
					value: function hasListeners(event) {
						return !!this.listeners(event).length;
					}
				}, {
					key: 'removeListener',
					value: function removeListener(event, fn) {
						this.off(event, fn);
					}
				}, {
					key: 'removeAllListeners',
					value: function removeAllListeners(event, fn) {
						this.off();
					}
				}, {
					key: 'removeEventListener',
					value: function removeEventListener(event, fn) {
						this.off(event, fn);
					}
				}]);

				return Events;
			})();

			_export('default', Events);
		}
	};
});

$__System.register('3', ['6', '7', '11', '45'], function (_export) {
	var _createClass, _classCallCheck, Events, _Object$keys, Obj;

	return {
		setters: [function (_) {
			_createClass = _['default'];
		}, function (_2) {
			_classCallCheck = _2['default'];
		}, function (_4) {
			Events = _4['default'];
		}, function (_3) {
			_Object$keys = _3['default'];
		}],
		execute: function () {
			//
			// Object Class
			//
			// Class base object class. Connects to the events handler.

			'use strict';

			Obj = (function () {
				function Obj(opts) {
					_classCallCheck(this, Obj);

					this.events = this.emitter();
					if (opts) this.extend(opts);
				}

				_createClass(Obj, [{
					key: 'merge',
					value: function merge(obj1, obj2) {
						var obj3 = {};

						for (var attrname in obj1) obj3[attrname] = obj1[attrname];

						for (var attrname in obj2) obj3[attrname] = obj2[attrname];

						return obj3;
					}
				}, {
					key: 'extend',
					value: function extend(obj) {
						var _this = this;

						if (obj && typeof obj === 'object') _Object$keys(obj).forEach(function (opt) {
							_this[opt] = obj[opt];
						});

						return this;
					}
				}, {
					key: 'emitter',
					value: function emitter(obj) {
						return new Events(obj || this);
					}
				}, {
					key: 'emit',
					value: function emit(eventname, obj) {
						this.events.emit(eventname, obj);
						return this;
					}
				}, {
					key: 'trigger',
					value: function trigger(eventname, obj) {
						this.emit(eventname, obj);
						return this;
					}
				}, {
					key: 'on',
					value: function on(eventname, callback, context) {
						return this.events.on(eventname, callback, context);
					}
				}, {
					key: 'off',
					value: function off(eventname, callback, context) {
						return this.events.off(eventname, callback, context);
					}
				}]);

				return Obj;
			})();

			_export('default', Obj);
		}
	};
});

$__System.register('e', ['3', '4', '5', '6', '7', '45'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, _Object$keys, Model;

	return {
		setters: [function (_6) {
			Obj = _6['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_5) {
			_Object$keys = _5['default'];
		}],
		execute: function () {
			'use strict';

			Model = (function (_Obj) {
				_inherits(Model, _Obj);

				function Model(opts) {
					_classCallCheck(this, Model);

					_get(Object.getPrototypeOf(Model.prototype), 'constructor', this).call(this);
					this.attributes = opts || {};
				}

				_createClass(Model, [{
					key: 'get',
					value: function get(attribute) {
						return attribute ? this.attributes[attribute] : this.attributes;
					}
				}, {
					key: 'set',
					value: function set(attribute, value) {
						var _this = this;

						//Previous values to compare against
						this.previousAttributes = {};

						_Object$keys(this.attributes).forEach(function (attr) {
							_this.previousAttributes[attr] = _this.attributes[attr];
						});

						//If Multi key/values
						if (typeof attribute === 'object') this.attributes = this.merge(this.attributes, attribute);

						//If setting one attr
						else if (attribute && value !== undefined) {
								this.attributes[attribute] = value;
							}

						//Check for changes
						var changed_attrs = {};

						_Object$keys(this.attributes).forEach(function (attr) {
							if (_this.previousAttributes[attr] != _this.attributes[attr]) changed_attrs[attr] = _this.attributes[attr];
						});

						//Return changes
						if (attribute) if (!attribute.silent) this.emit('change', changed_attrs);else delete attribute.silent;

						return this;
					}
				}, {
					key: 'escape',
					value: function escape(attribute) {
						return attribute ? this.attributes[attribute] : this;
					}
				}, {
					key: 'has',
					value: function has(attribute) {
						return attribute && this.attributes[attribute];
					}
				}, {
					key: 'clear',
					value: function clear(opts) {
						this.attributes = {};
						if (opts && !opts.silent) this.emit('change', null);
					}
				}]);

				return Model;
			})(Obj);

			_export('default', Model);
		}
	};
});

$__System.register('49', ['4', '5', '7', 'e'], function (_export) {
	var _get, _inherits, _classCallCheck, Model, SearchModel;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_e) {
			Model = _e['default'];
		}],
		execute: function () {
			//
			// Search Model
			//

			'use strict';

			SearchModel = (function (_Model) {
				_inherits(SearchModel, _Model);

				function SearchModel() {
					_classCallCheck(this, SearchModel);

					_get(Object.getPrototypeOf(SearchModel.prototype), 'constructor', this).apply(this, arguments);
				}

				return SearchModel;
			})(Model);

			_export('default', SearchModel);

			SearchModel.prototype.defaults = {
				value: ''
			};
		}
	};
});

$__System.register('4a', ['4', '5', '6', '7', '30', '31', '49', 'a', '2f'], function (_export) {
	var _get, _inherits, _createClass, _classCallCheck, SearchTemplate, SearchResultTemplate, SearchModel, _Object$assign, View, Name, Selector, DataKey, EventKey, SearchJSON, SearchView, FuzzySearchStrategy;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_5) {
			SearchTemplate = _5['default'];
		}, function (_6) {
			SearchResultTemplate = _6['default'];
		}, function (_7) {
			SearchModel = _7['default'];
		}, function (_a) {
			_Object$assign = _a['default'];
		}, function (_f) {
			View = _f['default'];
		}],
		execute: function () {
			//
			// Dismissible View
			//

			'use strict';

			Name = 'DLS-Search';
			Selector = '.panel-search';
			DataKey = 'dls.search';
			EventKey = '.' + DataKey;
			SearchJSON = {
				path: '/search.json',
				loaded: false,
				loading: false,
				data: null,
				limit: 20,
				excludedTerms: []
			};

			SearchView = (function (_View) {
				_inherits(SearchView, _View);

				function SearchView(opts) {
					_classCallCheck(this, SearchView);

					_get(Object.getPrototypeOf(SearchView.prototype), 'constructor', this).call(this, opts);

					var query = this.util.getQueryParameters();

					if (query.q !== undefined) this.model.set('value', decodeURI(query.q));

					this.strategy = new FuzzySearchStrategy();
					this.render();
					this.setProps();
					this.createSearch();
					this.listen();
					this.searchStart(this.searchField.get('value'));
				}

				//
				// Getters
				//

				_createClass(SearchView, [{
					key: 'render',

					//
					// Render
					//
					value: function render() {
						this.$el = $(DLS.util.template.render(SearchTemplate, this.model.attributes));
						this.target.empty().append(this.$el);
					}

					//
					// Set the properties
					//
				}, {
					key: 'setProps',
					value: function setProps() {
						this.$sel = {
							search: this.$el.find('[data-toggle="search"]'),
							info: this.$el.find('.dls-search-info'),
							results: this.$el.find('.dls-search-results'),
							noResults: $('<div class="dls-search-no-results">No results found.</div>')
						};
					}

					//
					// Create search
					//
				}, {
					key: 'createSearch',
					value: function createSearch() {
						this.searchField = DLS.create("search", {
							target: this.$sel.search
						});
					}

					//
					// Listen
					//
				}, {
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.searchField.on('change', $.proxy(this.searchStart, this));

						this.app.vent.on('search', function (value) {
							_this.searchField.set('value', value);
							_this.searchStart();
						});
					}

					//
					// Start search
					//
				}, {
					key: 'searchStart',
					value: function searchStart() {
						this.searchField.set('state', 'searching');
						this.app.vent.trigger('search:state', 'searching');
						this.querySearch();
					}
				}, {
					key: 'querySearch',
					value: function querySearch() {
						var _this2 = this;

						if (SearchJSON.loaded) {
							this.search();
						} else if (!SearchJSON.loading) {
							SearchJSON.loading = true;
							$.ajax({
								dataType: 'json',
								url: window.activeRelPath + SearchJSON.path,
								success: function success(res) {
									SearchJSON.loaded = true;
									SearchJSON.data = res;
									_this2.search();
								}
							});
						}
					}

					//
					// Search
					//
				}, {
					key: 'search',
					value: function search() {
						var val = this.searchField.get('value');

						this.app.vent.trigger('search:change', val);

						if (val == '') return this.reset();

						this.$sel.info.text('Results for "' + val + '"');
						this.searchField.set('state', 'active');
						this.app.vent.trigger('search:state', 'active');

						var results = this.findMatches(val, SearchJSON.data, this.strategy, SearchJSON);
						this.$sel.results.empty();

						if (!results.length) {
							this.$sel.results.append(this.$sel.noResults);
							return;
						}

						var resString = '';
						for (var i = 0, il = results.length; i < il; ++i) {
							resString += DLS.util.template.render(SearchResultTemplate, _Object$assign({}, results[i], {
								relPath: window.activeRelPath
							}));
						}

						this.$sel.results.append($(resString));
					}

					//
					// Find matches
					//
				}, {
					key: 'findMatches',
					value: function findMatches(val, data, strategy, opt) {
						var matches = [];
						for (var i = 0, il = data.length; i < il && matches.length < opt.limit; ++i) {
							var match = this.findMatchesInObject(val, data[i], strategy, opt);

							if (match) {
								matches.push(match);
							}
						}

						return matches;
					}

					//
					// Find matches in the object
					//
				}, {
					key: 'findMatchesInObject',
					value: function findMatchesInObject(val, obj, strategy, opt) {
						for (var key in obj) {
							if (!this.isExcluded(obj[key]) && strategy.matches(obj[key], val)) {
								return obj;
							}
						}
					}

					//
					// Check term is not excluded
					//
				}, {
					key: 'isExcluded',
					value: function isExcluded(term, excludedTerms) {
						var excluded = false;
						excludedTerms = excludedTerms || [];

						for (var i = 0, il = excludedTerms.length; i < il; ++i) {
							var excludedTerm = excludedTerms[i];
							if (!excluded && new RegExp(term).test(excludedTerm)) {
								excluded = true;
							}
						}

						return excluded;
					}

					//
					// Reset search to default state
					//
				}, {
					key: 'reset',
					value: function reset() {
						this.searchField.set('state', 'default');
						this.app.vent.trigger('search:state', 'default');
						this.$sel.info.text('');
						this.$sel.results.empty();
					}

					//
					// Interface
					//
				}, {
					key: 'destroy',

					//
					// Destroy
					//
					value: function destroy() {
						this.target.data(DataKey, null);
					}
				}], [{
					key: '_interface',
					value: function _interface(opts) {
						var $el = this,
						    data = $el.data(DataKey),
						    config = _Object$assign({}, $el.data(), typeof opts === 'object' && opts);

						if (!data) {

							data = new SearchView({
								target: $el,
								model: new SearchModel(config)
							});

							$el.data(DataKey, data);
						} else {
							// Component instance already exists
						}

						return data;
					}
				}, {
					key: 'Selector',
					get: function get() {
						return Selector;
					}
				}, {
					key: 'EventKey',
					get: function get() {
						return EventKey;
					}
				}]);

				return SearchView;
			})(View);

			_export('default', SearchView);

			FuzzySearchStrategy = (function () {
				function FuzzySearchStrategy() {
					_classCallCheck(this, FuzzySearchStrategy);
				}

				_createClass(FuzzySearchStrategy, [{
					key: 'matches',
					value: function matches(string, val) {
						if (typeof string !== 'string' || typeof val !== 'string') return false;

						var fuzzy = this.fuzzyFrom(val);
						return !!fuzzy.test(string);
					}
				}, {
					key: 'fuzzyFrom',
					value: function fuzzyFrom(string) {
						var fuzzy = string.trim().split('').join('.*?').replace('??', '?');
						return new RegExp(fuzzy, 'gi');
					}
				}]);

				return FuzzySearchStrategy;
			})();
		}
	};
});

$__System.register('4b', ['4', '5', '7', '25', '4a'], function (_export) {
	var _get, _inherits, _classCallCheck, Component, SearchView, Search;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_4) {
			Component = _4['default'];
		}, function (_a) {
			SearchView = _a['default'];
		}],
		execute: function () {
			//
			// Search Component
			//

			'use strict';

			Search = (function (_Component) {
				_inherits(Search, _Component);

				function Search(opts) {
					_classCallCheck(this, Search);

					_get(Object.getPrototypeOf(Search.prototype), 'constructor', this).call(this, opts);
					this.component = SearchView;

					this.render();
				}

				return Search;
			})(Component);

			_export('default', Search);
		}
	};
});

$__System.register('4c', ['3', '4', '5', '6', '7', '21', '22', '23', '24', '1b', '1c', '1d', '1e', '1f', '4b'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, TiltCard, ProgressAnimator, InputValidation, Loaders, CodeExample, Swatch, Scaler, SpeedSlider, ColorSlider, Search, Components;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_6) {
			TiltCard = _6['default'];
		}, function (_7) {
			ProgressAnimator = _7['default'];
		}, function (_8) {
			InputValidation = _8['default'];
		}, function (_9) {
			Loaders = _9['default'];
		}, function (_b) {
			CodeExample = _b['default'];
		}, function (_c) {
			Swatch = _c['default'];
		}, function (_d) {
			Scaler = _d['default'];
		}, function (_e) {
			SpeedSlider = _e['default'];
		}, function (_f) {
			ColorSlider = _f['default'];
		}, function (_b2) {
			Search = _b2['default'];
		}],
		execute: function () {
			//
			// Components
			//

			'use strict';

			Components = (function (_Obj) {
				_inherits(Components, _Obj);

				function Components(opts) {
					_classCallCheck(this, Components);

					_get(Object.getPrototypeOf(Components.prototype), 'constructor', this).call(this, opts);

					this.component = {
						search: new Search(),
						codeExample: new CodeExample({
							selector: '.dls-example'
						}),
						swatch: new Swatch({
							selector: '.dls-swatch'
						}),
						scaler: new Scaler({
							selector: '.dls-scaler'
						}),
						colorSlider: new ColorSlider({
							selector: '.dls-color-slider'
						}),
						speedSlider: new SpeedSlider({
							selector: '.dls-speed-slider'
						}),
						tiltCard: new TiltCard({
							selector: '.dls-tiltcard'
						}),
						progressAnimationSimple: new ProgressAnimator({
							progressSelector: '.nav-progress-bar',
							selector: '.nav-progress-simple'
						}),
						progressAnimationStep: new ProgressAnimator({
							selector: '.nav-progress-stepper.nav-progress-horizontal'
						}),
						progressAnimationVertical: new ProgressAnimator({
							selector: '.nav-progress-stepper.nav-progress-vertical'
						}),
						loaders: new Loaders({
							selector: '.progress-circle.progress-determinate'
						})
					};

					this.register();
					this.listen();
				}

				//
				// Register the components (for external access)
				//

				_createClass(Components, [{
					key: 'register',
					value: function register() {
						for (var key in this.component) {
							this[key] = this.component[key];
						}
					}

					//
					// Bind the listeners
					//
				}, {
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.app.on('render', function () {
							for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
								args[_key] = arguments[_key];
							}

							_this.render(args);
						});
					}

					// Render the components asynchronously
					// (generally would not be used but may be used in a web-app situation
					// where components may be added dynamically).
				}, {
					key: 'render',
					value: function render() {
						for (var key in this.component) {
							this[key].trigger('render');
						}
					}
				}]);

				return Components;
			})(Obj);

			_export('default', Components);
		}
	};
});

$__System.register('1', ['10', '19', '4c'], function (_export) {
	//
	// DLS Application JS
	//
	// Create the main application and pass across our
	// base config / options

	'use strict';

	var DLSApplication, UI, Components;
	return {
		setters: [function (_) {
			DLSApplication = _['default'];
		}, function (_2) {
			UI = _2['default'];
		}, function (_c) {
			Components = _c['default'];
		}],
		execute: function () {

			window.DLSApp = new DLSApplication({
				$el: $('#dls-app'),
				modules: {
					ui: UI,
					components: Components
				},
				breakpoints: {
					xs: 0,
					sm: 375,
					md: 768,
					lg: 1024,
					xl: 1200
				}
			});

			DLSApp.start();
		}
	};
});

})
(function(factory) {
  factory();
});