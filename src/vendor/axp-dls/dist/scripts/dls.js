!function(e){function r(e,r,o){return 4===arguments.length?t.apply(this,arguments):void n(e,{declarative:!0,deps:r,declare:o})}function t(e,r,t,o){n(e,{declarative:!1,deps:r,executingRequire:t,execute:o})}function n(e,r){r.name=e,e in p||(p[e]=r),r.normalizedDeps=r.deps}function o(e,r){if(r[e.groupIndex]=r[e.groupIndex]||[],-1==v.call(r[e.groupIndex],e)){r[e.groupIndex].push(e);for(var t=0,n=e.normalizedDeps.length;n>t;t++){var a=e.normalizedDeps[t],u=p[a];if(u&&!u.evaluated){var d=e.groupIndex+(u.declarative!=e.declarative);if(void 0===u.groupIndex||u.groupIndex<d){if(void 0!==u.groupIndex&&(r[u.groupIndex].splice(v.call(r[u.groupIndex],u),1),0==r[u.groupIndex].length))throw new TypeError("Mixed dependency cycle detected");u.groupIndex=d}o(u,r)}}}}function a(e){var r=p[e];r.groupIndex=0;var t=[];o(r,t);for(var n=!!r.declarative==t.length%2,a=t.length-1;a>=0;a--){for(var u=t[a],i=0;i<u.length;i++){var s=u[i];n?d(s):l(s)}n=!n}}function u(e){return x[e]||(x[e]={name:e,dependencies:[],exports:{},importers:[]})}function d(r){if(!r.module){var t=r.module=u(r.name),n=r.module.exports,o=r.declare.call(e,function(e,r){if(t.locked=!0,"object"==typeof e)for(var o in e)n[o]=e[o];else n[e]=r;for(var a=0,u=t.importers.length;u>a;a++){var d=t.importers[a];if(!d.locked)for(var i=0;i<d.dependencies.length;++i)d.dependencies[i]===t&&d.setters[i](n)}return t.locked=!1,r},r.name);t.setters=o.setters,t.execute=o.execute;for(var a=0,i=r.normalizedDeps.length;i>a;a++){var l,s=r.normalizedDeps[a],c=p[s],v=x[s];v?l=v.exports:c&&!c.declarative?l=c.esModule:c?(d(c),v=c.module,l=v.exports):l=f(s),v&&v.importers?(v.importers.push(t),t.dependencies.push(v)):t.dependencies.push(null),t.setters[a]&&t.setters[a](l)}}}function i(e){var r,t=p[e];if(t)t.declarative?c(e,[]):t.evaluated||l(t),r=t.module.exports;else if(r=f(e),!r)throw new Error("Unable to load dependency "+e+".");return(!t||t.declarative)&&r&&r.__useDefault?r["default"]:r}function l(r){if(!r.module){var t={},n=r.module={exports:t,id:r.name};if(!r.executingRequire)for(var o=0,a=r.normalizedDeps.length;a>o;o++){var u=r.normalizedDeps[o],d=p[u];d&&l(d)}r.evaluated=!0;var c=r.execute.call(e,function(e){for(var t=0,n=r.deps.length;n>t;t++)if(r.deps[t]==e)return i(r.normalizedDeps[t]);throw new TypeError("Module "+e+" not declared as a dependency.")},t,n);c&&(n.exports=c),t=n.exports,t&&t.__esModule?r.esModule=t:r.esModule=s(t)}}function s(r){if(r===e)return r;var t={};if("object"==typeof r||"function"==typeof r)if(g){var n;for(var o in r)(n=Object.getOwnPropertyDescriptor(r,o))&&h(t,o,n)}else{var a=r&&r.hasOwnProperty;for(var o in r)(!a||r.hasOwnProperty(o))&&(t[o]=r[o])}return t["default"]=r,h(t,"__useDefault",{value:!0}),t}function c(r,t){var n=p[r];if(n&&!n.evaluated&&n.declarative){t.push(r);for(var o=0,a=n.normalizedDeps.length;a>o;o++){var u=n.normalizedDeps[o];-1==v.call(t,u)&&(p[u]?c(u,t):f(u))}n.evaluated||(n.evaluated=!0,n.module.execute.call(e))}}function f(e){if(D[e])return D[e];if("@node/"==e.substr(0,6))return y(e.substr(6));var r=p[e];if(!r)throw"Module "+e+" not present.";return a(e),c(e,[]),p[e]=void 0,r.declarative&&h(r.module.exports,"__esModule",{value:!0}),D[e]=r.declarative?r.module.exports:r.esModule}var p={},v=Array.prototype.indexOf||function(e){for(var r=0,t=this.length;t>r;r++)if(this[r]===e)return r;return-1},g=!0;try{Object.getOwnPropertyDescriptor({a:0},"a")}catch(m){g=!1}var h;!function(){try{Object.defineProperty({},"a",{})&&(h=Object.defineProperty)}catch(e){h=function(e,r,t){try{e[r]=t.value||t.get.call(e)}catch(n){}}}}();var x={},y="undefined"!=typeof System&&System._nodeRequire||"undefined"!=typeof require&&require.resolve&&"undefined"!=typeof process&&require,D={"@empty":{}};return function(e,n,o){return function(a){a(function(a){for(var u={_nodeRequire:y,register:r,registerDynamic:t,get:f,set:function(e,r){D[e]=r},newModule:function(e){return e}},d=0;d<n.length;d++)(function(e,r){r&&r.__esModule?D[e]=r:D[e]=s(r)})(n[d],arguments[d]);o(u);var i=f(e[0]);if(e.length>1)for(var d=1;d<e.length;d++)f(e[d]);return i.__useDefault?i["default"]:i})}}}("undefined"!=typeof self?self:global)

(["1"], [], function($__System) {

!function(e){function n(e,n){e=e.replace(l,"");var r=e.match(u),t=(r[1].split(",")[n]||"require").replace(s,""),i=p[t]||(p[t]=new RegExp(a+t+f,"g"));i.lastIndex=0;for(var o,c=[];o=i.exec(e);)c.push(o[2]||o[3]);return c}function r(e,n,t,o){if("object"==typeof e&&!(e instanceof Array))return r.apply(null,Array.prototype.splice.call(arguments,1,arguments.length-1));if("string"==typeof e&&"function"==typeof n&&(e=[e]),!(e instanceof Array)){if("string"==typeof e){var l=i.get(e);return l.__useDefault?l["default"]:l}throw new TypeError("Invalid require")}for(var a=[],f=0;f<e.length;f++)a.push(i["import"](e[f],o));Promise.all(a).then(function(e){n&&n.apply(null,e)},t)}function t(t,l,a){"string"!=typeof t&&(a=l,l=t,t=null),l instanceof Array||(a=l,l=["require","exports","module"].splice(0,a.length)),"function"!=typeof a&&(a=function(e){return function(){return e}}(a)),void 0===l[l.length-1]&&l.pop();var f,u,s;-1!=(f=o.call(l,"require"))&&(l.splice(f,1),t||(l=l.concat(n(a.toString(),f)))),-1!=(u=o.call(l,"exports"))&&l.splice(u,1),-1!=(s=o.call(l,"module"))&&l.splice(s,1);var p={name:t,deps:l,execute:function(n,t,o){for(var p=[],c=0;c<l.length;c++)p.push(n(l[c]));o.uri=o.id,o.config=function(){},-1!=s&&p.splice(s,0,o),-1!=u&&p.splice(u,0,t),-1!=f&&p.splice(f,0,function(e,t,l){return"string"==typeof e&&"function"!=typeof t?n(e):r.call(i,e,t,l,o.id)});var d=a.apply(-1==u?e:t,p);return"undefined"==typeof d&&o&&(d=o.exports),"undefined"!=typeof d?d:void 0}};if(t)c.anonDefine||c.isBundle?c.anonDefine&&c.anonDefine.name&&(c.anonDefine=null):c.anonDefine=p,c.isBundle=!0,i.registerDynamic(p.name,p.deps,!1,p.execute);else{if(c.anonDefine&&!c.anonDefine.name)throw new Error("Multiple anonymous defines in module "+t);c.anonDefine=p}}var i=$__System,o=Array.prototype.indexOf||function(e){for(var n=0,r=this.length;r>n;n++)if(this[n]===e)return n;return-1},l=/(\/\*([\s\S]*?)\*\/|([^:]|^)\/\/(.*)$)/gm,a="(?:^|[^$_a-zA-Z\\xA0-\\uFFFF.])",f="\\s*\\(\\s*(\"([^\"]+)\"|'([^']+)')\\s*\\)",u=/\(([^\)]*)\)/,s=/^\s+|\s+$/g,p={};t.amd={};var c={isBundle:!1,anonDefine:null};i.amdDefine=t,i.amdRequire=r}("undefined"!=typeof self?self:global);
$__System.register('2', ['3', '4', '5', '6', '7'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, PlatformDetect;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// Platform Detection Handling
			//

			'use strict';

			PlatformDetect = (function (_Obj) {
				_inherits(PlatformDetect, _Obj);

				function PlatformDetect(opts) {
					_classCallCheck(this, PlatformDetect);

					_get(Object.getPrototypeOf(PlatformDetect.prototype), 'constructor', this).call(this, opts);
					this.detect();
					this.unify();
				}

				_createClass(PlatformDetect, [{
					key: 'detect',
					value: function detect() {
						this.detectInput();
					}
				}, {
					key: 'detectInput',
					value: function detectInput() {
						this.touch = 'ontouchstart' in window || navigator.MaxTouchPoints > 0 || navigator.msMaxTouchPoints > 0;
					}
				}, {
					key: 'unify',
					value: function unify() {
						this.unifyEvents();
					}
				}, {
					key: 'unifyEvents',
					value: function unifyEvents() {
						if (this.touch) {
							this.event = {
								down: 'touchstart',
								up: 'touchend',
								over: 'touchstart',
								out: 'touchend',
								click: 'touchstart',
								move: 'touchmove',
								cancel: 'touchcancel',
								enter: 'touchstart',
								leave: 'toucheend'
							};
						} else {
							this.event = {
								down: 'mousedown',
								up: 'mouseup',
								over: 'mouseover',
								out: 'mouseout',
								click: 'mousedown',
								move: 'mousemove',
								cancel: 'mouseup',
								enter: 'mouseenter',
								leave: 'mouseleave'
							};
						}
					}
				}]);

				return PlatformDetect;
			})(Obj);

			_export('default', PlatformDetect);
		}
	};
});

$__System.register('8', ['2', '3', '4', '5', '7'], function (_export) {
	var PlatformDetect, Obj, _get, _inherits, _classCallCheck, Platform;

	return {
		setters: [function (_5) {
			PlatformDetect = _5['default'];
		}, function (_4) {
			Obj = _4['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}],
		execute: function () {
			//
			// Platform Detection and unifier
			//

			'use strict';

			Platform = (function (_PlatformDetect) {
				_inherits(Platform, _PlatformDetect);

				function Platform(opts) {
					_classCallCheck(this, Platform);

					_get(Object.getPrototypeOf(Platform.prototype), 'constructor', this).call(this, opts);
				}

				return Platform;
			})(PlatformDetect);

			_export('default', Platform);
		}
	};
});

$__System.register('9', ['3', '4', '5', '6', '7', 'a', 'b'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, _Object$keys, _Object$assign, EventSplitter, Logs, uid, removeHandler, removeHandlers, getUID, makeCallback, Radio, Channel;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_a) {
			_Object$keys = _a['default'];
		}, function (_b) {
			_Object$assign = _b['default'];
		}],
		execute: function () {
			//
			// Requests
			//

			'use strict';

			EventSplitter = /\s+/;
			Logs = {};
			uid = 0;

			removeHandler = function removeHandler(store, name, callback, context) {
				var event = store[name];
				if ((!callback || callback === event.callback || callback === event.callback._callback) && (!context || context === event.context)) {
					delete store[name];
					return true;
				}
			};

			removeHandlers = function removeHandlers(store, name, callback, context) {
				store || (store = {});
				var names = name ? [name] : _Object$keys(store),
				    matched = false;

				for (var i = 0, il = names.length; i < il; ++i) {
					names = names[i];

					if (!store[name]) continue;

					if (removeHandler(store, name, callback, context)) {
						matched = true;
					}
				}

				return matched;
			};

			getUID = function getUID() {
				return uid++;
			};

			makeCallback = function makeCallback(callback) {
				return typeof callback === 'function' ? callback : function () {
					return callback;
				};
			};

			//
			// Radio
			//

			Radio = (function (_Obj) {
				_inherits(Radio, _Obj);

				function Radio(opts) {
					_classCallCheck(this, Radio);

					_get(Object.getPrototypeOf(Radio.prototype), 'constructor', this).call(this, opts);
				}

				//
				// Channel Creator
				//

				//
				// Perform request action
				//

				_createClass(Radio, [{
					key: 'request',
					value: function request(name) {
						for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
							args[_key - 1] = arguments[_key];
						}

						var results = Radio._eventsApi(this, 'request', name, args);

						if (results) return results;

						var channelName = this.channelName,
						    requests = this._requests;

						if (requests && (requests[name] || requests['default'])) {
							var handler = requests[name] || requests['default'];
							args = requests[name] ? args : arguments;
							return Radio._callHandler(handler.callback, handler.context, args);
						} else {
							//console.log([`An unhandled request was fired: ${name}`, name, channelName]);
						}
					}

					//
					// Apply a reply handler
					//
				}, {
					key: 'reply',
					value: function reply(name, callback, context) {
						if (Radio._eventsApi(this, 'reply', name, [callback, context])) return this;

						this._requests || (this._requests = {});

						if (this._requests[name]) {
							//console.log([`A request was overwritten: ${name}`, name, this.channelName]);
						}

						this._requests[name] = {
							callback: makeCallback(callback),
							context: context || this
						};

						return this;
					}

					//
					// Apply a one-time reply handler (subsequent requests will not be called)
					//
				}, {
					key: 'replyOnce',
					value: function replyOnce(name, callback, context) {
						if (Radio._eventsApi(this, 'replyOnce', name, [callback, context])) {
							return this;
						}

						var once = function once() {
							var _makeCallback;

							this.stopReplying(name);

							for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
								args[_key2] = arguments[_key2];
							}

							return (_makeCallback = makeCallback(callback)).apply.apply(_makeCallback, [this].concat(args));
						};

						return this.reply(name, once, context);
					}

					//
					// Stop replying
					//
				}, {
					key: 'stopReplying',
					value: function stopReplying(name, callback, context) {
						if (Radio._eventsApi(this, 'stopReplying', name)) return this;

						if (!name && !callback && !context) {
							delete this._requests;
						} else if (!removeHandlers(this._requests, name, callback, context)) {
							//console.log([`Attempted to remove the unregistered request`, name, this.channelName]);
						}

						return this;
					}

					//
					// Create a sub channel
					//
				}, {
					key: 'channel',
					value: function channel(channelName) {
						if (!channelName) throw new Error('You must provide a name for the channel.');

						if (this._channels[channelName]) return this._channels[channelName];else return this._channels[channelName] = new Channel(channelName);
					}

					//
					// Static API: Events
					//
				}], [{
					key: '_eventsApi',
					value: function _eventsApi(obj, action, name, rest) {
						if (!name) return false;

						var results = {};

						if (typeof name === 'object') {
							for (var key in name) {
								var result = obj[action].apply(obj, [key, name[key]].concat(rest));
								EventSplitter.test(key) ? _Object$assign(results, result) : results[key] = result;
							}

							return results;
						}

						if (EventSplitter.test(name)) {
							var names = name.split(EventSplitter);
							for (var i = 0, il = names.length; i < il; ++i) results[names[i]] = obj[action].apply(obj, [names[i]].concat(rest));
							return results;
						}

						return false;
					}

					//
					// Static API: Call handling
					//
				}, {
					key: '_callHandler',
					value: function _callHandler(callback, context, args) {
						var a1 = args[0],
						    a2 = args[1],
						    a3 = args[3];

						switch (args.length) {
							case 0:
								return callback.call(context);
							case 1:
								return callback.call(context, a1);
							case 2:
								return callback.call(context, a1, a2);
							case 3:
								return callback.call(context, a1, a2, a3);
							default:
								return callback.apply(context, args);
						}
					}
				}]);

				return Radio;
			})(Obj);

			Channel = (function (_Radio) {
				_inherits(Channel, _Radio);

				function Channel(opts) {
					_classCallCheck(this, Channel);

					_get(Object.getPrototypeOf(Channel.prototype), 'constructor', this).call(this, opts);
					this.channelName = this.channelName || 'channel-' + getUID();
				}

				_createClass(Channel, [{
					key: 'reset',
					value: function reset() {
						this.off();
						this.stopReplying();
						return this;
					}
				}]);

				return Channel;
			})(Radio);

			_export('default', Channel);
		}
	};
});

$__System.register('c', ['3', '4', '5', '6', '7'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Format;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// Formatting Utilities
			//

			'use strict';

			Format = (function (_Obj) {
				_inherits(Format, _Obj);

				function Format(opts) {
					_classCallCheck(this, Format);

					_get(Object.getPrototypeOf(Format.prototype), 'constructor', this).call(this, opts);
					this.listen();
				}

				//
				// Listen
				//

				_createClass(Format, [{
					key: 'listen',
					value: function listen() {
						this.util.reply('format:numbers:commas', this.numberWithCommas, this).reply('format:selector:autoprefix', this.autoPrefixSelector, this);
					}

					//
					// Number with commas
					//
					// Applies commas to a number (e.g. - 10000 > 10,000)
				}, {
					key: 'numberWithCommas',
					value: function numberWithCommas(x) {
						var parts = x.toString().split(".");
						parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						return parts.join(".");
					}

					//
					// Auto prefix supplied selectors where a type has not been specified.
					// e.g. -
					//	'class-name' => '.class-name'
					//	'.class-name' => '.class-name'
					//	'#class-name' => '#class-name'
					//
				}, {
					key: 'autoPrefixSelector',
					value: function autoPrefixSelector(selector) {
						var prefix = selector.substr(0, 1);
						return prefix !== '.' && prefix !== '#' ? '.' + selector : selector;
					}
				}]);

				return Format;
			})(Obj);

			_export('default', Format);
		}
	};
});

$__System.register('d', ['3', '4', '5', '6', '7'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Draggable;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// DLS Debugger Utility
			//

			'use strict';

			Draggable = (function (_Obj) {
				_inherits(Draggable, _Obj);

				function Draggable(opts) {
					_classCallCheck(this, Draggable);

					_get(Object.getPrototypeOf(Draggable.prototype), 'constructor', this).call(this, opts);

					this.debugMode = window.DLSDebug || false;
				}

				_createClass(Draggable, [{
					key: 'log',
					value: function log() {}
				}]);

				return Draggable;
			})(Obj);

			_export('default', Draggable);
		}
	};
});

$__System.register('e', ['3', '4', '5', '6', '7'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Templates;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}],
		execute: function () {
			//
			// Templates Handler
			//
			// On-the-fly templating

			'use strict';

			Templates = (function (_Obj) {
				_inherits(Templates, _Obj);

				function Templates(opts) {
					_classCallCheck(this, Templates);

					_get(Object.getPrototypeOf(Templates.prototype), 'constructor', this).call(this, opts);

					this.escaper = /\\|'|\r|\n|\u2028|\u2029/g;
					this.listen();
				}

				//
				// Listen
				//

				_createClass(Templates, [{
					key: 'listen',
					value: function listen() {
						this.util.reply('template:render', this.render, this);
					}

					//
					// Render
					//
				}, {
					key: 'render',
					value: function render(html, opts) {
						opts = opts || {};

						var template = html,
						    data = opts || {};

						//Must have template passed to render
						if (!template) return;

						//If No Opts fallback to defaults
						var settings = this.settings,
						    escaper = this.escaper,
						    escapes = this.escapes,
						    escapeChar = function escapeChar(match) {
							return '\\' + escapes[match];
						};

						//Compile Vars & Fn
						var index = 0,
						    source = "__p+='",
						    matcher = RegExp([(settings.escape || noMatch).source, (settings.interpolate || noMatch).source, (settings.evaluate || noMatch).source].join('|') + '|$', 'g');

						// Compile the template source, escaping string literals appropriately.
						template.replace(matcher, function (match, escape, interpolate, evaluate, offset) {
							source += template.slice(index, offset).replace(escaper, escapeChar);
							index = offset + match.length;

							if (escape) source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";else if (interpolate) source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";else if (evaluate) source += "';\n" + evaluate + "\n__p+='";

							// Adobe VMs need the match returned to produce the correct offest.
							return match;
						});

						source += "';\n";

						settings.variable = 'obj';
						source = 'with(obj||{}){\n' + source + '}\n';

						source = "var __t,__p='',__j=Array.prototype.join," + "print=function(){__p+=__j.call(arguments,'');};\n" + source + 'return __p;\n';

						var _this = this;

						this.source = 'function(' + settings.variable + '){\n' + source + '}';

						var output = new Function(settings.variable, '_', source).call(this, data, _this);
						output = output.trim().replace(/\r/g, '').replace(/\n/g, '').replace(/\s+/g, ' ').replace(/(">\s)/g, '">').replace(/('>\s)/g, "'>").replace(/(\s<)/g, '<');

						return output;
					}
				}]);

				return Templates;
			})(Obj);

			_export('default', Templates);

			Templates.prototype.settings = {
				evaluate: /<%([\s\S]+?)%>/g,
				interpolate: /<%=([\s\S]+?)%>/g,
				escape: /<%-([\s\S]+?)%>/g
			};

			Templates.prototype.escapes = {
				"'": "'",
				'\\': '\\',
				'\r': 'r',
				'\n': 'n',
				'\u2028': 'u2028',
				'\u2029': 'u2029'
			};
		}
	};
});

$__System.register('f', ['4', '5', '6', '7', '10'], function (_export) {
	var _get, _inherits, _createClass, _classCallCheck, Model, Draggable;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_5) {
			Model = _5['default'];
		}],
		execute: function () {
			//
			// Draggable Utility
			//
			// Makes a provided element become a draggable
			// component. This class only acts as a unified event
			// emitter; it does not handle the actual moving of
			// any object.

			'use strict';

			Draggable = (function (_Model) {
				_inherits(Draggable, _Model);

				function Draggable(opts) {
					_classCallCheck(this, Draggable);

					_get(Object.getPrototypeOf(Draggable.prototype), 'constructor', this).call(this, {
						movedX: 0,
						movedY: 0,
						offsetX: 0,
						offsetY: 0
					});

					this.set('uid', this.util.request('uid:gen'));

					this.animationFrame;
					this.target = opts.target;
					this.$scroll = opts.$scroll || $('body');

					if (!this.target) console.warn('[DLS > Util > Draggable]: Draggable elements require an $el to be specified.');
					this.configure();
					this.render();
				}

				//
				// Configure based on device
				//

				_createClass(Draggable, [{
					key: 'configure',
					value: function configure() {
						if (this.platform.touch) this.bindings = {
							down: this.onTouchStart
						};else this.bindings = {
							down: this.onMouseDown
						};
					}

					//
					// Render
					//
				}, {
					key: 'render',
					value: function render() {
						this.listen();
					}

					//
					// Bind the events
					//
				}, {
					key: 'listen',
					value: function listen() {
						var _this2 = this;

						this.target.on(this.platform.event.down, function (e) {
							_this2.bindings.down(e, _this2);
							e.stopPropagation();
							e.preventDefault();
						});
					}

					//
					// Skip to position
					//
				}, {
					key: 'skipToPosition',
					value: function skipToPosition(e) {
						this.handleDragStart(e, true);
					}

					//
					// On Drag start
					//
				}, {
					key: 'handleDragStart',
					value: function handleDragStart(e, skip) {
						var _this3 = this;

						if (typeof e.button === 'number' && e.button !== 0) return;

						if (e.targetTouches) this.set({ touchIdentifier: e.targetTouches[0].identifier });

						var _getControlPosition = this.getControlPosition(e);

						var clientX = _getControlPosition.clientX;
						var clientY = _getControlPosition.clientY;
						var offsetX = _getControlPosition.offsetX;
						var offsetY = _getControlPosition.offsetY;

						if (!this.get('dragging')) {
							this.target.addClass('dragging');
							this.emit('drag:start');
							$(this.$scroll).on(this.platform.event.move + '.draggable', function (e) {
								_this3.handleDrag(e);
								e.stopPropagation();
								e.preventDefault();
							});

							$(document).on(this.platform.event.up + ('.draggable.' + this.attributes.uid), function (e) {
								_this3.handleDragStop(e);
								e.stopPropagation();
								e.preventDefault();
							});
						}

						if (skip) {
							clientX = this.target.offset().left + this.target.width() * 0.5;
							clientY = this.target.offset().top + this.target.height() * 0.5;
						}

						this.set({
							dragging: true,
							startX: clientX - offsetX,
							startY: clientY - offsetY,
							offsetX: offsetX,
							offsetY: offsetY,
							lastX: clientX - offsetX,
							lastY: clientY - offsetY,
							movedX: 0,
							movedY: 0,
							scrollX: this.$scroll.scrollLeft,
							scrollY: this.$scroll.scrollTop
						});

						if (skip) {
							var _getControlPosition2 = this.getControlPosition(e);

							var _clientX = _getControlPosition2.clientX;
							var _clientY = _getControlPosition2.clientY;
							var _offsetX = _getControlPosition2.offsetX;
							var _offsetY = _getControlPosition2.offsetY;

							this.set({
								lastX: _clientX - _offsetX,
								lastY: _clientY - _offsetY,
								movedX: _clientX - this.get('startX'),
								movedY: _clientY - this.get('startY')
							});
						}
					}

					//
					// On Drag
					//
				}, {
					key: 'handleDrag',
					value: function handleDrag(e) {
						var _this4 = this;

						if (e.targetTouches && e.targetTouches[0].identifier !== this.attributes.touchIdentifier) return;

						var _getControlPosition3 = this.getControlPosition(e);

						var clientX = _getControlPosition3.clientX;
						var clientY = _getControlPosition3.clientY;
						var offsetX = _getControlPosition3.offsetX;
						var offsetY = _getControlPosition3.offsetY;

						cancelAnimationFrame(this.animationFrame);
						this.animationFrame = requestAnimationFrame(function (e) {
							_this4.set({
								lastX: clientX - offsetX,
								lastY: clientY - offsetY,
								movedX: clientX - _this4.get('startX'),
								movedY: clientY - _this4.get('startY')
							});
						});
					}

					//
					// On Drag end
					//
				}, {
					key: 'handleDragStop',
					value: function handleDragStop(e) {
						if (!this.get('dragging')) return;

						if (e.changedTouches && e.changedTouches[0].identifier !== this.attributes.touchIdentifier) return;

						var _getControlPosition4 = this.getControlPosition(e);

						var clientX = _getControlPosition4.clientX;
						var clientY = _getControlPosition4.clientY;
						var offsetX = _getControlPosition4.offsetX;
						var offsetY = _getControlPosition4.offsetY;

						this.set({
							dragging: false,
							lastX: null,
							lastY: null,
							offsetX: 0,
							offsetY: 0
						});

						$(this.$scroll).off(this.platform.event.move + '.draggable');

						$(document).off(this.platform.event.up + ('.draggable.' + this.attributes.uid));

						this.target.removeClass('dragging');
						this.emit('drag:end');
					}

					//
					// Get control position
					//
				}, {
					key: 'getControlPosition',
					value: function getControlPosition(e) {
						e = e.originalEvent;
						var position = e.targetTouches && e.targetTouches[0] || e.changedTouches && e.changedTouches[0] || e;

						return {
							clientX: position.clientX - this.attributes.offsetX,
							clientY: position.clientY - this.attributes.offsetY,
							offsetX: position.offsetX || 0,
							offsetY: position.offsetY || 0
						};
					}

					//
					// On Mouse Down
					//
				}, {
					key: 'onMouseDown',
					value: function onMouseDown(e, _this) {
						return _this.handleDragStart(e);
					}

					//
					// On Touch Start
					//
				}, {
					key: 'onTouchStart',
					value: function onTouchStart(e, _this) {
						return _this.handleDragStart(e);
					}

					//
					// Destroy (unused)
					//
				}, {
					key: 'destroy',
					value: function destroy() {}
				}]);

				return Draggable;
			})(Model);

			_export('default', Draggable);
		}
	};
});

$__System.register('11', ['3', '4', '5', '6', '7', '9', 'b', 'c', 'd', 'e', 'f'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Channel, _Object$assign, Format, Debugger, Templates, Draggable, uid, Util;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_6) {
			Channel = _6['default'];
		}, function (_b) {
			_Object$assign = _b['default'];
		}, function (_c) {
			Format = _c['default'];
		}, function (_d) {
			Debugger = _d['default'];
		}, function (_e) {
			Templates = _e['default'];
		}, function (_f) {
			Draggable = _f['default'];
		}],
		execute: function () {
			//
			// Utility Functions
			//

			'use strict';

			uid = 0;

			Util = (function (_Channel) {
				_inherits(Util, _Channel);

				function Util(opts) {
					_classCallCheck(this, Util);

					_get(Object.getPrototypeOf(Util.prototype), 'constructor', this).call(this, opts);
					this.listen();
					this.setup();
				}

				//
				//	Setup the utilities
				//

				_createClass(Util, [{
					key: 'setup',
					value: function setup() {
						_Object$assign(this, {
							'debugger': new Debugger({ util: this }),
							template: new Templates({ util: this }),
							format: new Format({ util: this }),
							draggable: Draggable
						});
					}
				}, {
					key: 'listen',
					value: function listen() {
						this.reply('uid:gen', this.getUID, this).reply('ui:draggable', this.createDraggable, this).reply('events:throttle', this.throttle, this);
					}

					//
					// Get unique ID
					//
				}, {
					key: 'getUID',
					value: function getUID(prefix) {
						uid++;
						return prefix ? prefix + uid : 'el' + uid;
					}

					//
					// Create and return a draggable component with the supplied opts
					//
				}, {
					key: 'createDraggable',
					value: function createDraggable(opts) {
						return new this.draggable(opts);
					}

					//
					// Throttle
					//
				}, {
					key: 'throttle',
					value: function throttle(func) {
						var ms = arguments.length <= 1 || arguments[1] === undefined ? 500 : arguments[1];
						var context = arguments.length <= 2 || arguments[2] === undefined ? window : arguments[2];

						var to = undefined,
						    pass = undefined,
						    wait = false;

						return function () {
							for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
								args[_key] = arguments[_key];
							}

							var later = function later() {
								func.apply(context, args);
							};

							if (!wait) {
								wait = true;
								clearTimeout(pass);
								to = setTimeout(function () {
									wait = false;
									later();
								}, ms);
							} else {
								clearTimeout(pass);
								pass = setTimeout(function () {
									later();
								}, ms);
							}
						};
					}
				}]);

				return Util;
			})(Channel);

			_export('default', Util);
		}
	};
});

$__System.register('12', ['4', '5', '7', '10'], function (_export) {
	var _get, _inherits, _classCallCheck, Model, SelectModel;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_4) {
			Model = _4['default'];
		}],
		execute: function () {
			//
			// Select Model
			//

			'use strict';

			SelectModel = (function (_Model) {
				_inherits(SelectModel, _Model);

				function SelectModel() {
					_classCallCheck(this, SelectModel);

					_get(Object.getPrototypeOf(SelectModel.prototype), 'constructor', this).apply(this, arguments);
				}

				return SelectModel;
			})(Model);

			_export('default', SelectModel);

			SelectModel.prototype.defaults = {};
		}
	};
});

$__System.register('13', ['4', '5', '6', '7', '12', '14', '15', 'b'], function (_export) {
	var _get, _inherits, _createClass, _classCallCheck, SelectModel, View, Component, _Object$assign, Name, Selector, DataKey, EventKey, Select, SelectView;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_4) {
			_createClass = _4['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_7) {
			SelectModel = _7['default'];
		}, function (_5) {
			View = _5['default'];
		}, function (_6) {
			Component = _6['default'];
		}, function (_b) {
			_Object$assign = _b['default'];
		}],
		execute: function () {
			//
			// Select Component
			//

			//
			// Configuration
			//
			'use strict';

			Name = 'Select';
			Selector = '[data-toggle="select"]:not([data-ignore])';
			DataKey = 'dls.select';
			EventKey = '.' + DataKey;

			//
			// Initiator
			//

			Select = (function (_Component) {
				_inherits(Select, _Component);

				function Select(opts) {
					_classCallCheck(this, Select);

					_get(Object.getPrototypeOf(Select.prototype), 'constructor', this).call(this, opts);
					this.component = SelectView;

					this.render();
				}

				//
				// Select View
				//
				return Select;
			})(Component);

			_export('default', Select);

			SelectView = (function (_View) {
				_inherits(SelectView, _View);

				function SelectView(opts) {
					_classCallCheck(this, SelectView);

					_get(Object.getPrototypeOf(SelectView.prototype), 'constructor', this).call(this, opts);
					this.render();
					this.listen();
				}

				//
				// Getters
				//

				_createClass(SelectView, [{
					key: 'render',

					//
					// Render
					//
					value: function render() {
						this.target.attr('data-rendered', true).attr('data-value', this.select.val());
					}

					//
					// Listen
					//
				}, {
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.select.on('change', function () {
							_this.model.set('value', _this.select.val());
							_this.target.attr('data-value', _this.select.val());
						}).on('focus', function () {
							_this.target.addClass('focus');
						}).on('blur', function () {
							_this.target.removeClass('focus');
						});
					}

					//
					// Interface
					//
				}, {
					key: 'destroy',

					//
					// Destroy
					//
					value: function destroy() {
						$.removeData(this.target, DataKey);
					}
				}], [{
					key: '_interface',
					value: function _interface(config) {
						var $el = $(this),
						    $select = $el.find('select'),
						    data = $el.data(DataKey);

						config = _Object$assign($el.data(), config);
						config.value = $el.find('select').val();

						if (!data) {
							data = new SelectView({
								target: $el,
								select: $select,
								model: new SelectModel(config)
							});

							$el.data(DataKey, data);
						}

						return data;
					}
				}, {
					key: 'Name',
					get: function get() {
						return Name;
					}
				}, {
					key: 'Selector',
					get: function get() {
						return Selector;
					}
				}, {
					key: 'EventKey',
					get: function get() {
						return EventKey;
					}
				}]);

				return SelectView;
			})(View);
		}
	};
});

$__System.register('16', ['4', '5', '7', '10'], function (_export) {
	var _get, _inherits, _classCallCheck, Model, AccordionModel;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_4) {
			Model = _4['default'];
		}],
		execute: function () {
			//
			// Accordion Model
			//

			'use strict';

			AccordionModel = (function (_Model) {
				_inherits(AccordionModel, _Model);

				function AccordionModel() {
					_classCallCheck(this, AccordionModel);

					_get(Object.getPrototypeOf(AccordionModel.prototype), 'constructor', this).apply(this, arguments);
				}

				return AccordionModel;
			})(Model);

			_export('default', AccordionModel);

			AccordionModel.prototype.defaults = {
				duration: 280,
				siblings: false,
				closable: true,
				instant: false,
				openAutoExpand: true,
				closeSiblings: true
			};
		}
	};
});

$__System.register('17', ['4', '5', '6', '7', '14', '15', '16', 'b'], function (_export) {
	var _get, _inherits, _createClass, _classCallCheck, View, Component, AccordionModel, _Object$assign, Name, Selector, DataKey, EventKey, Accordion, AccordionView;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_4) {
			_createClass = _4['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_5) {
			View = _5['default'];
		}, function (_6) {
			Component = _6['default'];
		}, function (_7) {
			AccordionModel = _7['default'];
		}, function (_b) {
			_Object$assign = _b['default'];
		}],
		execute: function () {
			//
			// Accordion Component
			//

			//
			// Configuration
			//
			'use strict';

			Name = 'Accordion';
			Selector = '[data-toggle="accordion"]:not([data-ignore])';
			DataKey = 'dls.accordion';
			EventKey = '.' + DataKey;

			//
			// Initiator
			//

			Accordion = (function (_Component) {
				_inherits(Accordion, _Component);

				function Accordion(opts) {
					_classCallCheck(this, Accordion);

					_get(Object.getPrototypeOf(Accordion.prototype), 'constructor', this).call(this, opts);
					this.component = AccordionView;

					this.onClickListen();
					this.onDataEventListen();
				}

				//
				// Accordion View
				//
				return Accordion;
			})(Component);

			_export('default', Accordion);

			AccordionView = (function (_View) {
				_inherits(AccordionView, _View);

				function AccordionView(opts) {
					_classCallCheck(this, AccordionView);

					_get(Object.getPrototypeOf(AccordionView.prototype), 'constructor', this).call(this, opts);

					if (!this.container.is('.open')) {
						this.el.hide();
					}
				}

				//
				// Getters
				//

				_createClass(AccordionView, [{
					key: 'toggle',

					//
					// Toggle
					//
					value: function toggle(opts) {
						opts = opts || {};

						if (this.target.is('.disabled')) return;

						this.emit('toggled');

						if (opts.expand && !this.container.is('.open')) this.expand(opts);else if (opts.expand === false) this.collapse(opts);else if (!this.container.is('.open')) {
							this.expand(opts);
						} else if (this.model.get('closable')) {
							this.collapse(opts);
						}
					}

					//
					// Expand
					//
				}, {
					key: 'expand',
					value: function expand(opts) {
						var _this = this;

						opts = _Object$assign({}, this.model.attributes, opts);

						if (this.transitioning) return;

						var _model$attributes = this.model.attributes;
						var openAutoExpand = _model$attributes.openAutoExpand;
						var closeSiblings = _model$attributes.closeSiblings;
						var siblings = _model$attributes.siblings;

						this.emit('expanding');

						this.target.attr('aria-expanded', 'true');
						this.transitioning = true;
						this.container.addClass('open');
						this.target.attr('aria-expanded', 'true');

						if (openAutoExpand) this.openAutoChildren();

						this.el.slideDown(opts.instant ? 0 : opts.duration, function () {
							_this.transitioning = false;
							_this.emit('expanded');
						});

						if (closeSiblings && this.el.closest('.nav').length && !this.siblings) this.closeSiblings();
					}

					//
					// Collapse
					//
				}, {
					key: 'collapse',
					value: function collapse(opts) {
						var _this2 = this;

						opts = _Object$assign({}, this.model.attributes, opts);

						this.transitioning = true;

						this.emit('collapsing');
						this.el.slideUp(opts.instant ? 0 : opts.duration, function () {
							_this2.target.attr('aria-expanded', 'false');
							_this2.container.removeClass('open');
							_this2.transitioning = false;
							$(window).trigger('resize');
							_this2.emit('collapsed');
						});

						this.closeChildren();
					}

					//
					// Open child accordions that have been flagged to auto open
					//
				}, {
					key: 'openAutoChildren',
					value: function openAutoChildren() {
						this.el.find('.accordion:not(.open) [data-auto-expand="true"]').trigger('data', { expand: true, instant: true });
					}

					//
					// Close sibling elements when prompted
					//
				}, {
					key: 'closeSiblings',
					value: function closeSiblings() {
						this.container.siblings('.accordion.open').each(function (key, el) {
							$('> ' + Selector, el).trigger('data', { expand: false });
						});
					}

					//
					// Close children
					//
				}, {
					key: 'closeChildren',
					value: function closeChildren() {
						this.el.find('.accordion.open > ' + Selector).trigger('data', { expand: false });
					}

					//
					// Interface
					//
				}, {
					key: 'destroy',

					//
					// Destroy
					//
					value: function destroy() {
						$.removeData(this.el, DataKey);
					}
				}], [{
					key: '_interface',
					value: function _interface(opts) {
						var $el = this,
						    data = $el.data(DataKey),
						    $accordionTarget = $el.siblings('.accordion-content'),
						    $container = $el.closest('.accordion'),
						    cmd = opts.invoke ? 'toggle' : $el.data(),
						    config = _Object$assign({}, $el.data(), typeof opts === 'object' && opts);

						if (!data) {
							data = new AccordionView({
								target: $el,
								el: $accordionTarget,
								container: $container,
								model: new AccordionModel(config)
							});

							$el.data(DataKey, data);
						}

						if (typeof cmd === 'string') if (data[cmd] !== undefined) {
							data[cmd](opts);
						}

						return data;
					}
				}, {
					key: 'Name',
					get: function get() {
						return Name;
					}
				}, {
					key: 'Selector',
					get: function get() {
						return Selector;
					}
				}, {
					key: 'EventKey',
					get: function get() {
						return EventKey;
					}
				}]);

				return AccordionView;
			})(View);
		}
	};
});

$__System.register('18', ['4', '5', '7', '10'], function (_export) {
	var _get, _inherits, _classCallCheck, Model, CollapsibleModel;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_4) {
			Model = _4['default'];
		}],
		execute: function () {
			//
			// Collapsible Model
			//

			'use strict';

			CollapsibleModel = (function (_Model) {
				_inherits(CollapsibleModel, _Model);

				function CollapsibleModel() {
					_classCallCheck(this, CollapsibleModel);

					_get(Object.getPrototypeOf(CollapsibleModel.prototype), 'constructor', this).apply(this, arguments);
				}

				return CollapsibleModel;
			})(Model);

			_export('default', CollapsibleModel);

			CollapsibleModel.prototype.defaults = {
				closable: true,
				duration: 300
			};
		}
	};
});

$__System.register('19', ['4', '5', '6', '7', '14', '15', '18', 'b'], function (_export) {
	var _get, _inherits, _createClass, _classCallCheck, View, Component, CollapsibleModel, _Object$assign, Name, Selector, DataKey, EventKey, Collapse, CollapsibleView;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_4) {
			_createClass = _4['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_5) {
			View = _5['default'];
		}, function (_6) {
			Component = _6['default'];
		}, function (_7) {
			CollapsibleModel = _7['default'];
		}, function (_b) {
			_Object$assign = _b['default'];
		}],
		execute: function () {
			//
			// Collapsible Component
			//

			//
			// Configuration
			//
			'use strict';

			Name = 'Collapsible';
			Selector = '[data-toggle="collapse"]:not([data-ignore])';
			DataKey = 'dls.collapsible';
			EventKey = '.' + DataKey;

			//
			// Initiator
			//

			Collapse = (function (_Component) {
				_inherits(Collapse, _Component);

				function Collapse(opts) {
					_classCallCheck(this, Collapse);

					_get(Object.getPrototypeOf(Collapse.prototype), 'constructor', this).call(this, opts);
					this.component = CollapsibleView;

					this.onClickListen();
				}

				//
				// Collapsible View
				//
				return Collapse;
			})(Component);

			_export('default', Collapse);

			CollapsibleView = (function (_View) {
				_inherits(CollapsibleView, _View);

				function CollapsibleView(opts) {
					_classCallCheck(this, CollapsibleView);

					_get(Object.getPrototypeOf(CollapsibleView.prototype), 'constructor', this).call(this, opts);

					if (!this.el.is('.in')) {
						this.el.hide();
					}
				}

				//
				// Getters
				//

				_createClass(CollapsibleView, [{
					key: 'toggle',

					//
					// Toggle
					//
					value: function toggle() {
						if (this.target.is('.disabled')) return;

						this.emit('toggled');

						if (!this.el.is('.in')) {
							this.expand();
						} else if (this.model.get('closable')) {
							this.collapse();
						}
					}

					//
					// Expand
					//
				}, {
					key: 'expand',
					value: function expand(opts) {
						var _this = this;

						opts = _Object$assign({}, this.model.attributes, opts);

						if (this.transitioning) return;

						this.emit('expanding');

						this.target.attr('aria-expanded', 'true');
						this.transitioning = true;
						this.el.addClass('in').attr('aria-expanded', 'true');

						this.el.slideDown(opts.instant ? 0 : opts.duration, function () {
							_this.transitioning = false;
							$(window).trigger('resize');
							_this.emit('expanded');
						});
					}

					//
					// Collapse
					//
				}, {
					key: 'collapse',
					value: function collapse(e, opts) {
						var _this2 = this;

						opts = _Object$assign({}, this.model.attributes, opts);

						if (this.transitioning) return;

						this.emit('collapsing');

						this.transitioning = true;

						this.el.slideUp(opts.instant ? 0 : opts.duration, function () {
							_this2.target.attr('aria-expanded', 'false');
							_this2.el.removeClass('in').attr('aria-expanded', 'false');

							_this2.transitioning = false;
							$(window).trigger('resize');
							_this2.emit('collapsed');
						});
					}

					//
					// Interface
					//
				}, {
					key: 'destroy',

					//
					// Destroy
					//
					value: function destroy() {
						$.removeData(this.el, DataKey);
					}
				}], [{
					key: '_interface',
					value: function _interface(opts) {
						var $el = this,
						    data = $el.data(DataKey),
						    collapseTarget = $el.attr('href'),
						    $collapseTarget = $(collapseTarget + ':first'),
						    cmd = opts.invoke ? 'toggle' : $el.data(),
						    config = _Object$assign({}, $el.data(), typeof opts === 'object' && opts);

						if (!data) {
							data = new CollapsibleView({
								target: $el,
								el: $collapseTarget,
								model: new CollapsibleModel(config)
							});

							$el.data(DataKey, data);
						}

						if (typeof cmd === 'string') if (data[cmd] !== undefined) {
							data[cmd]();
						}

						return data;
					}
				}, {
					key: 'Name',
					get: function get() {
						return Name;
					}
				}, {
					key: 'Selector',
					get: function get() {
						return Selector;
					}
				}, {
					key: 'EventKey',
					get: function get() {
						return EventKey;
					}
				}]);

				return CollapsibleView;
			})(View);
		}
	};
});

$__System.register('1a', ['4', '5', '7', '10'], function (_export) {
	var _get, _inherits, _classCallCheck, Model, DismissibleModel;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_4) {
			Model = _4['default'];
		}],
		execute: function () {
			//
			// Dismissible Model
			//

			'use strict';

			DismissibleModel = (function (_Model) {
				_inherits(DismissibleModel, _Model);

				function DismissibleModel() {
					_classCallCheck(this, DismissibleModel);

					_get(Object.getPrototypeOf(DismissibleModel.prototype), 'constructor', this).apply(this, arguments);
				}

				return DismissibleModel;
			})(Model);

			_export('default', DismissibleModel);

			DismissibleModel.prototype.defaults = {
				closed: false
			};
		}
	};
});

$__System.register('1b', ['4', '5', '6', '7', '14', '15', 'b', '1a'], function (_export) {
	var _get, _inherits, _createClass, _classCallCheck, View, Component, _Object$assign, DismissibleModel, Name, Selector, DataKey, EventKey, Dismiss, DismissibleView;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_4) {
			_createClass = _4['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_5) {
			View = _5['default'];
		}, function (_6) {
			Component = _6['default'];
		}, function (_b) {
			_Object$assign = _b['default'];
		}, function (_a) {
			DismissibleModel = _a['default'];
		}],
		execute: function () {
			//
			// Dismissible Component
			//

			//
			// Configuration
			//
			'use strict';

			Name = 'Dismissible';
			Selector = '[data-dismiss]:not([data-ignore])';
			DataKey = 'dls.dismissible';
			EventKey = '.' + DataKey;

			//
			// Initiator
			//

			Dismiss = (function (_Component) {
				_inherits(Dismiss, _Component);

				function Dismiss(opts) {
					_classCallCheck(this, Dismiss);

					_get(Object.getPrototypeOf(Dismiss.prototype), 'constructor', this).call(this, opts);
					this.component = DismissibleView;

					this.render();
					this.onClickListen();
				}

				//
				// Dismissable View
				//
				return Dismiss;
			})(Component);

			_export('default', Dismiss);

			DismissibleView = (function (_View) {
				_inherits(DismissibleView, _View);

				function DismissibleView(opts) {
					_classCallCheck(this, DismissibleView);

					_get(Object.getPrototypeOf(DismissibleView.prototype), 'constructor', this).call(this, opts);
				}

				//
				// Getters
				//

				_createClass(DismissibleView, [{
					key: 'close',

					//
					// Close
					//
					value: function close() {
						var _this = this;

						if (this.model.get('closed')) return;

						this.emit('closing');

						this.target.addClass('anim-fade').removeClass('in');

						this.model.set('closed', true);

						setTimeout(function () {
							_this.target.remove();
							_this.target.trigger('closed', _this);
							_this.emit('closed', _this);
							_this.destroy();
						}, 300);
					}

					//
					// Interface
					//
				}, {
					key: 'destroy',

					//
					// Destroy
					//
					value: function destroy() {
						$.removeData(this.target, DataKey);
					}
				}], [{
					key: '_interface',
					value: function _interface(opts) {
						var $el = this,
						    data = $el.data(DataKey),
						    dismissTarget = $el.data('dismiss-target'),
						    $dismissTarget = undefined,
						    cmd = opts.invoke ? 'close' : $el.data(),
						    config = _Object$assign({}, $el.data(), typeof opts === 'object' && opts);

						if (!data) {
							if (dismissTarget) $dismissTarget = $(dismissTarget);else $dismissTarget = $el.closest('.' + config.dismiss);

							data = new DismissibleView({
								target: $dismissTarget,
								el: $el,
								model: new DismissibleModel(config)
							});

							$el.data(DataKey, data);
						} else {
							// Component instance already exists
						}

						if (typeof cmd === 'string') if (data[cmd] !== undefined) {
							data[cmd]();
						}

						return data;
					}
				}, {
					key: 'Name',
					get: function get() {
						return Name;
					}
				}, {
					key: 'Selector',
					get: function get() {
						return Selector;
					}
				}, {
					key: 'EventKey',
					get: function get() {
						return EventKey;
					}
				}]);

				return DismissibleView;
			})(View);
		}
	};
});

$__System.register('1c', ['4', '5', '7', '10'], function (_export) {
	var _get, _inherits, _classCallCheck, Model, DropdownModel;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_4) {
			Model = _4['default'];
		}],
		execute: function () {
			//
			// Dropdown Model
			//

			'use strict';

			DropdownModel = (function (_Model) {
				_inherits(DropdownModel, _Model);

				function DropdownModel() {
					_classCallCheck(this, DropdownModel);

					_get(Object.getPrototypeOf(DropdownModel.prototype), 'constructor', this).apply(this, arguments);
				}

				return DropdownModel;
			})(Model);

			_export('default', DropdownModel);

			DropdownModel.prototype.defaults = {
				container: 'dropdown',
				menu: 'dropdown-menu',
				autoclose: true
			};
		}
	};
});

$__System.register('1d', ['4', '5', '6', '7', '14', '15', 'b', '1c'], function (_export) {
	var _get, _inherits, _createClass, _classCallCheck, View, Component, _Object$assign, DropdownModel, Name, Selector, DataKey, EventKey, Dropdown, DropdownView;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_4) {
			_createClass = _4['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_5) {
			View = _5['default'];
		}, function (_6) {
			Component = _6['default'];
		}, function (_b) {
			_Object$assign = _b['default'];
		}, function (_c) {
			DropdownModel = _c['default'];
		}],
		execute: function () {
			//
			// Dropdown Component
			//

			//
			// Configuration
			//
			'use strict';

			Name = 'Dropdown';
			Selector = '[data-toggle="dropdown"]:not([data-ignore])';
			DataKey = 'dls.dropdown';
			EventKey = '.' + DataKey;

			//
			// Initiator
			//

			Dropdown = (function (_Component) {
				_inherits(Dropdown, _Component);

				function Dropdown(opts) {
					_classCallCheck(this, Dropdown);

					_get(Object.getPrototypeOf(Dropdown.prototype), 'constructor', this).call(this, opts);
					this.component = DropdownView;

					this.render();
					this.onClickListen();
				}

				//
				// Dropdown View
				//
				return Dropdown;
			})(Component);

			_export('default', Dropdown);

			DropdownView = (function (_View) {
				_inherits(DropdownView, _View);

				function DropdownView(opts) {
					_classCallCheck(this, DropdownView);

					_get(Object.getPrototypeOf(DropdownView.prototype), 'constructor', this).call(this, opts);

					var container = DLS.util.request('format:selector:autoprefix', this.model.get('container')),
					    menu = DLS.util.request('format:selector:autoprefix', this.model.get('menu'));

					this.container = container.substr(0, 1) === '#' ? $(container) : this.target.closest(container), this.menu = menu.substr(0, 1) === '#' ? $(menu) : this.container.find(menu);

					if (!this.container.length || !this.menu.length) throw new Error('[DLS > ' + Name + ' Error]: Could not find\n\t\t\t\tthe \'container\' and/or \'menu\' element(s).');
				}

				//
				// Getters
				//

				_createClass(DropdownView, [{
					key: 'toggle',

					//
					// Toggle
					//
					value: function toggle() {
						if (!this.container.is('.open')) this.open();else this.close();
					}

					//
					// Open
					//
				}, {
					key: 'open',
					value: function open() {
						this.container.addClass('open');
						this.target.attr('aria-expanded', 'true');
						this.dropdownOpen();
					}

					//
					// Close
					//
				}, {
					key: 'close',
					value: function close() {
						this.container.removeClass('open');
						this.target.attr('aria-expanded', 'false');
						this.dropdownClose();
					}

					//
					// Dropdown opened
					//
				}, {
					key: 'dropdownOpen',
					value: function dropdownOpen() {
						$(document.body).on('click' + EventKey + '.' + this.uid, $.proxy(DropdownView._confirmClick, this));
					}

					//
					// Dropdown closed
					//
				}, {
					key: 'dropdownClose',
					value: function dropdownClose() {
						$(document.body).off('click' + EventKey + '.' + this.uid);
					}

					//
					// Confirm the click is our dropdown element
					//
				}, {
					key: 'destroy',

					//
					// Destroy
					//
					value: function destroy() {
						$.removeData(this.target, DataKey);
					}
				}], [{
					key: '_confirmClick',
					value: function _confirmClick(e) {
						if (!$.contains(this.container[0], e.target)) {
							if (this.container.is('.open')) this.close();
						} else {
							if ($(e.target).prop('tagName').toLowerCase() === 'a' && this.model.get('autoclose') && this.container.is('.open')) this.close();
						}
					}

					//
					// Interface
					//
				}, {
					key: '_interface',
					value: function _interface(opts) {
						var $el = $(this),
						    data = $el.data(DataKey),
						    cmd = opts.invoke ? 'toggle' : $el.data(),
						    config = _Object$assign({}, $el.data(), typeof opts === 'object' && opts);

						if (!data) {
							data = new DropdownView({
								target: $el,
								model: new DropdownModel(config)
							});

							$el.data(DataKey, data);
						}

						if (typeof cmd === 'string') if (data[cmd] !== undefined) data[cmd]();

						return data;
					}
				}, {
					key: 'Name',
					get: function get() {
						return Name;
					}
				}, {
					key: 'Selector',
					get: function get() {
						return Selector;
					}
				}, {
					key: 'EventKey',
					get: function get() {
						return EventKey;
					}
				}]);

				return DropdownView;
			})(View);
		}
	};
});

$__System.register('1e', ['4', '5', '7', '10'], function (_export) {
	var _get, _inherits, _classCallCheck, Model, ModalModel;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_4) {
			Model = _4['default'];
		}],
		execute: function () {
			//
			// Modal Model
			//

			'use strict';

			ModalModel = (function (_Model) {
				_inherits(ModalModel, _Model);

				function ModalModel() {
					_classCallCheck(this, ModalModel);

					_get(Object.getPrototypeOf(ModalModel.prototype), 'constructor', this).apply(this, arguments);
				}

				return ModalModel;
			})(Model);

			_export('default', ModalModel);

			ModalModel.prototype.defaults = {
				theme: 'default',
				classes: '',
				controlsLeft: 'back',
				controlsRight: '',
				size: 'fill', // fill, dynamic or boxed,
				title: 'Test',
				subtitle: '',
				content: '',
				animation: 'fade',
				container: $('body'),
				open: false
			};
		}
	};
});

(function() {
var define = $__System.amdDefine;
define("1f", [], function() {
  return "<div class=\"modal modal-<%= size %> modal-<%= theme %> <% if(animation) { %>anim-<%= animation %><% } %> <%= classes %>\" role=\"document\">\n\t<div class=\"modal-base\"></div>\n\t<div class=\"modal-dialog\">\n\t\t<div class=\"modal-header\">\n\t\t\t<div class=\"modal-controls-left\"><%= controlsLeft %></div>\n\t\t\t<div class=\"modal-heading\">\n\t\t\t\t<div class=\"modal-title\"><%= title %></div>\n\t\t\t\t<div class=\"modal-subtitle\"><%= subtitle %></div>\n\t\t\t</div>\n\t\t\t<div class=\"modal-controls-right\"><%= controlsRight %></div>\n\t\t</div>\n\t\t<div class=\"modal-body\">\n\t\t\t<div class=\"modal-content container\">\n\t\t\t\t<%= content %>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("20", [], function() {
  return "<a class=\"modal-control modal-control-back icon-hover dls-icon-left\" aria-label=\"Close\"></a>\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("21", [], function() {
  return "<a class=\"modal-control modal-control-back dls-glyph-close\" aria-label=\"Close\"></a>\n";
});

})();
$__System.register('22', ['4', '5', '6', '7', '14', '15', '20', '21', 'b', '1e', '1f'], function (_export) {
	var _get, _inherits, _createClass, _classCallCheck, View, Component, ModalControlBackTemplate, ModalControlCloseTemplate, _Object$assign, ModalModel, ModalTemplate, Name, DataKey, EventKey, Config, Modal, ModalView;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_5) {
			View = _5['default'];
		}, function (_6) {
			Component = _6['default'];
		}, function (_7) {
			ModalControlBackTemplate = _7['default'];
		}, function (_8) {
			ModalControlCloseTemplate = _8['default'];
		}, function (_b) {
			_Object$assign = _b['default'];
		}, function (_e) {
			ModalModel = _e['default'];
		}, function (_f) {
			ModalTemplate = _f['default'];
		}],
		execute: function () {
			//
			// Modal Component
			//

			//
			// Configuration
			//
			'use strict';

			Name = 'Modal';
			DataKey = 'dls.modal';
			EventKey = '.' + DataKey;
			Config = {
				timeout: {
					show: null,
					hide: null
				},
				hideDelay: 200
			};

			//
			// Initiator
			//

			Modal = (function (_Component) {
				_inherits(Modal, _Component);

				function Modal(opts) {
					_classCallCheck(this, Modal);

					_get(Object.getPrototypeOf(Modal.prototype), 'constructor', this).call(this, opts);
					this.component = ModalView;
				}

				//
				// Modal View
				//

				//
				// Modal `Create` (differs from standard create in that it doesn't)
				// require a target to be specified
				//

				_createClass(Modal, [{
					key: 'create',
					value: function create(options) {
						var _this = this;

						if (!this.component) return;

						if (!options.target) {
							options.target = $('<div />');
						}

						var elements = [];
						options.config = _Object$assign({}, options.config);
						options.target = $(options.target);
						options.target.each(function (idx, el) {
							var element = _this.component._interface.call($(el), options.config);

							if (element) elements.push(element);
						});

						return this.created(elements);
					}
				}]);

				return Modal;
			})(Component);

			_export('default', Modal);

			ModalView = (function (_View) {
				_inherits(ModalView, _View);

				function ModalView(opts) {
					_classCallCheck(this, ModalView);

					_get(Object.getPrototypeOf(ModalView.prototype), 'constructor', this).call(this, opts);
					this.setup();
					this.render();
					this.setProps();

					this.listen();
				}

				//
				// Getters
				//

				_createClass(ModalView, [{
					key: 'setup',

					//
					// Setup
					//
					value: function setup() {
						var _model$attributes = this.model.attributes;
						var controlsLeft = _model$attributes.controlsLeft;
						var controlsRight = _model$attributes.controlsRight;

						controlsLeft = this.generateControls(controlsLeft.replace(/ /g, '').split(','));

						controlsRight = this.generateControls(controlsRight.replace(/ /g, '').split(','));

						this.model.set('controlsLeft', controlsLeft);
						this.model.set('controlsRight', controlsRight);
					}

					//
					// Generate Controls
					//
				}, {
					key: 'generateControls',
					value: function generateControls(list) {
						var str = '';
						list.forEach(function (item) {
							switch (item) {
								case 'back':
									str += ModalControlBackTemplate;
									break;
								case 'close':
									str += ModalControlCloseTemplate;
									break;
								default:
									break;
							}
						});

						return str;
					}

					//
					// Render the component
					//
				}, {
					key: 'render',
					value: function render() {
						this.el = $(this.util.request('template:render', ModalTemplate, this.model.attributes));
					}

					//
					// Set the properties
					//
				}, {
					key: 'setProps',
					value: function setProps() {
						this.sel = {
							title: this.el.find('.modal-title'),
							content: this.el.find('.modal-content'),
							header: this.el.find('.modal-header'),
							controlsLeft: this.el.find('.modal-controls-left'),
							controlsRight: this.el.find('.modal-controls-right')
						};
					}

					//
					// Bind the listeners
					//
				}, {
					key: 'listen',
					value: function listen() {
						this.model.on('change', $.proxy(this.onChanged, this));

						this.app.vent.on('component:' + Name + ':open', $.proxy(this.onModalOpened, this)).on('component:' + Name + ':close', $.proxy(this.onModalClosed, this));
					}
				}, {
					key: 'activeListen',
					value: function activeListen() {
						var _this2 = this;

						this.el.on('click' + EventKey, '.modal-control-back', function () {
							return _this2.close();
						});
					}

					//
					// Model changed
					//
				}, {
					key: 'onChanged',
					value: function onChanged(obj) {

						if (obj.content != null) this.sel.content.html(obj.content);

						if (obj.title !== undefined) this.sel.title.html(obj.title);
					}

					//
					// On Modal Opened (triggered when ANY modal opens)
					//
				}, {
					key: 'onModalOpened',
					value: function onModalOpened(uid) {
						if (uid === this.uid) return;

						if (this.model.get('open')) this.close();
					}

					//
					// On Modal Closed (triggered when ANY modal closes)
					//
				}, {
					key: 'onModalClosed',
					value: function onModalClosed(uid) {
						if (uid === this.uid) return;
					}

					//
					// Proxy `open`
					//
				}, {
					key: 'show',
					value: function show() {
						return this.open.apply(this, arguments);
					}

					//
					// Proxy `close`
					//
				}, {
					key: 'hide',
					value: function hide() {
						return this.close.apply(this, arguments);
					}

					//
					// Open modal
					//
				}, {
					key: 'open',
					value: function open() {
						var _this3 = this;

						var container = this.model.get('container');
						clearTimeout(Config.timeout.show);
						clearTimeout(Config.timeout.hide);

						this.el.removeClass('in');
						this.model.set('open', true);

						this.emit('opened');

						this.app.vent.trigger('component:' + Name + ':open', this.uid);

						container.append(this.el);

						Config.timeout.show = setTimeout(function () {
							_this3.el.addClass('in');
						}, 10);

						this.emit('opening');

						this.activeListen();

						return this;
					}

					//
					// Close modal
					//
				}, {
					key: 'close',
					value: function close() {
						var _this4 = this;

						this.model.set('open', false);
						clearTimeout(Config.timeout.show);
						clearTimeout(Config.timeout.hide);

						this.app.vent.trigger('component:' + Name + ':close', this.uid);

						this.el.removeClass('in');

						this.emit('closing');

						Config.timeout.hide = setTimeout(function () {
							_this4.emit('closed');
							_this4.el.remove();
						}, Config.hideDelay);

						return this;
					}

					//
					// Interface
					//
				}, {
					key: 'destroy',

					//
					// Destroy
					//
					value: function destroy() {
						$.removeData(this.target, DataKey);
					}
				}], [{
					key: '_interface',
					value: function _interface(opts) {
						var $el = $(this),
						    data = $el.data(DataKey),
						    config = _Object$assign({}, $el.data(), opts);

						if (!data) {
							data = new ModalView({
								target: $el,
								model: new ModalModel(config)
							});

							$el.data(DataKey, data);
						} else {
							console.warn('DLS > ' + Name + ': A modal view is already attached to this element. ' + 'Use the destroy() method for the existing modal.');
						}

						return data;
					}
				}, {
					key: 'Name',
					get: function get() {
						return Name;
					}
				}, {
					key: 'EventKey',
					get: function get() {
						return EventKey;
					}
				}]);

				return ModalView;
			})(View);
		}
	};
});

$__System.register('23', ['4', '5', '7', '10'], function (_export) {
	var _get, _inherits, _classCallCheck, Model, SearchModel;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_4) {
			Model = _4['default'];
		}],
		execute: function () {
			//
			// Search Model
			//

			'use strict';

			SearchModel = (function (_Model) {
				_inherits(SearchModel, _Model);

				function SearchModel() {
					_classCallCheck(this, SearchModel);

					_get(Object.getPrototypeOf(SearchModel.prototype), 'constructor', this).apply(this, arguments);
				}

				return SearchModel;
			})(Model);

			_export('default', SearchModel);

			SearchModel.prototype.defaults = {
				live: true,
				throttle: 200,
				keyevent: 'keydown',
				value: '',
				state: 'default',
				tooltip: true,
				tooltipDelay: 0,
				tooltipDefault: '',
				tooltipSearching: '',
				tooltipActive: 'Clear Search',
				tooltipTheme: 'dark',
				tooltipClasses: '',
				tooltipStyle: '',
				tooltipPlacement: 'top',
				tooltipOffset: '0 0'
			};
		}
	};
});

$__System.register('24', ['4', '5', '6', '7', '14', '15', '23', 'b'], function (_export) {
	var _get, _inherits, _createClass, _classCallCheck, View, Component, SearchModel, _Object$assign, Name, DataKey, EventKey, inputIsDisabled, Search, SearchView;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_4) {
			_createClass = _4['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_5) {
			View = _5['default'];
		}, function (_6) {
			Component = _6['default'];
		}, function (_7) {
			SearchModel = _7['default'];
		}, function (_b) {
			_Object$assign = _b['default'];
		}],
		execute: function () {
			//
			// Search Component
			//

			//
			// Configuration
			//
			'use strict';

			Name = 'Search';
			DataKey = 'dls.search';
			EventKey = '.' + DataKey;

			inputIsDisabled = function inputIsDisabled(input) {
				return input.is(':disabled') || input.is('.disabled');
			};

			//
			// Initiator
			//

			Search = (function (_Component) {
				_inherits(Search, _Component);

				function Search(opts) {
					_classCallCheck(this, Search);

					_get(Object.getPrototypeOf(Search.prototype), 'constructor', this).call(this, opts);
					this.component = SearchView;
				}

				//
				// Search View
				//
				return Search;
			})(Component);

			_export('default', Search);

			SearchView = (function (_View) {
				_inherits(SearchView, _View);

				function SearchView(opts) {
					_classCallCheck(this, SearchView);

					_get(Object.getPrototypeOf(SearchView.prototype), 'constructor', this).call(this, opts);

					this.setProps();
					this.createTooltip();
					this.listen();
				}

				//
				// Getters
				//

				_createClass(SearchView, [{
					key: 'setProps',

					//
					// Store the properties
					//
					value: function setProps() {
						this.sel = {
							input: this.target.find('input'),
							submit: this.target.find('button[type="submit"]')
						};

						this.model.attributes.value = this.sel.input.val();
						this.sel.submit.prop('type', 'button');
					}

					//
					// Create tooltip
					//
				}, {
					key: 'createTooltip',
					value: function createTooltip() {
						if (!this.model.get('tooltip')) return;

						this.tooltip = this.app.create("tooltip", {
							target: this.sel.submit,
							config: {
								content: this.getTooltipLabel(),
								theme: this.model.get('tooltipTheme'),
								delay: this.model.get('tooltipDelay'),
								size: 'sm',
								placement: this.model.get('tooltipPlacement'),
								classes: this.model.get('tooltipClasses'),
								style: this.model.get('tooltipStyle'),
								trigger: 'manual',
								offset: this.model.get('tooltipOffset')
							}
						});
					}

					//
					// Get the tooltip label text based on the current state
					//
				}, {
					key: 'getTooltipLabel',
					value: function getTooltipLabel() {
						var state = this.model.get('state');
						state = state.charAt(0).toUpperCase() + state.slice(1);

						return this.model.get('tooltip' + state) || '';
					}

					//
					// Listen
					//
				}, {
					key: 'listen',
					value: function listen() {

						if (this.model.get('live')) this.sel.input.on(this.model.get('keyevent'), this.util.request('events:throttle', $.proxy(this.onChange, this), this.model.get('throttle'), this));

						this.target.on('submit', $.proxy(this.onFormSubmit, this));

						this.sel.submit.on(this.platform.event.over, $.proxy(this.onSubmitOver, this)).on(this.platform.event.out, $.proxy(this.onSubmitOut, this)).on('click.submit', $.proxy(this.onSubmit, this));

						this.model.on('change', $.proxy(this.onChanged, this));
					}

					//
					// Submit hover over
					//
				}, {
					key: 'onSubmitOver',
					value: function onSubmitOver() {
						if (!this.tooltip) return;

						this.tooltip.set('content', this.getTooltipLabel());
						if (this.tooltip.get('content') !== '') this.tooltip.show();
					}

					//
					// Submit hover out
					//
				}, {
					key: 'onSubmitOut',
					value: function onSubmitOut() {
						if (!this.tooltip) return;

						this.tooltip.hide();
					}
				}, {
					key: 'onFormSubmit',
					value: function onFormSubmit(e) {
						var state = this.model.get('state');
						e.preventDefault();

						if (inputIsDisabled(this.sel.input)) return;

						if (state === 'searching') return;else if (state === 'default' || state === 'active') this.onChange(true);
					}

					//
					// Submit search in its current state
					//
				}, {
					key: 'onSubmit',
					value: function onSubmit(e) {
						var state = this.model.get('state');
						e.preventDefault();
						if (this.inputIsDisabled(this.sel.input)) return;

						if (state === 'searching') return;else if (state === 'active') this.reset(true);else if (state === 'default') this.onChange(true);
					}

					//
					// Reset the search to its default state
					//
				}, {
					key: 'reset',
					value: function reset(focus) {
						this.sel.input.val('');
						this.onChange();

						if (focus) this.sel.input.focus();
					}

					//
					// Key changed
					//
				}, {
					key: 'onChange',
					value: function onChange(manual) {
						var val = this.sel.input.val();

						if (this.model.get('value') === val && !manual) return;

						this.model.attributes.value = this.sel.input.val();

						this.emit('change', this.model.get('value'));
					}

					//
					// Model changed
					//
				}, {
					key: 'onChanged',
					value: function onChanged(obj) {
						var value = this.model.get('value');

						if (obj.value !== undefined) this.sel.input.val(value);

						if (obj.state !== undefined) this.target.attr('data-state', obj.state);
					}

					//
					// Interface
					//
				}, {
					key: 'destroy',

					//
					// Destroy
					//
					value: function destroy() {
						$.removeData(this.target, DataKey);
					}
				}], [{
					key: '_interface',
					value: function _interface(config) {
						var $el = $(this),
						    data = $el.data(DataKey);

						config = _Object$assign($el.data(), config);

						if (!data) {
							data = new SearchView({
								target: $el,
								model: new SearchModel(config),
								value: $el.find('input').val()
							});

							$el.data(DataKey, data);
						}

						return data;
					}
				}, {
					key: 'Name',
					get: function get() {
						return Name;
					}
				}, {
					key: 'EventKey',
					get: function get() {
						return EventKey;
					}
				}]);

				return SearchView;
			})(View);
		}
	};
});

$__System.register('25', ['4', '5', '7', '10'], function (_export) {
	var _get, _inherits, _classCallCheck, Model, SliderModel;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_4) {
			Model = _4['default'];
		}],
		execute: function () {
			//
			// Slider Model
			//

			'use strict';

			SliderModel = (function (_Model) {
				_inherits(SliderModel, _Model);

				function SliderModel() {
					_classCallCheck(this, SliderModel);

					_get(Object.getPrototypeOf(SliderModel.prototype), 'constructor', this).apply(this, arguments);
				}

				return SliderModel;
			})(Model);

			_export('default', SliderModel);

			SliderModel.prototype.defaults = {
				controls: false,
				stepInterval: 50,
				stepDelay: 500,
				min: 0,
				max: 10,
				step: 1,
				value: 5,
				tooltip: 'true',
				tooltipTheme: 'light',
				prefix: '',
				suffix: '',
				render: true
			};
		}
	};
});

(function() {
var define = $__System.amdDefine;
define("26", [], function() {
  return "<div class=\"slider slider-horizontal <% if(controls) {%>slider-controls<% } %>\">\n\n\t<div class=\"slider-control\">\n\t\t<div class=\"slider-btn slider-subtract dls-glyph-minus\" role=\"button\"></div>\n\t</div>\n\n\t<div class=\"slider-bar\">\n\t\t<div class=\"slider-track\">\n\t\t\t<div class=\"slider-selection\"></div>\n\t\t</div>\n\n\t\t<div class=\"slider-handle\" role=\"slider\"\n\t\t\tdata-toggle=\"tooltip\"\n\t\t\tdata-manual=\"true\"\n\t\t\tdata-theme=\"<%= tooltipTheme %>\"\n\t\t\tdata-trigger=\"manual hover\"\n\t\t\tdata-delay=\"0\"\n\t\t\tdata-placement=\"top\"\n\t\t\taria-valuemin=\"<%= min %>\"\n\t\t\taria-valuemax=\"<%= max %>\"\n\t\t\taria-valuenow=\"<%= value %>\">\n\t\t</div>\n\t</div>\n\n\t<div class=\"slider-control\">\n\t\t<div class=\"slider-btn slider-add dls-glyph-plus\" role=\"button\"></div>\n\t</div>\n</div>\n";
});

})();
$__System.register('27', ['4', '5', '6', '7', '14', '15', '25', '26', 'b'], function (_export) {
	var _get, _inherits, _createClass, _classCallCheck, View, Component, SliderModel, SliderTemplate, _Object$assign, Name, DataKey, EventKey, Step, Slider, SliderView;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_4) {
			_createClass = _4['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_5) {
			View = _5['default'];
		}, function (_6) {
			Component = _6['default'];
		}, function (_7) {
			SliderModel = _7['default'];
		}, function (_8) {
			SliderTemplate = _8['default'];
		}, function (_b) {
			_Object$assign = _b['default'];
		}],
		execute: function () {
			//
			// Slider Component
			//

			//
			// Configuration
			//
			'use strict';

			Name = 'Slider';
			DataKey = 'dls.slider';
			EventKey = '.' + DataKey;
			Step = {
				amount: 0,
				timeout: null,
				interval: null
			};

			//
			// Initiator
			//

			Slider = (function (_Component) {
				_inherits(Slider, _Component);

				function Slider(opts) {
					_classCallCheck(this, Slider);

					_get(Object.getPrototypeOf(Slider.prototype), 'constructor', this).call(this, opts);
					this.component = SliderView;
				}

				//
				// Slider View
				//
				return Slider;
			})(Component);

			_export('default', Slider);

			SliderView = (function (_View) {
				_inherits(SliderView, _View);

				function SliderView(opts) {
					_classCallCheck(this, SliderView);

					_get(Object.getPrototypeOf(SliderView.prototype), 'constructor', this).call(this, opts);

					if (!this.model.get('step')) this.model.set('step', 1);

					this.Step = {
						amount: 0,
						timeout: null,
						interval: null
					};

					this.range = Math.sqrt(Math.pow(this.model.get('min') - this.model.get('max'), 2));

					this.render();

					this.setProps();
					this.createDraggable();
					this.createTooltip();

					this.listen();
					this.setPositionToValue(this.model.get('value'), true);
				}

				//
				// Getters
				//

				_createClass(SliderView, [{
					key: 'render',

					//
					// Render the component
					//
					value: function render() {
						this.el = $(this.util.request('template:render', SliderTemplate, this.model.attributes));

						this.target.hide().after(this.el);
					}

					//
					// Set the properties
					//
				}, {
					key: 'setProps',
					value: function setProps() {
						this.sel = {
							bar: this.el.find('.slider-bar'),
							handle: this.el.find('.slider-handle'),
							track: this.el.find('.slider-track'),
							selection: this.el.find('.slider-selection'),
							add: this.el.find('.slider-add'),
							subtract: this.el.find('.slider-subtract')
						};
					}

					//
					// Create draggable control
					//
				}, {
					key: 'createDraggable',
					value: function createDraggable() {
						this.draggable = this.util.request('ui:draggable', {
							target: this.sel.handle
						});
					}

					//
					// Create the tooltip
					///
				}, {
					key: 'createTooltip',
					value: function createTooltip() {
						if (this.model.get('tooltip') !== true) {
							this.sel.handle.removeAttr('data-toggle');
							return;
						}

						this.tooltip = this.app.create('tooltip', {
							target: this.sel.handle,
							config: {}
						});
					}

					//
					// Bind the listeners
					//
				}, {
					key: 'listen',
					value: function listen() {
						this.draggable.on('change', this.onDragState, this).on('drag:start', this.onDragStart, this).on('drag:end', this.onDragEnd, this);

						this.model.on('change', this.changed, this);

						this.sel.track.on(this.platform.event.down + EventKey, $.proxy(this.draggable.skipToPosition, this.draggable));

						this.sel.add.on(this.platform.event.down + EventKey, $.proxy(this.onAdd, this));

						this.sel.subtract.on(this.platform.event.down + EventKey, $.proxy(this.onSubtract, this));
					}

					//
					// Drag state change
					//
				}, {
					key: 'onDragState',
					value: function onDragState(e) {
						if (e.dragging) {
							var offsetLeft = this.sel.handle.offset().left,
							    width = this.sel.handle.width();

							this.startPos = offsetLeft + width * 0.5;
						}

						if (!this.draggable.attributes.dragging) return;

						this.updatePosition(e);
					}

					//
					// Drag Start
					//
				}, {
					key: 'onDragStart',
					value: function onDragStart(e) {
						if (this.tooltip) this.tooltip.show(true);
					}

					//
					// Drag end
					//
				}, {
					key: 'onDragEnd',
					value: function onDragEnd(e) {
						if (this.tooltip) this.tooltip.hide(true);
					}

					//
					// On Add
					//
				}, {
					key: 'onAdd',
					value: function onAdd(e) {
						if (e.which === 3) return;

						this.Step.amount = this.model.get('step');
						this.stepToggleOn();
					}

					//
					// On Subtract
				}, {
					key: 'onSubtract',
					value: function onSubtract(e) {
						if (e.which === 3) return;

						this.Step.amount = -this.model.get('step');
						this.stepToggleOn();
					}

					//
					// Step Toggle
					//
				}, {
					key: 'stepToggleOn',
					value: function stepToggleOn() {
						var _this = this;

						var _model$attributes = this.model.attributes;
						var value = _model$attributes.value;
						var min = _model$attributes.min;
						var max = _model$attributes.max;
						var stepDelay = _model$attributes.stepDelay;
						var stepInterval = _model$attributes.stepInterval;

						clearTimeout(this.Step.timeout);
						clearTimeout(this.Step.interval);

						if (this.tooltip) this.tooltip.show(true);

						value = Math.max(min, Math.min(value + this.Step.amount, max));

						this.setPositionToValue(value);

						this.Step.timeout = setTimeout(function () {
							_this.Step.interval = setInterval(function () {
								value = Math.max(min, Math.min(value + _this.Step.amount, max));

								_this.setPositionToValue(value);

								if (_this.tooltip) _this.tooltip.trigger('tether:position');
							}, stepInterval);
						}, stepDelay);

						if (this.tooltip) this.tooltip.trigger('tether:position');

						$(document).on(this.platform.event.up + EventKey + this.uid, $.proxy(this.stepToggleOff, this));
					}

					//
					// Step Toggle Off
					//
				}, {
					key: 'stepToggleOff',
					value: function stepToggleOff() {
						clearTimeout(this.Step.timeout);
						clearTimeout(this.Step.interval);

						if (this.tooltip) this.tooltip.delayedHide(1000);

						$(document).off(this.platform.event.up + EventKey + this.uid, $.proxy(this.stepToggleOff, this));
					}

					//
					// Update Position
					//
				}, {
					key: 'updatePosition',
					value: function updatePosition(e) {
						var min = this.model.get('min'),
						    percentage = this.getPercentage(this.draggable.attributes.movedX),
						    value = undefined,
						    calc = this.fixToStep(this.range / 100 * percentage, percentage);

						percentage = calc / this.range * 100;
						value = Number((this.range / 100 * percentage + min).toFixed(3));

						this.setPositionToValue(value);

						if (this.tooltip) this.tooltip.trigger('tether:position');
					}

					//
					// Fix to step
					//
				}, {
					key: 'fixToStep',
					value: function fixToStep(num, percentage) {
						var step = this.model.get('step'),
						    resto = num % step,
						    value = undefined;

						if (!percentage) value = 0;else if (percentage === 100) value = this.range;else if (resto <= step / 2) value = num - resto;else value = num + step - resto;

						return value;
					}

					//
					// Set position to the calculated value
					//
				}, {
					key: 'setPositionToValue',
					value: function setPositionToValue(value, force) {
						if (this.model.get('value') !== value || force) this.model.set('value', value);
					}

					//
					// Get Percentage
					//
				}, {
					key: 'getPercentage',
					value: function getPercentage(pos) {
						var offset = this.sel.bar.offset().left,
						    width = this.sel.bar.width(),
						    distance = this.startPos + pos - offset,
						    percentage = distance / width * 100;

						return Math.max(0, Math.min(100, percentage));
					}

					//
					// Changed
					//
				}, {
					key: 'changed',
					value: function changed() {
						var value = this.model.get('value'),
						    min = this.model.get('min'),
						    percentage = Math.abs(value - min) / this.range * 100;

						this.sel.handle.attr('aria-valuenow', value);
						this.target.val(value);

						this.updateTooltipValue(value);

						this.sel.handle.css({ left: percentage + '%' });

						this.sel.selection.css({ width: percentage + '%' });

						this.emit('change', value);
					}

					//
					// Update the tooltip value
					//
				}, {
					key: 'updateTooltipValue',
					value: function updateTooltipValue(value) {
						var prefix = this.model.get('prefix'),
						    suffix = this.model.get('suffix'),
						    formatted = this.util.request('format:numbers:commas', value),
						    content = prefix + formatted + suffix;

						if (this.tooltip) {
							this.tooltip.set('content', content);
							this.tooltip.trigger('tether:position');
						}
					}

					//
					// Interface
					//
				}, {
					key: 'destroy',

					//
					// Destroy
					//
					value: function destroy() {
						$.removeData(this.target, DataKey);
					}
				}], [{
					key: '_interface',
					value: function _interface(opts) {
						var $el = $(this),
						    data = $el.data(DataKey),
						    config = _Object$assign({
							min: Number($el.prop('min')),
							max: Number($el.prop('max')),
							value: Number($el.prop('value')),
							step: Number($el.prop('step'))
						}, $el.data(), opts);

						if (!data) {
							data = new SliderView({
								target: $el,
								model: new SliderModel(config)
							});

							$el.data(DataKey, data);
						}

						return data;
					}
				}, {
					key: 'Name',
					get: function get() {
						return Name;
					}
				}, {
					key: 'EventKey',
					get: function get() {
						return EventKey;
					}
				}]);

				return SliderView;
			})(View);
		}
	};
});

$__System.register('28', ['4', '5', '7', '10'], function (_export) {
	var _get, _inherits, _classCallCheck, Model, StepperModel;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_4) {
			Model = _4['default'];
		}],
		execute: function () {
			//
			// Stepper Model
			//

			'use strict';

			StepperModel = (function (_Model) {
				_inherits(StepperModel, _Model);

				function StepperModel() {
					_classCallCheck(this, StepperModel);

					_get(Object.getPrototypeOf(StepperModel.prototype), 'constructor', this).apply(this, arguments);
				}

				return StepperModel;
			})(Model);

			_export('default', StepperModel);

			StepperModel.prototype.defaults = {
				stepInterval: 50,
				stepDelay: 500,
				min: 0,
				max: 10,
				step: 1,
				value: 5,
				prefix: '',
				suffix: '',
				render: true
			};
		}
	};
});

(function() {
var define = $__System.amdDefine;
define("29", [], function() {
  return "<div class=\"stepper\">\n\t<button type=\"button\" class=\"btn-inline stepper-subtract dls-glyph-minus\"></button>\n\t<div class=\"stepper-field\">\n\t\t<input type=\"text\" class=\"stepper-value\" value=\"<%= value %>\" readonly />\n\t</div>\n\t<button type=\"button\" class=\"btn-inline stepper-add dls-glyph-plus\"></button>\n</div>\n";
});

})();
$__System.register('2a', ['4', '5', '6', '7', '14', '15', '28', '29', 'b'], function (_export) {
	var _get, _inherits, _createClass, _classCallCheck, View, Component, StepperModel, StepperTemplate, _Object$assign, Name, DataKey, EventKey, Step, Stepper, StepperView;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_4) {
			_createClass = _4['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_5) {
			View = _5['default'];
		}, function (_6) {
			Component = _6['default'];
		}, function (_7) {
			StepperModel = _7['default'];
		}, function (_8) {
			StepperTemplate = _8['default'];
		}, function (_b) {
			_Object$assign = _b['default'];
		}],
		execute: function () {
			//
			// Stepper Component
			//

			//
			// Configuration
			//
			'use strict';

			Name = 'Stepper';
			DataKey = 'dls.stepper';
			EventKey = '.' + DataKey;
			Step = {
				amount: 0,
				timeout: null,
				interval: null
			};

			//
			// Initiator
			//

			Stepper = (function (_Component) {
				_inherits(Stepper, _Component);

				function Stepper(opts) {
					_classCallCheck(this, Stepper);

					_get(Object.getPrototypeOf(Stepper.prototype), 'constructor', this).call(this, opts);
					this.component = StepperView;
				}

				//
				// Stepper View
				//
				return Stepper;
			})(Component);

			_export('default', Stepper);

			StepperView = (function (_View) {
				_inherits(StepperView, _View);

				function StepperView(opts) {
					_classCallCheck(this, StepperView);

					_get(Object.getPrototypeOf(StepperView.prototype), 'constructor', this).call(this, opts);

					if (!this.model.get('step')) this.model.set('step', 1);

					this.render();

					this.setProps();

					this.listen();
					this.setValue(this.model.get('value'), true);
				}

				//
				// Getters
				//

				_createClass(StepperView, [{
					key: 'render',

					//
					// Render the component
					//
					value: function render() {
						this.el = $(this.util.request('template:render', StepperTemplate, this.model.attributes));

						this.target.hide().after(this.el);
					}

					//
					// Set the properties
					//
				}, {
					key: 'setProps',
					value: function setProps() {
						this.sel = {
							value: this.el.find('.stepper-value'),
							add: this.el.find('.stepper-add'),
							subtract: this.el.find('.stepper-subtract')
						};
					}

					//
					// Bind the listeners
					//
				}, {
					key: 'listen',
					value: function listen() {
						this.model.on('change', this.changed, this);

						this.sel.add.on(this.platform.event.down + EventKey, $.proxy(this.onAdd, this));

						this.sel.subtract.on(this.platform.event.down + EventKey, $.proxy(this.onSubtract, this));
					}

					//
					// On Add
					//
				}, {
					key: 'onAdd',
					value: function onAdd(e) {
						if (e.which === 3) return;

						Step.amount = this.model.get('step');
						this.stepToggleOn();
					}

					//
					// On Subtract
				}, {
					key: 'onSubtract',
					value: function onSubtract(e) {
						if (e.which === 3) return;

						Step.amount = -this.model.get('step');
						this.stepToggleOn();
					}

					//
					// Step Toggle On
					//
				}, {
					key: 'stepToggleOn',
					value: function stepToggleOn() {
						var _this = this;

						var _model$attributes = this.model.attributes;
						var value = _model$attributes.value;
						var min = _model$attributes.min;
						var max = _model$attributes.max;
						var stepDelay = _model$attributes.stepDelay;
						var stepInterval = _model$attributes.stepInterval;

						clearTimeout(Step.timeout);
						clearTimeout(Step.interval);

						value = Math.max(min, Math.min(value + Step.amount, max));

						this.setValue(value);

						Step.timeout = setTimeout(function () {
							Step.interval = setInterval(function () {
								value = Math.max(min, Math.min(value + Step.amount, max));

								_this.setValue(value);
							}, stepInterval);
						}, stepDelay);

						$(document).on(this.platform.event.up + EventKey + this.uid, $.proxy(this.stepToggleOff, this));
					}

					//
					// Step Toggle Off
					//
				}, {
					key: 'stepToggleOff',
					value: function stepToggleOff() {
						clearTimeout(Step.timeout);
						clearTimeout(Step.interval);

						$(document).off(this.platform.event.up + EventKey + this.uid, $.proxy(this.stepToggleOff, this));
					}

					//
					// Set the value
					//
				}, {
					key: 'setValue',
					value: function setValue(value, force) {
						if (this.model.get('value') !== value || force) this.model.set('value', value);
					}

					//
					// Changed
					//
				}, {
					key: 'changed',
					value: function changed() {
						var value = this.model.get('value');

						this.target.val(value);

						this.sel.value.val(value);

						this.emit('change', value);
					}

					//
					// Interface
					//
				}, {
					key: 'destroy',

					//
					// Destroy
					//
					value: function destroy() {
						$.removeData(this.target, DataKey);
					}
				}], [{
					key: '_interface',
					value: function _interface(opts) {
						var $el = $(this),
						    data = $el.data(DataKey),
						    config = _Object$assign({
							min: Number($el.prop('min')),
							max: Number($el.prop('max')),
							value: Number($el.prop('value')),
							step: Number($el.prop('step'))
						}, $el.data(), opts);

						if (!data) {
							data = new StepperView({
								target: $el,
								model: new StepperModel(config)
							});

							$el.data(DataKey, data);
						}

						return data;
					}
				}, {
					key: 'Name',
					get: function get() {
						return Name;
					}
				}, {
					key: 'EventKey',
					get: function get() {
						return EventKey;
					}
				}]);

				return StepperView;
			})(View);
		}
	};
});

$__System.register('2b', ['4', '5', '7', '10'], function (_export) {
	var _get, _inherits, _classCallCheck, Model, TabsModel;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_4) {
			Model = _4['default'];
		}],
		execute: function () {
			//
			// Tabs Model
			//

			'use strict';

			TabsModel = (function (_Model) {
				_inherits(TabsModel, _Model);

				function TabsModel() {
					_classCallCheck(this, TabsModel);

					_get(Object.getPrototypeOf(TabsModel.prototype), 'constructor', this).apply(this, arguments);
				}

				return TabsModel;
			})(Model);

			_export('default', TabsModel);

			TabsModel.prototype.defaults = {
				closed: false
			};
		}
	};
});

$__System.register('2c', ['4', '5', '6', '7', '14', '15', 'b', '2b'], function (_export) {
	var _get, _inherits, _createClass, _classCallCheck, View, Component, _Object$assign, TabsModel, Name, Selector, SelectorButton, DataKey, EventKey, Tabs, TabsView;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_4) {
			_createClass = _4['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_5) {
			View = _5['default'];
		}, function (_6) {
			Component = _6['default'];
		}, function (_b) {
			_Object$assign = _b['default'];
		}, function (_b2) {
			TabsModel = _b2['default'];
		}],
		execute: function () {
			//
			// Tabs Component
			//

			//
			// Configuration
			//
			'use strict';

			Name = 'Tabs';
			Selector = '[data-toggle="tabs"][role="tablist"]';
			SelectorButton = '[role="tab"]';
			DataKey = 'dls.tabs';
			EventKey = '.' + DataKey;

			//
			// Initiator
			//

			Tabs = (function (_Component) {
				_inherits(Tabs, _Component);

				function Tabs(opts) {
					_classCallCheck(this, Tabs);

					_get(Object.getPrototypeOf(Tabs.prototype), 'constructor', this).call(this, opts);
					this.component = TabsView;

					this.render();
					this.onClickListen();
				}

				//
				// Tabs View
				//
				return Tabs;
			})(Component);

			_export('default', Tabs);

			TabsView = (function (_View) {
				_inherits(TabsView, _View);

				function TabsView(opts) {
					_classCallCheck(this, TabsView);

					_get(Object.getPrototypeOf(TabsView.prototype), 'constructor', this).call(this, opts);
				}

				//
				// Getters
				//

				_createClass(TabsView, [{
					key: 'toggle',
					value: function toggle(tab) {
						var _this = this;

						var controls = tab.attr('aria-controls'),
						    panels = [],
						    $panels = undefined,
						    panel = $('#' + controls);

						if (!panel.length || !this.target.length) return;

						this.target.find(SelectorButton).each(function (idx, el) {
							el = $(el);

							if (el.closest(Selector)[0] === _this.target[0]) {
								var _controls = el.attr('aria-controls'),
								    current = $('#' + _controls)[0];

								if ($(current).length) if (current !== panel[0]) panels.push(current);
							}
						}).not(tab).attr('aria-selected', false);
						$panels = $(panels);

						tab.attr('aria-selected', true);
						$panels.attr('aria-hidden', true);
						panel.attr('aria-hidden', false);
					}

					//
					// Interface
					//
				}, {
					key: 'destroy',

					//
					// Destroy
					//
					value: function destroy() {
						$.removeData(this.target, DataKey);
					}
				}], [{
					key: '_interface',
					value: function _interface(opts) {
						var $tab = this,
						    $el = $tab.closest(Selector),
						    data = $el.data(DataKey),
						    cmd = opts.invoke ? 'toggle' : null,
						    config = _Object$assign({}, $el.data(), typeof opts === 'object' && opts);

						if (!data) {

							data = new TabsView({
								target: $el,
								model: new TabsModel(config)
							});

							$el.data(DataKey, data);
						} else {
							// Component instance already exists
						}

						if (typeof cmd === 'string') if (data[cmd] !== undefined) {
							data[cmd]($tab);
						}

						return data;
					}
				}, {
					key: 'Name',
					get: function get() {
						return Name;
					}
				}, {
					key: 'Selector',
					get: function get() {
						return Selector + ':not([data-ignore]) ' + SelectorButton;
					}
				}, {
					key: 'EventKey',
					get: function get() {
						return EventKey;
					}
				}]);

				return TabsView;
			})(View);
		}
	};
});

$__System.registerDynamic("2d", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  "format cjs";
  (function(root, factory) {
    if (typeof define === 'function' && define.amd) {
      define(factory);
    } else if (typeof exports === 'object') {
      module.exports = factory($__require, exports, module);
    } else {
      root.Tether = factory();
    }
  }(this, function($__require, exports, module) {
    'use strict';
    var _createClass = (function() {
      function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
          var descriptor = props[i];
          descriptor.enumerable = descriptor.enumerable || false;
          descriptor.configurable = true;
          if ('value' in descriptor)
            descriptor.writable = true;
          Object.defineProperty(target, descriptor.key, descriptor);
        }
      }
      return function(Constructor, protoProps, staticProps) {
        if (protoProps)
          defineProperties(Constructor.prototype, protoProps);
        if (staticProps)
          defineProperties(Constructor, staticProps);
        return Constructor;
      };
    })();
    function _classCallCheck(instance, Constructor) {
      if (!(instance instanceof Constructor)) {
        throw new TypeError('Cannot call a class as a function');
      }
    }
    var TetherBase = undefined;
    if (typeof TetherBase === 'undefined') {
      TetherBase = {modules: []};
    }
    function getScrollParent(el) {
      var _getComputedStyle = getComputedStyle(el);
      var position = _getComputedStyle.position;
      if (position === 'fixed') {
        return el;
      }
      var parent = el;
      while (parent = parent.parentNode) {
        var style = undefined;
        try {
          style = getComputedStyle(parent);
        } catch (err) {}
        if (typeof style === 'undefined' || style === null) {
          return parent;
        }
        var _style = style;
        var overflow = _style.overflow;
        var overflowX = _style.overflowX;
        var overflowY = _style.overflowY;
        if (/(auto|scroll)/.test(overflow + overflowY + overflowX)) {
          if (position !== 'absolute' || ['relative', 'absolute', 'fixed'].indexOf(style.position) >= 0) {
            return parent;
          }
        }
      }
      return document.body;
    }
    var uniqueId = (function() {
      var id = 0;
      return function() {
        return ++id;
      };
    })();
    var zeroPosCache = {};
    var getOrigin = function getOrigin(doc) {
      var node = doc._tetherZeroElement;
      if (typeof node === 'undefined') {
        node = doc.createElement('div');
        node.setAttribute('data-tether-id', uniqueId());
        extend(node.style, {
          top: 0,
          left: 0,
          position: 'absolute'
        });
        doc.body.appendChild(node);
        doc._tetherZeroElement = node;
      }
      var id = node.getAttribute('data-tether-id');
      if (typeof zeroPosCache[id] === 'undefined') {
        zeroPosCache[id] = {};
        var rect = node.getBoundingClientRect();
        for (var k in rect) {
          zeroPosCache[id][k] = rect[k];
        }
        defer(function() {
          delete zeroPosCache[id];
        });
      }
      return zeroPosCache[id];
    };
    function getBounds(el) {
      var doc = undefined;
      if (el === document) {
        doc = document;
        el = document.documentElement;
      } else {
        doc = el.ownerDocument;
      }
      var docEl = doc.documentElement;
      var box = {};
      var rect = el.getBoundingClientRect();
      for (var k in rect) {
        box[k] = rect[k];
      }
      var origin = getOrigin(doc);
      box.top -= origin.top;
      box.left -= origin.left;
      if (typeof box.width === 'undefined') {
        box.width = document.body.scrollWidth - box.left - box.right;
      }
      if (typeof box.height === 'undefined') {
        box.height = document.body.scrollHeight - box.top - box.bottom;
      }
      box.top = box.top - docEl.clientTop;
      box.left = box.left - docEl.clientLeft;
      box.right = doc.body.clientWidth - box.width - box.left;
      box.bottom = doc.body.clientHeight - box.height - box.top;
      return box;
    }
    function getOffsetParent(el) {
      return el.offsetParent || document.documentElement;
    }
    function getScrollBarSize() {
      var inner = document.createElement('div');
      inner.style.width = '100%';
      inner.style.height = '200px';
      var outer = document.createElement('div');
      extend(outer.style, {
        position: 'absolute',
        top: 0,
        left: 0,
        pointerEvents: 'none',
        visibility: 'hidden',
        width: '200px',
        height: '150px',
        overflow: 'hidden'
      });
      outer.appendChild(inner);
      document.body.appendChild(outer);
      var widthContained = inner.offsetWidth;
      outer.style.overflow = 'scroll';
      var widthScroll = inner.offsetWidth;
      if (widthContained === widthScroll) {
        widthScroll = outer.clientWidth;
      }
      document.body.removeChild(outer);
      var width = widthContained - widthScroll;
      return {
        width: width,
        height: width
      };
    }
    function extend() {
      var out = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
      var args = [];
      Array.prototype.push.apply(args, arguments);
      args.slice(1).forEach(function(obj) {
        if (obj) {
          for (var key in obj) {
            if (({}).hasOwnProperty.call(obj, key)) {
              out[key] = obj[key];
            }
          }
        }
      });
      return out;
    }
    function removeClass(el, name) {
      if (typeof el.classList !== 'undefined') {
        name.split(' ').forEach(function(cls) {
          if (cls.trim()) {
            el.classList.remove(cls);
          }
        });
      } else {
        var regex = new RegExp('(^| )' + name.split(' ').join('|') + '( |$)', 'gi');
        var className = getClassName(el).replace(regex, ' ');
        setClassName(el, className);
      }
    }
    function addClass(el, name) {
      if (typeof el.classList !== 'undefined') {
        name.split(' ').forEach(function(cls) {
          if (cls.trim()) {
            el.classList.add(cls);
          }
        });
      } else {
        removeClass(el, name);
        var cls = getClassName(el) + (' ' + name);
        setClassName(el, cls);
      }
    }
    function hasClass(el, name) {
      if (typeof el.classList !== 'undefined') {
        return el.classList.contains(name);
      }
      var className = getClassName(el);
      return new RegExp('(^| )' + name + '( |$)', 'gi').test(className);
    }
    function getClassName(el) {
      if (el.className instanceof SVGAnimatedString) {
        return el.className.baseVal;
      }
      return el.className;
    }
    function setClassName(el, className) {
      el.setAttribute('class', className);
    }
    function updateClasses(el, add, all) {
      all.forEach(function(cls) {
        if (add.indexOf(cls) === -1 && hasClass(el, cls)) {
          removeClass(el, cls);
        }
      });
      add.forEach(function(cls) {
        if (!hasClass(el, cls)) {
          addClass(el, cls);
        }
      });
    }
    var deferred = [];
    var defer = function defer(fn) {
      deferred.push(fn);
    };
    var flush = function flush() {
      var fn = undefined;
      while (fn = deferred.pop()) {
        fn();
      }
    };
    var Evented = (function() {
      function Evented() {
        _classCallCheck(this, Evented);
      }
      _createClass(Evented, [{
        key: 'on',
        value: function on(event, handler, ctx) {
          var once = arguments.length <= 3 || arguments[3] === undefined ? false : arguments[3];
          if (typeof this.bindings === 'undefined') {
            this.bindings = {};
          }
          if (typeof this.bindings[event] === 'undefined') {
            this.bindings[event] = [];
          }
          this.bindings[event].push({
            handler: handler,
            ctx: ctx,
            once: once
          });
        }
      }, {
        key: 'once',
        value: function once(event, handler, ctx) {
          this.on(event, handler, ctx, true);
        }
      }, {
        key: 'off',
        value: function off(event, handler) {
          if (typeof this.bindings !== 'undefined' && typeof this.bindings[event] !== 'undefined') {
            return;
          }
          if (typeof handler === 'undefined') {
            delete this.bindings[event];
          } else {
            var i = 0;
            while (i < this.bindings[event].length) {
              if (this.bindings[event][i].handler === handler) {
                this.bindings[event].splice(i, 1);
              } else {
                ++i;
              }
            }
          }
        }
      }, {
        key: 'trigger',
        value: function trigger(event) {
          if (typeof this.bindings !== 'undefined' && this.bindings[event]) {
            var i = 0;
            for (var _len = arguments.length,
                args = Array(_len > 1 ? _len - 1 : 0),
                _key = 1; _key < _len; _key++) {
              args[_key - 1] = arguments[_key];
            }
            while (i < this.bindings[event].length) {
              var _bindings$event$i = this.bindings[event][i];
              var handler = _bindings$event$i.handler;
              var ctx = _bindings$event$i.ctx;
              var once = _bindings$event$i.once;
              var context = ctx;
              if (typeof context === 'undefined') {
                context = this;
              }
              handler.apply(context, args);
              if (once) {
                this.bindings[event].splice(i, 1);
              } else {
                ++i;
              }
            }
          }
        }
      }]);
      return Evented;
    })();
    TetherBase.Utils = {
      getScrollParent: getScrollParent,
      getBounds: getBounds,
      getOffsetParent: getOffsetParent,
      extend: extend,
      addClass: addClass,
      removeClass: removeClass,
      hasClass: hasClass,
      updateClasses: updateClasses,
      defer: defer,
      flush: flush,
      uniqueId: uniqueId,
      Evented: Evented,
      getScrollBarSize: getScrollBarSize
    };
    'use strict';
    var _slicedToArray = (function() {
      function sliceIterator(arr, i) {
        var _arr = [];
        var _n = true;
        var _d = false;
        var _e = undefined;
        try {
          for (var _i = arr[Symbol.iterator](),
              _s; !(_n = (_s = _i.next()).done); _n = true) {
            _arr.push(_s.value);
            if (i && _arr.length === i)
              break;
          }
        } catch (err) {
          _d = true;
          _e = err;
        } finally {
          try {
            if (!_n && _i['return'])
              _i['return']();
          } finally {
            if (_d)
              throw _e;
          }
        }
        return _arr;
      }
      return function(arr, i) {
        if (Array.isArray(arr)) {
          return arr;
        } else if (Symbol.iterator in Object(arr)) {
          return sliceIterator(arr, i);
        } else {
          throw new TypeError('Invalid attempt to destructure non-iterable instance');
        }
      };
    })();
    var _createClass = (function() {
      function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
          var descriptor = props[i];
          descriptor.enumerable = descriptor.enumerable || false;
          descriptor.configurable = true;
          if ('value' in descriptor)
            descriptor.writable = true;
          Object.defineProperty(target, descriptor.key, descriptor);
        }
      }
      return function(Constructor, protoProps, staticProps) {
        if (protoProps)
          defineProperties(Constructor.prototype, protoProps);
        if (staticProps)
          defineProperties(Constructor, staticProps);
        return Constructor;
      };
    })();
    function _classCallCheck(instance, Constructor) {
      if (!(instance instanceof Constructor)) {
        throw new TypeError('Cannot call a class as a function');
      }
    }
    if (typeof TetherBase === 'undefined') {
      throw new Error('You must include the utils.js file before tether.js');
    }
    var _TetherBase$Utils = TetherBase.Utils;
    var getScrollParent = _TetherBase$Utils.getScrollParent;
    var getBounds = _TetherBase$Utils.getBounds;
    var getOffsetParent = _TetherBase$Utils.getOffsetParent;
    var extend = _TetherBase$Utils.extend;
    var addClass = _TetherBase$Utils.addClass;
    var removeClass = _TetherBase$Utils.removeClass;
    var updateClasses = _TetherBase$Utils.updateClasses;
    var defer = _TetherBase$Utils.defer;
    var flush = _TetherBase$Utils.flush;
    var getScrollBarSize = _TetherBase$Utils.getScrollBarSize;
    function within(a, b) {
      var diff = arguments.length <= 2 || arguments[2] === undefined ? 1 : arguments[2];
      return a + diff >= b && b >= a - diff;
    }
    var transformKey = (function() {
      if (typeof document === 'undefined') {
        return '';
      }
      var el = document.createElement('div');
      var transforms = ['transform', 'webkitTransform', 'OTransform', 'MozTransform', 'msTransform'];
      for (var i = 0; i < transforms.length; ++i) {
        var key = transforms[i];
        if (el.style[key] !== undefined) {
          return key;
        }
      }
    })();
    var tethers = [];
    var position = function position() {
      tethers.forEach(function(tether) {
        tether.position(false);
      });
      flush();
    };
    function now() {
      if (typeof performance !== 'undefined' && typeof performance.now !== 'undefined') {
        return performance.now();
      }
      return +new Date();
    }
    (function() {
      var lastCall = null;
      var lastDuration = null;
      var pendingTimeout = null;
      var tick = function tick() {
        if (typeof lastDuration !== 'undefined' && lastDuration > 16) {
          lastDuration = Math.min(lastDuration - 16, 250);
          pendingTimeout = setTimeout(tick, 250);
          return;
        }
        if (typeof lastCall !== 'undefined' && now() - lastCall < 10) {
          return;
        }
        if (typeof pendingTimeout !== 'undefined') {
          clearTimeout(pendingTimeout);
          pendingTimeout = null;
        }
        lastCall = now();
        position();
        lastDuration = now() - lastCall;
      };
      if (typeof window !== 'undefined') {
        ['resize', 'scroll', 'touchmove'].forEach(function(event) {
          window.addEventListener(event, tick);
        });
      }
    })();
    var MIRROR_LR = {
      center: 'center',
      left: 'right',
      right: 'left'
    };
    var MIRROR_TB = {
      middle: 'middle',
      top: 'bottom',
      bottom: 'top'
    };
    var OFFSET_MAP = {
      top: 0,
      left: 0,
      middle: '50%',
      center: '50%',
      bottom: '100%',
      right: '100%'
    };
    var autoToFixedAttachment = function autoToFixedAttachment(attachment, relativeToAttachment) {
      var left = attachment.left;
      var top = attachment.top;
      if (left === 'auto') {
        left = MIRROR_LR[relativeToAttachment.left];
      }
      if (top === 'auto') {
        top = MIRROR_TB[relativeToAttachment.top];
      }
      return {
        left: left,
        top: top
      };
    };
    var attachmentToOffset = function attachmentToOffset(attachment) {
      var left = attachment.left;
      var top = attachment.top;
      if (typeof OFFSET_MAP[attachment.left] !== 'undefined') {
        left = OFFSET_MAP[attachment.left];
      }
      if (typeof OFFSET_MAP[attachment.top] !== 'undefined') {
        top = OFFSET_MAP[attachment.top];
      }
      return {
        left: left,
        top: top
      };
    };
    function addOffset() {
      var out = {
        top: 0,
        left: 0
      };
      for (var _len = arguments.length,
          offsets = Array(_len),
          _key = 0; _key < _len; _key++) {
        offsets[_key] = arguments[_key];
      }
      offsets.forEach(function(_ref) {
        var top = _ref.top;
        var left = _ref.left;
        if (typeof top === 'string') {
          top = parseFloat(top, 10);
        }
        if (typeof left === 'string') {
          left = parseFloat(left, 10);
        }
        out.top += top;
        out.left += left;
      });
      return out;
    }
    function offsetToPx(offset, size) {
      if (typeof offset.left === 'string' && offset.left.indexOf('%') !== -1) {
        offset.left = parseFloat(offset.left, 10) / 100 * size.width;
      }
      if (typeof offset.top === 'string' && offset.top.indexOf('%') !== -1) {
        offset.top = parseFloat(offset.top, 10) / 100 * size.height;
      }
      return offset;
    }
    var parseOffset = function parseOffset(value) {
      var _value$split = value.split(' ');
      var _value$split2 = _slicedToArray(_value$split, 2);
      var top = _value$split2[0];
      var left = _value$split2[1];
      return {
        top: top,
        left: left
      };
    };
    var parseAttachment = parseOffset;
    var TetherClass = (function() {
      function TetherClass(options) {
        var _this = this;
        _classCallCheck(this, TetherClass);
        this.position = this.position.bind(this);
        tethers.push(this);
        this.history = [];
        this.setOptions(options, false);
        TetherBase.modules.forEach(function(module) {
          if (typeof module.initialize !== 'undefined') {
            module.initialize.call(_this);
          }
        });
        this.position();
      }
      _createClass(TetherClass, [{
        key: 'getClass',
        value: function getClass() {
          var key = arguments.length <= 0 || arguments[0] === undefined ? '' : arguments[0];
          var classes = this.options.classes;
          if (typeof classes !== 'undefined' && classes[key]) {
            return this.options.classes[key];
          } else if (this.options.classPrefix) {
            return this.options.classPrefix + '-' + key;
          } else {
            return key;
          }
        }
      }, {
        key: 'setOptions',
        value: function setOptions(options) {
          var _this2 = this;
          var pos = arguments.length <= 1 || arguments[1] === undefined ? true : arguments[1];
          var defaults = {
            offset: '0 0',
            targetOffset: '0 0',
            targetAttachment: 'auto auto',
            classPrefix: 'tether'
          };
          this.options = extend(defaults, options);
          var _options = this.options;
          var element = _options.element;
          var target = _options.target;
          var targetModifier = _options.targetModifier;
          this.element = element;
          this.target = target;
          this.targetModifier = targetModifier;
          if (this.target === 'viewport') {
            this.target = document.body;
            this.targetModifier = 'visible';
          } else if (this.target === 'scroll-handle') {
            this.target = document.body;
            this.targetModifier = 'scroll-handle';
          }
          ['element', 'target'].forEach(function(key) {
            if (typeof _this2[key] === 'undefined') {
              throw new Error('Tether Error: Both element and target must be defined');
            }
            if (typeof _this2[key].jquery !== 'undefined') {
              _this2[key] = _this2[key][0];
            } else if (typeof _this2[key] === 'string') {
              _this2[key] = document.querySelector(_this2[key]);
            }
          });
          addClass(this.element, this.getClass('element'));
          if (!(this.options.addTargetClasses === false)) {
            addClass(this.target, this.getClass('target'));
          }
          if (!this.options.attachment) {
            throw new Error('Tether Error: You must provide an attachment');
          }
          this.targetAttachment = parseAttachment(this.options.targetAttachment);
          this.attachment = parseAttachment(this.options.attachment);
          this.offset = parseOffset(this.options.offset);
          this.targetOffset = parseOffset(this.options.targetOffset);
          if (typeof this.scrollParent !== 'undefined') {
            this.disable();
          }
          if (this.targetModifier === 'scroll-handle') {
            this.scrollParent = this.target;
          } else {
            this.scrollParent = getScrollParent(this.target);
          }
          if (!(this.options.enabled === false)) {
            this.enable(pos);
          }
        }
      }, {
        key: 'getTargetBounds',
        value: function getTargetBounds() {
          if (typeof this.targetModifier !== 'undefined') {
            if (this.targetModifier === 'visible') {
              if (this.target === document.body) {
                return {
                  top: pageYOffset,
                  left: pageXOffset,
                  height: innerHeight,
                  width: innerWidth
                };
              } else {
                var bounds = getBounds(this.target);
                var out = {
                  height: bounds.height,
                  width: bounds.width,
                  top: bounds.top,
                  left: bounds.left
                };
                out.height = Math.min(out.height, bounds.height - (pageYOffset - bounds.top));
                out.height = Math.min(out.height, bounds.height - (bounds.top + bounds.height - (pageYOffset + innerHeight)));
                out.height = Math.min(innerHeight, out.height);
                out.height -= 2;
                out.width = Math.min(out.width, bounds.width - (pageXOffset - bounds.left));
                out.width = Math.min(out.width, bounds.width - (bounds.left + bounds.width - (pageXOffset + innerWidth)));
                out.width = Math.min(innerWidth, out.width);
                out.width -= 2;
                if (out.top < pageYOffset) {
                  out.top = pageYOffset;
                }
                if (out.left < pageXOffset) {
                  out.left = pageXOffset;
                }
                return out;
              }
            } else if (this.targetModifier === 'scroll-handle') {
              var bounds = undefined;
              var target = this.target;
              if (target === document.body) {
                target = document.documentElement;
                bounds = {
                  left: pageXOffset,
                  top: pageYOffset,
                  height: innerHeight,
                  width: innerWidth
                };
              } else {
                bounds = getBounds(target);
              }
              var style = getComputedStyle(target);
              var hasBottomScroll = target.scrollWidth > target.clientWidth || [style.overflow, style.overflowX].indexOf('scroll') >= 0 || this.target !== document.body;
              var scrollBottom = 0;
              if (hasBottomScroll) {
                scrollBottom = 15;
              }
              var height = bounds.height - parseFloat(style.borderTopWidth) - parseFloat(style.borderBottomWidth) - scrollBottom;
              var out = {
                width: 15,
                height: height * 0.975 * (height / target.scrollHeight),
                left: bounds.left + bounds.width - parseFloat(style.borderLeftWidth) - 15
              };
              var fitAdj = 0;
              if (height < 408 && this.target === document.body) {
                fitAdj = -0.00011 * Math.pow(height, 2) - 0.00727 * height + 22.58;
              }
              if (this.target !== document.body) {
                out.height = Math.max(out.height, 24);
              }
              var scrollPercentage = this.target.scrollTop / (target.scrollHeight - height);
              out.top = scrollPercentage * (height - out.height - fitAdj) + bounds.top + parseFloat(style.borderTopWidth);
              if (this.target === document.body) {
                out.height = Math.max(out.height, 24);
              }
              return out;
            }
          } else {
            return getBounds(this.target);
          }
        }
      }, {
        key: 'clearCache',
        value: function clearCache() {
          this._cache = {};
        }
      }, {
        key: 'cache',
        value: function cache(k, getter) {
          if (typeof this._cache === 'undefined') {
            this._cache = {};
          }
          if (typeof this._cache[k] === 'undefined') {
            this._cache[k] = getter.call(this);
          }
          return this._cache[k];
        }
      }, {
        key: 'enable',
        value: function enable() {
          var pos = arguments.length <= 0 || arguments[0] === undefined ? true : arguments[0];
          if (!(this.options.addTargetClasses === false)) {
            addClass(this.target, this.getClass('enabled'));
          }
          addClass(this.element, this.getClass('enabled'));
          this.enabled = true;
          if (this.scrollParent !== document) {
            this.scrollParent.addEventListener('scroll', this.position);
          }
          if (pos) {
            this.position();
          }
        }
      }, {
        key: 'disable',
        value: function disable() {
          removeClass(this.target, this.getClass('enabled'));
          removeClass(this.element, this.getClass('enabled'));
          this.enabled = false;
          if (typeof this.scrollParent !== 'undefined') {
            this.scrollParent.removeEventListener('scroll', this.position);
          }
        }
      }, {
        key: 'destroy',
        value: function destroy() {
          var _this3 = this;
          this.disable();
          tethers.forEach(function(tether, i) {
            if (tether === _this3) {
              tethers.splice(i, 1);
              return;
            }
          });
        }
      }, {
        key: 'updateAttachClasses',
        value: function updateAttachClasses(elementAttach, targetAttach) {
          var _this4 = this;
          elementAttach = elementAttach || this.attachment;
          targetAttach = targetAttach || this.targetAttachment;
          var sides = ['left', 'top', 'bottom', 'right', 'middle', 'center'];
          if (typeof this._addAttachClasses !== 'undefined' && this._addAttachClasses.length) {
            this._addAttachClasses.splice(0, this._addAttachClasses.length);
          }
          if (typeof this._addAttachClasses === 'undefined') {
            this._addAttachClasses = [];
          }
          var add = this._addAttachClasses;
          if (elementAttach.top) {
            add.push(this.getClass('element-attached') + '-' + elementAttach.top);
          }
          if (elementAttach.left) {
            add.push(this.getClass('element-attached') + '-' + elementAttach.left);
          }
          if (targetAttach.top) {
            add.push(this.getClass('target-attached') + '-' + targetAttach.top);
          }
          if (targetAttach.left) {
            add.push(this.getClass('target-attached') + '-' + targetAttach.left);
          }
          var all = [];
          sides.forEach(function(side) {
            all.push(_this4.getClass('element-attached') + '-' + side);
            all.push(_this4.getClass('target-attached') + '-' + side);
          });
          defer(function() {
            if (!(typeof _this4._addAttachClasses !== 'undefined')) {
              return;
            }
            updateClasses(_this4.element, _this4._addAttachClasses, all);
            if (!(_this4.options.addTargetClasses === false)) {
              updateClasses(_this4.target, _this4._addAttachClasses, all);
            }
            delete _this4._addAttachClasses;
          });
        }
      }, {
        key: 'position',
        value: function position() {
          var _this5 = this;
          var flushChanges = arguments.length <= 0 || arguments[0] === undefined ? true : arguments[0];
          if (!this.enabled) {
            return;
          }
          this.clearCache();
          var targetAttachment = autoToFixedAttachment(this.targetAttachment, this.attachment);
          this.updateAttachClasses(this.attachment, targetAttachment);
          var elementPos = this.cache('element-bounds', function() {
            return getBounds(_this5.element);
          });
          var width = elementPos.width;
          var height = elementPos.height;
          if (width === 0 && height === 0 && typeof this.lastSize !== 'undefined') {
            var _lastSize = this.lastSize;
            width = _lastSize.width;
            height = _lastSize.height;
          } else {
            this.lastSize = {
              width: width,
              height: height
            };
          }
          var targetPos = this.cache('target-bounds', function() {
            return _this5.getTargetBounds();
          });
          var targetSize = targetPos;
          var offset = offsetToPx(attachmentToOffset(this.attachment), {
            width: width,
            height: height
          });
          var targetOffset = offsetToPx(attachmentToOffset(targetAttachment), targetSize);
          var manualOffset = offsetToPx(this.offset, {
            width: width,
            height: height
          });
          var manualTargetOffset = offsetToPx(this.targetOffset, targetSize);
          offset = addOffset(offset, manualOffset);
          targetOffset = addOffset(targetOffset, manualTargetOffset);
          var left = targetPos.left + targetOffset.left - offset.left;
          var top = targetPos.top + targetOffset.top - offset.top;
          for (var i = 0; i < TetherBase.modules.length; ++i) {
            var _module2 = TetherBase.modules[i];
            var ret = _module2.position.call(this, {
              left: left,
              top: top,
              targetAttachment: targetAttachment,
              targetPos: targetPos,
              elementPos: elementPos,
              offset: offset,
              targetOffset: targetOffset,
              manualOffset: manualOffset,
              manualTargetOffset: manualTargetOffset,
              scrollbarSize: scrollbarSize,
              attachment: this.attachment
            });
            if (ret === false) {
              return false;
            } else if (typeof ret === 'undefined' || typeof ret !== 'object') {
              continue;
            } else {
              top = ret.top;
              left = ret.left;
            }
          }
          var next = {
            page: {
              top: top,
              left: left
            },
            viewport: {
              top: top - pageYOffset,
              bottom: pageYOffset - top - height + innerHeight,
              left: left - pageXOffset,
              right: pageXOffset - left - width + innerWidth
            }
          };
          var scrollbarSize = undefined;
          if (document.body.scrollWidth > window.innerWidth) {
            scrollbarSize = this.cache('scrollbar-size', getScrollBarSize);
            next.viewport.bottom -= scrollbarSize.height;
          }
          if (document.body.scrollHeight > window.innerHeight) {
            scrollbarSize = this.cache('scrollbar-size', getScrollBarSize);
            next.viewport.right -= scrollbarSize.width;
          }
          if (['', 'static'].indexOf(document.body.style.position) === -1 || ['', 'static'].indexOf(document.body.parentElement.style.position) === -1) {
            next.page.bottom = document.body.scrollHeight - top - height;
            next.page.right = document.body.scrollWidth - left - width;
          }
          if (typeof this.options.optimizations !== 'undefined' && this.options.optimizations.moveElement !== false && !(typeof this.targetModifier !== 'undefined')) {
            (function() {
              var offsetParent = _this5.cache('target-offsetparent', function() {
                return getOffsetParent(_this5.target);
              });
              var offsetPosition = _this5.cache('target-offsetparent-bounds', function() {
                return getBounds(offsetParent);
              });
              var offsetParentStyle = getComputedStyle(offsetParent);
              var offsetParentSize = offsetPosition;
              var offsetBorder = {};
              ['Top', 'Left', 'Bottom', 'Right'].forEach(function(side) {
                offsetBorder[side.toLowerCase()] = parseFloat(offsetParentStyle['border' + side + 'Width']);
              });
              offsetPosition.right = document.body.scrollWidth - offsetPosition.left - offsetParentSize.width + offsetBorder.right;
              offsetPosition.bottom = document.body.scrollHeight - offsetPosition.top - offsetParentSize.height + offsetBorder.bottom;
              if (next.page.top >= offsetPosition.top + offsetBorder.top && next.page.bottom >= offsetPosition.bottom) {
                if (next.page.left >= offsetPosition.left + offsetBorder.left && next.page.right >= offsetPosition.right) {
                  var scrollTop = offsetParent.scrollTop;
                  var scrollLeft = offsetParent.scrollLeft;
                  next.offset = {
                    top: next.page.top - offsetPosition.top + scrollTop - offsetBorder.top,
                    left: next.page.left - offsetPosition.left + scrollLeft - offsetBorder.left
                  };
                }
              }
            })();
          }
          this.move(next);
          this.history.unshift(next);
          if (this.history.length > 3) {
            this.history.pop();
          }
          if (flushChanges) {
            flush();
          }
          return true;
        }
      }, {
        key: 'move',
        value: function move(pos) {
          var _this6 = this;
          if (!(typeof this.element.parentNode !== 'undefined')) {
            return;
          }
          var same = {};
          for (var type in pos) {
            same[type] = {};
            for (var key in pos[type]) {
              var found = false;
              for (var i = 0; i < this.history.length; ++i) {
                var point = this.history[i];
                if (typeof point[type] !== 'undefined' && !within(point[type][key], pos[type][key])) {
                  found = true;
                  break;
                }
              }
              if (!found) {
                same[type][key] = true;
              }
            }
          }
          var css = {
            top: '',
            left: '',
            right: '',
            bottom: ''
          };
          var transcribe = function transcribe(_same, _pos) {
            var hasOptimizations = typeof _this6.options.optimizations !== 'undefined';
            var gpu = hasOptimizations ? _this6.options.optimizations.gpu : null;
            if (gpu !== false) {
              var yPos = undefined,
                  xPos = undefined;
              if (_same.top) {
                css.top = 0;
                yPos = _pos.top;
              } else {
                css.bottom = 0;
                yPos = -_pos.bottom;
              }
              if (_same.left) {
                css.left = 0;
                xPos = _pos.left;
              } else {
                css.right = 0;
                xPos = -_pos.right;
              }
              css[transformKey] = 'translateX(' + Math.round(xPos) + 'px) translateY(' + Math.round(yPos) + 'px)';
              if (transformKey !== 'msTransform') {
                css[transformKey] += " translateZ(0)";
              }
            } else {
              if (_same.top) {
                css.top = _pos.top + 'px';
              } else {
                css.bottom = _pos.bottom + 'px';
              }
              if (_same.left) {
                css.left = _pos.left + 'px';
              } else {
                css.right = _pos.right + 'px';
              }
            }
          };
          var moved = false;
          if ((same.page.top || same.page.bottom) && (same.page.left || same.page.right)) {
            css.position = 'absolute';
            transcribe(same.page, pos.page);
          } else if ((same.viewport.top || same.viewport.bottom) && (same.viewport.left || same.viewport.right)) {
            css.position = 'fixed';
            transcribe(same.viewport, pos.viewport);
          } else if (typeof same.offset !== 'undefined' && same.offset.top && same.offset.left) {
            (function() {
              css.position = 'absolute';
              var offsetParent = _this6.cache('target-offsetparent', function() {
                return getOffsetParent(_this6.target);
              });
              if (getOffsetParent(_this6.element) !== offsetParent) {
                defer(function() {
                  _this6.element.parentNode.removeChild(_this6.element);
                  offsetParent.appendChild(_this6.element);
                });
              }
              transcribe(same.offset, pos.offset);
              moved = true;
            })();
          } else {
            css.position = 'absolute';
            transcribe({
              top: true,
              left: true
            }, pos.page);
          }
          if (!moved) {
            var offsetParentIsBody = true;
            var currentNode = this.element.parentNode;
            while (currentNode && currentNode.tagName !== 'BODY') {
              if (getComputedStyle(currentNode).position !== 'static') {
                offsetParentIsBody = false;
                break;
              }
              currentNode = currentNode.parentNode;
            }
            if (!offsetParentIsBody) {
              this.element.parentNode.removeChild(this.element);
              document.body.appendChild(this.element);
            }
          }
          var writeCSS = {};
          var write = false;
          for (var key in css) {
            var val = css[key];
            var elVal = this.element.style[key];
            if (elVal !== '' && val !== '' && ['top', 'left', 'bottom', 'right'].indexOf(key) >= 0) {
              elVal = parseFloat(elVal);
              val = parseFloat(val);
            }
            if (elVal !== val) {
              write = true;
              writeCSS[key] = val;
            }
          }
          if (write) {
            defer(function() {
              extend(_this6.element.style, writeCSS);
            });
          }
        }
      }]);
      return TetherClass;
    })();
    TetherClass.modules = [];
    TetherBase.position = position;
    var Tether = extend(TetherClass, TetherBase);
    'use strict';
    var _slicedToArray = (function() {
      function sliceIterator(arr, i) {
        var _arr = [];
        var _n = true;
        var _d = false;
        var _e = undefined;
        try {
          for (var _i = arr[Symbol.iterator](),
              _s; !(_n = (_s = _i.next()).done); _n = true) {
            _arr.push(_s.value);
            if (i && _arr.length === i)
              break;
          }
        } catch (err) {
          _d = true;
          _e = err;
        } finally {
          try {
            if (!_n && _i['return'])
              _i['return']();
          } finally {
            if (_d)
              throw _e;
          }
        }
        return _arr;
      }
      return function(arr, i) {
        if (Array.isArray(arr)) {
          return arr;
        } else if (Symbol.iterator in Object(arr)) {
          return sliceIterator(arr, i);
        } else {
          throw new TypeError('Invalid attempt to destructure non-iterable instance');
        }
      };
    })();
    var _TetherBase$Utils = TetherBase.Utils;
    var getBounds = _TetherBase$Utils.getBounds;
    var extend = _TetherBase$Utils.extend;
    var updateClasses = _TetherBase$Utils.updateClasses;
    var defer = _TetherBase$Utils.defer;
    var BOUNDS_FORMAT = ['left', 'top', 'right', 'bottom'];
    function getBoundingRect(tether, to) {
      if (to === 'scrollParent') {
        to = tether.scrollParent;
      } else if (to === 'window') {
        to = [pageXOffset, pageYOffset, innerWidth + pageXOffset, innerHeight + pageYOffset];
      }
      if (to === document) {
        to = to.documentElement;
      }
      if (typeof to.nodeType !== 'undefined') {
        (function() {
          var size = getBounds(to);
          var pos = size;
          var style = getComputedStyle(to);
          to = [pos.left, pos.top, size.width + pos.left, size.height + pos.top];
          BOUNDS_FORMAT.forEach(function(side, i) {
            side = side[0].toUpperCase() + side.substr(1);
            if (side === 'Top' || side === 'Left') {
              to[i] += parseFloat(style['border' + side + 'Width']);
            } else {
              to[i] -= parseFloat(style['border' + side + 'Width']);
            }
          });
        })();
      }
      return to;
    }
    TetherBase.modules.push({position: function position(_ref) {
        var _this = this;
        var top = _ref.top;
        var left = _ref.left;
        var targetAttachment = _ref.targetAttachment;
        if (!this.options.constraints) {
          return true;
        }
        var _cache = this.cache('element-bounds', function() {
          return getBounds(_this.element);
        });
        var height = _cache.height;
        var width = _cache.width;
        if (width === 0 && height === 0 && typeof this.lastSize !== 'undefined') {
          var _lastSize = this.lastSize;
          width = _lastSize.width;
          height = _lastSize.height;
        }
        var targetSize = this.cache('target-bounds', function() {
          return _this.getTargetBounds();
        });
        var targetHeight = targetSize.height;
        var targetWidth = targetSize.width;
        var allClasses = [this.getClass('pinned'), this.getClass('out-of-bounds')];
        this.options.constraints.forEach(function(constraint) {
          var outOfBoundsClass = constraint.outOfBoundsClass;
          var pinnedClass = constraint.pinnedClass;
          if (outOfBoundsClass) {
            allClasses.push(outOfBoundsClass);
          }
          if (pinnedClass) {
            allClasses.push(pinnedClass);
          }
        });
        allClasses.forEach(function(cls) {
          ['left', 'top', 'right', 'bottom'].forEach(function(side) {
            allClasses.push(cls + '-' + side);
          });
        });
        var addClasses = [];
        var tAttachment = extend({}, targetAttachment);
        var eAttachment = extend({}, this.attachment);
        this.options.constraints.forEach(function(constraint) {
          var to = constraint.to;
          var attachment = constraint.attachment;
          var pin = constraint.pin;
          if (typeof attachment === 'undefined') {
            attachment = '';
          }
          var changeAttachX = undefined,
              changeAttachY = undefined;
          if (attachment.indexOf(' ') >= 0) {
            var _attachment$split = attachment.split(' ');
            var _attachment$split2 = _slicedToArray(_attachment$split, 2);
            changeAttachY = _attachment$split2[0];
            changeAttachX = _attachment$split2[1];
          } else {
            changeAttachX = changeAttachY = attachment;
          }
          var bounds = getBoundingRect(_this, to);
          if (changeAttachY === 'target' || changeAttachY === 'both') {
            if (top < bounds[1] && tAttachment.top === 'top') {
              top += targetHeight;
              tAttachment.top = 'bottom';
            }
            if (top + height > bounds[3] && tAttachment.top === 'bottom') {
              top -= targetHeight;
              tAttachment.top = 'top';
            }
          }
          if (changeAttachY === 'together') {
            if (top < bounds[1] && tAttachment.top === 'top') {
              if (eAttachment.top === 'bottom') {
                top += targetHeight;
                tAttachment.top = 'bottom';
                top += height;
                eAttachment.top = 'top';
              } else if (eAttachment.top === 'top') {
                top += targetHeight;
                tAttachment.top = 'bottom';
                top -= height;
                eAttachment.top = 'bottom';
              }
            }
            if (top + height > bounds[3] && tAttachment.top === 'bottom') {
              if (eAttachment.top === 'top') {
                top -= targetHeight;
                tAttachment.top = 'top';
                top -= height;
                eAttachment.top = 'bottom';
              } else if (eAttachment.top === 'bottom') {
                top -= targetHeight;
                tAttachment.top = 'top';
                top += height;
                eAttachment.top = 'top';
              }
            }
            if (tAttachment.top === 'middle') {
              if (top + height > bounds[3] && eAttachment.top === 'top') {
                top -= height;
                eAttachment.top = 'bottom';
              } else if (top < bounds[1] && eAttachment.top === 'bottom') {
                top += height;
                eAttachment.top = 'top';
              }
            }
          }
          if (changeAttachX === 'target' || changeAttachX === 'both') {
            if (left < bounds[0] && tAttachment.left === 'left') {
              left += targetWidth;
              tAttachment.left = 'right';
            }
            if (left + width > bounds[2] && tAttachment.left === 'right') {
              left -= targetWidth;
              tAttachment.left = 'left';
            }
          }
          if (changeAttachX === 'together') {
            if (left < bounds[0] && tAttachment.left === 'left') {
              if (eAttachment.left === 'right') {
                left += targetWidth;
                tAttachment.left = 'right';
                left += width;
                eAttachment.left = 'left';
              } else if (eAttachment.left === 'left') {
                left += targetWidth;
                tAttachment.left = 'right';
                left -= width;
                eAttachment.left = 'right';
              }
            } else if (left + width > bounds[2] && tAttachment.left === 'right') {
              if (eAttachment.left === 'left') {
                left -= targetWidth;
                tAttachment.left = 'left';
                left -= width;
                eAttachment.left = 'right';
              } else if (eAttachment.left === 'right') {
                left -= targetWidth;
                tAttachment.left = 'left';
                left += width;
                eAttachment.left = 'left';
              }
            } else if (tAttachment.left === 'center') {
              if (left + width > bounds[2] && eAttachment.left === 'left') {
                left -= width;
                eAttachment.left = 'right';
              } else if (left < bounds[0] && eAttachment.left === 'right') {
                left += width;
                eAttachment.left = 'left';
              }
            }
          }
          if (changeAttachY === 'element' || changeAttachY === 'both') {
            if (top < bounds[1] && eAttachment.top === 'bottom') {
              top += height;
              eAttachment.top = 'top';
            }
            if (top + height > bounds[3] && eAttachment.top === 'top') {
              top -= height;
              eAttachment.top = 'bottom';
            }
          }
          if (changeAttachX === 'element' || changeAttachX === 'both') {
            if (left < bounds[0] && eAttachment.left === 'right') {
              left += width;
              eAttachment.left = 'left';
            }
            if (left + width > bounds[2] && eAttachment.left === 'left') {
              left -= width;
              eAttachment.left = 'right';
            }
          }
          if (typeof pin === 'string') {
            pin = pin.split(',').map(function(p) {
              return p.trim();
            });
          } else if (pin === true) {
            pin = ['top', 'left', 'right', 'bottom'];
          }
          pin = pin || [];
          var pinned = [];
          var oob = [];
          if (top < bounds[1]) {
            if (pin.indexOf('top') >= 0) {
              top = bounds[1];
              pinned.push('top');
            } else {
              oob.push('top');
            }
          }
          if (top + height > bounds[3]) {
            if (pin.indexOf('bottom') >= 0) {
              top = bounds[3] - height;
              pinned.push('bottom');
            } else {
              oob.push('bottom');
            }
          }
          if (left < bounds[0]) {
            if (pin.indexOf('left') >= 0) {
              left = bounds[0];
              pinned.push('left');
            } else {
              oob.push('left');
            }
          }
          if (left + width > bounds[2]) {
            if (pin.indexOf('right') >= 0) {
              left = bounds[2] - width;
              pinned.push('right');
            } else {
              oob.push('right');
            }
          }
          if (pinned.length) {
            (function() {
              var pinnedClass = undefined;
              if (typeof _this.options.pinnedClass !== 'undefined') {
                pinnedClass = _this.options.pinnedClass;
              } else {
                pinnedClass = _this.getClass('pinned');
              }
              addClasses.push(pinnedClass);
              pinned.forEach(function(side) {
                addClasses.push(pinnedClass + '-' + side);
              });
            })();
          }
          if (oob.length) {
            (function() {
              var oobClass = undefined;
              if (typeof _this.options.outOfBoundsClass !== 'undefined') {
                oobClass = _this.options.outOfBoundsClass;
              } else {
                oobClass = _this.getClass('out-of-bounds');
              }
              addClasses.push(oobClass);
              oob.forEach(function(side) {
                addClasses.push(oobClass + '-' + side);
              });
            })();
          }
          if (pinned.indexOf('left') >= 0 || pinned.indexOf('right') >= 0) {
            eAttachment.left = tAttachment.left = false;
          }
          if (pinned.indexOf('top') >= 0 || pinned.indexOf('bottom') >= 0) {
            eAttachment.top = tAttachment.top = false;
          }
          if (tAttachment.top !== targetAttachment.top || tAttachment.left !== targetAttachment.left || eAttachment.top !== _this.attachment.top || eAttachment.left !== _this.attachment.left) {
            _this.updateAttachClasses(eAttachment, tAttachment);
          }
        });
        defer(function() {
          if (!(_this.options.addTargetClasses === false)) {
            updateClasses(_this.target, addClasses, allClasses);
          }
          updateClasses(_this.element, addClasses, allClasses);
        });
        return {
          top: top,
          left: left
        };
      }});
    'use strict';
    var _TetherBase$Utils = TetherBase.Utils;
    var getBounds = _TetherBase$Utils.getBounds;
    var updateClasses = _TetherBase$Utils.updateClasses;
    var defer = _TetherBase$Utils.defer;
    TetherBase.modules.push({position: function position(_ref) {
        var _this = this;
        var top = _ref.top;
        var left = _ref.left;
        var _cache = this.cache('element-bounds', function() {
          return getBounds(_this.element);
        });
        var height = _cache.height;
        var width = _cache.width;
        var targetPos = this.getTargetBounds();
        var bottom = top + height;
        var right = left + width;
        var abutted = [];
        if (top <= targetPos.bottom && bottom >= targetPos.top) {
          ['left', 'right'].forEach(function(side) {
            var targetPosSide = targetPos[side];
            if (targetPosSide === left || targetPosSide === right) {
              abutted.push(side);
            }
          });
        }
        if (left <= targetPos.right && right >= targetPos.left) {
          ['top', 'bottom'].forEach(function(side) {
            var targetPosSide = targetPos[side];
            if (targetPosSide === top || targetPosSide === bottom) {
              abutted.push(side);
            }
          });
        }
        var allClasses = [];
        var addClasses = [];
        var sides = ['left', 'top', 'right', 'bottom'];
        allClasses.push(this.getClass('abutted'));
        sides.forEach(function(side) {
          allClasses.push(_this.getClass('abutted') + '-' + side);
        });
        if (abutted.length) {
          addClasses.push(this.getClass('abutted'));
        }
        abutted.forEach(function(side) {
          addClasses.push(_this.getClass('abutted') + '-' + side);
        });
        defer(function() {
          if (!(_this.options.addTargetClasses === false)) {
            updateClasses(_this.target, addClasses, allClasses);
          }
          updateClasses(_this.element, addClasses, allClasses);
        });
        return true;
      }});
    'use strict';
    var _slicedToArray = (function() {
      function sliceIterator(arr, i) {
        var _arr = [];
        var _n = true;
        var _d = false;
        var _e = undefined;
        try {
          for (var _i = arr[Symbol.iterator](),
              _s; !(_n = (_s = _i.next()).done); _n = true) {
            _arr.push(_s.value);
            if (i && _arr.length === i)
              break;
          }
        } catch (err) {
          _d = true;
          _e = err;
        } finally {
          try {
            if (!_n && _i['return'])
              _i['return']();
          } finally {
            if (_d)
              throw _e;
          }
        }
        return _arr;
      }
      return function(arr, i) {
        if (Array.isArray(arr)) {
          return arr;
        } else if (Symbol.iterator in Object(arr)) {
          return sliceIterator(arr, i);
        } else {
          throw new TypeError('Invalid attempt to destructure non-iterable instance');
        }
      };
    })();
    TetherBase.modules.push({position: function position(_ref) {
        var top = _ref.top;
        var left = _ref.left;
        if (!this.options.shift) {
          return;
        }
        var shift = this.options.shift;
        if (typeof this.options.shift === 'function') {
          shift = this.options.shift.call(this, {
            top: top,
            left: left
          });
        }
        var shiftTop = undefined,
            shiftLeft = undefined;
        if (typeof shift === 'string') {
          shift = shift.split(' ');
          shift[1] = shift[1] || shift[0];
          var _shift = shift;
          var _shift2 = _slicedToArray(_shift, 2);
          shiftTop = _shift2[0];
          shiftLeft = _shift2[1];
          shiftTop = parseFloat(shiftTop, 10);
          shiftLeft = parseFloat(shiftLeft, 10);
        } else {
          shiftTop = shift.top;
          shiftLeft = shift.left;
        }
        top += shiftTop;
        left += shiftLeft;
        return {
          top: top,
          left: left
        };
      }});
    return Tether;
  }));
  return module.exports;
});

$__System.registerDynamic("2e", ["2d"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = $__require('2d');
  return module.exports;
});

$__System.register('2f', ['4', '5', '7', '10'], function (_export) {
	var _get, _inherits, _classCallCheck, Model, TooltipModel;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_4) {
			Model = _4['default'];
		}],
		execute: function () {
			//
			// Tooltip Model
			//
			// The model for a tooltip, each parameter may be controlled with
			// data attributes on the selected element, or by specifying a Configuration
			// manually when creating a tooltip.

			'use strict';

			TooltipModel = (function (_Model) {
				_inherits(TooltipModel, _Model);

				function TooltipModel() {
					_classCallCheck(this, TooltipModel);

					_get(Object.getPrototypeOf(TooltipModel.prototype), 'constructor', this).apply(this, arguments);
				}

				return TooltipModel;
			})(Model);

			_export('default', TooltipModel);

			TooltipModel.prototype.defaults = {
				force: false,
				manual: false,
				type: 'default',
				content: '',
				theme: 'default',
				classes: '',
				style: '',
				placement: 'top',
				size: 'rg',
				delay: 0.2,
				animation: 'anim-fade-tooltip',
				trigger: 'hover click', // accepts combinations of: 'hover click manual'
				constraints: [{
					to: 'scrollParent',
					attachment: 'together',
					pin: true
				}],
				offset: '0 0'
			};
		}
	};
});

(function() {
var define = $__System.amdDefine;
define("30", [], function() {
  return "<div id=\"<%= uid %>\" class=\"tooltip tooltip-<%= placement %> tooltip-<%= theme %> tooltip-<%= size %> <%= animation %> <%= classes %>\" style=\"<%= style %>\" role=\"tooltip\">\n\t<div class=\"tooltip-arrow tooltip-arrow-shadow\">\n\t\t<div class=\"tooltip-arrow-pointer\"></div>\n\t</div>\n\t<div class=\"tooltip-arrow\">\n\t\t<div class=\"tooltip-arrow-pointer\"></div>\n\t</div>\n\t<div class=\"tooltip-inner\">\n\t\t<span><%= content %></span>\n\t</div>\n</div>\n";
});

})();
(function() {
var define = $__System.amdDefine;
define("31", [], function() {
  return "<div id=\"<%= uid %>\" class=\"tooltip tooltip-info tooltip-<%= placement %> tooltip-<%= theme %> tooltip-<%= size %> <%= animation %> <%= classes %>\" style=\"<%= style %>\" role=\"tooltip\">\n\t<div class=\"tooltip-arrow tooltip-arrow-shadow\">\n\t\t<div class=\"tooltip-arrow-pointer\"></div>\n\t</div>\n\t<div class=\"tooltip-arrow\">\n\t\t<div class=\"tooltip-arrow-pointer\"></div>\n\t</div>\n\t<div class=\"tooltip-inner\">\n\t\t<span><%= content %></span>\n\t\t<a class=\"tooltip-close\">Close</a>\n\t</div>\n</div>\n";
});

})();
$__System.register('32', ['4', '5', '6', '7', '14', '15', '30', '31', 'b', '2e', '2f'], function (_export) {
	var _get, _inherits, _createClass, _classCallCheck, View, Component, TooltipTemplate, TooltipInfoTemplate, _Object$assign, Tether, TooltipModel, Name, Selector, DataKey, EventKey, Config, Tooltip, TooltipView;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_4) {
			_createClass = _4['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_5) {
			View = _5['default'];
		}, function (_6) {
			Component = _6['default'];
		}, function (_7) {
			TooltipTemplate = _7['default'];
		}, function (_8) {
			TooltipInfoTemplate = _8['default'];
		}, function (_b) {
			_Object$assign = _b['default'];
		}, function (_e) {
			Tether = _e['default'];
		}, function (_f) {
			TooltipModel = _f['default'];
		}],
		execute: function () {
			//
			// Tooltip Component
			//

			//
			// Configuration
			//
			'use strict';

			Name = 'Tooltip';
			Selector = '[data-toggle="tooltip"]:not([data-ignore])';
			DataKey = 'dls.tooltip';
			EventKey = '.' + DataKey;
			Config = {
				tether: {
					map: {
						top: 'bottom center',
						right: 'middle left',
						bottom: 'top center',
						left: 'middle right'
					}
				}
			};

			//
			// Initiator
			//

			Tooltip = (function (_Component) {
				_inherits(Tooltip, _Component);

				function Tooltip(opts) {
					_classCallCheck(this, Tooltip);

					_get(Object.getPrototypeOf(Tooltip.prototype), 'constructor', this).call(this, opts);
					this.component = TooltipView;

					this.render();
					this.onClickListen();
					this.onHoverListen();
					this.onFocusListen();
				}

				//
				// Tooltip View
				//
				return Tooltip;
			})(Component);

			_export('default', Tooltip);

			TooltipView = (function (_View) {
				_inherits(TooltipView, _View);

				function TooltipView(opts) {
					_classCallCheck(this, TooltipView);

					_get(Object.getPrototypeOf(TooltipView.prototype), 'constructor', this).call(this, opts);

					this.model.set('uid', Name + this.uid);
					this.triggers = this.model.get('trigger').split(' ');

					this.timeout = {};
					this.enabled = true;

					this.render();
					this.setProps();
					this.listen();
				}

				//
				// Getters
				//

				_createClass(TooltipView, [{
					key: 'render',

					//
					// Render
					//
					value: function render() {
						var type = this.model.get('type'),
						    template = type === 'info' ? TooltipInfoTemplate : TooltipTemplate;

						this.el = $(this.util.request('template:render', template, this.model.attributes));
					}

					//
					// Set the properties
					//
				}, {
					key: 'setProps',
					value: function setProps() {
						this.sel = {
							content: this.el.find('.tooltip-inner > span')
						};

						this.el.data(DataKey, this);
					}

					//
					// Listen
					//
				}, {
					key: 'listen',
					value: function listen() {
						this.on('tether:position', this.refresh, this);

						this.model.on('change', this.update, this);

						this.app.vent.on('component:' + Name + ':close:all', $.proxy(this.onCloseAllTooltips, this)).on('component:Modal:open', $.proxy(this.onCloseAllTooltips, this));
					}

					//
					// Force all tooltips to close (e.g. - when a modal is opened)
					//
				}, {
					key: 'onCloseAllTooltips',
					value: function onCloseAllTooltips() {
						var _this = this;

						setTimeout(function () {
							_this.leave(true);
						}, 1);
					}

					//
					// Update the content of the tooltip from the model
					//
				}, {
					key: 'update',
					value: function update(e) {
						if (e.content === undefined) return;

						var content = this.model.get('content');

						this.sel.content.html(content);
					}

					//
					// Enter
					//
				}, {
					key: 'enter',
					value: function enter(manual) {
						var _this2 = this;

						if (this.triggers.indexOf('hover') === -1 && !manual) return;

						clearTimeout(this.timeout.close);

						if (this.el.hasClass('in') || this.state === 'in') {
							this.state = 'in';
							return;
						}

						this.emit('enter', this);

						this.addActiveMonitor();

						this.state = 'in';

						clearTimeout(this.timeout.change);

						this.timeout.change = setTimeout(function () {
							if (_this2.state === 'in') _this2.show();
						}, this.model.get('delay') * 1000);
					}

					//
					// Leave
					//
				}, {
					key: 'leave',
					value: function leave(manual) {
						var _this3 = this;

						if (this.triggers.indexOf('hover') === -1 && !manual || this.state === 'out') return;

						this.emit('leave', this);
						clearTimeout(this.timeout.change);

						this.removeActiveMonitor();

						this.state = 'out';
						this.timeout.change = setTimeout(function () {
							if (_this3.state === 'out') _this3.hide();
						}, this.model.get('delay') * 1000);
					}

					//
					// Toggle
					//
				}, {
					key: 'toggle',
					value: function toggle(e) {
						if (this.el.hasClass('in')) this.leave(true);else this.enter(true);
					}

					//
					// Show the tooltip
					//
				}, {
					key: 'show',
					value: function show(manual) {
						this.emit('showing', this);
						clearTimeout(this.timeout.close);

						if (manual) this.manual = true;

						var contains = $.contains(this.target[0].ownerDocument.documentElement, this.target[0]);

						if (!this.enabled || !contains) return this;

						this.el.appendTo(document.body);
						this.target.attr('aria-describedby', this.model.get('uid'));

						this.addTether();
						this.fadeInTooltip();
						this.tooltipListen();
					}

					//
					// Hide
					//
				}, {
					key: 'hide',
					value: function hide(manual) {
						this.emit('hiding', this);
						clearTimeout(this.timeout.close);

						if (manual) this.manual = false;

						if (this.manual && !manual) return;

						this.fadeOutTooltip();
					}

					//
					// Fade in tooltip
					//
				}, {
					key: 'fadeInTooltip',
					value: function fadeInTooltip() {
						this.el.addClass('in');

						clearTimeout(this.timeout.fade);
						this.timeout.fade = setTimeout($.proxy(this.onShown, this), 250);
						if (!this.model.get('animation') === '') this.onShown();
					}

					//
					// Fade out tooltip
					//
				}, {
					key: 'fadeOutTooltip',
					value: function fadeOutTooltip() {
						this.el.removeClass('in');
						clearTimeout(this.timeout.fade);

						if (this.model.get('animation') !== '') this.timeout.fade = setTimeout($.proxy(this.onHidden, this), 250);else this.onHidden();
					}

					//
					// Bind temporary listeners (remove when the element is removed from DOM)
					//
				}, {
					key: 'tooltipListen',
					value: function tooltipListen() {
						var _this4 = this;

						this.el.on('click' + EventKey, '.tooltip-close', function (e) {
							_this4.toggle();
							e.preventDefault();
						});
					}

					//
					// On Shown
					//
				}, {
					key: 'onShown',
					value: function onShown() {
						clearTimeout(this.timeout.fade);
						var prevState = this.state;
						this.state = null;

						this.emit('shown');

						if (prevState === 'out') this.leave(null);
					}

					//
					// On Hidden
					//
				}, {
					key: 'onHidden',
					value: function onHidden() {
						clearTimeout(this.timeout.fade);

						if (this.state !== 'in' && this.el.parent().length) {
							this.el.remove();
							this.target.removeAttr('aria-describedby');

							this.untether();

							this.emit('hidden');
						}
					}

					//
					// Add active state monitor
					//
				}, {
					key: 'addActiveMonitor',
					value: function addActiveMonitor() {
						var _this5 = this;

						var uid = this.model.get('uid');

						clearTimeout(this.timeout.monitor);
						this.timeout.monitor = setTimeout(function () {
							$(document.body).on('' + _this5.platform.event.down + EventKey + '.' + uid, $.proxy(_this5.confirmClick, _this5));
						}, 10);

						$(window).on('resize' + EventKey + '.' + uid, $.proxy(this.refresh, this));
					}

					//
					// Remove active state monitor
					//
				}, {
					key: 'removeActiveMonitor',
					value: function removeActiveMonitor() {
						var uid = this.model.get('uid');

						clearTimeout(this.timeout.monitor);

						$(document.body).off('click' + EventKey + '.' + uid, $.proxy(this.confirmClick, this));

						$(window).off('resize' + EventKey + '.' + uid, $.proxy(this.refresh, this));
					}

					//
					// Delayed Hide
					//
				}, {
					key: 'delayedHide',
					value: function delayedHide(time) {
						var _this6 = this;

						clearTimeout(this.timeout.close);
						this.timeout.close = setTimeout(function () {
							_this6.hide(true);
						}, time);
					}

					//
					// Confirm Click
					//
				}, {
					key: 'confirmClick',
					value: function confirmClick(e) {
						if (!$.contains(this.el[0], e.target)) {
							if (this.el.hasClass('in')) {
								this.leave(true);
							}
						}
					}

					//
					// Refresh the tether
					//
				}, {
					key: 'refresh',
					value: function refresh() {
						if (this.tether) this.tether.position();
					}

					//
					// Add tether
					//
				}, {
					key: 'addTether',
					value: function addTether() {
						var _this7 = this;

						var attachment = this.attachment(this.model.get('placement'));

						this.untether();
						this.tether = new Tether({
							attachment: attachment,
							element: this.el,
							target: this.target,
							offset: this.model.get('offset'),
							constraints: this.model.get('constraints'),
							classPrefix: 'tooltip'
						});

						this.app.vent.trigger('resize:trigger');
						this.tether.position();

						// Mini throttle tether position to ensure it is placed correctly
						setTimeout(function () {
							if (_this7.tether) _this7.tether.position();
						}, 10);
					}

					//
					// Untether
					//
				}, {
					key: 'untether',
					value: function untether() {
						if (this.tether) this.tether.destroy();
					}

					//
					// Get attachment position
					//
				}, {
					key: 'attachment',
					value: function attachment(placement) {
						return Config.tether.map[placement] || 'bottom center';
					}

					//
					// Interface
					//
				}, {
					key: 'destroy',

					//
					// Destroy
					//
					value: function destroy() {
						var originalTitle = this.model.get('originalTitle');
						clearTimeout(this.timeout.close);
						clearTimeout(this.timeout.change);
						clearTimeout(this.timeout.fade);

						this.removeActiveMonitor();
						this.untether();

						if (this.el) this.el.remove();

						if (originalTitle != null) this.target.attr('title', originalTitle);

						$.removeData(this.target, DataKey);
					}
				}], [{
					key: '_interface',
					value: function _interface(opts) {
						var $el = this,
						    data = $el.data(DataKey),
						    cmd = $el.data(),
						    config = _Object$assign({}, $el.data(), typeof opts === 'object' && opts);

						if (opts.invoke) {
							var invokeState = opts.invokeEnter ? 'enter' : 'leave';

							if (opts.invokeHover || opts.invokeFocus) {
								cmd = '' + invokeState;
							} else {
								cmd = 'toggle';
							}
						}

						if (!data) {
							var title = $el.attr('title');

							if (config.type === 'info' && !config.theme) config.theme = 'white';

							if (title && config.content === undefined) {
								config.originalTitle = title;
								config.content = title;
								$el.removeAttr('title');
							}

							data = new TooltipView({
								target: $el,
								model: new TooltipModel(config)
							});

							$el.data(DataKey, data);
						}

						if (typeof cmd === 'string') if (data[cmd] !== undefined) {
							if (data.get('trigger').indexOf('click') === -1 && cmd === 'toggle') return;

							data[cmd]();
						}

						return data;
					}
				}, {
					key: 'Name',
					get: function get() {
						return Name;
					}
				}, {
					key: 'Selector',
					get: function get() {
						return Selector;
					}
				}, {
					key: 'EventKey',
					get: function get() {
						return EventKey;
					}
				}]);

				return TooltipView;
			})(View);
		}
	};
});

$__System.register('33', ['4', '5', '7', '10'], function (_export) {
	var _get, _inherits, _classCallCheck, Model, SmartFieldModel;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_4) {
			Model = _4['default'];
		}],
		execute: function () {
			//
			// SmartField Model
			//

			'use strict';

			SmartFieldModel = (function (_Model) {
				_inherits(SmartFieldModel, _Model);

				function SmartFieldModel() {
					_classCallCheck(this, SmartFieldModel);

					_get(Object.getPrototypeOf(SmartFieldModel.prototype), 'constructor', this).apply(this, arguments);
				}

				return SmartFieldModel;
			})(Model);

			_export('default', SmartFieldModel);

			SmartFieldModel.prototype.defaults = {
				legend: '',
				forceFocus: false
			};
		}
	};
});

(function() {
var define = $__System.amdDefine;
define("34", [], function() {
  return "<div>\n\t<div class=\"smart-field\">\n\t\t<div class=\"smart-el-placeholder\">\n\t\t\t<legend><%= legend %></legend>\n\t\t</div>\n\t\t<div class=\"smart-el-field\">\n\t\t\t<div class=\"label\"><%= legend %></div>\n\t\t\t<input class=\"smart-el-value\" value=\"\" tabindex=\"-1\" readonly>\n\t\t</div>\n\t\t<div class=\"smart-el-inputs\">\n\t\t\t<% fields.forEach(function(field) { %>\n\t\t\t\t<div class=\"smart-el-input <%= field.state %>\">\n\t\t\t\t\t<label for=\"<%= field.name %>\"><%= field.label %></label>\n\t\t\t\t\t<input type=\"<%= field.type %>\"\n\t\t\t\t\t\tid=\"<%= field.name %>\"\n\t\t\t\t\t\tname=\"<%= field.name %>\"\n\t\t\t\t\t\tvalue=\"<%= field.value %>\"\n\t\t\t\t\t\t<%= field.described %>\n\t\t\t\t\t\t<%= field.required %>>\n\t\t\t\t</div>\n\t\t\t<% }); %>\n\t\t</div>\n\t</div>\n\n\t<div class=\"smart-field-validation\">\n\t\t<% fields.forEach(function(field) { %>\n\t\t\t<%= field.validation %>\n\t\t<% }); %>\n\t</div>\n</div>\n";
});

})();
$__System.register('35', ['4', '5', '6', '7', '14', '15', '33', '34', 'b'], function (_export) {
	var _get, _inherits, _createClass, _classCallCheck, View, Component, SmartFieldModel, SmartFieldTemplate, _Object$assign, Name, Selector, DataKey, EventKey, SmartField, SmartFieldView;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_4) {
			_createClass = _4['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_5) {
			View = _5['default'];
		}, function (_6) {
			Component = _6['default'];
		}, function (_7) {
			SmartFieldModel = _7['default'];
		}, function (_8) {
			SmartFieldTemplate = _8['default'];
		}, function (_b) {
			_Object$assign = _b['default'];
		}],
		execute: function () {
			//
			// SmartField Component
			//

			//
			// Configuration
			//
			'use strict';

			Name = 'SmartField';
			Selector = '[data-toggle="smartfield"]:not([data-ignore])';
			DataKey = 'dls.smartfield';
			EventKey = '.' + DataKey;

			//
			// Initiator
			//

			SmartField = (function (_Component) {
				_inherits(SmartField, _Component);

				function SmartField(opts) {
					_classCallCheck(this, SmartField);

					_get(Object.getPrototypeOf(SmartField.prototype), 'constructor', this).call(this, opts);
					this.component = SmartFieldView;

					this.render();
				}

				//
				// SmartField View
				//
				return SmartField;
			})(Component);

			_export('default', SmartField);

			SmartFieldView = (function (_View) {
				_inherits(SmartFieldView, _View);

				function SmartFieldView(opts) {
					_classCallCheck(this, SmartFieldView);

					_get(Object.getPrototypeOf(SmartFieldView.prototype), 'constructor', this).call(this, opts);

					this.configure();
					this.render();

					this.setProps();
					this.updateValidation(true);
					this.defaultState();

					this.listen();
				}

				//
				// Getters
				//

				_createClass(SmartFieldView, [{
					key: 'configure',

					//
					// Configure
					//
					value: function configure() {
						var _this = this;

						var fields = [],
						    legend = this.target.find('legend'),
						    inputs = this.target.find('input');

						if (legend.length) this.model.set('legend', legend.html());

						inputs.each(function (idx, el) {
							el = $(el);

							var state = el.hasClass('has-warning') ? 'has-warning' : '',
							    required = el.prop('required') ? 'required' : '',
							    describedBy = el.attr('aria-describedby'),
							    validation = _this.target.find('#' + describedBy),
							    name = el.attr('name') || el.attr('id'),
							    label = _this.target.find('label[for="' + name + '"]'),
							    field = {
								type: el.prop('type') || 'text',
								label: '&nbsp;',
								state: state,
								name: name,
								value: el.val(),
								required: required,
								validation: '',
								described: ''
							};

							if (label.length === 1) field.label = label.html();

							if (validation.length === 1) {
								field.described = 'aria-describedby="' + describedBy + '"';
								field.validation = validation[0].outerHTML;
							}

							fields.push(field);
						});

						this.model.set('fields', fields);
					}

					//
					// Render
					//
				}, {
					key: 'render',
					value: function render() {
						var field = $(this.util.request('template:render', SmartFieldTemplate, this.model.attributes));

						this.el = field.find('.smart-field');
						this.validation = field.find('.smart-field-validation');

						this.target.empty().prepend(field.children());
					}

					//
					// Set Props
					//
				}, {
					key: 'setProps',
					value: function setProps() {
						this.sel = {
							placeholder: this.el.find('.smart-el-placeholder'),
							field: this.el.find('.smart-el-field'),
							fields: this.el.find('.smart-el-inputs'),
							input: this.el.find('.smart-el-input')
						};
					}

					//
					// Bind the events
					//
				}, {
					key: 'listen',
					value: function listen() {
						this.el.on(this.platform.event.down + EventKey, $.proxy(this.onMouseDown, this)).on('click' + EventKey, $.proxy(this.onFocus, this));

						this.sel.fields.find('input').on('change' + EventKey, $.proxy(this.onChange, this)).on('focus' + EventKey, $.proxy(this.onFocus, this)).on('blur' + EventKey, this.util.request('events:throttle', $.proxy(this.onBlur, this), 1));

						if (this.model.get('forceFocus')) {
							this.onChange();
							this.onFocus();
						}
					}

					//
					// Field Pressed
					//
				}, {
					key: 'onMouseDown',
					value: function onMouseDown(e) {
						this.pressed = true;

						$(document).on(this.platform.event.up + EventKey + this.uid, $.proxy(this.onMouseUp, this));
					}

					//
					// Field Released
					//
				}, {
					key: 'onMouseUp',
					value: function onMouseUp(e) {
						this.pressed = false;
						this.onBlur();
						$(document).off(this.platform.event.up + EventKey + this.uid, $.proxy(this.onMouseUp, this));
					}

					//
					// Field changed
					//
				}, {
					key: 'onChange',
					value: function onChange() {
						this.changed = true;
					}

					//
					// Field Focus
					//
				}, {
					key: 'onFocus',
					value: function onFocus() {
						if (this.target.is(':disabled') || this.state === 'active') return this;

						this.updateState('active');
					}

					//
					// Field Blur
					//
				}, {
					key: 'onBlur',
					value: function onBlur() {
						if (this.sel.input.find('input:focus').length || this.state === 'default' || this.pressed) return this;

						this.updateState('default');
					}

					//
					// Update State
					//
				}, {
					key: 'updateState',
					value: function updateState(state) {
						if (state) this.state = state;

						if (this.state === 'active') this.activeState();else if (this.state === 'default') this.defaultState();
					}

					//
					// Field is in active (selected) state
					//
				}, {
					key: 'activeState',
					value: function activeState() {
						this.el.addClass('active focus');

						if (this.model.get('forceFocus') && !this.firstForce) {
							this.firstForce = true;
						} else {
							this.sel.input.first().find('input').trigger('focus');
						}
					}

					//
					// Field is in its default (unselected) state
					//
				}, {
					key: 'defaultState',
					value: function defaultState() {
						if (!this.model.get('forceFocus')) this.el.removeClass('active focus');

						this.updateValue();
						this.validate();
						this.updateValidation();
					}

					//
					// Update the value
					//
				}, {
					key: 'updateValue',
					value: function updateValue() {
						var value = '';

						this.sel.fields.find('input').each(function (idx, el) {
							var val = $(el).val();

							if (val !== '') value += (value !== '' ? ' ' : '') + val;
						});

						this.sel.field.find('input').val(value);

						if (value !== '') this.el.addClass('has-value');else this.el.removeClass('has-value');
					}

					//
					// Validate the field, if necessary
					//
				}, {
					key: 'validate',
					value: function validate(force) {
						var _this2 = this;

						this.sel.input.each(function (idx, el) {
							el = $(el);

							var input = el.find('input'),
							    valid = true,
							    type = input.prop('type'),
							    val = input.val();

							if (input.is(':required')) {
								switch (type) {
									case 'text':
										if (val.replace(/ /g, '') === '') valid = false;
										break;
									default:
										break;
								};
							}

							if (_this2.changed && !force) {
								var alertID = input.attr('aria-describedby'),
								    _alert = $('#' + alertID);

								if (!valid) {
									el.addClass('has-warning');
									if (_alert.length) _alert.addClass('active');
								} else {
									el.removeClass('has-warning');
									if (_alert.length) _alert.removeClass('active');
								}
							}
						});
					}

					//
					// Update validation
					//
				}, {
					key: 'updateValidation',
					value: function updateValidation(force) {
						if (!this.changed && !force) return;

						var warning = false,
						    success = true;

						this.sel.input.each(function (idx, el) {
							el = $(el);

							var input = el.find('input'),
							    alertID = input.attr('aria-describedby'),
							    alert = $('#' + alertID);

							if (el.hasClass('has-warning')) {
								success = false;
								warning = true;

								if (alert.length) alert.addClass('active');
							} else if (input.is(':required') && input.val() === '') {
								success = false;
							}
						});

						if (warning) this.el.addClass('smart-field-warning');else this.el.removeClass('smart-field-warning');

						if (success && !warning) this.el.addClass('smart-field-success');else this.el.removeClass('smart-field-success');
					}

					//
					// Interface
					//
				}, {
					key: 'destroy',

					//
					// Destroy
					//
					value: function destroy() {
						$.removeData(this.target, DataKey);
					}
				}], [{
					key: '_interface',
					value: function _interface(opts) {
						var $el = $(this),
						    data = $el.data(DataKey),
						    config = _Object$assign({}, $el.data(), opts);

						if (!data) {
							data = new SmartFieldView({
								target: $el,
								model: new SmartFieldModel(config)
							});

							$el.data(DataKey, data);
						}

						return data;
					}
				}, {
					key: 'Name',
					get: function get() {
						return Name;
					}
				}, {
					key: 'Selector',
					get: function get() {
						return Selector;
					}
				}, {
					key: 'EventKey',
					get: function get() {
						return EventKey;
					}
				}]);

				return SmartFieldView;
			})(View);
		}
	};
});

$__System.register('14', ['3', '4', '5', '6', '7', '10'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Model, View;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_6) {
			Model = _6['default'];
		}],
		execute: function () {
			//
			// View
			//

			'use strict';

			View = (function (_Obj) {
				_inherits(View, _Obj);

				function View(opts) {
					_classCallCheck(this, View);

					_get(Object.getPrototypeOf(View.prototype), 'constructor', this).call(this, opts);

					this.uid = this.util.request('uid:gen');

					if (!this.target) console.warn('[DLS > View]: No target specified for view');
					if (!this.model) this.model = new Model(opts);

					/*this.model.on('change', (...args) => {
     	this.emit('change', args);
     });*/
				}

				_createClass(View, [{
					key: 'render',
					value: function render() {
						console.warn('[DLS > View]: No render available.');
					}

					//
					// Pass the 'get' to the model
					//
				}, {
					key: 'get',
					value: function get() {
						var _model;

						return (_model = this.model).get.apply(_model, arguments);
					}

					//
					// Pass the 'set' to the model
					//
				}, {
					key: 'set',
					value: function set() {
						var _model2;

						return (_model2 = this.model).set.apply(_model2, arguments);
					}
				}]);

				return View;
			})(Obj);

			_export('default', View);
		}
	};
});

$__System.register('15', ['3', '4', '5', '6', '7', 'b'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, _Object$assign, Component;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_b) {
			_Object$assign = _b['default'];
		}],
		execute: function () {
			//
			// Component Base Class
			//
			// The component base class unifies all the different components with a standard
			// set of utility functions that are consistent across each component.
			//

			'use strict';

			Component = (function (_Obj) {
				_inherits(Component, _Obj);

				function Component(opts) {
					_classCallCheck(this, Component);

					_get(Object.getPrototypeOf(Component.prototype), 'constructor', this).call(this, opts);
					this.listenCore();
				}

				//
				// Throw standard error messages
				//

				_createClass(Component, [{
					key: 'throwError',
					value: function throwError(message) {
						var name = 'Component';

						if (this.component.Name) name = this.component.Name;

						console.warn('[DLS > ' + name + ' Error]: ' + message);
						return this;
					}

					//
					// Core listen that is applied to the component automatically
					//
				}, {
					key: 'listenCore',
					value: function listenCore() {
						var _this = this;

						this.on('render', function () {
							_this.render();
						});
					}

					//
					// Default onClick listen
					//
				}, {
					key: 'onClickListen',
					value: function onClickListen() {
						var _this2 = this;

						if (!this.component) return;

						this.scope.on('click' + this.component.EventKey, this.component.Selector, function (e) {
							e.preventDefault();
							return _this2.create({
								target: e.currentTarget,
								config: {
									invoke: true
								}
							});
						});
					}

					//
					// Bind a custom data event listener that can trigger a
					// non-standard interaction event (usually utilised for specific
					// command within a component).
				}, {
					key: 'onDataEventListen',
					value: function onDataEventListen() {
						var _this3 = this;

						if (!this.component) return;

						this.scope.on('data', this.component.Selector, function (e, opts) {
							e.preventDefault();

							return _this3.create({
								target: e.currentTarget,
								config: _Object$assign({
									invoke: true,
									invokeData: true
								}, opts)
							});
						});
					}

					//
					// Default onHover listen
					//
				}, {
					key: 'onHoverListen',
					value: function onHoverListen() {
						var _this4 = this;

						if (!this.component) return;

						this.scope.on('' + this.platform.event.enter + this.component.EventKey, this.component.Selector, function (e) {

							return _this4.create({
								target: e.currentTarget,
								config: {
									invoke: true,
									invokeHover: true,
									invokeEnter: true
								}
							});
						}).on('' + this.platform.event.leave + this.component.EventKey, this.component.Selector, function (e) {

							return _this4.create({
								target: e.currentTarget,
								config: {
									invoke: true,
									invokeHover: true,
									invokeEnter: false
								}
							});
						});
					}

					//
					// Default onFocus listen
					//
				}, {
					key: 'onFocusListen',
					value: function onFocusListen() {
						var _this5 = this;

						if (!this.component) return;

						this.scope.on('focusin' + this.component.EventKey, this.component.Selector, function (e) {
							e.preventDefault();

							return _this5.create({
								target: e.currentTarget,
								config: {
									invoke: true,
									invokeFocus: true,
									invokeEnter: true
								}
							});
						}).on('focusout' + this.component.EventKey, this.component.Selector, function (e) {
							e.preventDefault();

							return _this5.create({
								target: e.currentTarget,
								config: {
									invoke: true,
									invokeFocus: true,
									invokeEnter: false
								}
							});
						});
					}

					//
					// Default Render
					//
				}, {
					key: 'render',
					value: function render() {
						var _this6 = this;

						if (!this.component) return;

						this.scope.find(this.component.Selector).each(function (idx, el) {
							_this6.create({
								target: el,
								config: {}
							});
						});
					}

					//
					// Default `Create`
					//
				}, {
					key: 'create',
					value: function create(options) {
						var _this7 = this;

						if (!this.component) return;

						if (!options.target) {
							this.throwError('A `target` must be specified!');
							return this;
						}

						var elements = [];
						options.config = _Object$assign({}, options.config);
						options.target = $(options.target);
						options.target.each(function (idx, el) {
							var element = _this7.component._interface.call($(el), options.config);

							if (element) elements.push(element);
						});

						return this.created(elements);
					}

					//
					// Return the created elements in flexible format
					//
				}, {
					key: 'created',
					value: function created(elements) {
						if (elements.length === 1) return elements[0];else if (elements.length) return elements;else return null;
					}
				}]);

				return Component;
			})(Obj);

			_export('default', Component);
		}
	};
});

$__System.registerDynamic("36", ["37", "38"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var IObject = $__require('37'),
      defined = $__require('38');
  module.exports = function(it) {
    return IObject(defined(it));
  };
  return module.exports;
});

$__System.registerDynamic("39", ["36", "3a"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var toIObject = $__require('36');
  $__require('3a')('getOwnPropertyDescriptor', function($getOwnPropertyDescriptor) {
    return function getOwnPropertyDescriptor(it, key) {
      return $getOwnPropertyDescriptor(toIObject(it), key);
    };
  });
  return module.exports;
});

$__System.registerDynamic("3b", ["3c", "39"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var $ = $__require('3c');
  $__require('39');
  module.exports = function getOwnPropertyDescriptor(it, key) {
    return $.getDesc(it, key);
  };
  return module.exports;
});

$__System.registerDynamic("3d", ["3b"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = {
    "default": $__require('3b'),
    __esModule: true
  };
  return module.exports;
});

$__System.registerDynamic("4", ["3d"], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  var _Object$getOwnPropertyDescriptor = $__require('3d')["default"];
  exports["default"] = function get(_x, _x2, _x3) {
    var _again = true;
    _function: while (_again) {
      var object = _x,
          property = _x2,
          receiver = _x3;
      _again = false;
      if (object === null)
        object = Function.prototype;
      var desc = _Object$getOwnPropertyDescriptor(object, property);
      if (desc === undefined) {
        var parent = Object.getPrototypeOf(object);
        if (parent === null) {
          return undefined;
        } else {
          _x = parent;
          _x2 = property;
          _x3 = receiver;
          _again = true;
          desc = parent = undefined;
          continue _function;
        }
      } else if ("value" in desc) {
        return desc.value;
      } else {
        var getter = desc.get;
        if (getter === undefined) {
          return undefined;
        }
        return getter.call(receiver);
      }
    }
  };
  exports.__esModule = true;
  return module.exports;
});

$__System.registerDynamic("3e", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = function(it) {
    return typeof it === 'object' ? it !== null : typeof it === 'function';
  };
  return module.exports;
});

$__System.registerDynamic("3f", ["3e"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var isObject = $__require('3e');
  module.exports = function(it) {
    if (!isObject(it))
      throw TypeError(it + ' is not an object!');
    return it;
  };
  return module.exports;
});

$__System.registerDynamic("40", ["3c", "3e", "3f", "41"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var getDesc = $__require('3c').getDesc,
      isObject = $__require('3e'),
      anObject = $__require('3f');
  var check = function(O, proto) {
    anObject(O);
    if (!isObject(proto) && proto !== null)
      throw TypeError(proto + ": can't set as prototype!");
  };
  module.exports = {
    set: Object.setPrototypeOf || ('__proto__' in {} ? function(test, buggy, set) {
      try {
        set = $__require('41')(Function.call, getDesc(Object.prototype, '__proto__').set, 2);
        set(test, []);
        buggy = !(test instanceof Array);
      } catch (e) {
        buggy = true;
      }
      return function setPrototypeOf(O, proto) {
        check(O, proto);
        if (buggy)
          O.__proto__ = proto;
        else
          set(O, proto);
        return O;
      };
    }({}, false) : undefined),
    check: check
  };
  return module.exports;
});

$__System.registerDynamic("42", ["43", "40"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var $export = $__require('43');
  $export($export.S, 'Object', {setPrototypeOf: $__require('40').set});
  return module.exports;
});

$__System.registerDynamic("44", ["42", "45"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  $__require('42');
  module.exports = $__require('45').Object.setPrototypeOf;
  return module.exports;
});

$__System.registerDynamic("46", ["44"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = {
    "default": $__require('44'),
    __esModule: true
  };
  return module.exports;
});

$__System.registerDynamic("5", ["47", "46"], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  var _Object$create = $__require('47')["default"];
  var _Object$setPrototypeOf = $__require('46')["default"];
  exports["default"] = function(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }
    subClass.prototype = _Object$create(superClass && superClass.prototype, {constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }});
    if (superClass)
      _Object$setPrototypeOf ? _Object$setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  };
  exports.__esModule = true;
  return module.exports;
});

$__System.registerDynamic("48", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var toString = {}.toString;
  module.exports = function(it) {
    return toString.call(it).slice(8, -1);
  };
  return module.exports;
});

$__System.registerDynamic("37", ["48"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var cof = $__require('48');
  module.exports = Object('z').propertyIsEnumerable(0) ? Object : function(it) {
    return cof(it) == 'String' ? it.split('') : Object(it);
  };
  return module.exports;
});

$__System.registerDynamic("49", ["3c", "4a", "37", "4b"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var $ = $__require('3c'),
      toObject = $__require('4a'),
      IObject = $__require('37');
  module.exports = $__require('4b')(function() {
    var a = Object.assign,
        A = {},
        B = {},
        S = Symbol(),
        K = 'abcdefghijklmnopqrst';
    A[S] = 7;
    K.split('').forEach(function(k) {
      B[k] = k;
    });
    return a({}, A)[S] != 7 || Object.keys(a({}, B)).join('') != K;
  }) ? function assign(target, source) {
    var T = toObject(target),
        $$ = arguments,
        $$len = $$.length,
        index = 1,
        getKeys = $.getKeys,
        getSymbols = $.getSymbols,
        isEnum = $.isEnum;
    while ($$len > index) {
      var S = IObject($$[index++]),
          keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S),
          length = keys.length,
          j = 0,
          key;
      while (length > j)
        if (isEnum.call(S, key = keys[j++]))
          T[key] = S[key];
    }
    return T;
  } : Object.assign;
  return module.exports;
});

$__System.registerDynamic("4c", ["43", "49"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var $export = $__require('43');
  $export($export.S + $export.F, 'Object', {assign: $__require('49')});
  return module.exports;
});

$__System.registerDynamic("4d", ["4c", "45"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  $__require('4c');
  module.exports = $__require('45').Object.assign;
  return module.exports;
});

$__System.registerDynamic("b", ["4d"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = {
    "default": $__require('4d'),
    __esModule: true
  };
  return module.exports;
});

$__System.registerDynamic("38", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = function(it) {
    if (it == undefined)
      throw TypeError("Can't call method on  " + it);
    return it;
  };
  return module.exports;
});

$__System.registerDynamic("4a", ["38"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var defined = $__require('38');
  module.exports = function(it) {
    return Object(defined(it));
  };
  return module.exports;
});

$__System.registerDynamic("4e", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var global = module.exports = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();
  if (typeof __g == 'number')
    __g = global;
  return module.exports;
});

$__System.registerDynamic("4f", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = function(it) {
    if (typeof it != 'function')
      throw TypeError(it + ' is not a function!');
    return it;
  };
  return module.exports;
});

$__System.registerDynamic("41", ["4f"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var aFunction = $__require('4f');
  module.exports = function(fn, that, length) {
    aFunction(fn);
    if (that === undefined)
      return fn;
    switch (length) {
      case 1:
        return function(a) {
          return fn.call(that, a);
        };
      case 2:
        return function(a, b) {
          return fn.call(that, a, b);
        };
      case 3:
        return function(a, b, c) {
          return fn.call(that, a, b, c);
        };
    }
    return function() {
      return fn.apply(that, arguments);
    };
  };
  return module.exports;
});

$__System.registerDynamic("43", ["4e", "45", "41"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var global = $__require('4e'),
      core = $__require('45'),
      ctx = $__require('41'),
      PROTOTYPE = 'prototype';
  var $export = function(type, name, source) {
    var IS_FORCED = type & $export.F,
        IS_GLOBAL = type & $export.G,
        IS_STATIC = type & $export.S,
        IS_PROTO = type & $export.P,
        IS_BIND = type & $export.B,
        IS_WRAP = type & $export.W,
        exports = IS_GLOBAL ? core : core[name] || (core[name] = {}),
        target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE],
        key,
        own,
        out;
    if (IS_GLOBAL)
      source = name;
    for (key in source) {
      own = !IS_FORCED && target && key in target;
      if (own && key in exports)
        continue;
      out = own ? target[key] : source[key];
      exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key] : IS_BIND && own ? ctx(out, global) : IS_WRAP && target[key] == out ? (function(C) {
        var F = function(param) {
          return this instanceof C ? new C(param) : C(param);
        };
        F[PROTOTYPE] = C[PROTOTYPE];
        return F;
      })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
      if (IS_PROTO)
        (exports[PROTOTYPE] || (exports[PROTOTYPE] = {}))[key] = out;
    }
  };
  $export.F = 1;
  $export.G = 2;
  $export.S = 4;
  $export.P = 8;
  $export.B = 16;
  $export.W = 32;
  module.exports = $export;
  return module.exports;
});

$__System.registerDynamic("4b", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = function(exec) {
    try {
      return !!exec();
    } catch (e) {
      return true;
    }
  };
  return module.exports;
});

$__System.registerDynamic("3a", ["43", "45", "4b"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var $export = $__require('43'),
      core = $__require('45'),
      fails = $__require('4b');
  module.exports = function(KEY, exec) {
    var fn = (core.Object || {})[KEY] || Object[KEY],
        exp = {};
    exp[KEY] = exec(fn);
    $export($export.S + $export.F * fails(function() {
      fn(1);
    }), 'Object', exp);
  };
  return module.exports;
});

$__System.registerDynamic("50", ["4a", "3a"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var toObject = $__require('4a');
  $__require('3a')('keys', function($keys) {
    return function keys(it) {
      return $keys(toObject(it));
    };
  });
  return module.exports;
});

$__System.registerDynamic("45", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var core = module.exports = {version: '1.2.6'};
  if (typeof __e == 'number')
    __e = core;
  return module.exports;
});

$__System.registerDynamic("51", ["50", "45"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  $__require('50');
  module.exports = $__require('45').Object.keys;
  return module.exports;
});

$__System.registerDynamic("a", ["51"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = {
    "default": $__require('51'),
    __esModule: true
  };
  return module.exports;
});

$__System.registerDynamic("52", ["3c"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var $ = $__require('3c');
  module.exports = function defineProperty(it, key, desc) {
    return $.setDesc(it, key, desc);
  };
  return module.exports;
});

$__System.registerDynamic("53", ["52"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = {
    "default": $__require('52'),
    __esModule: true
  };
  return module.exports;
});

$__System.registerDynamic("6", ["53"], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  var _Object$defineProperty = $__require('53')["default"];
  exports["default"] = (function() {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor)
          descriptor.writable = true;
        _Object$defineProperty(target, descriptor.key, descriptor);
      }
    }
    return function(Constructor, protoProps, staticProps) {
      if (protoProps)
        defineProperties(Constructor.prototype, protoProps);
      if (staticProps)
        defineProperties(Constructor, staticProps);
      return Constructor;
    };
  })();
  exports.__esModule = true;
  return module.exports;
});

$__System.registerDynamic("7", [], true, function($__require, exports, module) {
  "use strict";
  ;
  var define,
      global = this,
      GLOBAL = this;
  exports["default"] = function(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  };
  exports.__esModule = true;
  return module.exports;
});

$__System.registerDynamic("3c", [], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var $Object = Object;
  module.exports = {
    create: $Object.create,
    getProto: $Object.getPrototypeOf,
    isEnum: {}.propertyIsEnumerable,
    getDesc: $Object.getOwnPropertyDescriptor,
    setDesc: $Object.defineProperty,
    setDescs: $Object.defineProperties,
    getKeys: $Object.keys,
    getNames: $Object.getOwnPropertyNames,
    getSymbols: $Object.getOwnPropertySymbols,
    each: [].forEach
  };
  return module.exports;
});

$__System.registerDynamic("54", ["3c"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  var $ = $__require('3c');
  module.exports = function create(P, D) {
    return $.create(P, D);
  };
  return module.exports;
});

$__System.registerDynamic("47", ["54"], true, function($__require, exports, module) {
  ;
  var define,
      global = this,
      GLOBAL = this;
  module.exports = {
    "default": $__require('54'),
    __esModule: true
  };
  return module.exports;
});

$__System.register('55', ['6', '7', '47'], function (_export) {
	var _createClass, _classCallCheck, _Object$create, Events;

	return {
		setters: [function (_) {
			_createClass = _['default'];
		}, function (_2) {
			_classCallCheck = _2['default'];
		}, function (_3) {
			_Object$create = _3['default'];
		}],
		execute: function () {
			//
			// Events Class
			//
			// Event Publisher/Subscriber handling

			'use strict';

			Events = (function () {
				function Events() {
					_classCallCheck(this, Events);

					this._callbacks = {};
					this._queue = {};
				}

				_createClass(Events, [{
					key: 'trigger',
					value: function trigger() {
						return this.emit.apply(this, arguments);
					}
				}, {
					key: 'emit',
					value: function emit(event) {
						var args = [].slice.call(arguments, 1),
						    callbacks = this._callbacks['$' + event];

						if (callbacks) {
							callbacks = callbacks.slice(0);

							for (var i = 0, len = callbacks.length; i < len; ++i) {
								if (callbacks[i].callback !== undefined) {
									callbacks[i].callback.apply(callbacks[i].context, args);
								} else {
									console.warn('[DLS > Events] Undefined Event callback for event: ' + event + ' ');
								}
							}
						} else {
							this._queue['$' + event] = arguments;
						}

						return this;
					}
				}, {
					key: 'refireQueue',
					value: function refireQueue(event) {
						if (this._queue['$' + event]) {
							var _queuedEvent = _Object$create(this._queue['$' + event]);

							delete this._queue['$' + event];
							this.emit(_queuedEvent[0], _queuedEvent[1]);
						}
					}
				}, {
					key: 'on',
					value: function on(eventname, callback, context) {
						if (!this._callbacks['$' + eventname]) this._callbacks['$' + eventname] = [];

						this._callbacks['$' + eventname].push({ callback: callback, context: context });
						this.refireQueue(eventname);

						return this;
					}
				}, {
					key: 'once',
					value: function once(eventname, callback, context) {
						var on = function on() {
							this.off(eventname, on);
							calback.apply(context, arguments);
						};

						on.fn = callback;
						this.on(eventname, on);
						return this;
					}
				}, {
					key: 'off',
					value: function off(event, fn) {
						this._callbacks = this._callbacks || {};

						// all
						if (0 == arguments.length) {
							this._callbacks = {};
							return this;
						}

						// specific event
						var callbacks = this._callbacks['$' + event];
						if (!callbacks) return this;

						// remove all handlers
						if (1 == arguments.length) {
							delete this._callbacks['$' + event];
							return this;
						}

						// remove specific handler
						var cb;
						for (var i = 0; i < callbacks.length; i++) {
							cb = callbacks[i];
							if (cb.callback === fn || cb.fn === fn) {
								callbacks.splice(i, 1);
								break;
							}
						}
						return this;
					}
				}, {
					key: 'addEventListener',
					value: function addEventListener(event, fn) {
						this.on(event, fn);
					}
				}, {
					key: 'listeners',
					value: function listeners(event) {
						return this._callbacks['$' + event] || [];
					}
				}, {
					key: 'hasListeners',
					value: function hasListeners(event) {
						return !!this.listeners(event).length;
					}
				}, {
					key: 'removeListener',
					value: function removeListener(event, fn) {
						this.off(event, fn);
					}
				}, {
					key: 'removeAllListeners',
					value: function removeAllListeners(event, fn) {
						this.off();
					}
				}, {
					key: 'removeEventListener',
					value: function removeEventListener(event, fn) {
						this.off(event, fn);
					}
				}]);

				return Events;
			})();

			_export('default', Events);
		}
	};
});

$__System.register('3', ['6', '7', '55', 'a'], function (_export) {
	var _createClass, _classCallCheck, Events, _Object$keys, Obj;

	return {
		setters: [function (_) {
			_createClass = _['default'];
		}, function (_2) {
			_classCallCheck = _2['default'];
		}, function (_3) {
			Events = _3['default'];
		}, function (_a) {
			_Object$keys = _a['default'];
		}],
		execute: function () {
			'use strict';

			Obj = (function () {
				function Obj(opts) {
					_classCallCheck(this, Obj);

					this.events = this.emitter();
					if (opts) this.extend(opts);
				}

				_createClass(Obj, [{
					key: 'merge',
					value: function merge(obj1, obj2) {
						var obj3 = {};

						for (var attrname in obj1) obj3[attrname] = obj1[attrname];

						for (var attrname in obj2) obj3[attrname] = obj2[attrname];

						return obj3;
					}
				}, {
					key: 'extend',
					value: function extend(obj) {
						var _this = this;

						if (obj && typeof obj === 'object') _Object$keys(obj).forEach(function (opt) {
							_this[opt] = obj[opt];
						});

						return this;
					}
				}, {
					key: 'emitter',
					value: function emitter(obj) {
						return new Events(obj || this);
					}
				}, {
					key: 'emit',
					value: function emit(eventname, obj) {
						this.events.emit(eventname, obj);
						return this;
					}
				}, {
					key: 'trigger',
					value: function trigger(eventname, obj) {
						this.emit(eventname, obj);
						return this;
					}
				}, {
					key: 'on',
					value: function on(eventname, callback, context) {
						return this.events.on(eventname, callback, context);
					}
				}, {
					key: 'off',
					value: function off(eventname, callback, context) {
						return this.events.off(eventname, callback, context);
					}
				}]);

				return Obj;
			})();

			_export('default', Obj);
		}
	};
});

$__System.register('10', ['3', '4', '5', '6', '7', 'b', 'a'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, _Object$assign, _Object$keys, Model;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_b) {
			_Object$assign = _b['default'];
		}, function (_a) {
			_Object$keys = _a['default'];
		}],
		execute: function () {
			'use strict';

			Model = (function (_Obj) {
				_inherits(Model, _Obj);

				function Model(opts) {
					_classCallCheck(this, Model);

					_get(Object.getPrototypeOf(Model.prototype), 'constructor', this).call(this);

					this.attributes = _Object$assign({}, this.defaults, opts);
				}

				_createClass(Model, [{
					key: 'get',
					value: function get(attribute) {
						return attribute ? this.attributes[attribute] : this.attributes;
					}
				}, {
					key: 'set',
					value: function set(attribute, value) {
						var _this = this;

						//Previous values to compare against
						this.previousAttributes = {};

						_Object$keys(this.attributes).forEach(function (attr) {
							_this.previousAttributes[attr] = _this.attributes[attr];
						});

						//If Multi key/values
						if (typeof attribute === 'object') this.attributes = this.merge(this.attributes, attribute);

						//If setting one attr
						else if (attribute && value !== undefined) {
								this.attributes[attribute] = value;
							}

						//Check for changes
						var changed_attrs = {};

						_Object$keys(this.attributes).forEach(function (attr) {
							if (_this.previousAttributes[attr] != _this.attributes[attr]) changed_attrs[attr] = _this.attributes[attr];
						});

						//Return changes
						if (attribute) if (!attribute.silent) this.emit('change', changed_attrs);else delete attribute.silent;

						return this;
					}
				}, {
					key: 'escape',
					value: function escape(attribute) {
						return attribute ? this.attributes[attribute] : this;
					}
				}, {
					key: 'has',
					value: function has(attribute) {
						return attribute && this.attributes[attribute];
					}
				}, {
					key: 'clear',
					value: function clear(opts) {
						this.attributes = {};
						if (opts && !opts.silent) this.emit('change', null);
					}
				}]);

				return Model;
			})(Obj);

			_export('default', Model);
		}
	};
});

$__System.register('56', ['4', '5', '7', '10'], function (_export) {
	var _get, _inherits, _classCallCheck, Model, CarouselModel;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_4) {
			Model = _4['default'];
		}],
		execute: function () {
			//
			// Carousel Model
			//

			'use strict';

			CarouselModel = (function (_Model) {
				_inherits(CarouselModel, _Model);

				function CarouselModel() {
					_classCallCheck(this, CarouselModel);

					_get(Object.getPrototypeOf(CarouselModel.prototype), 'constructor', this).apply(this, arguments);
				}

				return CarouselModel;
			})(Model);

			_export('default', CarouselModel);

			CarouselModel.prototype.defaults = {
				autoplay: true,
				interval: 5000,
				keyboard: true,
				paused: false,
				slide: 1,
				previous: -1,
				total: 0,
				staggerStart: 0.2,
				staggerIncrement: 0.1
			};
		}
	};
});

(function() {
var define = $__System.amdDefine;
define("57", [], function() {
  return "<li role=\"presentation\">\n\t<a class=\"<% if(selected) {%>active<% } %>\" role=\"tab\" aria-controls=\"<%= id %>\" aria-selected=\"<%= selected %>\" alt=\"<%= index %>\" data-index=\"<%= index %>\"></a>\n</li>\n";
});

})();
$__System.register('58', ['4', '5', '6', '7', '14', '15', '56', '57', 'b'], function (_export) {
	var _get, _inherits, _createClass, _classCallCheck, View, Component, CarouselModel, IndicatorTemplate, _Object$assign, Name, DataKey, EventKey, Carousel, CarouselView;

	return {
		setters: [function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_4) {
			_createClass = _4['default'];
		}, function (_3) {
			_classCallCheck = _3['default'];
		}, function (_5) {
			View = _5['default'];
		}, function (_6) {
			Component = _6['default'];
		}, function (_7) {
			CarouselModel = _7['default'];
		}, function (_8) {
			IndicatorTemplate = _8['default'];
		}, function (_b) {
			_Object$assign = _b['default'];
		}],
		execute: function () {
			//
			// Carousel Component
			//

			//
			// Configuration
			//
			'use strict';

			Name = 'Carousel';
			DataKey = 'dls.carousel';
			EventKey = '.' + DataKey;

			//
			// Initiator
			//

			Carousel = (function (_Component) {
				_inherits(Carousel, _Component);

				function Carousel(opts) {
					_classCallCheck(this, Carousel);

					_get(Object.getPrototypeOf(Carousel.prototype), 'constructor', this).call(this, opts);
					this.component = CarouselView;
				}

				//
				// Carousel View
				//
				return Carousel;
			})(Component);

			_export('default', Carousel);

			CarouselView = (function (_View) {
				_inherits(CarouselView, _View);

				function CarouselView(opts) {
					_classCallCheck(this, CarouselView);

					_get(Object.getPrototypeOf(CarouselView.prototype), 'constructor', this).call(this, opts);

					this.setProps();
					this.render();
					this.prepareSlideAnimation();
					this.listen();
					this.refresh();
					this.activateSlide(this.model.get('slide'), true);
				}

				//
				// Getters
				//

				_createClass(CarouselView, [{
					key: 'setProps',

					//
					// Set core props
					//
					value: function setProps() {
						this.sel = {
							slides: this.target.find('.carousel-inner'),
							slide: this.target.find('.carousel-item'),
							controls: this.target.find('.carousel-controls'),
							indicators: this.target.find('.carousel-indicators')
						};

						if (!this.sel.indicators.length) this.sel.indicators = $('<div/>');

						this.target.attr('tabindex', 0);
					}

					//
					// Render
					//
				}, {
					key: 'render',
					value: function render() {
						var _this = this;

						var indicators = '',
						    current = this.model.get('slide'),
						    items = this.target.find('.carousel-inner > .carousel-item');

						items.each(function (idx, el) {
							el = $(el);

							var uid = _this.util.request('uid:gen', 'slide-uid-'),
							    index = idx + 1;

							el.attr('aria-labelledby', uid).attr('aria-hidden', current !== index).attr('role', 'tabpanel');

							indicators += _this.util.request('template:render', IndicatorTemplate, {
								id: uid,
								index: index,
								selected: current === index
							});
						});

						this.model.set('total', items.length);

						this.sel.indicators.empty().append($(indicators));

						return this;
					}

					//
					// Prepare slide animation
					//
				}, {
					key: 'prepareSlideAnimation',
					value: function prepareSlideAnimation() {
						var _this2 = this;

						this.sel.slide.each(function (idx, el) {
							el = $(el);
							_this2.addStaggerAnimation(el);
						});
					}

					//
					// Stagger animation
					//
				}, {
					key: 'addStaggerAnimation',
					value: function addStaggerAnimation(el) {
						var delay = Number(this.model.get('staggerStart')),
						    increment = Number(this.model.get('staggerIncrement')),
						    stagger = el.find('.carousel-anim-stagger');

						stagger.each(function (idx, el) {
							$(el).css({ animationDelay: delay + 's' });
							delay += increment;
						});
					}

					//
					// Listen
					//
				}, {
					key: 'listen',
					value: function listen() {
						var keyboard = this.model.get('keyboard');

						this.sel.indicators.on('click' + EventKey, '> li > a', $.proxy(this.onIndicatorClick, this));

						this.sel.controls.on('click' + EventKey, '.carousel-control', $.proxy(this.onControlClick, this));

						this.model.on('change', this.onChange, this);

						if (keyboard) this.target.on('keydown' + EventKey, $.proxy(this.onKeyDown, this));

						//
						// Expose triggers
						//
						this.on('next', this.next, this).on('prev', this.prev, this).on('pause', this.pause, this).on('cycle', this.cycle, this).on('redraw', this.redraw, this);
					}

					//
					// Indicator Click
					//
				}, {
					key: 'onIndicatorClick',
					value: function onIndicatorClick(e) {
						var el = $(e.currentTarget),
						    index = el.data('index');

						this.set('slide', index);
						e.preventDefault();
					}

					//
					// On Control Click
					//
				}, {
					key: 'onControlClick',
					value: function onControlClick(e) {
						var el = $(e.currentTarget),
						    type = el.data('control');

						if (this[type] !== undefined) {
							e.preventDefault();
							return this[type]();
						}
					}

					//
					// On Key Down
					//
				}, {
					key: 'onKeyDown',
					value: function onKeyDown(e) {
						e.preventDefault();
						if (/input|textarea/i.test(e.target.tagName)) return;
						switch (e.which) {
							case 9:
								this.next();break;
							case 37:
								this.prev();break;
							case 39:
								this.next();break;
							default:
								return;
						}
					}

					//
					// Model changed
					//
				}, {
					key: 'onChange',
					value: function onChange(e) {
						if (e.slide != null) {
							this.activateSlide(e.slide);
						}

						if (e.interval != null) {
							this.cycle();
						}
					}

					//
					// Next Slide
					//
				}, {
					key: 'next',
					value: function next() {
						var _model$attributes = this.model.attributes;
						var slide = _model$attributes.slide;
						var total = _model$attributes.total;

						slide++;

						if (slide > total) slide = 1;

						this.model.set('direction', 'next');
						this.model.set('slide', slide);
						return this;
					}

					//
					// Previous Slide
					//
				}, {
					key: 'prev',
					value: function prev() {
						var _model$attributes2 = this.model.attributes;
						var slide = _model$attributes2.slide;
						var total = _model$attributes2.total;

						slide--;

						if (slide < 1) slide = total;

						this.model.set('direction', 'prev');
						this.model.set('slide', slide);
						return this;
					}

					//
					// Set the slide to the specified index
					//
				}, {
					key: 'activateSlide',
					value: function activateSlide(index, instant) {

						if (index > this.model.get('total')) {
							this.model.attributes.slide = 1;
							index = 1;
						}

						var current = index - 1,
						    slideID = this.sel.indicators.find('> li > a:eq(' + current + ')').attr('aria-controls'),
						    slide = this.sel.slides.find('[aria-labelledby="' + slideID + '"]');

						this.show(slide, instant);
						this.refresh();

						return this;
					}

					//
					// Show the selected slide
					//
				}, {
					key: 'show',
					value: function show(el, instant) {
						var _model$attributes3 = this.model.attributes;
						var slide = _model$attributes3.slide;
						var previous = _model$attributes3.previous;
						var direction = _model$attributes3.direction;
						var offset = 0;
						var others = this.sel.slide.not(el[0]);
						var isNext = direction === 'next' || slide > previous && direction == null;
						var isPrev = direction === 'prev' || slide < previous && direction == null;

						if (direction !== null) {
							this.model.set('direction', null);
						}

						offset = isNext ? 60 : isPrev ? -60 : 0;

						el.css({
							transform: instant ? '' : 'translateX(' + offset + 'px)',
							opacity: instant ? 1 : 0
						});

						this.sel.slides.append(el);

						setTimeout(function () {
							others.css({
								opacity: 0
							});

							el.css({
								transform: 'translateX(0px)',
								opacity: 1
							});
						}, 100);

						el.addClass('in').addClass('carousel-slide-active').attr('aria-hidden', false);

						others.removeClass('carousel-slide-active').removeClass('in').attr('aria-hidden', true);

						this.model.set('previous', this.model.get('slide'));
						this.cycle();
					}

					//
					// Pause
					//
				}, {
					key: 'pause',
					value: function pause() {
						this.model.set('paused', true);
						clearTimeout(this.timeout);

						return this;
					}

					//
					// Play
					//
				}, {
					key: 'play',
					value: function play() {
						this.model.set('autoplay', true);
						this.model.set('paused', false);
						this.cycle();

						return this;
					}

					//
					// Cycle
					//
				}, {
					key: 'cycle',
					value: function cycle() {
						var _this3 = this;

						var _model$attributes4 = this.model.attributes;
						var autoplay = _model$attributes4.autoplay;
						var paused = _model$attributes4.paused;
						var interval = _model$attributes4.interval;

						if (!autoplay || paused) return;

						clearTimeout(this.timeout);
						this.timeout = setTimeout(function () {
							_this3.next();
						}, interval);

						return this;
					}

					//
					// Refresh
					//
				}, {
					key: 'refresh',
					value: function refresh() {
						var current = this.model.get('slide') - 1;

						this.sel.indicators.find('> li > a:eq(' + current + ')').addClass('active').attr('aria-selected', true);

						this.sel.indicators.find('> li > a:not(:eq(' + current + '))').removeClass('active').attr('aria-selected', false);
					}

					//
					// Redraw the UI elements of the carousel (such as the indicators),
					// May be used in the circumstance that an item is added to the carousel.
					//
				}, {
					key: 'redraw',
					value: function redraw() {
						return this.render();
					}

					//
					// Interface
					//
				}, {
					key: 'destroy',

					//
					// Destroy
					//
					value: function destroy() {
						$.removeData(this.target, DataKey);
					}
				}], [{
					key: '_interface',
					value: function _interface(config) {
						var $el = $(this),
						    data = $el.data(DataKey);

						config = _Object$assign($el.data(), config);

						if (!data) {
							data = new CarouselView({
								target: $el,
								model: new CarouselModel(config)
							});

							$el.data(DataKey, data);
						}

						return data;
					}
				}, {
					key: 'Name',
					get: function get() {
						return Name;
					}
				}, {
					key: 'EventKey',
					get: function get() {
						return EventKey;
					}
				}]);

				return CarouselView;
			})(View);
		}
	};
});

$__System.register('59', ['3', '4', '5', '6', '7', '13', '17', '19', '22', '24', '27', '32', '35', '58', '1b', '1d', '2a', '2c'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Select, Accordion, Collapse, Modal, Search, Slider, Tooltip, SmartField, Carousel, Dismiss, Dropdown, Stepper, Tabs, Components;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_6) {
			Select = _6['default'];
		}, function (_7) {
			Accordion = _7['default'];
		}, function (_8) {
			Collapse = _8['default'];
		}, function (_9) {
			Modal = _9['default'];
		}, function (_10) {
			Search = _10['default'];
		}, function (_11) {
			Slider = _11['default'];
		}, function (_12) {
			Tooltip = _12['default'];
		}, function (_13) {
			SmartField = _13['default'];
		}, function (_14) {
			Carousel = _14['default'];
		}, function (_b) {
			Dismiss = _b['default'];
		}, function (_d) {
			Dropdown = _d['default'];
		}, function (_a) {
			Stepper = _a['default'];
		}, function (_c) {
			Tabs = _c['default'];
		}],
		execute: function () {
			//
			// Components
			// Manages the creation of DLS components
			//

			'use strict';

			Components = (function (_Obj) {
				_inherits(Components, _Obj);

				function Components(opts) {
					_classCallCheck(this, Components);

					_get(Object.getPrototypeOf(Components.prototype), 'constructor', this).call(this, opts);

					this.component = {
						accordion: new Accordion(),
						collapse: new Collapse(),
						dismiss: new Dismiss(),
						dropdown: new Dropdown(),
						modal: new Modal(),
						search: new Search(),
						select: new Select(),
						slider: new Slider(),
						stepper: new Stepper(),
						tabs: new Tabs(),
						tooltip: new Tooltip(),
						smartfield: new SmartField(),
						carousel: new Carousel()
					};

					this.register();
					this.listen();
				}

				//
				// Register the components (for external access)
				//

				_createClass(Components, [{
					key: 'register',
					value: function register() {
						for (var key in this.component) {
							this[key] = this.component[key];
						}
					}

					//
					// Bind the listeners
					//
				}, {
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.app.on('render', function () {
							for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
								args[_key] = arguments[_key];
							}

							_this.render(args);
						});
					}

					// Render the components asynchronously
					// (generally would not be used but may be used in a web-app situation
					// where components may be added dynamically).
				}, {
					key: 'render',
					value: function render() {
						for (var key in this.component) {
							this[key].trigger('render');
						}
					}
				}]);

				return Components;
			})(Obj);

			_export('default', Components);
		}
	};
});

$__System.register('5a', ['3', '4', '5', '6', '7', '8', '11', '55', '59'], function (_export) {
	var Obj, _get, _inherits, _createClass, _classCallCheck, Platform, Util, EventAggregator, Components, DLSApp;

	return {
		setters: [function (_5) {
			Obj = _5['default'];
		}, function (_) {
			_get = _['default'];
		}, function (_2) {
			_inherits = _2['default'];
		}, function (_3) {
			_createClass = _3['default'];
		}, function (_4) {
			_classCallCheck = _4['default'];
		}, function (_7) {
			Platform = _7['default'];
		}, function (_8) {
			Util = _8['default'];
		}, function (_6) {
			EventAggregator = _6['default'];
		}, function (_9) {
			Components = _9['default'];
		}],
		execute: function () {
			//
			// DLS
			//
			// It all starts here.

			'use strict';

			DLSApp = (function (_Obj) {
				_inherits(DLSApp, _Obj);

				function DLSApp(opts) {
					_classCallCheck(this, DLSApp);

					_get(Object.getPrototypeOf(DLSApp.prototype), 'constructor', this).call(this, opts);

					this.prepare();

					Obj.prototype.app = this;
					Obj.prototype.scope = $(this.scope);

					this.createVent();
					this.registerPlatform();
					this.createUtils();

					this.listen();
				}

				// Prepare

				_createClass(DLSApp, [{
					key: 'prepare',
					value: function prepare() {
						if (!this.scope) this.scope = $('body');
					}

					// Create the vent event aggregator that we can
					// subscribe and publish events to. A la Marionette
				}, {
					key: 'createVent',
					value: function createVent() {
						this.vent = new EventAggregator();
					}

					// Register Platform
				}, {
					key: 'registerPlatform',
					value: function registerPlatform() {
						Obj.prototype.platform = new Platform();
					}
				}, {
					key: 'createUtils',

					// Create the utility helpers
					value: function createUtils() {
						Obj.prototype.util = new Util({
							vent: this.vent
						});
					}

					// Bind our key events
				}, {
					key: 'listen',
					value: function listen() {
						var _this = this;

						this.on('before:start', function (e) {
							_this.beforeStart();
						}).on('after:start', function (e) {
							_this.afterStart();
						});
					}

					// Before Start tasks
				}, {
					key: 'beforeStart',
					value: function beforeStart() {}

					// After start tasks
				}, {
					key: 'afterStart',
					value: function afterStart() {}

					// Start the application
				}, {
					key: 'start',
					value: function start() {
						this.emit('before:start');
						this.initialize();
						this.emit('after:start');
					}

					// The initial render, should only occur internally to
					// start the modules
				}, {
					key: 'initialize',
					value: function initialize() {
						this.createComponents();
						this.createModules();
					}

					// Manually render DLS
					// Accessible via DLS.trigger('render', {opts});
					// or DLS.render({opts})
				}, {
					key: 'render',
					value: function render() {
						this.trigger('render');
						return this;
					}

					// Shorthand to create component
				}, {
					key: 'create',
					value: function create(type, params) {
						if (!this.component[type]) {
							console.warn('DLS: Component of type `' + type + '` does not exist!');
							return this;
						}

						return this.component[type].create(params);
					}

					// Create the components
				}, {
					key: 'createComponents',
					value: function createComponents() {
						this.component = new Components();
					}

					// Create and start all the supplied app modules
				}, {
					key: 'createModules',
					value: function createModules() {
						var _this2 = this;

						$.each(this.modules, function (key, Module) {
							_this2.modules[key] = new Module();
						});
					}
				}]);

				return DLSApp;
			})(Obj);

			_export('default', DLSApp);
		}
	};
});

$__System.register('1', ['5a'], function (_export) {
	//
	// DLS
	//
	// Create the main application and pass across our
	// base config / options

	'use strict';

	var DLSApp;
	return {
		setters: [function (_a) {
			DLSApp = _a['default'];
		}],
		execute: function () {
			window.DLS = new DLSApp({
				scope: $('body'),
				modules: {}
			});

			DLS.start();
		}
	};
});

})
(function(factory) {
  factory();
});