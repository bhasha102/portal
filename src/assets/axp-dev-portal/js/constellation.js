
/*
	Copyright (c) 2016 by Acauã Montiel (http://codepen.io/acauamontiel/pen/mJdnw)

	Permission is hereby granted, free of charge, to any person obtaining a copy of this
	software and associated documentation files (the "Software"), to deal in the Software
	without restriction, including without limitation the rights to use, copy, modify, merge,
	publish, distribute, sublicense, and/or sell copies of the Software, and to permit
	persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial
	portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
	BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
	DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/*
	Debugging by Paul Wright
*/

(function() {

function Constellation (canvas, options) {
 	var $canvas = $(canvas),
 	context = canvas.getContext('2d'),
 	rAF = false,
 	defaults = {
 		star: {
 			color: 'rgba(255, 255, 255, 0.5)',
 			width: 8
 		},
 		line: {
 			color: 'rgba(255, 255, 255, 0.5)',
 			width: 2
 		},
 		position: {
				x: 0.1, // This value will be overwritten at startup
				y: 0 // This value will be overwritten at startup
			},
		width: $canvas.innerWidth(),
		height: $canvas.innerHeight(),
		velocity: 0.02,
		starCount: 50,
		distance: 200,
		radius: 10,
		stars: []
	},
	config = $.extend(true, {}, defaults, options);

	function Star () {
		this.x = Math.random() * canvas.width;
		this.y = Math.random() * canvas.height;
		this.vx = (config.velocity - (Math.random() * 0.5));
		this.vy = (config.velocity - (Math.random() * 0.5));
		this.radius = Math.random() * config.star.width;
	}

	Star.prototype = {
		draw: function(){
			for (var i = 0; i < config.starCount; i++) {
				var star = config.stars[i];
				context.beginPath();
				context.arc(star.x, star.y, star.radius, 0, Math.PI * 2);
				context.fill();
			}
		},

		animate: function(){
			var i;
			for (i = 0; i < config.starCount; i++) {
				var star = config.stars[i];


				if (star.y < 0 || star.y > canvas.height) {
					star.vx = star.vx;
					star.vy = - star.vy;


				} else if (star.x < 0 || star.x > canvas.width) {
					star.vx = - star.vx;
					star.vy = star.vy;
				}

				star.x += star.vx;
				star.y += star.vy;
			}
		},

		line: function(){
			var length = config.starCount,
			iStar,
			jStar,
			i,
			j;

			for (i = 0; i < length; i++) {
				for (j = 0; j < length; j++) {
					iStar = config.stars[i];
					jStar = config.stars[j];

					if (
						(iStar.x - jStar.x) < config.distance &&
						(iStar.y - jStar.y) < config.distance &&
						(iStar.x - jStar.x) > - config.distance &&
						(iStar.y - jStar.y) > - config.distance
						) {
						if (
							(iStar.x - config.position.x) < config.radius &&
							(iStar.y - config.position.y) < config.radius &&
							(iStar.x - config.position.x) > - config.radius &&
							(iStar.y - config.position.y) > - config.radius
							) {
							context.beginPath();
							context.moveTo(iStar.x, iStar.y);
							context.lineTo(jStar.x, jStar.y);
							context.stroke();
							context.closePath();
						}
					}
				}
			}
		}
	};

	this.createStars = function () {
		config.stars = [];

		while (config.stars.length < config.starCount) {
			var newStar = new Star();
			config.stars.push(newStar);
		}
	};

	this.animate = function() {
		context.clearRect(0, 0, canvas.width, canvas.height);
		Star.prototype.animate();
		Star.prototype.draw();
		Star.prototype.line();
	};

	this.setCanvas = function () {
		canvas.width = config.width;
		canvas.height = config.height;
	};

	this.setContext = function () {
		context.clearRect(0, 0, canvas.width, canvas.height);
		context.fillStyle = config.star.color;
		context.strokeStyle = config.line.color;
		context.lineWidth = config.line.width;
		context.globalCompositeOperation ='destination-over';
		// context.globalAlpha = 0.5;
	};

	this.setInitialPosition = function () {
		if (!options || !options.hasOwnProperty('position')) {
			config.position = {
				x: canvas.width * 0.6,
				y: canvas.height * 0.6
			};
		}
	};

	this.loop = function (callback) {
		callback();

		rAF = requestAnimationFrame(function () {
			this.loop(callback);
		}.bind(this));
	};

	this.bindmouse = function () {
		$canvas.on('mousemove', function(e){
			config.position.x = e.pageX - $canvas.offset().left;
			config.position.y = e.pageY - $canvas.offset().top;
		});
	};

	this.init = function () {

		this.setCanvas();
		this.setContext();
		this.setInitialPosition();
		this.createStars();
		if( !(navigator.userAgent.toLowerCase().indexOf('firefox') > -1) ) {
			this.loop(this.animate);
		}
		// this.bindmouse();
		// this.bindresize();

	};

}

$.fn.constellation = function (options) {
	return this.each(function () {
		var c = new Constellation(this, options);
		c.init();
	});
};

})();