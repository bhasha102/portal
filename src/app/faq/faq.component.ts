import { Component, OnInit } from '@angular/core';
import { FaqService } from './faq.service';
import { FAQ } from './faq';
import { Observable } from 'rxjs/Rx';
import { FilterPipe } from './faqfilter.pipe'

@Component({
    selector: 'faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.less']
})

export class FaqComponent implements OnInit {
    pageTitle: string;
    question: FAQ[];
    errorMessage: string;
    pageSize:number = 10;
    pageStart:number = 0;

    public constructor(private faqService: FaqService) {
    }

    ngOnInit() {
        this.getQuestion();
    }

    getQuestion() {
        this.faqService.getQuestions().subscribe(
            question => this.question = question,
            error => { console.log(error) });
    }
}