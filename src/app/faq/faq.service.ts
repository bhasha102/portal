import { Injectable } from '@angular/core';
import { FAQ } from './faq';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class FaqService {
  private baseUrl: string = "./app/faq/faq.json";
  constructor(private http: Http) {
  }
  getQuestions(): Observable<FAQ[]> {
    return this.http.get(this.baseUrl)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }

}