import { Pipe, PipeTransform } from '@angular/core';
import { FAQ } from './faq';

@Pipe({
    name: 'faq_filter',
})
export class FilterPipe implements PipeTransform {

    transform(question: FAQ[], nameFilter: string): any {
        if (nameFilter === undefined) {
            return question;
        }

        return question.filter(function (qt) {
            if (qt.questn.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt.questn.toLowerCase().includes(nameFilter.toLowerCase());
            if (qt.answer.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt.answer.toLowerCase().includes(nameFilter.toLowerCase());
        })
    }
}