import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { ProductListComponent } from './product/product-list/product-list.component';
import { ProductDetailComponent } from './product/product-details/product-detail.component';
import { ApiReferenceComponent } from './product/product-details/api-reference.component';
import { ManageUserComponent } from './admin/platform/manageuser.component';
import { AddPlatformComponent } from './admin/platform/add-platform/addplatform.component';
import { EditPlatformComponent } from './admin/platform/edit-platform/editplatform.component';
import { AdminProductComponent } from './admin/product/add-product/addproduct.component';
import { DeletePlatformComponent } from './admin/platform/delete-platform/delete-platform.component';
import { DeleteProductComponent } from './admin/product/delete-product/delete-product.component';
import { AdminEditProductComponent } from './admin/product/edit-product/editproduct.component';
//-------
import { FaqComponent } from './faq/faq.component';
import { EauthComponent } from './eauth/eauth.component';
import { PlatformAdminComponent } from './platform-admin/platform-admin.component';
import { PublicProductComponent } from './product/product-list/public-product.component';
import { UserProductComponent } from './product/product-list/user-product.component';
import { AdminComponent } from './admin/admin.component';
import { MapPlatformProductComponent } from './admin/map-platform-product/mapplatformproduct.component';
import { ViewProductsComponent } from './admin/view-products/view-products.component';
import { ViewAllUsersComponent } from './admin/viewAllUsers/view-all-users.component';

import { PA_ManageUserComponent } from './platform-admin/manageUsers/pa-manageUsers.component';
import { PA_AddProductComponent } from './platform-admin/products/addProduct/pa-addProduct.component';
import { PA_EditProductComponent } from './platform-admin/products/editProduct/pa-editProduct.component';
import { PA_DeleteProductComponent } from './platform-admin/products/deleteProduct/pa-deleteProduct.component';

import { ExternalAdminComponent } from './ext-admin/ext-admin.component';
import { External_ManageUserComponent } from './ext-admin/manageUsers/ext-manageUsers.component';
import { External_AddProductComponent } from './ext-admin/products/addProduct/ext-addProduct.component';
import { External_EditProductComponent } from './ext-admin/products/editProduct/ext-editProduct.component';
import { External_DeleteProductComponent } from './ext-admin/products/deleteProduct/ext-deleteProduct.component';

import { AddPartnerComponent } from './ext-admin/partner/add-partner/addpartner.component';
import { EditPartnerComponent } from './ext-admin/partner/edit-partner/editpartner.component';
import { DeletePartnerComponent } from './ext-admin/partner/delete-partner/delete-partner.component';
import { MapPartnerProductComponent } from './ext-admin/map-partner-product/mappartnerproduct.component';
import { TokenPartnerComponent } from './ext-admin/partner-token/tokenpartner.component';
import { createAppEauthComponent } from './eauth/create-App/create-App.component';
import { searchAppComponent } from './eauth/search-App/search.applicationName/search.applicationName.component';
import { searchMerchantComponent } from './eauth/search-App/search.merchantName/search.merchantName.component';

import { HomeComponent } from './home/home.component';
import { AppDetailsComponent } from './app-details-approval/app-details/appdetails.component';
import { AppInformationComponent } from './app-details-approval/app-information/appinformation.component';
import { UserProfileComponent } from './app-details-approval/user-profile/userprofile.component';
import { WorkflowHistoryComponent } from './app-details-approval/workflow-history/workflowhistory.component';

import { ApplistComponent } from './view-user-apps/applist/applist.component';
import { CreateAppComponent } from './view-user-apps/createapp/createapp.component';
import { ViewUserAppsComponent } from './view-user-apps/view-user-apps.component';
import { NotfoundComponent } from './notfound/notfound.component';


const APP_ROUTE: Routes = [

    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    {
        path: 'products', component: ProductListComponent, children: [
            { path: '', component: PublicProductComponent, outlet: 'product-nav', redirectTo: 'publicProduct' },
            { path: 'publicProduct', component: PublicProductComponent, outlet: 'product-nav' },
            { path: 'userProduct', component: UserProductComponent, outlet: 'product-nav' },
            { path: '**', redirectTo: '404' }
        ]
    },
    { path: 'productdetails/:id/:name', component: ProductDetailComponent, data: [{ isPublic: true }] },
    { path: 'productdetails/:id/:name/:roleId/:orgId', component: ProductDetailComponent, data: [{ isPublic: false }] },
    { path: 'apiReference/:id/:name', component: ApiReferenceComponent, data: [{ isPublic: true }] },
    { path: 'apiReference/:id/:name/:roleId/:orgId', component: ApiReferenceComponent, data: [{ isPublic: false }] },
    { path: 'faq', component: FaqComponent },

    {
        path: 'admin', component: AdminComponent, children: [
            { path: '', component: ManageUserComponent, outlet: 'admin-nav', redirectTo: 'platform' },
            { path: 'platform', component: ManageUserComponent, outlet: 'admin-nav' },
            { path: 'addPlatform', component: AddPlatformComponent, outlet: 'admin-nav' },
            { path: 'deletePlatform', component: DeletePlatformComponent, outlet: 'admin-nav' },
            { path: 'deleteProduct', component: DeleteProductComponent, outlet: 'admin-nav' },
            { path: 'editPlatform', component: EditPlatformComponent, outlet: 'admin-nav' },
            { path: 'adminaddproduct', component: AdminProductComponent, outlet: 'admin-nav' },
            { path: 'admineditproduct', component: AdminEditProductComponent, outlet: 'admin-nav' },
            { path: 'mapplatformproduct', component: MapPlatformProductComponent, outlet: 'admin-nav' },
            { path: 'adminviewproduct', component: ViewProductsComponent, outlet: 'admin-nav' },
            { path: 'viewallusers', component: ViewAllUsersComponent, outlet: 'admin-nav' },
            { path: '**', redirectTo: '404' }
            //------
        ]
    },
    {
        path: 'eauth', component: EauthComponent, children: [
            { path: '', component: createAppEauthComponent, outlet: 'Eauth-nav', redirectTo: 'create-App' },
            { path: 'create-App', component: createAppEauthComponent, outlet: 'Eauth-nav' },
            { path: 'search.applicationName', component: searchAppComponent, outlet: 'Eauth-nav' },
            { path: 'search.merchantName', component: searchMerchantComponent, outlet: 'Eauth-nav' },
            { path: '**', redirectTo: '404' }
        ]
    },
    {
        path: 'platformAdmin', component: PlatformAdminComponent, children: [
            { path: '', component: PA_ManageUserComponent, outlet: 'platformAdmin-nav', redirectTo: 'pa-platform' },
            { path: 'pa-platform', component: PA_ManageUserComponent, outlet: 'platformAdmin-nav' },
            { path: 'pa_addproduct', component: PA_AddProductComponent, outlet: 'platformAdmin-nav' },
            { path: 'pa_editproduct', component: PA_EditProductComponent, outlet: 'platformAdmin-nav' },
            { path: 'pa_deleteproduct', component: PA_DeleteProductComponent, outlet: 'platformAdmin-nav' },
            { path: '**', redirectTo: '404' }

        ]
    },
    {
        path: 'myapps', component: ViewUserAppsComponent, children: [
            { path: '', component: ApplistComponent, outlet: 'myapps-nav', redirectTo: 'myAppList' },
            { path: 'myAppList', component: ApplistComponent, outlet: 'myapps-nav' },
            { path: 'createMyApp', component: CreateAppComponent, outlet: 'myapps-nav' },
            { path: '**', redirectTo: '404' }
        ]
    },
    {
        path: 'extAdmin', component: ExternalAdminComponent, children: [
            { path: '', component: External_ManageUserComponent, outlet: 'externalAdmin-nav', redirectTo: 'ext-manageUsers' },
            { path: 'ext-manageUsers', component: External_ManageUserComponent, outlet: 'externalAdmin-nav' },
            { path: 'ext_addproduct', component: External_AddProductComponent, outlet: 'externalAdmin-nav' },
            { path: 'ext_editproduct', component: External_EditProductComponent, outlet: 'externalAdmin-nav' },
            { path: 'ext_deleteproduct', component: External_DeleteProductComponent, outlet: 'externalAdmin-nav' },
            { path: 'addPartner', component: AddPartnerComponent, outlet: 'externalAdmin-nav' },
            { path: 'editPartner', component: EditPartnerComponent, outlet: 'externalAdmin-nav' },
            { path: 'deletepartner', component: DeletePartnerComponent, outlet: 'externalAdmin-nav' },
            { path: 'ext_mappartnerproduct', component: MapPartnerProductComponent, outlet: 'externalAdmin-nav' },
            { path: 'ext_partnerToken', component: TokenPartnerComponent, outlet: 'externalAdmin-nav' },
            { path: '**', redirectTo: '404' }
        ]
    },
    { path: '404', component: NotfoundComponent },
    { path: '**', redirectTo: '404' },
    {
        path: 'orgAdminActivities', component: AppDetailsComponent, children: [
            { path: '', component: UserProfileComponent, outlet: 'appdetails-nav', redirectTo: 'userProfile' },
            { path: 'userProfile', component: UserProfileComponent, outlet: 'appdetails-nav' },
            { path: 'appInformation', component: AppInformationComponent, outlet: 'appdetails-nav' },
            { path: 'workflowHistory', component: WorkflowHistoryComponent, outlet: 'appdetails-nav' },
            { path: '**', redirectTo: '404' }
        ]
    }
]

export const routing = RouterModule.forRoot(APP_ROUTE);



