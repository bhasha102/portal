import { Component, OnInit  } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import {Observable} from 'rxjs/Rx';
import { SearchApp} from './search.applicationName';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
// import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { CreateEAuthService } from '../../create-App/create-App.service';

@Component({
    selector: 'eauth',
        templateUrl:'./search.applicationName.component.html',
        styleUrls :  ['./search.applicationName.component.less']
})
export class searchAppComponent{
public submitted: boolean;
public myForm: FormGroup; // our model driven form
errorMessage:string;
updatedMsg :boolean = false;
searchapp:any;
searchappdetail:SearchApp;
isValid:boolean=false;
i:number;
clientId:string;
clientSecret:string;
errorMsg : boolean = false;
NoMsg:boolean=false;
SuccessMsg:boolean=false;
viewSearchProgress:boolean = false;
applicationName:string;


constructor(private _fb: FormBuilder,private _searchApp : CreateEAuthService) { } 

ngOnInit(){ this.myForm = this._fb.group({
             applicationName: [''],
               });

   //this.getPlatformList();
   
// this.myForm.controls['applicationName'].valid = true;   
// this.myForm.controls['clientId'].valid = true;   
// this.myForm.controls['clientSecret'].valid = true;   

}
save(formValue, isValid: boolean) {
       let input = new Object();
               input['applicationName']= this.applicationName;      


 this._searchApp.getApplicationNameDetails(input).subscribe(
    searchappdetail => { this.searchapp = searchappdetail;
                           
 
                    if(this.searchapp['client_id']!=null)
                    {
                    console.log("Success");
                    this.updatedMsg = true;
                    this.updatePlatformDetails();
                    this.isValid=true;
                    this.NoMsg=false;
                    }
                   
                    else {
                   this.NoMsg=true;
                    } },
                      error => { console.log(error) },
            () => { console.log(this.searchapp) }
        
 )
}
    removeUpdateMsg()
    {
        this.updatedMsg = false;
    }

    // getPlatformList(item)
    //  {
    //     this._searchApp.getPlatformDetails(item).subscribe(
    //                    searchappdetail => {this.searchapp = searchappdetail;
    //                        this.updatePlatformDetails();
    //                     },
    //                     error =>  {console.log(error)});
                        
    //  }
     updatePlatformDetails()
     {
         this.clientId = this.searchapp['client_id'];
         this.clientSecret=this.searchapp['client_secret'];
         
         
     }

    //  check(item) {
    //      //this.viewSearchProgress = true;
    //      for (this.i = 0; this.i <= this.searchapp[0].applicationName.length; this.i++) {

    //          if (item == this.searchapp[0].applicationName) {
    //              this.SuccessMsg = true;
    //              this.isValid = true;
    //              this.NoMsg = false;
    //          }
    //          else {
    //          this.NoMsg = true;
    //              this.SuccessMsg = false;
    //              this.isValid = false;
    //          }
    //      }
    //     // this.viewSearchProgress = false;
    //  }

     onChange() {
         console.log(this.updatedMsg);
         this.updatedMsg = false;
         this.errorMsg = false;
     }
     
}