import { Component, OnInit  } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import {Observable} from 'rxjs/Rx';
import {MerchantApp} from './search.merchantName';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { CreateEAuthService } from '../../create-App/create-App.service';


@Component({
    selector: 'eauth',
        templateUrl:'./search.merchantName.component.html',
        styleUrls :  ['./search.merchantName.component.less']
})
export class searchMerchantComponent implements OnInit{
public submitted: boolean;
errorMessage:string;
public myForm: FormGroup;
updatedMsg :boolean = false;
merchant:any;
eAuthList:MerchantApp[];
createappdetail:MerchantApp[];
i:number;
isValid:boolean=false;
clientId:string;
clientSecret:string;
NoMsg :boolean=false;
SuccessMsg:boolean=false;
merchantName:string;
merchantapp:any;
errorMsg : boolean = false;
name:String;

constructor( private _fb: FormBuilder,private _merchant:CreateEAuthService) { } 

ngOnInit(){ this.myForm = this._fb.group({
            merchantName: [''],
               });

}
save(formValue, isValid: boolean) {
       let input = new Object();
               input['merchantName']= this.merchantName;      


 this._merchant.getMerchantNameDetails(input).subscribe(
    searchappdetail => { this.merchantapp = searchappdetail;
                           
 
                    if(this.merchantapp['app'][0]['consumerSecret']!=null)
                    {
                    console.log("Success");
                    this.updatedMsg = true;
                    this.updatePlatformDetails();
                    this.isValid=true;
                    }
                   
                    else {
                   this.NoMsg=true;
                    } },
                      error => { console.log(error) },
            () => { console.log(this.merchantapp) }
        
 )
}
    removeUpdateMsg()
    {
        this.updatedMsg = false;
    }

     updatePlatformDetails()
     {
         this.clientId = this.merchantapp['app'][0]['consumerKey'];
         this.clientSecret=this.merchantapp['app'][0]['consumerSecret'];
         this.name=this.merchantapp['app'][0]['name']
         
         
     }

     onChange() {
         console.log(this.updatedMsg);
         this.updatedMsg = false;
         this.errorMsg = false;
     }
     
}