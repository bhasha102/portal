import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { CreateApp } from './create-App';
import { authProd } from './create-Application';
import { CreateEAuthService } from './create-App.service';


@Component({
    selector: 'create-App',
    templateUrl: './create-App.component.html',
    styleUrls: ['./create-App.component.less']
})
export class createAppEauthComponent implements OnInit {

    public myForm: FormGroup; // our model driven form
    public submitted: boolean;
    errorMessage: string;
    updatedMsg: boolean = false;
    createapp: CreateApp;
    createappdetail: CreateApp[];
    emailValue: string;
    eAuthList: authProd[];
    prodList: authProd[];
    saveList: Array<string> = [];
    newProductList: Array<authProd>;
    returnVal: any;
    isValid: boolean = false;
    failureMsg: string = "";
    isFailure: boolean = false;
    prodNameList: authProd[];
    temp1: String[];
    isSubmitted: boolean = true;
    clientId: string;
    applicationName: string;
    clientSecret: string;

    constructor(private _fb: FormBuilder, private _createApp: CreateEAuthService) { }

    ngOnInit() {
        this.myForm = this._fb.group({
            applicationName: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            merchantName: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            volume: ['', [<any>Validators.required, Validators.pattern("[0-9]*")]],
            callbackURL: ['', [<any>Validators.required, Validators.pattern("https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,}")]],
        });

        this.getPlatformList();
    }
    // save(model: any, isValid: boolean) {

    //         this.updatedMsg = true;
    //         // check if model is valid
    //         // if valid, call API to save customer
    //         console.log(model, isValid);
    //         //console.log(this.prodList);
    //         //this.myForm.reset();
    //         this.submitted = true; // set form submit to true
    //         console.log("1"+model.applicationName);
    //         console.log("2"+model.callbackURL);
    //         console.log("3"+model.volume);
    //         console.log("4"+this.createapp['e-mail']);
    //         // console.log("5"+);
    //          console.log("6"+model.merchantName);

    //          this.submitted = true; 

    //     }
    save(formValue, isValid) {
        console.log(formValue);
        console.log("list print" + this.prodNameList);
        console.log("length" + this.prodNameList.length)

        // for(let i=0;i< this.prodNameList.length;i++)
        // {
        //     this.temp1[i]=this.prodNameList[i].prodName;
        // }
        let input = new Object();
        var input1 = new FormData();

        // input['publicGuid']=this.apigeeListResp['publicProducts'].publicGuid;
        input['productsIds'] = this.prodNameList;

        input['applicationName'] = formValue.applicationName;
        input['merchantName'] = formValue.merchantName;
        input['e_mail'] = this.createapp['e-mail'];
        input['volume'] = formValue.volume;
        input['callbackURL'] = formValue.callbackURL;

        console.log(formValue);
        this._createApp.saveEauthApplicationDetails(input).subscribe(
            adminPl => {
                this.returnVal = adminPl
                console.log(this.returnVal['client_id'] != null)
                if (this.returnVal['client_id'] != null) {
                    console.log("Success");
                    this.updatedMsg = true;
                    this.isSubmitted = false;
                    this.isValid = true;
                    console.log("hufhricbfrybcfyrbc" + this.returnVal['appName'])
                    console.log("hufhricbfrybcfyrbc" + this.returnVal['client_id'])
                    this.applicationName = this.returnVal['appName'];
                    this.clientId = this.returnVal['client_id'];
                    this.clientSecret = this.returnVal['client_id'];
                }

                else {

                    this.failureMsg = "Failed to create App. Please try after some time.";
                    console.log("Failure");
                    this.updatedMsg = false;
                    this.isFailure = true;
                    this.isSubmitted = true;
                }
            }
            ,
            error => { console.log(error) },
            () => { console.log(this.returnVal) }
        )

    }

    removeUpdateMsg() {
        this.updatedMsg = false;
    }

    getPlatformList() {
        this._createApp.getPlatformDetails().subscribe(
            createappdetail => {
                this.createapp = createappdetail;
                this.updatePlatformDetails();
            },
            error => { console.log(error) });

    }
    updatePlatformDetails() {
        //this.myForm.patchValue({'city':this.platformDetails[0].city},{'orgName': this.platformDetails[0].orgName});

        // console.log(this.createapp);
        // this.myForm.controls['email'].patchValue(this.createapp['email']);
        // this.myForm.controls['prodName'].patchValue(this.createapp);

        this.emailValue = this.createapp['e-mail'];
        this.eAuthList = this.createapp['eauthproducts'];
        this.prodNameList = this.eAuthList;
        console.log(this.createapp['e-mail']);



    }
    onClick() {
        this.isSubmitted = true;
        this.isValid = false;
        this.myForm.reset();



    }
}