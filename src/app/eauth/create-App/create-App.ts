
import {authProd} from './create-Application'


export interface CreateApp
{
    eauthproducts:authProd[],
    applicationName:string,
    merchantName : string,
    'e-mail' : string,
    volume : string,
    callbackURL : string

}


