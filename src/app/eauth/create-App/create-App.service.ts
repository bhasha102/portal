
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { CreateApp } from './create-App';
import { JsonpModule } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Component, EventEmitter, Input, OnChanges, OnInit, ViewEncapsulation, ElementRef, Output } from '@angular/core';

@Injectable()
export class CreateEAuthService {
  private baseUrl_Eauth: string = 'http://localhost:8085/dev-portal-intra/eAuthAdminSubmit'; //"./app/eauth/create-App/create-App.component.json";//'http://localhost:8085/dev-portal-intra/eAuthActivites/';
  private baseUrl: string = 'http://localhost:8085/dev-portal-intra/eAuthActivites/';
  private baseUrl1: string = 'http://localhost:8085/dev-portal-intra/srchbyappnm';//"./app/eauth/search-App/search.applicationName/search.applicationName.component.json";
 private baseUrl2: string = 'http://localhost:8085/dev-portal-intra/srchbymernm/';
  constructor(private http: Http) {
  }

  getPlatformDetails(): Observable<CreateApp> {
    return this.http.get(this.baseUrl, { withCredentials: true })
      // ...and calling .json() on the response to return data
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw('Server error'))
  }

  saveEauthApplicationDetails(formData: Object): Observable<JSON> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json')
    return this.http.post(this.baseUrl_Eauth, formData, { headers: headers, withCredentials: true })// ...using post request
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error')); //...errors if any
  }

  getApplicationNameDetails(app: Object): Observable<JSON> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.baseUrl1, app, { headers: headers, withCredentials: true })
      // ...and calling .json() on the response to return data
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw('Server error'))
  }

  getMerchantNameDetails(app: Object): Observable<JSON> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.baseUrl2, app, { headers: headers, withCredentials: true })
      // ...and calling .json() on the response to return data
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw('Server error'))
  }

}


