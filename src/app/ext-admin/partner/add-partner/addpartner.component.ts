import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Partner } from '../partner';
import { AddPartnerService } from './addpartner.service';
import { Country } from '../country';
import { States } from '../states';

@Component({

    selector: 'addPartner',
    templateUrl: './addpartner.component.html',
    styleUrls: ['./addpartner.component.less']
})
export class AddPartnerComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
    country: Array<Country>;
    states: Array<States>;
    errorMessage: string;
    selectCountry: string = 'US';
    selectStates: string = '001';
    updatedMsg: boolean = false;
    returnVal: JSON;
    isFailure = false;
    failureMsg = "";
    partnerNameResp = JSON;
    isNamenotAvailable = false;
    constructor(private _fb: FormBuilder, private _addplatformService: AddPartnerService) { } // form builder simplify form initialization

    ngOnInit() {
        this.myForm = this._fb.group({
            partnerName: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            phoneNumber: ['', [<any>Validators.required]],
            addressline1: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            addressline2: [''],
            city: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z][a-zA-Z0-9 \-]*")]],
            zip: ['', [<any>Validators.required, Validators.minLength(5), Validators.maxLength(5)]],
            country: [''],
            states: ['']
        });

        this.getCountryList();
        this.getStatesList('US');
    }
    checkPartnerName() {
        let input = this.myForm.controls['partnerName'].value;
        this._addplatformService.checkPartnerName(input).subscribe(
            prodName => {
                this.partnerNameResp = prodName;
                if (this.partnerNameResp['result'] == "exists") {
                    this.isNamenotAvailable = true;
                }
                else {
                    this.isNamenotAvailable = false;
                }
            },
            error => { console.log(error) }
        )
    }

    getCountryList() {
        this._addplatformService.getCountryList().subscribe(
            country => this.country = country,
            error => { console.log(error) }
        );

    }

    getStatesList(countryId) {
        this._addplatformService.getStatesList(countryId).subscribe(
            states => {
                this.states = states;
                this.selectStates = '001';
            },
            error => { console.log(error) },
        );
    }

    save(model, isValid: boolean) {
        if (this.isNamenotAvailable) {
            return false;
        }
        let input = new Object;
        input["orgName"] = model.partnerName;
        input["addressLine1"] = model.addressline1;
        input["addressLine2"] = model.addressline2;
        input["country"] = model.country;
        input["state"] = model.states;
        input["phoneNumber"] = model.phoneNumber;
        input["city"] = model.city;
        input["zip"] = model.zip;
        let action = "I";

        this._addplatformService.submitPartner(input, action).subscribe(
            adminPl => {

                this.returnVal = adminPl;
                if (this.returnVal['statusCode'] == 100) {
                    this.updatedMsg = true;
                    this.myForm.reset();
                    this.submitted = true;
                    this.myForm.reset();
                    this.submitted = true; // set form submit to true
                    this.isFailure = false;
                }

                else {
                    this.failureMsg = "Failed to update Partner.Please try after some time.";
                    this.isFailure = true;
                    this.updatedMsg = false;
                }
            }
            ,
            error => { console.log(error) },
            () => { console.log(this.returnVal) }
        )
    }

    removeUpdateMsg() {
        this.updatedMsg = false;
    }
}