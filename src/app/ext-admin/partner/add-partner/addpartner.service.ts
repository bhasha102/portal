
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Country } from '../country';
import { JsonpModule } from '@angular/http';
import { States } from '../states';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AddPartnerService {

  private baseUrl_Country: string = 'http://localhost:8085/dev-portal-intra/adminAddOrg';
  private baseUrl_state: string = 'http://localhost:8085/dev-portal-intra/getState';
  private baseUrl_editPartner: string = 'http://localhost:8085/dev-portal-intra/editOrgDetails';
  private baseUrl_partner: string = 'http://localhost:8085/dev-portal-intra/getOrgNames';

  headers = new Headers();

  constructor(private http: Http) {
  }

  getCountryList(): Observable<Country[]> {
    return this.http.get(this.baseUrl_Country, { withCredentials: true })
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw('Server error'))
  }

  getStatesList(countryCode): Observable<States[]> {
    let params = new URLSearchParams();
    params.set('countryCode', countryCode);
    return this.http.get(this.baseUrl_state, { search: params, withCredentials: true })
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw('Server error'))
  }

  checkPartnerName(input): Observable<JSON> {
    let params = new URLSearchParams();
    params.set('orgName', input);
    return this.http.post(this.baseUrl_partner, params, { withCredentials: true })
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }

  submitPartner(form, action): Observable<JSON> {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    let jsonData = JSON.stringify({ "action": action, "orgdata": form });
    return this.http.post(this.baseUrl_editPartner, jsonData, { headers: this.headers, withCredentials: true }) // ...using post request
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }
}