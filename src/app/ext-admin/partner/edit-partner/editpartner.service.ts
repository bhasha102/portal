
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Country } from '../country';
import { JsonpModule } from '@angular/http';
import { States } from '../states';
import { Partner } from '../partner';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class EditPartnerService {

  private baseUrl_editPartner: string = "http://localhost:8085/dev-portal-intra/getOrgListById";

  constructor(private http: Http) {
  }

  getPartnerDetails(orgId): Observable<Partner[]> {
    let params = new URLSearchParams();
    params.set('orgId', orgId);
    return this.http.post(this.baseUrl_editPartner, params, { withCredentials: true })
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw('Server error'))
  }
}