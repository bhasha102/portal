import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { Partner } from '../partner';
import { EditPartnerService } from './editpartner.service';
import { AddPartnerService } from '../add-partner/addpartner.service';
import { External_ManageUserService} from '../../manageUsers/ext-manageUsers.service';
import { Country } from '../country';
import { States } from '../states';


@Component({
  
    selector: 'editPartner',
    templateUrl: './editpartner.component.html',
    styleUrls: ['./editpartner.component.less']
})
export class EditPartnerComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
    country : Array<Country>;
    states :  Array<States>;
    errorMessage:string;
    partnerList : Partner[];
    partnerDetails : Partner[];
    success: string;
    error : string;
    showMessage = false;
    submitMessage :string;
    successMsg = false;
    selectCountry : string = 'US';
    selectStates :string = '001';
    updatedMsg :boolean = false;
     returnVal :JSON;
     selectedPartner: string = "0";
     failureMsg = "";
    isFailure = false;
    partnerNameResp = JSON;
    isNamenotAvailable =  false;
    constructor(private _fb: FormBuilder, private _editpartnerService : EditPartnerService,
    private _partnerListService : External_ManageUserService,  private _addpartnerService :AddPartnerService) { } // form builder simplify form initialization

    ngOnInit() {
        this.myForm = this._fb.group({
            orgName : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            partnerName : ['',  Validators.compose([Validators.required, this.validateDefaultProduct])],
            phoneNumber : ['', [<any>Validators.required]],
            addressLine1: ['', [<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            addressLine2: [''],
            city : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z][a-zA-Z0-9 \-]*")]],
            zip : ['', [<any>Validators.required,Validators.minLength(5),Validators.maxLength(5)]],
            country : [''],
            states: ['']
        });

        this.getPartnerList();
        this.getCountryList();
        this.getStatesList("US");
       
     
    }
    validateDefaultProduct(fieldControl: FormControl){
        //console.log("selectOption" + this.selectOption);
         if(fieldControl != undefined && fieldControl != null)
        {
            return fieldControl.value == '0' ? {defaultError : "error"} : null;
        }
        else
        {
            return null;
        }
     }
     checkPartnerName(){
        console.log(this.myForm.controls['orgName'].value);
        let input = this.myForm.controls['orgName'].value;
        this._addpartnerService.checkPartnerName(input).subscribe(
        prodName => {
            this.partnerNameResp = prodName;
            console.log(this.partnerNameResp);
            if(this.partnerNameResp['result'] == "exists")
            {
                this.isNamenotAvailable = true;
            }
            else{
                this.isNamenotAvailable = false;
            }
        },
        error =>{console.log(error)}

        )
    }
    removeUpdateMsg()
        {
            this.updatedMsg = false;
                      //this.successMsg = false;
        }
  
    getPartnerList()
     {
                         this._partnerListService.getPartners().subscribe(
                         partnerDetails => this.partnerList = partnerDetails,
                         error =>  {console.log(error)});
     }
     onChange(orgId)
     {
            // Need to pass orgId in service
              if (orgId =="0")
               {
                   console.log(orgId)
                   this.myForm.reset();
                    this.selectedPartner = "0";
               }
               else
               {
                     console.log("get details")
                     console.log(orgId)
                     this._editpartnerService.getPartnerDetails(orgId).subscribe(
                           UserDetails => this.partnerDetails = UserDetails,
                            error =>  {console.log(error)},
                             () => {this.updatePartnerDetails() }
                      );
                      this.showMessage =false;
     //   this._addpartnerService.getUserDetails(plat).subscribe(
    //        response => this.responseObj = response,
     //       error => {console.log(error)}
     //   );
     }
     }
     updatePartnerDetails()
     {
         //this.myForm.patchValue({'city':this.partnerDetails[0].city},{'orgName': this.partnerDetails[0].orgName});
         this.myForm.controls['orgName'].patchValue((this.partnerDetails[0].orgName).trim());
         this.myForm.controls['phoneNumber'].patchValue(this.partnerDetails[0].phoneNumber);
         this.myForm.controls['addressLine1'].patchValue((this.partnerDetails[0].addressLine1).trim());
         this.myForm.controls['addressLine2'].patchValue((this.partnerDetails[0].addressLine2).trim());
         this.myForm.controls['zip'].patchValue(this.partnerDetails[0].zip.toString().trim());
         this.myForm.controls['city'].patchValue((this.partnerDetails[0].city).trim());
         this.getStatesList(this.partnerDetails[0].country); //country = countryCode
         this.selectCountry = this.partnerDetails[0].country;
         
     }

    getCountryList()
    {
        this._addpartnerService.getCountryList().subscribe(
             country => this.country  = country,
             error =>  {console.log(error)});
    }
    getStatesList(countryCode)
    {
        // Pass country code to service
        this._addpartnerService.getStatesList(countryCode).subscribe(
             states => this.states  = states,
             error =>  {console.log(error)},
             () => {

                 this.selectStates = this.partnerDetails != undefined ? this.partnerDetails[0].state : '001'}
             );
    }

    save(model: any, isValid: boolean) {
   
        //this.submitted = true; 
         if(this.isNamenotAvailable){
            return false;
        }
         let input = new Object;
         input["orgName"]= model.orgName;
         input["addressLine1"]= model.addressLine1;
         input["addressLine2"]= model.addressLine2;
         input["country"]= model.country;
          input["state"]= model.states;
           input["phoneNumber"]= model.phoneNumber;
           input["city"]= model.city;
            input["zip"]= model.zip;
             input["orgId"]= +this.partnerDetails[0].organizationId;
              console.log(model);
              let action = "U";
                this._addpartnerService.submitPartner(input,action).subscribe(
                     adminPl =>{ 
                         this.returnVal = adminPl;
                         if(this.returnVal['statusCode'] == 200)
                {
                    console.log("Success");
                    //this.isSuccess = true;
                    this.updatedMsg = true;
                    this.isFailure = false;
                    //this.myForm.reset();
                    this.submitted = true;
                }
                else{
                    this.failureMsg = "Failed to update Partner. Please try after some time.";
                    console.log("Failure");
                    this.isFailure = true;
                    this.updatedMsg = false;
                }
            }
            ,
            error => { console.log(error) },
            () => { console.log(this.returnVal) }
        )
 console.log(model, isValid);
    }
}