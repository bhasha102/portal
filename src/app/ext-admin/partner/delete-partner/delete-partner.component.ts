import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { External_ManageUserService } from '../../manageUsers/ext-manageUsers.service';
import { AddPartnerService } from '../add-partner/addpartner.service';
import { Partner } from '../partner';
@Component({

    selector: 'deletepartner',
    templateUrl: './delete-partner.component.html',
    styleUrls: ['./delete-partner.component.less']
})
export class DeletePartnerComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
    errorMessage: string;
    partner: Partner[];
    updatedMsg: boolean = false;
    deletepartner: string = "0";
    errorMsg: boolean = false;
    returnVal: JSON;
    errorString: string = "Please select a Partner.";
    errorClass: boolean = false;

    constructor(private _fb: FormBuilder, private _addpartnerService: AddPartnerService, private _manageUserService: External_ManageUserService) { }

    ngOnInit() {
        this.myForm = this._fb.group({
            deletepartner: ['']
        });
        this.getPartnersList();
    }

    getPartnersList() {
        this._manageUserService.getPartners().subscribe(
            partnerDetails => this.partner = partnerDetails,
            error => { console.log(error) });
    }

    onChange() {
        if (this.deletepartner != undefined || this.deletepartner === "0") {
            this.updatedMsg = false;
            this.errorMsg = false;
            this.errorClass = false;
        }
    }

    deleteRowForm() {
        if (this.deletepartner == undefined || this.deletepartner === "0") {
            this.errorString = "Please select a Partner.";
            this.errorMsg = true;
            this.updatedMsg = false;
            this.errorClass = true;
        }
        else {
            let action = "D";
            let orgObj = { "orgId": +this.deletepartner };
            let toRemovePartner = this.deletepartner;
            this._addpartnerService.submitPartner(orgObj, action).subscribe(
                adminPl => {
                    this.returnVal = adminPl;
                    if (this.returnVal['statusCode'] == 300) {
                        this.updatedMsg = true;
                        this.myForm.reset();
                        this.submitted = true;
                        this.updatedMsg = true;
                        this.errorMsg = false;
                        this.errorClass = false;
                        this.partner = this.partner.filter(function (qt) {
                            if (qt.organizationId != +toRemovePartner)
                                return qt;
                        })
                        this.deletepartner = "0";
                    }
                    else {
                        this.errorString = "Failed to delete Partner. Please try after some time.";
                        this.errorMsg = true;
                        this.updatedMsg = false;
                    }
                },

                error => { console.log(error) },
                () => { console.log(this.returnVal) }
            )
        }
    }
}