import {Component, OnInit }from '@angular/core'; 
import {Partner }from '../partner/partner'; 
import {UserList}from '../partner/user-list'; 
import {External_ManageUserService}from './ext-manageUsers.service'; 
import {FormGroup, FormControl, FormBuilder, Validators  }from '@angular/forms'; 
import {ChangeDetectorRef}from '@angular/core'; 
import { ModelLocator } from '../../modelLocator';

@Component( {
  selector:'ext-platform', 
  templateUrl:'./ext-manageUsers.component.html', 
  styleUrls:['./ext-manageUsers.component.less']
})

export class External_ManageUserComponent implements OnInit {

errorMessage:string; 
Partners:Partner[]; 
public myForm:FormGroup; 
responseObj:JSON; 
responseTxt:string; 
userDetails:UserList[]; 
selectedState:any; 
showtable = false; 
showModal:boolean = false; 
model =  {options:0 }; 
userId:string; 
roles:Object[]; 
updatedMsg:boolean = false; 
userRole:string; 
sortVal:string; 
sortType:string = 'Asc'; 
errorMsg:boolean = false; 
statusIDs:Array < string >  = []; 
statusArray:Array < string >  = []; 
userRoleId:string = "0"; 
selectedObj:Object =  {}; 
updateResponse:any; 
roleUpdatedMsg:string; 
failureMsg:boolean = false; 
rolefailureMsg:string; 
viewModal:boolean = false; 
selectedPlatform : string = "0";
showNoData:boolean = false;

_model = ModelLocator.getInstance();

    public constructor(private _fb:FormBuilder, private ext_manageUserService:External_ManageUserService, private changeDetectorRef:ChangeDetectorRef) {
    }

    bool: boolean = true;
 

    ngOnInit() {
        this.myForm = this._fb.group({
            adminPlatform: [''],
            search: ['']
        }),
        this.getSchema();
    }

    getSchema() {
     if( this._model.ext_details == null ||  this._model.ext_details == undefined){
        var input = new Object();
        input['dbSchema'] = 'EXTERNAL';
        this.ext_manageUserService.getApigeeList(input).subscribe(

            apigeeList => {
                this._model.ext_details = apigeeList;
                this.getPartnerList();
                this.getRoles();
            },
            error => {
                console.log(error);
                this.getPartnerList();
                this.getRoles();
            });
        }
        else {
            this.getPartnerList();
            this.getRoles();
        }
    } 

     getPartnerList() {
        this.ext_manageUserService.getPartners().subscribe(
                        partnerDetails => this.Partners = partnerDetails, 
                        error =>  {console.log(error)}); 
                        
     }
   getRoles() {
       this.ext_manageUserService.getRoles().subscribe(
                        roles => this.roles = roles, 
                        error =>  {console.log(error)}); 
   }


  platformDetail() {
    }

  onChange(orgId) {
      if (orgId == "0") {
          this.errorMsg = false;
          this.showtable = false;
          this.showNoData = false;
      }
      else {
          this.ext_manageUserService.getUdetails(orgId.toString()).subscribe(
              UserDetails => {
                  this.userDetails = UserDetails;
                  if (this.userDetails.length == 0) {
                      this.errorMsg = false;
                      this.showtable = false;
                      this.showNoData = true;
                  }
                  else {

                      this.errorMsg = false;
                      this.showtable = true;
                      this.showNoData = false;
                  }
              },
              error => {
                  console.log(error);
                  this.errorMsg = false;
                  this.showtable = false;
                  this.showNoData = true;
              });
      }
  }

  sort(type, key) {
      this.errorMsg = false;
      if (type == 'Asc') {
          this.sortVal = key;
          this.sortType = 'Desc';
      } else if (type == 'Desc') {
          this.sortVal = "-" + key;
          this.sortType = 'Asc';
      }
  }


changeStatusOrDelete(indicator) {
    let j = 0;
        for (let i = 0; i < this.userDetails.length; i++) {
            if (this.userDetails[i].state) {
                this.statusIDs.push(this.userDetails[i].userId.toString());
                this.statusArray.push(this.userDetails[i].status);
                j++;
            }
        }
        if (j === 0) {
            this.errorMsg = true;
        }
        else {
            this.ext_manageUserService.changeStatusOrDelete(this.selectedPlatform.toString(), this.statusIDs, this.statusArray, indicator).subscribe(
                UserDetails => this.userDetails = UserDetails,
                error => { console.log(error) },
                () => {
                    this.statusIDs = [];
                    this.statusArray = [];
                }
            );
            this.errorMsg = false;
        }
}

    useridactive(isvisible, item) {
        this.userId = item.userId; 
        this.showModal = isvisible; 
        this.selectedObj = item; 
        this.userRoleId = item != '' || item != null?item.roleId:''; 
        this.updatedMsg = false; 
        this.errorMsg = false; 
        this.failureMsg = false; 
    }

    closeModal(event) {
        if (event.target.className == "modal") {
            this.showModal = false; 
            this.viewModal = false; 
        }
    }

    updateUserRole(selectedPlatform) {
        let roleName = ""; 
        this.roles.forEach(element =>  {
            if (element['roleId'] == this.userRoleId) {
                roleName = element['roleName']; 
            }
        }); 
       
        if (this.selectedObj['roleId'] === this.userRoleId) {
            this.updatedMsg = true; 
            this.failureMsg = false; 
            this.roleUpdatedMsg = "Please choose a different role to update"; 
        }
        else {
            this.ext_manageUserService.updateUserRole(selectedPlatform.toString(), this.userRoleId, roleName, this.selectedObj['guid'], this.selectedObj['userId']).subscribe(
                UserDetails =>  {
                    this.updateResponse = UserDetails; 
                    if (this.updateResponse[0]['userId'] != null) {
                        this.updatedMsg = true; 
                        this.failureMsg = false; 
                        this.updateResponse = UserDetails; 
                        this.roleUpdatedMsg = "Roles Updated. Please close the popup and refresh the page to see the change"; 
                    }
                    else {
                        this.failureMsg = true; 
                        this.updatedMsg = false; 
                        this.rolefailureMsg = "Failed to Update Role. Kindly try after sometime."; 
                    }
                }, 
                error =>  {console.log(error)}, 
                () =>  {
                }); 
        }
    }

    adduser(show) {
        this.viewModal = show; 
    }
}
