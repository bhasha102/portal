import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Partner } from '../partner/partner';
import { UserList } from '../partner/user-list';
import { JsonpModule } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class External_ManageUserService {

    //   private baseUrl_Partners:string = "http://localhost:8085/dev-portal-intra/getOrgList"//'./app/ext-admin/partner.json'; 
    //   private baseUrl_UserData:string = "./app/ext-admin/userList.json"; 
    //   private baseUrl_Roles:string = "./app/ext-admin/roles.json"; 
    //   private baseUrl_UpdateUserRole:string = "http://localhost:8085/dev-portal-intra/updateUserRoles";


    private baseUrl_Partners: string = "http://localhost:8085/dev-portal-intra/getOrgList";
    private baseUrl_UserData: string = "http://localhost:8085/dev-portal-intra/updateUserData";
    private baseUrl_Roles: string = "http://localhost:8085/dev-portal-intra//getRoles";
    private baseUrl_UpdateUserRole: string = "http://localhost:8085/dev-portal-intra/updateUserRoles";
    private baseUrl_Apigee: string = "http://localhost:8085/dev-portal-intra/setSchema";

    responseObj: any;
    headers = new Headers();

    constructor(private http: Http) {
        
    }

    getApigeeList(obj: Object): Observable<JSON> {
    //let jsonData = JSON.stringify(formData);
    return this.http.post(this.baseUrl_Apigee, obj, { withCredentials: true })// ...using post request
      .map((res: Response) => res.json())
      .do(data => {
        this.responseObj = data;
      })
      .catch((error: any) => Observable.throw(error || 'Server error'));

  } 

    getPartners(): Observable<Partner[]> {
        const headers = new Headers();
        return this.http.get(this.baseUrl_Partners, { withCredentials: true })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw('Server error'))
    }

    getUdetails(orgId: string): Observable<UserList[]> {
        let params = new URLSearchParams();
        params.set('orgId', orgId);
        params.set('indicator', 'S');
        params.set('userId', '');
        params.set('status', '');
        return this.http.post(this.baseUrl_UserData, params,{ withCredentials: true })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }

    getRoles(): Observable<Object[]> {
        return this.http.get(this.baseUrl_Roles, { withCredentials: true })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    changeStatusOrDelete(orgId: string, userId: Array<string>, status: Array<string>, indicator: string): Observable<UserList[]> {
        let params = new URLSearchParams();
        params.set('orgId', orgId);
        params.set('indicator', indicator);
        params.set('userId', userId.join(','));
        params.set('status', status.join(','));
        return this.http.post(this.baseUrl_UserData, params, { withCredentials: true })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }

    updateUserRole(orgId, roleID, roleName, guid, userId): Observable<any> {
        let params = new URLSearchParams();
        params.set('orgId', orgId);
        params.set('roleId', roleID);
        params.set('userId', userId);
        params.set('guid', guid);
        params.set('roleNm', roleName);
        return this.http.post(this.baseUrl_UpdateUserRole, params, { withCredentials: true })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

}