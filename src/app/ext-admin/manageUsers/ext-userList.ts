export interface UserList {
    guid :string,
    fName :string,
    lName :string,
    emailAddress :string,
	userId : number,
    state : boolean,
    status : string,
    roles : [{
        roleId : number,
        roleName : string
    }]
}