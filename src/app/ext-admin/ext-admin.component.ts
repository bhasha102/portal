import { Component, OnInit } from '@angular/core';
import { ModelLocator } from '../modelLocator';

@Component({
  selector: 'app-ext-admin',
  templateUrl: './ext-admin.component.html',
  styleUrls: ['./ext-admin.component.less']
})
export class ExternalAdminComponent implements OnInit {

_model = ModelLocator.getInstance();

  constructor() { }

  ngOnInit() {
    this._model.ext_details = null;
  }

}
