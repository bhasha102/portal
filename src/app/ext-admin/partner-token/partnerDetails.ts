export interface TokenDetails {
    orgId : number,
    orgName: string,
    orgToken: string,
    tokenExpiry: number,
    tokenQuota:number,
    extUrl: string
}