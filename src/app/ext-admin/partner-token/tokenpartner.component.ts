import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { TokenPartnerService } from './tokenpartner.service';
import { Partner } from '../partner/partner';
import { TokenDetails } from './partnerDetails';
import { External_ManageUserService } from '../manageUsers/ext-manageUsers.service';

declare var moment: any;

@Component({

    selector: 'tokenpartner',
    templateUrl: './tokenpartner.component.html',
    styleUrls: ['./tokenpartner.component.less']
})

export class TokenPartnerComponent implements OnInit {

    tempk: number;
    quo: number;
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changs
    errorMessage: string;
    partnerList: Partner[];
    partnerDetails: TokenDetails;
    success: string;
    error: string;
    showMessage = false;
    showtable = false;
    showField = true;
    showBut = true;
    showQuota = false;
    submitMessage: string;
    successMsg = false;
    showExp: boolean = false;
    showModal: boolean = false;
    quotaError: boolean = false;
    myVar: any;
    startDate = new Date();
    endDate = new Date();
    minDate = new Date();
    maxDate = new Date();
    myDate = new Date();
    returnVal: JSON;
    isFailure = false;
    failureMsg = "";
    partnerNameResp = JSON;
    isNamenotAvailable = false;
    deleteResp = JSON;
    isActiveToken = false;
    selectedPlatform: string = "0";
    tokenQuota: number;
    showExists: boolean = false;
    showNotExists: boolean = false;
    updatedMsg: boolean = false;
    updatedMsg1: boolean = false;

    constructor(private _fb: FormBuilder,
        private _partnerListService: External_ManageUserService, private _addplatformService: TokenPartnerService) { }


    ngOnInit() {
        this.myForm = this._fb.group({
            partnerName: [''],
            orgToken: [''],
            tokenQuota: ['', [Validators.required, Validators.maxLength(2)]],
            extUrl: [''],
            tokenExpiry: ['', Validators.required],
            tokenQuotaDisabled: ['']
        });

        let tempDate = moment(new Date());
        this.maxDate.setDate(this.maxDate.getDate() + 4);
        this.minDate.setDate(this.minDate.getDate() - 1);
        this.getPartnerList();
    }

    getPartnerList() {
        this._partnerListService.getPartners().subscribe(
            partnerDetails => this.partnerList = partnerDetails,
            error => { console.log(error) });
    }

    useridactive(isvisible, orgName) {
        this.showModal = isvisible;
        this.updatedMsg = false;
    }

    closeModal(event) {
        if (event.target.className == "modal") {
            this.showModal = false;
        }
    }

    removeUpdateMsg() {
        this.updatedMsg = false;
    }

    onChange(orgId) {

        this.showExists = false;
        this.showNotExists = false;
        this.updatedMsg = false;
        this.updatedMsg1 = false;

        this._addplatformService.getPartnerDetails(orgId).subscribe(
            UserDetails => {

                this.partnerDetails = UserDetails;
                if (this.partnerDetails != null) {
                    this.showBut = false;
                    this.updatePartnerDetails(this.partnerDetails);
                    this.showExists = true;
                    this.showField = true;
                    this.showNotExists = false;
                    this.showExp = true;
                    this.showQuota = false;
                }
                else {
                    this.showField = false;
                    this.showtable = true;
                    this.showBut = true;
                    this.showNotExists = true;
                    this.showQuota = true;
                    this.showExp = true;
                    this.isActiveToken = false;
                    this.myForm.reset();
                    this.myForm.controls['partnerName'].patchValue(orgId.toString());
                    this.quotaError = false;
                }
            },
            error => {
                this.showField = false;
                this.showtable = true;
                this.showBut = true;
                this.showNotExists = true;
                this.showQuota = true;
                this.showExp = true;
                this.isActiveToken = false;
                this.myForm.reset();
                this.myForm.controls['partnerName'].patchValue(orgId.toString());
                this.quotaError = false;
            },
            () => { }
        );
        this.showMessage = false;
    }

    updatePartnerDetails(details) {
        this.showtable = true;
        this.myForm.controls['orgToken'].patchValue(details.orgToken);
        this.myForm.controls['extUrl'].patchValue(details.extUrl);
        this.myVar = moment(new Date(+details.tokenExpiry)).format("MM/DD/YYYY");
        this.myForm.controls['tokenExpiry'].patchValue(this.myVar);
        this.tokenQuota = details.tokenQuota;
        this.isActiveToken = true;
    }

    keyPress(event: any) {
        const pattern = /[0-9\+\-\ ]/;
        let inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            event.preventDefault();
            this.quotaError = true;
        }
        else if (!pattern.test(inputChar).valueOf()) {
            event.preventDefault();
            this.quotaError = true;
        }
    }

    validation(value) {
        this.quotaError = false;
        if (value > 20) {
            this.quotaError = true;

        }
        else if (value < 1) {
            this.quotaError = true;
        }
    }

    valuechange(event: any) {
        const pattern = /[0-9\+\-\ ]/;
        let inputChar = String.fromCharCode(event);
        var temp = this.myForm.controls['tokenQuota'].value.concat(inputChar);
        this.tempk = parseInt(temp);
        if (!pattern.test(inputChar)) {
            event.preventDefault();
        }
        else if (!pattern.test(inputChar).valueOf()) {
            event.preventDefault();
        }
        if (this.tempk > 20) {
            this.quotaError = true;
        }
        else if (this.tempk < 1) {
            this.quotaError = true;
        }
    }

    deactivateToken(model) {
        this.showExists = false;
        this.showNotExists = false;
        this.updatedMsg = false;
        this.updatedMsg1 = false;

        this._addplatformService.deactivateToken(model).subscribe(
            admin => {
                this.deleteResp = admin;
                if (this.deleteResp['orgToken'] == null) {
                    this.updatedMsg = true;
                    this.updatedMsg1 = false;
                    this.showExists = false;
                    this.showField = false;
                    this.showNotExists = false;
                    this.showExp = true;
                    this.showQuota = true;
                    this.showBut = true;
                    this.myForm.reset();
                    this.myForm.controls['partnerName'].patchValue(model.toString());
                    this.quotaError = false;
                    this.isActiveToken = false;
                }
                else {

                }
            },
            error => { console.log(error) },
            () => { console.log(this.deleteResp) }
        )
    }

    save(model: any, isValid: boolean) {

        this.showExists = false;
        this.showNotExists = false;
        this.updatedMsg = false;
        this.updatedMsg1 = false;

        this.showNotExists = false;
        let input = new Object;
        let tempDate = new Date(model.tokenExpiry).getTime();
        input['orgId'] = +model.partnerName;
        input['tokenExpiry'] = +tempDate;
        input['tokenQuota'] = +this.tokenQuota;

        this._addplatformService.generateToken(input).subscribe(
            adminPl => {
                this.returnVal = adminPl;
                if (this.returnVal != null) {
                    this.updatedMsg = false;
                    this.updatedMsg1 = true;
                    this.submitted = true;
                    this.isFailure = false;
                    this.showExists = false;
                    this.showNotExists = false;

                    let organizationID: number;
                    organizationID = +model.partnerName;
                    this.showBut = false;
                    this.updatePartnerDetails(this.returnVal);
                    this.showField = true;
                    this.showExp = true;
                    this.showQuota = false;
                }
                else {
                    this.failureMsg = "Failed to add Partner token.Please try after some time.";
                    this.isFailure = true;
                    this.updatedMsg1 = false;
                }
            },
            error => { console.log(error) },
            () => { console.log(this.returnVal) }
        )
    }

    copyToClipboard(elementId) {
        var copyDiv = document.getElementById('extUrl');
        copyDiv.focus();
        document.execCommand('SelectAll');
        document.execCommand("Copy", false, null);
    }

  
}