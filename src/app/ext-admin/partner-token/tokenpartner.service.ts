import { Injectable } from '@angular/core';
import { TokenDetails } from './partnerDetails';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class TokenPartnerService {

  private baseUrl_PartnerDetails: string = "http://localhost:8085/dev-portal-intra/orgtoken/";
  private baseUrl_generateToken: string = "http://localhost:8085/dev-portal-intra/orgtoken/";
  private baseUrl_deactivateToken: string = "http://localhost:8085/dev-portal-intra/orgtoken1/";
  headers = new Headers();

  constructor(private http: Http) {
  }

  deactivateToken(orgId: string): Observable<JSON> {
    console.log("OrgId:" + orgId);
    let params = new URLSearchParams();
    params.set('orgId', orgId);

    return this.http.post(this.baseUrl_deactivateToken, params, { withCredentials: true })
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw('Server error'))
  }

  getPartnerDetails(orgId: string): Observable<TokenDetails> {
    console.log("OrgId:" + orgId);
    let params = new URLSearchParams();
    params.set('orgId', orgId);

    return this.http.get(this.baseUrl_PartnerDetails, { search: params, withCredentials: true })
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw('Server error'))
  }


  generateToken(input): Observable<JSON> {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    return this.http.post(this.baseUrl_generateToken, input, { headers: this.headers, withCredentials: true }) // ...using post request
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

}