import { Component, OnInit } from '@angular/core';
import {  FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { External_EditProductService } from '../editProduct/ext-editProduct.service';
import { External_DeleteProductService } from './ext-deleteProduct.service';

import { ProdList} from './delete-product';

@Component({
  
    selector: 'deleteproduct',
    templateUrl: './ext-deleteProduct.component.html',
    styleUrls: ['./ext-deleteProduct.component.less']
})

export class External_DeleteProductComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
     errorMessage:string;
     product:ProdList[];
     updatedMsg : boolean = false;
     errorMsg : boolean = false;
     deleteProduct : string = "0";
     response :any ;
     errorString : string = "Please select a Product.";


     
     constructor(private _fb: FormBuilder, private _editproductService : External_EditProductService,
     private _delproductService : External_DeleteProductService) { }
     ngOnInit() {
        this.myForm = this._fb.group({
             deleteProduct: ['']
               });
      this.getProductsList();
     }

   getProductsList()
   {
       let input=[];
       input['dbSchema']='EXTERNAL';
        this._editproductService.getAdminProductList(input).subscribe(
                    productDetails => this.product = productDetails,
                    error =>  {console.log(error)});
         console.log(this.product);
    }

    onChange(){
        console.log(this.updatedMsg);
        if (this.deleteProduct != undefined)
        { 
            this.updatedMsg = false;
            this.errorMsg = false;
        }
    }

    deleteRowForm(){
       
          if (this.deleteProduct === undefined)
          { 
            this.errorString = "Please select a Product.";
            this.errorMsg = true;
            this.updatedMsg = false;
         }
         else
         { 
             let productID = this.deleteProduct['product_ID'];
            console.log(this.deleteProduct);
            console.log(this.deleteProduct['product_ID']);
            let deletedProdID = this.deleteProduct['product_ID'];
           this._delproductService.deleteProduct(productID,this.deleteProduct['product_name']).subscribe(
                adminPl => {
                    this.response  = adminPl;
                    console.log(this.response);
                    this.updatedMsg = true;
                     if(this.response['result'] == "Success")
                    {
                            
                            this.updatedMsg = true;
                            console.log("success"+ this.updatedMsg);
                            this.errorMsg = false;
                            this.product = this.product.filter(function(qt){
                                if (qt.product_ID != deletedProdID)
                                return qt;
                            })
                        //this.deleteProduct = undefined;
                        this.deleteProduct = "0";
                    }
                    else{
                        console.log("false"+ this.updatedMsg);
                        this.errorString = "Error in deleting the product.Please try after sometime.";
                        this.updatedMsg = false;
                        this.errorMsg = true;
   
                    }
                    
                },
                error =>  {console.log(error)}
           )          
        }
    }
}