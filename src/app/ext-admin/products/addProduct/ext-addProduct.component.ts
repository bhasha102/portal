import { Component, OnInit  } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Observable} from 'rxjs/Rx';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { External_AddProductService} from './ext-addProduct.service';
import { Platform } from './ext-platform';
import { ApigeeProduct} from './ext-apigeeProducts';
import {ModelLocator} from '../../../../app/modelLocator';
import {External_ManageUserService}from '../../manageUsers/ext-manageUsers.service';

@Component({
        selector: 'ext-addproduct',
        templateUrl:'./ext-addProduct.component.html',
        styleUrls :  ['./ext-addProduct.component.less']
})

export class External_AddProductComponent  implements OnInit {
   public myForm: FormGroup; // our model driven form
   public submitted: boolean; // keep track on whether form is submitted
   public events: any[] = []; // use later to display form changes
   errorMessage:string;
   showModal : boolean = false;
   loaded: boolean = false;
   imageLoaded: boolean = false;
   imageSrc: string = '';
   file : Blob;
   returnVal : any;
   textValue = [];
   saveList: Array<string> = [];
   isApigeeProd :string = "nonapigee";
   showtext : boolean = true;
   showmultiple : boolean;
   selectOption : any;
   apigeeList : JSON;
 apigeeListResp = JSON;
   select : any;
   platformDetails : Platform[];
   newApigeeList : Array<ApigeeProduct>;
   selectCategory:string='nonapigee';
   selectcategory = [{name:"Non-Apigee Product",code:"nonapigee"},{name:"Link Apigee Product",code:"realtime"}];
   isSuccess :boolean = false;
   isFailure : boolean = false; 
   prodCategory: string = "PRIVATE";
   prodApproval: string = 'Y';
  failureMsg: string ="";
 emailRegex = '^[a-zA-Z0-9]+(\.[_a-zA-Z0-9]+)*@[a-zA-Z0-9]+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,15})$';
 publicGuid:ModelLocator;

_model = ModelLocator.getInstance();
  constructor(private _fb: FormBuilder, private _addproductService :External_AddProductService,private ext_manageUserService:External_ManageUserService) { } 

    ngOnInit(){
            this.myForm = this._fb.group({
            prodDescription : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z0-9][\na-zA-Z0-9 \-]*")]],
            prodName : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z][a-zA-Z0-9 \-]*")]],
            prodLocation : ['',[<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            chooseCategory : [''],
           // prodCategory :[''],
            prodOwnerEmail:['',[<any>Validators.required,<any>Validators.pattern(this.emailRegex)]],
            prodPRCGroup: ['',[<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
           // prodApproval:['']
         
 
        });
        this.platformDetails=[];

        this.getApigeeList();
        this.select ="Non-Apigee Product";
            
    }
   

 getApigeeList() {
        if (this._model.ext_details == null || this._model.ext_details == undefined) {
            var input = new Object();
            input['dbSchema'] = 'EXTERNAL';
            this.ext_manageUserService.getApigeeList(input).subscribe(
                apigeeList => {
                    this.apigeeListResp = apigeeList;
                    if (this.apigeeListResp == null || this.apigeeListResp['oAuthMapExt'] == null) {
                        this.failureMsg = "Failed to fetch Apigee List.";
                        this.isFailure = true;
                        this.isSuccess = false;
                    }
                    else {
                        this.apigeeList = this.apigeeListResp['oAuthMapExt'];
                        this.isFailure = false;
                    }
                    console.log(this.apigeeList);
                },
                error => { console.log(error) },
                () => { this.convertApigeeList(this.apigeeList) }
            );
        }
        else {
            this.apigeeList = (this._model.ext_details['oAuthMapExt']);
            this.convertApigeeList(this.apigeeList);
        }
    } 

    isApigeeProduct(apigeeProd){

            console.log("isApigeeProduct func : "+apigeeProd);
            if("realtime" ==  apigeeProd)
            {
                   this.showModal = true;
                    this.showtext = false;
                    this.showmultiple = true;
            }
            else
            {
                this.selectCategory = "nonapigee";
                    console.log(" isApigeeProduct nonapigee");
                    this.showModal = false;
                    this.showtext = true;
                    this.showmultiple = false;
            }
    }
 
   removeUpdateMsg()
    {
        this.isSuccess = false;
        this.isFailure = false;
    }


    handleInputChange(e){
         this.file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0]; 
    }

    convertApigeeList(list)
    {
        console.log("convertApigeeList");
        this.newApigeeList= [];
        for (var key in list) {
            var value = list[key];
           
           let tempObj:ApigeeProduct ={name:"", author:"", state:false};
            tempObj['name'] = key; 
            tempObj['author'] = value; 
            tempObj['state'] = false;  
            this.newApigeeList.push(tempObj);
        }

    }
    
    save(formValue, isValid) {
        var input = new FormData();

       var input1=new FormData();

      // input['publicGuid']=this.apigeeListResp['publicProducts'].publicGuid;
        input1['product_name']= formValue.prodName;
        input1['product_desc']= formValue.prodDescription;
        input1['product_loc']= formValue.prodLocation;
        input1['prcGroup']= formValue.prodPRCGroup;
        input1['category']= this.prodCategory;
        input1['prodOwnerDl']= formValue.prodOwnerEmail;
        input1['autoApproved']= this.prodApproval;
        input1['publicGuid']='b66762c1f8c6481aa4a2a533f7c77e92';
        input['products']=input1;
          console.log("cdhebvgshevchdj cbd////////////////"+this.selectCategory)
        if(this.selectCategory == "nonapigee")
        {
            input['nonApigeeProducts']= "NonApigeeProducts";
        }
        else{
            input['apigeeproducts']= this.saveList;
        }
        
        this._addproductService.submitproduct(input).subscribe(
            adminPl =>{ 
                this.returnVal = adminPl;
               
                if(this.returnVal['status'] == "pass")
                {
                    console.log("Success")
                    this.isSuccess = true;
                    this.isFailure = false;
                    this.submitted = true;
                    this.myForm.reset();
                    this.prodApproval= 'Y';
                    this.prodCategory = 'PRIVATE';
                    this.selectCategory ='nonapigee';
                    this.showModal = false;
                    this.showtext = true;
                    this.showmultiple = false;
                    
                }
                else{
                    this.failureMsg = "Failed to add Product. Please try after some time.";
                    console.log("Failure");
                    this.isFailure = true;
                    this.isSuccess = false;
                }
            }
            ,
            error => { console.log(error) },
            () => { console.log(this.returnVal) }
        )
    }
    displaySelect()
    {
        let j=0;
        this.textValue = [];
        this.saveList = [];
        for(let i =0;i< this.newApigeeList.length;i++)
        {
                if(this.newApigeeList[i].state)
                {
                    console.log(this.newApigeeList[i].state)
                        this.textValue[j] = this.newApigeeList[i].name;
                        this.saveList[j] = this.newApigeeList[i].name+"^"+this.newApigeeList[i].author; 
                        
                        console.log(this.textValue[j]);
                        j++;
                }
        }
        
        if(j === 0)
        {
                this.isApigeeProd = "nonapigee";
                  this.selectCategory = "nonapigee";
                this.showtext = true;
                this.showmultiple = false;
        }
        else
        {
                this.isApigeeProd = "realtime";
                this.selectCategory = "realtime";
                this.showtext = false;
                this.showmultiple = true;
        }
        this.showModal =false;
    }
    close()
    {
            this.isApigeeProd = "nonapigee";
            this.selectCategory = "nonapigee";
            this.showtext = true;
            this.showmultiple = false;
            this.showModal =false;
            console.log(this.isApigeeProd);
    }

   
    closeModal(event){
        console.log(event.target.className);
        if(event.target.className == "modal"){
            this.showModal =false;
            this.showtext = true;
            this.showmultiple = false;
            this.selectCategory = "nonapigee";
        }
    }
}
    
