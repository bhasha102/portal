
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Platform } from './ext-platform';


import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class External_AddProductService {


  //   private baseUrl_Apigee:string = "./app/ext-admin/products/addProduct/admin-product-apigee.json"; 
  //   private baseUrl_AddProduct:string = "http://localhost:8085/dev-portal-intra/addProdDetails";


  private baseUrl_AddProduct: string = "http://localhost:8085/dev-portal-intra/addExtProdDetails/";

  responseObj: any;
  headers = new Headers();
  constructor(private http: Http) {
  }

  submitproduct(formData: Object): Observable<JSON> {
    this.headers.append('Content-Type', 'application/json');
    return this.http.post(this.baseUrl_AddProduct, formData, { headers: this.headers, withCredentials: true }) // ...using post request
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error')); //...errors if any
  }

}