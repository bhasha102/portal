
import { Injectable } from '@angular/core';
import { AdminProduct } from './adminproduct';
//import {AdminProductSelect} from './adminproductselect';
import { Platform } from '../../../admin/platform/platform';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';


import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class External_EditProductService {

  // private baseUrl_Platforms: string ="./app/ext-admin/platform.json";
  // private baseUrl_Products: string ="http://localhost:8085/dev-portal-intra/getProdList";
  // private baseUrl_ProdDetails: string = "/app/ext-admin/prodDetails.json";
  // private baseUrl_EditProduct :string  ="http://localhost:8085/dev-portal-intra/editProdDetails";


  //private baseUrl_Platforms: string ="http://localhost:8085/dev-portal-intra/getOrgList";
  private baseUrl_Products: string = "http://localhost:8085/dev-portal-intra/getProdList";
  private baseUrl_ProdDetails: string = "http://localhost:8085/dev-portal-intra/getProductData";
  private baseUrl_EditProduct: string = "http://localhost:8085/dev-portal-intra/updateProd";

  headers = new Headers();
  constructor(private http: Http) {
  }


  getAdminProductList(input: Object): Observable<AdminProduct[]> {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    return this.http.post(this.baseUrl_Products, input, { headers: this.headers, withCredentials: true })
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }

  getAdminProductDetails(prodId: string): Observable<AdminProduct[]> {
    let params = new URLSearchParams();
    this.headers = new Headers();
    console.log("PROD ID : " + prodId)
    params.set('prodId', prodId);
    return this.http.get(this.baseUrl_ProdDetails, { search: params, withCredentials: true })
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }

  saveEditProductDetails(formData: Object): Observable<String> {
    return this.http.post(this.baseUrl_EditProduct, formData, { withCredentials: true })// ...using post request
      .map((res: Response) => res)
      .catch((error: any) => Observable.throw(error || 'Server error')); //...errors if any
  }
}