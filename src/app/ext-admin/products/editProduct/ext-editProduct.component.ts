import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AdminProduct } from './adminproduct';
import { External_EditProductService } from './ext-editProduct.service';
import { Platform } from '../../../admin/platform/platform';
//import { AdminProductSelect } from './adminproductselect';

@Component({
    selector: 'ext-Product',
    templateUrl: './ext-editProduct.component.html',
    styleUrls: ['./ext-editProduct.component.less']
})

export class External_EditProductComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
    errorMessage: string;
    editPRO: AdminProduct[];
    showModal: boolean = false;
    loaded: boolean = false;
    imageLoaded: boolean = false;
    imageSrc: string = '';
    file: Blob;
    prodApproval: string = "Y";
    returnVal: any;
    textValue = [];
    saveList: Array<string> = [];
    //platformDetails: Platform[];
    showtext: boolean = true;
    showmultiple: boolean;
    selectOption: any;
    select: any;
    listPRO: AdminProduct[];
    categoryUpdate: string = "PRIVATE";
    prodCategory: string = "PRIVATE";
    fileUpdated: string;
    apProdList: Array<string> = [];
    isApigeeProd: string = "nonapigee";
    updatedPlatformId: string;
    //selectPlatform: string;
    selectCategory: string = 'nonapigee';
    updatedMsg: boolean = false;
    category = [{ name: "Non-Apigee Product", code: "nonapigee" }, { name: "Link Apigee Product", code: "realtime" }];
    isError: boolean = false;
    errorMsg: string;
    selectedProduct: string = "0";
    isSuccess: boolean = false;
    isFailure: boolean = false;
    failureMsg: string = "";
    adminProduct: AdminProduct[];
    emailRegex = '^[a-zA-Z0-9]+(\.[_a-zA-Z0-9]+)*@[a-zA-Z0-9]+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,15})$';

    constructor(private _fb: FormBuilder, private _editproductService: External_EditProductService) { }

    ngOnInit() {
        this.myForm = this._fb.group({
            prodDescription: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][\na-zA-Z0-9 \-]*")]],
            prodName: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z][a-zA-Z0-9 \-]*")]],
            editProduct: ['', Validators.compose([Validators.required, this.validateDefaultProduct])],
            prodLocation: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            // prodCategory: ['', [<any>Validators.required]],
            prodOwnerEmail: ['', [<any>Validators.required, <any>Validators.pattern(this.emailRegex)]],
            prodPRCGroup: ['', [<any>Validators.required]],
            // prodApproval: ['', [<any>Validators.required]]
        });

        this.getDetails();
        // this.select = "Non-Apigee Product";

    }
    validateDefaultProduct(fieldControl: FormControl) {
        //console.log("selectOption" + this.selectOption);
        if (fieldControl.value != undefined && fieldControl.value != null) {
            return fieldControl.value === '0' ? { defaultError: "error" } : null;
        }
        else {
            return null;
        }
    }
    isApigeeProduct(apigeeProd) {
        console.log(apigeeProd)
        if ("realtime" == apigeeProd) {
            this.selectCategory = this.category[1].code;
            this.showmultiple = true;
            this.showtext = false;
        }
        else {
            this.selectCategory = this.category[0].code;
            this.showtext = true;
            this.showmultiple = false;
        }
    }

    getDetails() {
        // this._editproductService.getPlatforms().subscribe(
        //     UserDetails => this.platformDetails = UserDetails,
        //     error => { console.log(error) }
        // );
        var input = new Object();
        input['dbSchema'] = 'EXTERNAL';
        this._editproductService.getAdminProductList(input).subscribe(
            // adminPRO => this.listPRO = adminPRO,
            // error => { console.log(error) });

            adminPRO => {
                this.listPRO = adminPRO;
                if (this.listPRO == null || this.listPRO == null) {
                    this.failureMsg = "Failed to fetch Apigee List.";
                    this.isFailure = true;
                    this.isSuccess = false;
                }
                else {
                    this.adminProduct = this.listPRO;
                    this.isFailure = false;
                }
                console.log(this.adminProduct);
            },
            error => { console.log(error) },
            //() =>  {this.convertApigeeList(this.apigeeList)} //this.selectOption = this.platformDetails[2],console.log(this.selectOption)
        );
    }


    onChange(prodId) {

        if (prodId == "0") {

            this.myForm.reset();
            this.selectedProduct = "0";
            this.selectCategory = 'nonapigee';
            this.prodCategory = "PRIVATE";
            this.prodApproval = "Y";
            // return false;
        }
        else {
            console.log("ProdId" + prodId);        // Need to pass orgId in service
            this._editproductService.getAdminProductDetails(prodId).subscribe(
                adminPRO => {
                    this.editPRO = adminPRO;


                    if (this.editPRO[0] == null) {

                        this.isError = true;
                        this.errorMsg = "Failed to retrieve Product Details."
                        // this.myForm.reset();
                        // this.selectedProduct = "0";
                        // this.prodCategory = "PRIVATE";
                        // this.prodApproval="Y";

                    }
                    else {
                        this.updateProductDetails();
                    }
                },
                error => { console.log(error) }
            );
        }
    }


    updateProductDetails() {
        console.log("editPro");
        console.log(this.editPRO);
        this.myForm.controls['prodName'].patchValue(this.editPRO[0].product_name);
        this.myForm.controls['prodDescription'].patchValue(this.editPRO[0].product_desc);
        this.myForm.controls['prodLocation'].patchValue(this.editPRO[0].product_loc);
        this.myForm.controls['prodPRCGroup'].patchValue(this.editPRO[0].prcGroup);
        //this.myForm.controls['prodApproval'].patchValue(this.editPRO[0].autoApproved.trim());
        this.prodApproval = this.editPRO[0].autoApproved;
        this.prodCategory = this.editPRO[0].category;
        this.myForm.controls['prodOwnerEmail'].patchValue(this.editPRO[0].prodOwnerDl);
        //this.selectPlatform = this.editPRO.orgId.toString();
    }

    save(formValue, isValid) {
        console.log(formValue);
        let input = new Object();
        input['product_ID'] = this.editPRO[0].product_ID;
        input['product_name'] = formValue.prodName;
        input['product_desc'] = formValue.prodDescription;
        input['product_loc'] = formValue.prodLocation;
        input['prcGroup'] = formValue.prodPRCGroup;
        input['category'] = this.editPRO[0].category;
        input['prodOwnerDl'] = formValue.prodOwnerEmail;
        input['autoApproved'] = this.editPRO[0].autoApproved;

        console.log(formValue);
        this._editproductService.saveEditProductDetails(input).subscribe(
            adminPl => {
                this.returnVal = adminPl
                console.log(this.returnVal['status'])
                if (this.returnVal['status']) {
                    console.log("Success");
                    this.updatedMsg = true;
                    this.isError = false;
                    // this.myForm.reset();
                    this.selectCategory = 'nonapigee';
                    this.prodCategory = "PRIVATE";
                    this.prodApproval = "Y";
                    this.isError = false;

                }

                else {
                    this.failureMsg = "Failed to add Product. Please try after some time.";
                    console.log("Failure");
                    this.updatedMsg = false;
                    this.isError = true;
                    this.errorMsg = "Failed to update Product. Kindly try after some time."
                }
            }
            ,
            error => { console.log(error) },
            () => { console.log(this.returnVal) }
        )

    }

    removeUpdateMsg() {
        this.updatedMsg = false;
        this.isError = false
    }

    displaySelect() {

    }
}
