import { Component, EventEmitter, Input, OnChanges, OnInit, ViewChildren } from '@angular/core';
import { External_ManageUserService } from '../manageUsers/ext-manageUsers.service';
import { MapPartnerProductService } from './mappartnerproduct.service';
import { Partner } from '../partner/partner';
import { Observable } from 'rxjs/Rx';
import { MapPartnerProduct } from './mappartnerproduct';
import { MapPartnerFilterPipe } from './map-partner-product.filter.pipe';


@Component({
    selector: 'map-partner-product',
    templateUrl: './mappartnerproduct.component.html',
    styleUrls: ['./mappartnerproduct.component.less']
})
export class MapPartnerProductComponent implements OnInit {
    @ViewChildren('list1') selectElRef;
    @ViewChildren('list2') selectPlRef;
    @ViewChildren('list3') deletePlRef;
    partnerDetails: Partner[];
    errorMessage: string;
    sortVal: string;
    sortType: string = 'Asc';
    previousKey = "";
    organizationId: string;
    responseObj: JSON;
    masterProdList: MapPartnerProduct[];
    prodList: MapPartnerProduct[] = [];
    orgProdList: MapPartnerProduct[] = [];
    prodListOrg: MapPartnerProduct[] = [];
    Indicator: string;
    showModal: boolean = false;
    pname: string;
    selectedOrgList: Array<string> = [''];
    selectedProdList: Array<string> = [''];
    List: any = [];
    List2: any = [];
    List3: any = [];
    isSelected: boolean = false;
    nameFilter: string;
    productnameFilter: string;
    failureMsg = "";
    isFailure = false;
    textprodlist: string = 'Empty';
    textorglist: string = 'Empty';
    showorgfilter: boolean = false;
    showprodfilter: boolean = false;
    deletedList: Array<string> = [''];
    deleteMapList: MapPartnerProduct[] = [];
    rightEvent: string = '';
    leftEvent: string = '';
    sortList: string = 'product_name';
    pageSize:number = 10;
    pageStart:number = 0;
    constructor(private _partnerProductService: MapPartnerProductService, private _adminService: External_ManageUserService) {
    }


    ngOnInit() {
        this.getPartnerList();
    }

    getPartnerList() {
        this._adminService.getPartners().subscribe(
            partner => this.partnerDetails = partner,
            error => {console.log(error) });

    }
    closeModal(event) {
        if (event.target.className == "modal") {
            this.isFailure = false;
            this.showModal = false;
            this.prodList = [];
            this.orgProdList = [];
            this.selectedOrgList = [];
            this.selectedProdList = [];
            this.List = [];
            this.List2 = [];
            this.isSelected = false;
            this.List3 = [];
            this.nameFilter = "";
            this.productnameFilter = "";
            this.showprodfilter = false;
            this.prodListOrg = [];
            this.rightEvent = "";
            this.leftEvent = "";
            this.text();
        }
    }

    valuechange(event, direction) {

        if (direction == 'right') {
            this.rightEvent = event;
        }
        else {
            this.leftEvent = event;
        }
        this.selectedOrgList = [];
        this.selectedProdList = [];
        this.text();
    }

    close() {
        this.isFailure = false;
        this.showModal = false;
        this.prodList = [];
        this.orgProdList = [];
        this.selectedOrgList = [];
        this.selectedProdList = [];
        this.List = [];
        this.List2 = [];
        this.isSelected = false;
        this.List3 = [];
        this.nameFilter = "";
        this.productnameFilter = "";
        this.showprodfilter = false;
        this.rightEvent = "";
        this.leftEvent = "";
        this.text();
        this.prodListOrg = [];
    }

    text() {

        let tempProdFilter: any;
        let tempOrgFilter: any;
        if (this.rightEvent !== '') {
            let evnt = this.rightEvent;
            tempProdFilter = this.prodList.filter(function (qt) {
                if (qt.product_name.toLowerCase().indexOf(evnt.toLowerCase()) !== -1)
                    return qt.product_name.toLowerCase().includes(evnt.toLowerCase());
            })
        }

        if (this.rightEvent !== '' && this.prodList.length > tempProdFilter.length) {
            this.showprodfilter = true;
            this.textprodlist = "<span style='color: #fff;background-color: #f0ad4e !important;border-radius:2px;padding:1px'>Filtered</span> " + tempProdFilter.length + " from " + this.prodList.length;

        }
        else {
            this.showprodfilter = false;
            this.textprodlist = this.prodList.length > 0 ? 'showing all ' + this.prodList.length : 'Empty';
        }

        if (this.leftEvent !== '') {
            let evnt = this.leftEvent
            tempOrgFilter = this.orgProdList.filter(function (qt) {
                if (qt.product_name.toLowerCase().indexOf(evnt.toLowerCase()) !== -1)
                    return qt.product_name.toLowerCase().includes(evnt.toLowerCase());
            })
        }

        if (this.leftEvent !== '' && this.orgProdList.length > tempOrgFilter.length) {
            this.showorgfilter = true;
            this.textorglist = "<span style='color: #fff;background-color: #f0ad4e !important;border-radius:2px;padding:1px'>Filtered</span> " + tempOrgFilter.length + " from " + this.orgProdList.length;

        }
        else {
            this.showorgfilter = false;
            this.textorglist = this.orgProdList.length > 0 ? 'showing all ' + this.orgProdList.length : 'Empty';
        }
    }
    getMapPartnerProductDetails() {
        this._partnerProductService.getMapProductDetails(this.organizationId).subscribe(
            response => {
                this.responseObj = response;
                this.prodListOrg = this.responseObj['prodListforOrg'];
                this.masterProdList = this.responseObj['prodList'];
                this.prodList = this.masterProdList;
                this.text();
            },
            error => {console.log(error) },
        );
    }

    productDetail(isVisible, item) {
        this.showModal = isVisible;
        this.organizationId = item.organizationId;
        this.pname = item.orgName;
        this.getMapPartnerProductDetails();
    }

    sort(type, key) {
        if (this.previousKey != key) {
            this.sortType = 'Asc';
            type = 'Asc';
        }

        if (type == 'Asc') {
            this.sortVal = key;
            this.sortType = 'Desc';
        } else if (type == 'Desc') {
            this.sortVal = "-" + key;
            this.sortType = 'Asc';
        }
        this.previousKey = key;
    }

    mapOrganisationToProducts() {
        let prodId: Array<string> = [];
        let temporgProdList: MapPartnerProduct[] = [];
        if (this.productnameFilter === undefined || this.productnameFilter === '')
            temporgProdList = this.orgProdList;
        else
            this.orgProdList.forEach((a) => { if (a.product_name.toLowerCase().indexOf(this.productnameFilter.toLowerCase()) !== -1 && temporgProdList.indexOf(a) == -1) temporgProdList.push(a) })
        temporgProdList.forEach((a) => { prodId.push(a.product_ID) })
        if (temporgProdList.length > 0) {
            this._partnerProductService.addMapProductDetails(prodId, 'I').subscribe(
                response => {
                    this.responseObj = response;
                    console.log(this.responseObj)
                    if(this.responseObj['prodListforOrg'] == null && this.responseObj['prodList'] == null){
                        this.failureMsg = "Failed to Map Products. Please try after some time.";
                        this.isFailure = true;
                    }
                    else{ 
                        this.isFailure = false;
                        this.prodListOrg = this.responseObj['prodListforOrg'];
                        this.masterProdList = this.responseObj['prodList'];
                        this.prodList = this.masterProdList;
                        this.orgProdList = [];
                        this.selectedOrgList = [];
                        this.selectedProdList = [];
                        this.List = [];
                        this.List2 = [];
                        this.isSelected = false;
                        this.List3 = [];
                        this.nameFilter = "";
                        this.productnameFilter = "";
                        this.rightEvent = "";
                        this.leftEvent = "";
                        this.text();
                    }
                },
                error => {console.log(error)},
                () => {
                });



        }

    }
    deleteOrganisationToProducts() {

        this.deletePlRef.toArray().find((e) => {
            this.deletedList = Array.apply(null, e.nativeElement.options)
                .filter(option => option.selected)
                .map(option => option.value)
        })
        let temp = this.deletedList;
        this._partnerProductService.addMapProductDetails(temp, 'D').subscribe(
            response => {
                this.responseObj = response;
                if(this.responseObj['prodListforOrg'] == null && this.responseObj['prodList'] == null){
                    this.failureMsg = "Failed to Delete Products. Please try after some time.";
                    this.isFailure = true;
                }
                else{
                    this.isFailure = false;
                    this.prodListOrg = this.responseObj['prodListforOrg'];
                    this.masterProdList = this.responseObj['prodList'];
                    this.prodList = this.masterProdList;
                    this.orgProdList = [];
                    this.selectedOrgList = [];
                    this.selectedProdList = [];
                    this.List = [];
                    this.List2 = [];
                    this.isSelected = false;
                    this.List3 = [];
                    this.nameFilter = "";
                    this.productnameFilter = "";
                    this.rightEvent = "";
                    this.leftEvent = "";
                    this.text();
                }
            },
            error => { },
            () => {
            });

    }

    move(item, direction) {

        if (direction == 'right') {

            if (item == 'all') {
                this.prodList = [];
                this.List3 = [];
                this.List2 = [];
                this.List = [];
                if (this.nameFilter === undefined || this.nameFilter === '') {
                    this.orgProdList = this.masterProdList;
                }
                else {
                    this.masterProdList.forEach((a) => { if (a.product_name.toLowerCase().indexOf(this.nameFilter.toLowerCase()) !== -1 && this.orgProdList.indexOf(a) == -1) this.orgProdList.push(a) })
                    this.prodList = this.masterProdList;
                    this.orgProdList.forEach((a) =>
                        this.prodList = this.prodList.filter(ft => {
                            if (ft.product_ID !== a.product_ID)
                                return true;
                        }))
                    this.List3 = this.prodList;
                    this.orgProdList.forEach((a) => { if (this.List.indexOf(a) == -1) this.List.push(a) })
                }
                this.selectedOrgList = [];
                this.selectedProdList = [];
                this.isSelected = false;

            }

            if (item == 'selected') {
                this.selectElRef.toArray().find((e) => {
                    this.change(e.nativeElement.options, "right")
                })
            }
            if (item == 'selected' && this.isSelected) {

                this.prodList = this.masterProdList;
                this.orgProdList = this.masterProdList;
                let temp = this.selectedProdList;
                this.orgProdList = this.orgProdList.filter(function (qt) {
                    for (let i = 0; i < temp.length; i++) {
                        if (qt.product_ID == temp[i])
                            return qt;
                    }
                });
                this.orgProdList.forEach((a) => { if (this.List.indexOf(a) == -1) this.List.push(a) })
                this.orgProdList = this.List;
                this.orgProdList.forEach((a) =>
                    this.prodList = this.prodList.filter(ft => {
                        if (ft.product_ID !== a.product_ID)
                            return true;
                    }))
                this.List3 = this.prodList;
                this.List2 = [];
                this.selectedProdList = [];
                this.selectedOrgList = [];
                this.isSelected = false;
            }

        }

        else {

            if (item == 'all') {
                this.orgProdList = [];
                this.List = [];
                if (this.productnameFilter === undefined || this.productnameFilter === '') {
                    this.prodList = this.masterProdList;
                    this.List2 = [];
                }
                else {
                    this.masterProdList.forEach((a) => { if (a.product_name.toLowerCase().indexOf(this.productnameFilter.toLowerCase()) !== -1 && this.prodList.indexOf(a) == -1) this.prodList.push(a) })
                    this.orgProdList = this.masterProdList;
                    this.prodList.forEach((a) =>
                        this.orgProdList = this.orgProdList.filter(ft => {
                            if (ft.product_ID !== a.product_ID)
                                return true;
                        }))
                    this.orgProdList.forEach((a) => { if (this.List.indexOf(a) == -1) this.List.push(a) })
                }
                this.selectedOrgList = [];
                this.selectedProdList = [];
                this.List3 = this.prodList;
                this.isSelected = false;
            }
            if (item == 'selected') {
                this.selectPlRef.toArray().find((e) => {
                    this.change(e.nativeElement.options, "left")
                })
            }
            if (item == 'selected' && this.isSelected) {
                this.List2 = this.List3;
                this.prodList = this.masterProdList;
                let temp = this.selectedOrgList;
                this.prodList = this.prodList.filter(function (qt) {
                    for (let i = 0; i < temp.length; i++) {
                        if (qt.product_ID == temp[i])
                            return qt;
                    }
                });

                this.prodList.forEach((a) => { if (this.List2.indexOf(a) == -1) this.List2.push(a) })
                this.prodList = this.List2;
                this.prodList.forEach((a) =>
                    this.orgProdList = this.orgProdList.filter(ft => {
                        if (ft.product_ID !== a.product_ID)
                            return true;
                    }))
                this.List = [];
                this.orgProdList.forEach((a) => { if (this.List.indexOf(a) == -1) this.List.push(a) })
                this.selectedProdList = [];
                this.selectedOrgList = [];
                this.isSelected = false;
            }
        }
        this.text();
    }


    change(options, direction) {
        if (options.length > 0 && options.selectedIndex !== -1)
            this.isSelected = true;
        if (direction == 'right') {
            this.selectedOrgList = []
            this.selectedProdList = Array.apply(null, options)
                .filter(option => option.selected)
                .map(option => option.value)
        }
        else {
            this.selectedProdList = []
            this.selectedOrgList = Array.apply(null, options)
                .filter(option => option.selected)
                .map(option => option.value)
        }
    }

    resetFilter(direction) {
        this.selectedOrgList = [];
        this.selectedProdList = [];
        if (direction == 'right') {
            this.nameFilter = "";
            this.showprodfilter = false;
            this.textprodlist = this.prodList.length > 0 ? 'showing all ' + this.prodList.length : 'Empty';
        }
        else {
            this.productnameFilter = "";
            this.showorgfilter = false;
            this.textorglist = this.orgProdList.length > 0 ? 'showing all ' + this.orgProdList.length : 'Empty';
        }
    }

    done() {
        this.showModal = false;
        this.prodList = [];
        this.orgProdList = [];
        this.selectedOrgList = [];
        this.selectedProdList = [];
        this.List = [];
        this.List2 = [];
        this.isSelected = false;
        this.List3 = [];
        this.nameFilter = "";
        this.showprodfilter = false;
        this.productnameFilter = "";
        this.rightEvent = "";
        this.leftEvent = "";
        this.text();
        this.prodListOrg = [];
        this.isFailure = false;
    }

}