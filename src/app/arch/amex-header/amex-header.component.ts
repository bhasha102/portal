import { Component, OnInit } from '@angular/core';

import { AppService } from '../../app.service';
import { Observable } from 'rxjs/Rx';
import { ModelLocator } from '../../modelLocator';


@Component({
    selector: 'app-amex-header',
    templateUrl: './amex-header.component.html',
    styleUrls: ['./amex-header.component.less']
})
export class AmexHeaderComponent implements OnInit {
    username: string = "";
    isIntAdmin = false;
    isExtAdmin = false;
    isEAuth = false;
    isPlatformAdmin = false;
    _model = ModelLocator.getInstance();
    roleObj: JSON;

    constructor(private _appService: AppService) { }

    ngOnInit() {
        // this._model.globalStorage.clear();
        if (this._model.globalStorage.getItem("isSessionActive") == null) {
            this._appService.getAppRole(this._model.globalStorage.getItem("guid")).subscribe(
                roleItem => {
                    this._model.globalStorage.setItem("isSessionActive", "true");
                    console.log("active session : " + this._model.globalStorage.getItem("isSessionActive"));
                    this.roleObj = roleItem;
                    this._model.globalStorage.setItem("userRoleObj", JSON.stringify(this.roleObj));
                    this.updateHeader();
                },
                error => { console.log(error) },
            )
        }
        else {
            let userObj = this._model.globalStorage.getItem("userRoleObj");
            // console.log("ELSE userObj : " + userObj);
            if (userObj != null) {
                this.roleObj = JSON.parse(this._model.globalStorage.getItem("userRoleObj"));
                console.log(this.roleObj);
                this.updateHeader();
            }
            else {
                console.log("kindly login.");
            }
        }
    }

    updateHeader() {
        this.username = this.roleObj['fname'];
        if (this.roleObj['userType'] === "Application Owner") {
            this.isIntAdmin = true;
            this.isExtAdmin = true;
            //this.isEAuth = true;
            //this.isPlatformAdmin = true;

        }
        if (this.roleObj['userTypeOrg'] === "Org Admin") {
            /*this.isIntAdmin = false;
            this.isExtAdmin = false;
            this.isEAuth = false;*/
            this.isPlatformAdmin = true;

        }
        if (this.roleObj['userRole'] === "EAuth") {
            this.isEAuth = true;
        }
        else {
            // Normal User
        }
    }

    logoutUser() {
        this._model.globalStorage.clear();
    }

}
