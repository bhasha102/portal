import { Component, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { ShapeOptions, LineProgressComponent } from 'angular2-progressbar';

@Component({
  selector: 'progressBar',
  template:
    `<div class="line-container">
      <ks-line-progress [options]="lineOptions"></ks-line-progress>
    </div>`,
  styles: [`.line-container {
      margin: 20px;
      width: 500px;
      height: 10px;
    }`]
})

export class ProgressBarComponent {

  @ViewChild(LineProgressComponent) lineComp: LineProgressComponent;

  private lineOptions: ShapeOptions = {
    strokeWidth: 5,
    easing: 'easeOut',
    duration: 19000,
    color: 'blue',
    trailColor: '#eee',
    trailWidth: 5,
    svgStyle: { width: '100%' }
  };

  ngAfterViewInit() {
    this.lineComp.animate(1);
  }

}