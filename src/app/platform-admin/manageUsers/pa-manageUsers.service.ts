import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Platform } from './pa-platform';
import { UserList } from './pa-userList';
import { JsonpModule } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PA_ManageUserService {

    //private baseUrl_Platforms_apigeeList: string = './app/platform-admin/platform.json';
    // private baseUrl_UserData: string = "./app/platform-admin/userList.json";
    // private baseUrl_Roles: string = "./app/platform-admin/roles.json";
    // private baseUrl_UpdateUserRole: string = "http://localhost:8085/dev-portal-intra/updateUserRoles";
    //private baseUrl_userDetails: string = "http://localhost:8085/dev-portal-intra/getAdsUserDetails";
    //private baseUrl_addUser: string = "http://localhost:8085/dev-portal-intra/addAdsUserDetails";
    //headers = new Headers();

    private baseUrl_Platforms_apigeeList: string = "http://localhost:8085/dev-portal-intra/orgAdminActivities/";
    private baseUrl_UserData: string = "http://localhost:8085/dev-portal-intra/updateUserData";
    private baseUrl_Roles: string = "http://localhost:8085/dev-portal-intra//getRoles";
    private baseUrl_UpdateUserRole: string = "http://localhost:8085/dev-portal-intra/updateUserRoles";
    private baseUrl_userDetails: string = "http://localhost:8085/dev-portal-intra/getAdsUserDetails";
    private baseUrl_addUser: string = "http://localhost:8085/dev-portal-intra/addAdsUserDetails";
    headers = new Headers();

    constructor(private http: Http) {
    }

    getPlatforms_Apigee(): Observable<JSON> {
        this.headers = new Headers();
        return this.http.get(this.baseUrl_Platforms_apigeeList, { withCredentials: true })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw('Server error'))
    }

    getUdetails(orgId: string): Observable<UserList[]> {
        let params = new URLSearchParams();
        params.set('orgId', orgId);
        params.set('indicator', 'S');
        params.set('userId', '');
        params.set('status', '');
        return this.http.post(this.baseUrl_UserData, params, { withCredentials: true })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }

    getRoles(): Observable<Object[]> {
        return this.http.get(this.baseUrl_Roles, { withCredentials: true })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    changeStatusOrDelete(orgId: string, userId: Array<string>, status: Array<string>, indicator: string): Observable<UserList[]> {
        let params = new URLSearchParams();
        params.set('orgId', orgId);
        params.set('indicator', indicator);
        params.set('userId', userId.join(','));
        params.set('status', status.join(','));
        return this.http.post(this.baseUrl_UserData, params, { withCredentials: true })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }

    updateUserRole(orgId, roleID, roleName, guid, userId): Observable<any> {
        let params = new URLSearchParams();
        params.set('orgId', orgId);
        params.set('roleId', roleID);
        params.set('userId', userId);
        params.set('guid', guid);
        params.set('roleNm', roleName);
        return this.http.post(this.baseUrl_UpdateUserRole, params, { withCredentials: true })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }

    getUserDetails(adsId: string): Observable<JSON> {
        let params = new URLSearchParams();
        params.set('adsId', adsId);
        return this.http.post(this.baseUrl_userDetails, params, { withCredentials: true })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    addUser(userData: Object): Observable<JSON> {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        return this.http.post(this.baseUrl_addUser, userData, { headers: this.headers, withCredentials: true })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

}