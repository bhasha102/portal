import {Pipe, PipeTransform }from '@angular/core'; 
import {UserList}from './pa-userList'; 

@Pipe( {
    name:'pa_filter', 
})
export class PA_ManageUserFilterPipe implements PipeTransform {

    transform(list:UserList[], nameFilter:string):any {
        console.log(list ); 
         if (nameFilter === undefined) {
               console.log("nameFilter : " + nameFilter); 
               return list; 
         }
       
          return list.filter(function (qt) {
              if (qt.fName.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
              return qt.fName.toLowerCase().includes(nameFilter.toLowerCase()); 
              if (qt.lName.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
              return qt.lName.toLowerCase().includes(nameFilter.toLowerCase()); 
                      if (qt.userId.toString().indexOf(nameFilter) !== -1)
              return qt.userId.toString().toLowerCase().includes(nameFilter.toLowerCase()); 
                  if (qt.emailAddress.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
              return qt.emailAddress.toLowerCase().includes(nameFilter.toLowerCase()); 
          })
    }
}