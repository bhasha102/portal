import { Component, OnInit } from '@angular/core';
import { Platform } from './pa-platform';
import { UserList } from './pa-userList';
import { PA_ManageUserService } from './pa-manageUsers.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ChangeDetectorRef } from '@angular/core';
import { ModelLocator } from '../../modelLocator';

@Component({
    selector: 'pa-platform',
    templateUrl: './pa-manageUsers.component.html',
    styleUrls: ['./pa-manageUsers.component.less']
})

export class PA_ManageUserComponent implements OnInit {

    errorMessage: string;
    Platform: JSON;
    public myForm: FormGroup;
    public searchForm: FormGroup;
    responseObj: JSON;
    responseTxt: string;
    userDetails: UserList[];
    selectedState: any;
    showtable = false;
    showModal: boolean = false;
    model = { options: 0 };
    userId: string;
    roles: Object[];
    updatedMsg: boolean = false;
    userRole: string;
    sortVal: string;
    sortType: string = 'Asc';
    errorMsg: boolean = false;
    statusIDs: Array<string> = [];
    statusArray: Array<string> = [];
    userRoleId: string = "0";
    selectedObj: Object = {};
    updateResponse: any;
    roleUpdatedMsg: string;
    failureMsg: boolean = false;
    rolefailureMsg: string;
    viewModal: boolean = false;
    selectedPlatform: string = "0";
    platformDetails: JSON;
    Apigee: JSON;
    previousKey = "";

    userSearchDetails = JSON;
    searchUserRole = "0";
    addUserResp: JSON;
    newUserAdded = false;
    isNewUser = false;
    noUserAvailable = false;
    noIdEntered = false;
    rolesVisible = false;
    userAddFailureMsg = "";
    buttonVisible = false;
    errorRolesMsg = false;
    errorRolesClass = false;
    loggedInUser = false;
    _model = ModelLocator.getInstance();


    public constructor(private _fb: FormBuilder, private pa_manageUserService: PA_ManageUserService, private changeDetectorRef: ChangeDetectorRef) {
    }

    bool: boolean = true;

    ngOnInit() {
        this.myForm = this._fb.group({
            adminPlatform: [''],
            search: ['']
        }),

            this.searchForm = this._fb.group({
                firstName: [],
                middleName: [],
                lastName: [],
                dept: [],
                leader: [],
                userEmailId: []
            })

        this.getPA_Detail();
    }

    getPA_Detail() {
        if (this._model.pa_details == null || this._model.pa_details == undefined) {

            this.pa_manageUserService.getPlatforms_Apigee().subscribe(
                platformDetails => {
                    this._model.pa_details = platformDetails;
                    this.Platform = platformDetails['orgList'];
                    this.Apigee = platformDetails['oAuthMap'];
                    this.getRoles();
                },
                error => {
                    this.getRoles();
                });
        }
        else {
            this.Platform = (this._model.pa_details['orgList']);
            this.getRoles();
        }
    }

    getRoles() {
        this.pa_manageUserService.getRoles().subscribe(
            roles => this.roles = roles,
            error => { console.log(error) });
    }

    onChange(orgId) {
        if (orgId == "0") {
            this.errorMsg = false;
            this.showtable = false;
        }
        else {
            this.errorMsg = false;
            this.showtable = true;
            this.pa_manageUserService.getUdetails(orgId.toString()).subscribe(
                UserDetails => this.userDetails = UserDetails,
                error => { console.log(error) });

        }
    }

    sort(type, key) {
        this.errorMsg = false;
        if (this.previousKey != key) {
            this.sortType = 'Asc';
            type = 'Asc';
        }

        if (type == 'Asc') {
            this.sortVal = key;
            this.sortType = 'Desc';
        } else if (type == 'Desc') {
            this.sortVal = "-" + key;
            this.sortType = 'Asc';
        }
        this.previousKey = key;

    }

    changeStatusOrDelete(indicator) {

        let j = 0;
        for (let i = 0; i < this.userDetails.length; i++) {
            if (this.userDetails[i].state) {
                this.statusIDs.push(this.userDetails[i].userId.toString());
                this.statusArray.push(this.userDetails[i].status);
                j++;
            }
        }
        if (j === 0) {
            this.errorMsg = true;
        }
        else {
            this.pa_manageUserService.changeStatusOrDelete(this.selectedPlatform.toString(), this.statusIDs, this.statusArray, indicator).subscribe(
                UserDetails => this.userDetails = UserDetails,
                error => { console.log(error) },
                () => {
                    this.statusIDs = [];
                    this.statusArray = [];
                }
            );
            this.errorMsg = false;
        }
    }

    useridactive(isvisible, item) {
        this.userId = item.userId;
        this.showModal = isvisible;
        this.selectedObj = item;
        this.userRoleId = item != '' || item != null ? item.roleId : '';
        this.updatedMsg = false;
        this.errorMsg = false;
        this.failureMsg = false;
    }

    closeModal(event) {
        if (event.target.className == "modal") {
            this.errorRolesMsg = false;
            this.errorRolesClass = false;
            this.newUserAdded = false;
            this.isNewUser = false;
            this.noUserAvailable = false;
            this.noIdEntered = false;
            this.showModal = false;
            this.viewModal = false;

            this.searchForm.reset();
            this.searchUserRole = "0";
            this.rolesVisible = false;
            this.loggedInUser = false;
        }
    }

    updateUserRole() {

        let roleName = "";
        this.roles.forEach(element => {
            if (element['roleId'] == this.userRoleId) {
                roleName = element['roleName'];
            }
        });

        if (this.selectedObj['roleId'] === this.userRoleId) {
            this.updatedMsg = true;
            this.failureMsg = false;
            this.roleUpdatedMsg = "Please choose a different role to update";
        }
        else {
            this.pa_manageUserService.updateUserRole(this.selectedPlatform.toString(), this.userRoleId, roleName, this.selectedObj['guid'], this.selectedObj['userId']).subscribe(
                UserDetails => {
                    this.updateResponse = UserDetails;
                    if (this.updateResponse[0]['userId'] != null) {
                        this.updatedMsg = true;
                        this.failureMsg = false;
                        this.updateResponse = UserDetails;
                        this.roleUpdatedMsg = "Roles Updated. Please close the popup and refresh the page to see the change";
                    }
                    else {
                        this.failureMsg = true;
                        this.updatedMsg = false;
                        this.rolefailureMsg = "Failed to Update Role. Kindly try after sometime.";
                    }
                },
                error => { console.log(error) },
                () => { }
            );
        }
    }

    isOpenUserModel(show) {
        this.errorRolesMsg = false;
        this.errorRolesClass = false;
        this.isNewUser = false;
        this.noUserAvailable = false;
        this.noIdEntered = false;
        this.newUserAdded = false;
        this.viewModal = show;
        if (show) {

        }
        else {
            this.searchForm.reset();
            this.searchUserRole = "0";
            this.rolesVisible = false;
        }
        this.loggedInUser = false;

    }

    getUserDetails(adsId) {
        this.errorRolesMsg = false;
        this.errorRolesClass = false;
        if (adsId === undefined || adsId === '') {
            this.noIdEntered = true;
            this.noUserAvailable = false;
            this.loggedInUser = false;
        }
        else {
            this.noIdEntered = false;
            this.pa_manageUserService.getUserDetails(adsId).subscribe(
                searchDetails => {
                    this.userSearchDetails = searchDetails;
                    if (JSON.stringify(this.userSearchDetails) !== "[]" && this.userSearchDetails[0].axppGuid === this._model.globalStorage.getItem("guid")) {
                        this.loggedInUser = true;
                        this.noUserAvailable = false;
                        this.buttonVisible = false;
                    }
                    else {
                        this.loggedInUser = false;
                        if (JSON.stringify(this.userSearchDetails) == "[]") {
                            this.noUserAvailable = true;
                        }
                        else {
                            this.noUserAvailable = false;
                            this.insertSearchDetails();
                            this.buttonVisible = true;
                        }
                    }
                },
                error => { console.log(error) }
            );
        }
    }

    insertSearchDetails() {
        this.searchForm.controls['firstName'].patchValue(this.userSearchDetails[0].givenName);
        this.searchForm.controls['lastName'].patchValue(this.userSearchDetails[0].sn);
        this.searchForm.controls['middleName'].patchValue("");
        this.searchForm.controls['dept'].patchValue(this.userSearchDetails[0].department);
        this.searchForm.controls['leader'].patchValue("");
        this.searchForm.controls['userEmailId'].patchValue(this.userSearchDetails[0].mail);
        this.rolesVisible = true;
    }
    rolesSelected() {

    }
    saveForm(model: any, isValid: boolean) {
        if (this.searchUserRole === '0') {
            this.errorRolesMsg = true;
            this.errorRolesClass = true;
        }
        else {
            this.errorRolesMsg = false;
            this.errorRolesClass = false;
            let roleName = "";
            let companyName = "";
            this.roles.forEach(element => {
                if (element['roleId'] == this.searchUserRole) {
                    roleName = element['roleName'];
                }
            });

            for (var i in this.Platform) {
                if (this.Platform[i].organizationId.toString() == this.selectedPlatform) {
                    companyName = this.Platform[i].orgName;
                }
            }


            let input = new Object;
            input["publicGUID"] = (this.userSearchDetails[0].axppGuid === undefined ? '' : this.userSearchDetails[0].axppGuid);
            input["firstname"] = model.firstName;
            input["lastname"] = model.lastName;
            input["emailaddress"] = model.userEmailId;
            input["companyid"] = this.selectedPlatform;
            input["companyname"] = companyName;
            input["roleId"] = this.searchUserRole;
            input["roleName"] = roleName;
            input["publicGUID"] = this.userSearchDetails[0].axppGuid;
            input["phonenumber_1"] = this.userSearchDetails[0].telephoneNumber;

            let organizationID: string = '';
            organizationID = this.selectedPlatform;

            this.pa_manageUserService.addUser(input).subscribe(
                adminPl => {
                    this.addUserResp = adminPl;
                    if (this.addUserResp['result'] == "pass") {
                        this.newUserAdded = true;
                        this.isNewUser = false;
                        this.rolesVisible = false;
                        this.buttonVisible = false;
                        this.searchForm.reset();
                        this.searchUserRole = "0"
                        model.submitted = true;

                        this.pa_manageUserService.getUdetails(organizationID).subscribe(
                            UserDetails => this.userDetails = UserDetails,
                            error => { console.log(error) });

                    }
                    else if (this.addUserResp['result'] == "fail") {
                        this.newUserAdded = false;
                        this.isNewUser = true;
                        this.rolesVisible = false;
                        this.buttonVisible = false;
                        this.searchForm.reset();
                        this.searchUserRole = "0"
                        this.userAddFailureMsg = "Failed to add new user.";
                    }
                    else {
                        this.newUserAdded = false;
                        this.isNewUser = true;
                        this.rolesVisible = false;
                        this.buttonVisible = false;
                        this.searchForm.reset();
                        this.searchUserRole = "0"
                        this.userAddFailureMsg = "User already exists . Kindly close the popup and click on userid to assign roles.";
                    }
                }
                ,
                error => { console.log(error) },
                () => { }
            )
        }
    }
}
