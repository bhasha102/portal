import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Platform } from './pa-platform';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PA_AddProductService {

  private baseUrl_AddProduct: string = "http://localhost:8085/dev-portal-intra/addMapProdDetails";
  private baseUrl_prodNames: string = "http://localhost:8085/dev-portal-intra/getProductNames";

  // private baseUrl_AddProduct: string = "http://localhost:8085/dev-portal-intra/addProdDetails";

  responseObj: any;
  headers = new Headers();
  constructor(private http: Http) {
  }

  submitproduct(formData: Object): Observable<JSON> {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    return this.http.post(this.baseUrl_AddProduct, formData, { headers: this.headers, withCredentials: true }) // ...using post request
      .map((res: Response) => res.json())
      .do(data => {
        this.responseObj = data;
      })
      .catch((error: any) => Observable.throw(error || 'Server error')); //...errors if any
  }

  checkProdctName(input): Observable<JSON> {
    let params = new URLSearchParams();
    params.set('prodName', input);
    return this.http.post(this.baseUrl_prodNames, params, { withCredentials: true })
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }
}