import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { PA_AddProductService } from './pa-addProduct.service';
import { PA_ManageUserService } from '../../manageUsers/pa-manageUsers.service';

import { Platform } from './pa-platform';
import { ApigeeProduct } from './pa-apigeeProducts';
import { ModelLocator } from '../../../modelLocator';

@Component({
    selector: 'pa-addproduct',
    templateUrl: './pa-addProduct.component.html',
    styleUrls: ['./pa-addProduct.component.less']
})

export class PA_AddProductComponent implements OnInit {
    public myForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    errorMessage: string;
    showModal: boolean = false;
    loaded: boolean = false;
    imageLoaded: boolean = false;
    imageSrc: string = '';
    file: Blob;
    returnVal: JSON;
    apigeeList: JSON;
    textValue = [];
    saveList: Array<string> = [];
    isApigeeProd: string = "nonapigee";
    showtext: boolean = true;
    showmultiple: boolean;
    selectOption: any = "0";
    select: any;
    platformDetails: JSON;
    newApigeeList: Array<ApigeeProduct>;
    selectCategory: string = 'nonapigee';
    selectcategory = [{ name: "Non-Apigee Product", code: "nonapigee" }, { name: "Link Apigee Product", code: "realtime" }];
    isSuccess: boolean = false;
    isFailure: boolean = false;
    prodCategory: string = "PRIVATE";
    failureMsg: string = "";
    errshowmultiple: boolean = false;
    productNameResp = JSON;
    isNamenotAvailable = false;
    fr: FileReader = new FileReader();
    isInvalidFile = false;
    isEmptyFile = true;

    _model = ModelLocator.getInstance();

    constructor(private _fb: FormBuilder, private _addproductService: PA_AddProductService, private _manageUserService: PA_ManageUserService) { }

    ngOnInit() {
        this.myForm = this._fb.group({
            prodDescription: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][\na-zA-Z0-9 \-]*")]],
            prodName: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z][a-zA-Z0-9 \-]*")]],
            adminProduct: [''],
            prodLocation: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            fileUpload: [''],
            chooseCategory: [''],
            prodCategory: ['']

        });
        this.platformDetails = null;
        this.getAddPlatform();
        this.select = "Non-Apigee Product";

    }

    isEmpty(obj) {
        return Object.keys(obj).length === 0;
    }

    isApigeeProduct(apigeeProd) {
        this.errshowmultiple = false;
        console.log(this.apigeeList);
        if ("realtime" == apigeeProd) {
            if (this.isEmpty(this.apigeeList) == true) {
                this.errshowmultiple = true;
                this.showModal = false;
                this.showtext = false;
                this.showmultiple = true;
            }
            else {
                this.convertApigeeList(this.apigeeList)
            }
        }
        else {
            this.selectCategory = "nonapigee";
            this.showModal = false;
            this.showtext = true;
            this.showmultiple = false;
            this.errshowmultiple = false;
        }
    }

    removeUpdateMsg() {
        this.isSuccess = false;
        this.isFailure = false;
    }

    getAddPlatform() {
        if (this._model.pa_details == null || this._model.pa_details == undefined || this._model.pa_details['orgList'] == null || this._model.pa_details['orgList'] == undefined
            || this._model.pa_details['oAuthMap'] == null || this._model.pa_details['oAuthMap'] == undefined) {
            this._manageUserService.getPlatforms_Apigee().subscribe(
                UserDetails => {
                    this.platformDetails = (UserDetails['orgList']);
                    this.apigeeList = (UserDetails['oAuthMap']);
                },
                error => { console.log(error) }
            );
        }
        else {
            this.platformDetails = (this._model.pa_details['orgList']);
            this.apigeeList = (this._model.pa_details['oAuthMap']);
        }
    }

    handleInputChange(e) {
        this.file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        if (this.file == undefined) {
            this.isEmptyFile = true;
            return;
        }
        else {
            this.isEmptyFile = false;
        }
        let ext = this.file['name'].split('.');
        if (ext[ext.length - 1] != 'json') {
            this.isInvalidFile = true;
        }
        else {
            this.isInvalidFile = false;
            this.fr.readAsBinaryString(this.file); 
            console.log(this.fr); 
        }
    }

    convertApigeeList(list) {
        this.newApigeeList = [];
        for (var key in list) {
            var value = list[key];
            let tempObj: ApigeeProduct = { name: "", author: "", state: false };
            tempObj['name'] = key;
            tempObj['author'] = value;
            tempObj['state'] = false;
            this.newApigeeList.push(tempObj);
        }
        this.selectCategory = "realtime";
        this.showModal = true;
        this.showmultiple = true;
        this.showtext = false;
        this.errshowmultiple = false;
    }

    save(formValue, isValid) {
        var input = new Object();
         if (this.isNamenotAvailable) {
            return false;
        }
        if (this.isInvalidFile || this.isEmptyFile) {
            isValid = false;
            this.isInvalidFile = true;
            return false;
        }

        var input1 = new Object();
        input1['product_desc'] = formValue.prodDescription;
        input1['product_name'] = formValue.prodName;
        input1['orgId'] = +formValue.adminProduct;
        input1['product_loc'] = formValue.prodLocation;
        if (formValue.chooseCategory == "nonapigee") {
            input['nonApigeeProducts'] = "NonApigeeProduct";
        }
        else {
            input["apigeeproducts"] = this.saveList;
        }

        input1['category'] = formValue.prodCategory;
        input['products'] = input1;
        input['fileString'] = this.fr.result;
        this._addproductService.submitproduct(input).subscribe(
            adminPl => {
                this.returnVal = adminPl;
                if (this.returnVal['status'] == "pass") {
                    this.isSuccess = true;
                    this.isFailure = false;
                    this.submitted = true;
                    this.myForm.reset();
                    this.selectCategory = 'nonapigee';
                    this.prodCategory = "PRIVATE";
                    this.selectOption = "0";
                    this.showModal = false;
                    this.showtext = true;
                    this.showmultiple = false;
                }
                else if (this.returnVal['status'] == "EMPTY") {
                    this.failureMsg = "JSON file is empty.Kindly upload valid JSON file.";
                    this.isFailure = true;
                    this.isSuccess = false;
                }
                else if (this.returnVal['status'] == "Invalid_Json") {
                    this.failureMsg = "JSON file is invalid.Kindly upload valid JSON file.";
                    this.isFailure = true;
                    this.isSuccess = false;
                }
                else {
                    this.failureMsg = "Failed to add Product. Please try after some time.";
                    this.isFailure = true;
                    this.isSuccess = false;
                }
            },
            error => { console.log(error) },
            () => { }
        )
    }


    displaySelect() {
        let j = 0;
        this.textValue = [];
        this.saveList = [];
        for (let i = 0; i < this.newApigeeList.length; i++) {
            if (this.newApigeeList[i].state) {
                this.textValue[j] = this.newApigeeList[i].name;
                this.saveList[j] = this.newApigeeList[i].name + "^" + this.newApigeeList[i].author;

                j++;
            }
        }

        if (j === 0) {
            this.isApigeeProd = "nonapigee";
            this.selectCategory = "nonapigee";
            this.showtext = true;
            this.showmultiple = false;
        }
        else {
            this.isApigeeProd = "realtime";
            this.selectCategory = "realtime";
            this.showtext = false;
            this.showmultiple = true;
        }
        this.showModal = false;
    }

    close() {
        this.isApigeeProd = "nonapigee";
        this.selectCategory = "nonapigee";
        this.showtext = true;
        this.showmultiple = false;
        this.showModal = false;
    }

    closeModal(event) {
        if (event.target.className == "modal") {
            this.showModal = false;
            this.showtext = true;
            this.showmultiple = false;
            this.selectCategory = "nonapigee";
        }
    }

    checkProductName() {
        let input = this.myForm.controls['prodName'].value;
        this._addproductService.checkProdctName(input).subscribe(
            prodName => {
                this.productNameResp = prodName;
                if (this.productNameResp['result'] == "exists") {
                    this.isNamenotAvailable = true;
                }
                else {
                    this.isNamenotAvailable = false;
                }
            },
            error => { console.log(error) }
        )
    }
}

