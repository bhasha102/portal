import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AdminProduct } from './adminproduct';
import { PA_EditProductService } from './pa-editProduct.service';
import { PA_ManageUserService } from '../../manageUsers/pa-manageUsers.service';
import { Platform } from '../../../admin/platform/platform';
import { AdminProductSelect } from './adminproductselect';
import { ModelLocator } from '../../../modelLocator';
import { PA_AddProductService } from '../addProduct/pa-addProduct.service';

@Component({
    selector: 'editProduct',
    templateUrl: './pa-editProduct.component.html',
    styleUrls: ['./pa-editProduct.component.less']
})

export class PA_EditProductComponent implements OnInit {
    public myForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    errorMessage: string;
    editPRO: AdminProduct;
    showModal: boolean = false;
    loaded: boolean = false;
    imageLoaded: boolean = false;
    imageSrc: string = '';
    file: Blob;
    returnVal: any;
    textValue = [];
    saveList: Array<string> = [];
    platformDetails: JSON;
    showtext: boolean = true;
    showmultiple: boolean;
    selectOption: any;
    select: any;
    listPRO: AdminProductSelect[];
    categoryUpdate: string = "PRIVATE";
    prodCategory: string = "PRIVATE";
    fileUpdated: string;
    apProdList: Array<string> = [];
    isApigeeProd: string = "nonapigee";
    updatedPlatformId: string;
    selectPlatform: string = "0";
    selectCategory: string = 'nonapigee';
    updatedMsg: boolean = false;
    category = [{ name: "Non-Apigee Product", code: "nonapigee" }, { name: "Link Apigee Product", code: "realtime" }];
    isError: boolean = false;
    errorMsg: string;
    selectedProduct: string = "0";
    fr: FileReader = new FileReader();

    productNameResp = JSON;
    selectProName: boolean = false;
    isNamenotAvailable = false;
    pName: string = '';

    _model = ModelLocator.getInstance();

    constructor(private _fb: FormBuilder, private _editproductService: PA_EditProductService, private _manageUserService: PA_ManageUserService, private _addproductService: PA_AddProductService) { }

    ngOnInit() {
        this.myForm = this._fb.group({
            prodDescription: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][\na-zA-Z0-9 \-]*")]],
            prodName: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z][a-zA-Z0-9 \-]*")]],
            editProduct: ['', Validators.compose([Validators.required, this.validateDefaultProduct])],
            prodLocation: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            fileUpload: [{ value: '', disabled: true }],
            chooseCategory: [{ value: '', disabled: true }],
            platform: [{ value: '', disabled: true }, Validators.compose([Validators.required, this.validateDefaultProduct])],
            prodCategory: [{ value: '', disabled: true }]
        });

        this.getDetails();
        this.select = "Non-Apigee Product";

    }
    validateDefaultProduct(fieldControl: FormControl) {
        if (fieldControl.value != undefined && fieldControl.value != null) {
            return fieldControl.value === '0' ? { defaultError: "error" } : null;
        }
        else {
            return null;
        }
    }

    isApigeeProduct(apigeeProd) {
        if ("realtime" == apigeeProd) {
            this.selectCategory = this.category[1].code;
            this.showmultiple = true;
            this.showtext = false;
        }
        else {
            this.selectCategory = this.category[0].code;
            this.showtext = true;
            this.showmultiple = false;
        }
    }

    getDetails() {
        if (this._model.pa_details == null || this._model.pa_details == undefined ||
            this._model.pa_details['orgList'] == null || this._model.pa_details['orgList'] == undefined) {
            this._manageUserService.getPlatforms_Apigee().subscribe(
                UserDetails => {
                    this.platformDetails = (UserDetails['orgList']);
                },
                error => { console.log(error) }
            );
        }
        else {
            this.platformDetails = (this._model.pa_details['orgList']);
        }


        this._editproductService.getProducts().subscribe(
            adminPRO => this.listPRO = adminPRO,
            error => { console.log(error) });
    }

    onChange(prodId) {
        if (prodId == "0") {
            this.myForm.get("prodName").reset();
            this.myForm.get("prodDescription").reset();
            this.myForm.get("prodLocation").reset();
            this.myForm.get("platform").reset();
            this.myForm.get("fileUpload").reset();
            this.showtext = true;
            this.showmultiple = false;
            this.selectedProduct = "0";
            this.selectPlatform = "0";
            this.fileUpdated = "";
            this.myForm.get("platform").disable();
            this.myForm.get("prodCategory").disable();
            this.myForm.get("fileUpload").disable();
            this.myForm.get("chooseCategory").disable();
            this.prodCategory = "PRIVATE";
            this.selectCategory = "nonapigee";
        }
        else {
            this.myForm.get("prodName").reset();
            this.myForm.get("prodDescription").reset();
            this.myForm.get("prodLocation").reset();
            this.myForm.get("platform").reset();
            this.myForm.get("fileUpload").reset();
            this.showtext = true;
            this.showmultiple = false;
            this.selectedProduct = "0";
            this.selectPlatform = "0";
            this.fileUpdated = "";
            this.myForm.get("platform").disable();
            this.myForm.get("prodCategory").disable();
            this.myForm.get("fileUpload").disable();
            this.myForm.get("chooseCategory").disable();
            this.prodCategory = "PRIVATE";
            this.selectCategory = "nonapigee";

            this._editproductService.getAdminProductDetails(prodId).subscribe(
                adminPRO => {
                    this.editPRO = adminPRO;
                    this.updateProductDetails();
                },
                error => { console.log(error) },
                () => { }
            );
        }
    }

    updateProductDetails() {
        this.myForm.controls['prodName'].patchValue(this.editPRO.prodName);
        this.myForm.controls['prodDescription'].patchValue(this.editPRO.prodDs);
        this.myForm.controls['prodLocation'].patchValue(this.editPRO.prodLocTx);
        if (this.editPRO.apProdList[0] === 'NonApigeeProduct') {
            this.showtext = true;
            this.showmultiple = false;
            this.isApigeeProd = "nonapigee";
            this.selectCategory = this.category[0].code;
        }
        else {
            this.showmultiple = true;
            this.showtext = false;
            this.apProdList = this.editPRO.apProdList;
            this.isApigeeProd = "realtime";
            this.selectCategory = this.category[1].code;
        }
        this.selectPlatform = this.editPRO.orgId.toString();
        this.prodCategory = this.editPRO.pvtCtgyId.trim();
        this.fileUpdated = this.editPRO.prodName + ".json";
        this.pName = this.editPRO.prodName;
        this.selectProName = true;
        this.myForm.get("platform").enable();
        this.myForm.get("prodCategory").enable();
        this.myForm.get("fileUpload").enable();
        this.myForm.get("chooseCategory").enable();
    }

    handleInputChange(e) {
        this.file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        this.fr.readAsBinaryString(this.file);
    }

    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    }

    save(formValue, isValid) {
        var input = new Object();
        var input1 = new Object();
        input1['product_ID'] = this.editPRO.prodId;
        input1['product_desc'] = formValue.prodDescription;
        input1['product_name'] = formValue.prodName;
        input1['orgId'] = +formValue.platform.toString();
        input1['product_loc'] = formValue.prodLocation;
        input1['category'] = formValue.prodCategory;
        input['products'] = input1;
        input['prevProdName'] = this.editPRO.prodName;
        if (formValue.chooseCategory == "nonapigee") {
            input['nonApigeeProducts'] = "NonApigeeProduct";
        }
        else {
            input["apigeeproducts"] = this.textValue;
        }

        input['fileString'] = this.fr.result;
        this._editproductService.saveEditProductDetails(input).subscribe(
            adminPl => {
                this.returnVal = adminPl
                if (this.returnVal[0]['prodId'] != 0) {
                    if (this.returnVal[0]['product_desc'] != null) {
                        this.updatedMsg = false;
                        this.isError = true;
                        this.errorMsg = "JSON file is invalid.Kindly upload valid JSON file."
                    }
                    else {
                        this.updatedMsg = true;
                        this.isError = false;
                        this.submitted = true;
                        this.pName = formValue.prodName;
                    }
                }
                else {
                    this.updatedMsg = false;
                    this.isError = true;
                    this.errorMsg = "Failed to update Product. Kindly try after some time."
                }
            },
            error => { console.log(error) },
            () => { })
    }

    removeUpdateMsg() {
        this.updatedMsg = false;
        this.isError = false
    }

    displaySelect() {

    }

    checkProductName() {
        let input = this.myForm.controls['prodName'].value;
        if ((this.pName.toLowerCase() === input.toLowerCase() && input !== '') || input === null)
            this.isNamenotAvailable = false;
        if (this.pName.toLowerCase() !== input.toLowerCase() && input !== '' && input !== null) {
            this._addproductService.checkProdctName(input).subscribe(
                prodName => {
                    this.productNameResp = prodName;
                    if (this.productNameResp['result'] == "exists") {
                        this.isNamenotAvailable = true;
                    }
                    else {
                        this.isNamenotAvailable = false;
                    }
                },
                error => { console.log(error) }
            )
        }
    }
}
