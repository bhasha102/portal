import { Injectable } from '@angular/core';
import { AdminProduct } from './adminproduct';
import { AdminProductSelect } from './adminproductselect';
import { Platform } from '../../../admin/platform/platform';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PA_EditProductService {

  // private baseUrl_Products: string = "./app/platform-admin/products.json";
  // private baseUrl_ProdDetails: string = "/app/platform-admin/prodDetails.json";
  // private baseUrl_EditProduct: string = "http://localhost:8085/dev-portal-intra/editProdDetails";


  private baseUrl_Products: string = "http://localhost:8085/dev-portal-intra/getProductsForEditDelete";
  private baseUrl_ProdDetails: string = "http://localhost:8085/dev-portal-intra/getProdDetail";
  private baseUrl_EditProduct: string = "http://localhost:8085/dev-portal-intra/editProdDetails";

  headers = new Headers();

  constructor(private http: Http) {
  }

  getProducts(): Observable<AdminProductSelect[]> {
    this.headers = new Headers();
    return this.http.get(this.baseUrl_Products, { headers: this.headers, withCredentials: true })
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }

  getAdminProductDetails(prodId: string): Observable<AdminProduct> {
    this.headers = new Headers();
    let params = new URLSearchParams();
    params.set('prodId', prodId);
    return this.http.post(this.baseUrl_ProdDetails, params, { headers: this.headers, withCredentials: true })
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }

  saveEditProductDetails(formData: Object): Observable<String> {
    console.log(formData);
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    return this.http.post(this.baseUrl_EditProduct, formData, { headers: this.headers, withCredentials: true }) // ...using post request
      .map((res: Response) => res)
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

}