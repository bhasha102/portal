import { Injectable } from '@angular/core';
import { ProdList } from './delete-product';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PA_DeleteProductService {

  private baseUrl_DeleteProduct: string = "http://localhost:8085/dev-portal-intra/deleteProdDetails";

  //private baseUrl_Products: string ="http://localhost:8085/dev-portal-intra/getProdList";
  // private baseUrl_DeleteProduct: string ="http://localhost:8085/dev-portal-intra/deleteProdDetails";

  constructor(private http: Http) {
  }

  deleteProduct(prodId, prodName): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });//... Set content type to JSON

    let body = JSON.stringify({ "product_ID": prodId, "product_name": prodName });
    return this.http.post(this.baseUrl_DeleteProduct, body, { headers: headers, withCredentials: true })
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
}