import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { PA_DeleteProductService } from './pa-deleteProduct.service';
import { PA_EditProductService } from '../editProduct/pa-editProduct.service';
import { ProdList } from './delete-product';
import { ModelLocator } from '../../../modelLocator';

@Component({

    selector: 'deleteproduct',
    templateUrl: './pa-deleteProduct.component.html',
    styleUrls: ['./pa-deleteProduct.component.less']
})

export class PA_DeleteProductComponent implements OnInit {
    public myForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    errorMessage: string;
    product: ProdList[];
    updatedMsg: boolean = false;
    errorMsg: boolean = false;
    deleteProduct: string = "0";
    response: any;
    errorString: string = "Please select a Product.";
    errorClass: boolean = false;
    _model = ModelLocator.getInstance();

    constructor(private _fb: FormBuilder, private _delproductService: PA_DeleteProductService, private _editproductService: PA_EditProductService) { }
    ngOnInit() {
        this.myForm = this._fb.group({
            deleteProduct: ['']
        });
        this.getProductsList();
    }

    getProductsList() {
        this._editproductService.getProducts().subscribe(
            productDetails => this.product = productDetails,
            error => { console.log(error) });
    }

    onChange() {
        if (this.deleteProduct != undefined || this.deleteProduct === "0") {
            this.updatedMsg = false;
            this.errorMsg = false;
            this.errorClass = false;
        }
    }

    deleteRowForm() {

        if (this.deleteProduct === undefined || this.deleteProduct === "0") {
            this.errorString = "Please select a Product.";
            this.errorMsg = true;
            this.updatedMsg = false;
            this.errorClass = true;
        }
        else {
            let productID = this.deleteProduct['product_ID'];
            this._delproductService.deleteProduct(productID, this.deleteProduct['product_name']).subscribe(
                adminPl => {
                    this.response = adminPl;
                    this.updatedMsg = true;
                    if (this.response['result'] == "Success") {

                        this.updatedMsg = true;
                        this.errorMsg = false;
                        this.errorClass = false;
                        this.product = this.product.filter(function (qt) {
                            if (qt.product_ID !== productID)
                                return qt;
                        })
                        this.deleteProduct = undefined;
                    }
                    else {
                        this.errorString = "Error in deleting the product.Please try after sometime.";
                        this.updatedMsg = false;
                        this.errorMsg = true;
                    }
                },
                error => { console.log(error) }
            )
        }
    }
}