import { Component, OnInit } from '@angular/core';
import { ModelLocator } from '../modelLocator';

@Component({
  selector: 'app-platform-admin',
  templateUrl: './platform-admin.component.html',
  styleUrls: ['./platform-admin.component.less']
})
export class PlatformAdminComponent implements OnInit {

_model = ModelLocator.getInstance();

  constructor() { }

  ngOnInit() {
     this._model.pa_details = null;
  }
}
