export class ModelLocator {
    private static instance: ModelLocator;
    //Assign "new Singleton()" here to avoid lazy initialisation

    constructor() {
        if (ModelLocator.instance) {
            return ModelLocator.instance;
        }

        ModelLocator.instance = this;
        this.globalStorage.setItem("guid", "b66762c1f8c6481aa4a2a533f7c77e92");
        //guid = "b66762c1f8c6481aa4a2a533f7c77e92";  //Admin
        //guid = "0e90dc4d65264b55a05badaa8deee37a"; //Org-Admin
        //guid = "857417578666496ba260d421673da1df"; //Normal User
        // guid = "4c6676a25a0b4355b5f088e24101798b";  //E-Auth Admin
    }

    static getInstance(): ModelLocator {
        ModelLocator.instance = ModelLocator.instance || new ModelLocator();
        return ModelLocator.instance;
    }

    public globalStorage = window.sessionStorage;
    public isLoaderVisible: boolean = false;
    public pa_details: JSON;

    public ext_details: JSON;
    public int_details: JSON;

}