import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModelLocator } from '../modelLocator';

@Component({
    selector: 'admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.less']
})
export class AdminComponent implements OnInit {
    errorMessage: string;
    _model = ModelLocator.getInstance();

    public constructor() {
    }

    ngOnInit() {
        this._model.int_details = null;
    }

}

