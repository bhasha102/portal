import { Pipe, PipeTransform } from '@angular/core';
import { ViewProducts } from './view-product';

@Pipe({
    name: 'viewProductFilter',
})
export class ViewProductsPipe implements PipeTransform {

    transform(question: ViewProducts[], nameFilter: string): any {
        console.log(question);
        if (nameFilter === undefined) {
            console.log("nameFilter : " + nameFilter);
            return question;
        }

        return question.filter(function (qt) {
            if (qt.product_name.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt.product_name.toLowerCase().includes(nameFilter.toLowerCase());
            if (qt.orgName.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt.orgName.toLowerCase().includes(nameFilter.toLowerCase());
            if (qt.dateCreated.toString().toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt.dateCreated.toString().toLowerCase().includes(nameFilter.toLowerCase());
        })
    }
}