import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ManageUserService } from '../manageuser.service';
import { AddPlatformService } from '../add-platform/addplatform.service';
import { Platform } from '../platform';
@Component({

    selector: 'deleteplatform',
    templateUrl: './delete-platform.component.html',
    styleUrls: ['./delete-platform.component.less']
})
export class DeletePlatformComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
    errorMessage: string;
    platform: Platform[];
    updatedMsg: boolean = false;
    deletePlatform: string = "0";
    errorMsg: boolean = false;
    returnVal: JSON;
    errorString: string = "Please select a Platform.";
    errorClass: boolean = false;
    constructor(private _fb: FormBuilder, private _addplatformService: AddPlatformService, private _manageUserService: ManageUserService) { }
    ngOnInit() {
        this.myForm = this._fb.group({
            deleteplatform: ['']
        });
        this.getPlatformsList();
    }

    getPlatformsList() {
        this._manageUserService.getPlatforms().subscribe(
            platformDetails => this.platform = platformDetails,
            error => { console.log(error) });
    }

    onChange() {
        if (this.deletePlatform != undefined || this.deletePlatform === "0") {
            this.updatedMsg = false;
            this.errorMsg = false;
            this.errorClass = false;
        }
    }

    deleteRowForm() {
        if (this.deletePlatform == undefined || this.deletePlatform === "0") {
            this.errorString = "Please select a Platform.";
            this.errorMsg = true;
            this.updatedMsg = false;
            this.errorClass = true;
        }
        else {
            let action = "D";
            let orgObj = { "orgId": +this.deletePlatform };
            let toRemovePlatform = this.deletePlatform;
            this._addplatformService.submitPlatform(orgObj, action).subscribe(
                adminPl => {
                    this.returnVal = adminPl;
                    if (this.returnVal['statusCode'] == 300) {
                        this.updatedMsg = true;
                        this.submitted = true;
                        this.updatedMsg = true;
                        this.errorMsg = false;
                        this.errorClass = false;
                        this.platform = this.platform.filter(function (qt) {
                            if (qt.organizationId.toString() !== toRemovePlatform)
                                return qt;
                        })
                        this.deletePlatform = "0";
                    }
                    else {
                        this.errorString = "Failed to delete Platform. Please try after some time.";
                        this.errorMsg = true;
                        this.updatedMsg = false;
                        this.errorClass = false;
                    }
                },
                error => { console.log(error) },
                () => { console.log(this.returnVal) }
            )

        }
    }
}