
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Country } from '../country';
import { JsonpModule } from '@angular/http';
import { States } from '../states';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AddPlatformService {
  /*private baseUrl: string = "./app/admin/platform/add-platform/countries.json";//'http://localhost:8085/dev-portal-intra/dummyProducts';
  private baseUrl1: string = "./app/admin/platform/add-platform/states.json";
  */
  private baseUrl: string = 'http://localhost:8085/dev-portal-intra/adminAddOrg';
  private baseUrl1: string = 'http://localhost:8085/dev-portal-intra/getState';
  private baseUrl2: string = 'http://localhost:8085/dev-portal-intra/editOrgDetails';
  private baseUrl3: string = 'http://localhost:8085/dev-portal-intra/getOrgNames';

  headers = new Headers();
  constructor(private http: Http) {
  }

  getCountryList(): Observable<Country[]> {
    this.headers = new Headers();

    return this.http.get(this.baseUrl,{ headers: this.headers,withCredentials:true })
      // ...and calling .json() on the response to return data
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw('Server error'))
  }

  getStatesList(countryCode): Observable<States[]> {
    this.headers = new Headers();

    let params = new URLSearchParams();
    params.set('countryCode', countryCode);

    return this.http.get(this.baseUrl1, { search: params, headers: this.headers,withCredentials:true })
      // ...and calling .json() on the response to return data
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw('Server error'))
  }

  checkPlatformName(input): Observable<JSON> {
    this.headers=new Headers();
    let params = new URLSearchParams();
    params.set('orgName', input);
    return this.http.post(this.baseUrl3, params,{ headers: this.headers,withCredentials:true })
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }
  submitPlatform(form, action): Observable<JSON> {
    this.headers = new Headers();

    this.headers.append('Content-Type', 'application/json');
    let jsonData = JSON.stringify({ "action": action, "orgdata": form });
    console.log(form);
    console.log(jsonData);
    return this.http.post(this.baseUrl2, jsonData, { headers: this.headers,withCredentials:true }) // ...using post request
      .map((res: Response) => res.json())
      // ...and calling .json() on the response to return data

      .catch((error: any) => Observable.throw(error || 'Server error')); //...errors if any

  }
}