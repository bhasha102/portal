import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Platform } from '../platform';
import { AddPlatformService } from './addplatform.service';
import { Country } from '../country';
import { States } from '../states';

@Component({

    selector: 'addPlatform',
    templateUrl: './addplatform.component.html',
    styleUrls: ['./addplatform.component.less']
})
export class AddPlatformComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
    country: Array<Country>;
    states: Array<States>;
    errorMessage: string;
    selectCountry: string = 'US';
    selectStates: string = '001';
    updatedMsg: boolean = false;
    returnVal: JSON;
    isFailure = false;
    failureMsg = "";
    platformNameResp = JSON;
    isNamenotAvailable = false;
    constructor(private _fb: FormBuilder, private _addplatformService: AddPlatformService) { } // form builder simplify form initialization

    ngOnInit() {
        this.myForm = this._fb.group({
            platformName: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            phoneNumber: ['', [<any>Validators.required]],
            addressline1: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            addressline2: [''],
            city: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z][a-zA-Z0-9 \-]*")]],
            zip: ['', [<any>Validators.required, Validators.minLength(5), Validators.maxLength(5)]],
            country: [''],
            states: ['']
        });

        this.getCountryList();
        this.getStatesList('US');
    }

    checkPlatformName() {
        let input = this.myForm.controls['platformName'].value;
        this._addplatformService.checkPlatformName(input).subscribe(
            prodName => {
                this.platformNameResp = prodName;
                if (this.platformNameResp['result'] == "exists") {
                    this.isNamenotAvailable = true;
                }
                else {
                    this.isNamenotAvailable = false;
                }
            },
            error => { console.log(error) }

        )
    }

    getCountryList() {
        this._addplatformService.getCountryList().subscribe(
            country => this.country = country,
            error => { console.log(error) }
        );

    }
    getStatesList(countryId) {
        console.log(countryId);
        this._addplatformService.getStatesList(countryId).subscribe(
            states => {
                this.states = states;
                this.selectStates = '001';
            },
            error => { console.log(error) },

        );
    }

    save(model, isValid: boolean) {
        if (this.isNamenotAvailable) {
            return false;
        }
        let input = new Object;
        input["orgName"] = model.platformName;
        input["addressLine1"] = model.addressline1;
        input["addressLine2"] = model.addressline2;
        input["country"] = model.country;
        input["state"] = model.states;
        input["phoneNumber"] = model.phoneNumber;
        input["city"] = model.city;
        input["zip"] = model.zip;
        let action = "I";

        this._addplatformService.submitPlatform(input, action).subscribe(
            adminPl => {
                this.returnVal = adminPl;
                console.log(this.returnVal);
                if (this.returnVal['statusCode'] == 100) {
                    console.log("Success");
                    this.updatedMsg = true;
                    this.myForm.reset();
                    this.submitted = true;
                    this.myForm.reset();
                    this.submitted = true; // set form submit to true
                    this.isFailure = false;
                }
                else {
                    this.failureMsg = "Failed to update Platform.Please try after some time.";
                    console.log("Failure");
                    this.isFailure = true;
                    this.updatedMsg = false;
                }
            }
            ,
            error => { console.log(error) },
            () => { console.log(this.returnVal) }
        )
    }

    removeUpdateMsg() {
        this.updatedMsg = false;
    }
}