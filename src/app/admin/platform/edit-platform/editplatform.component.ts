import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Platform } from '../platform';
import { EditPlatformService } from './editplatform.service';
import { AddPlatformService } from '../add-platform/addplatform.service';
import { ManageUserService } from '../manageuser.service';
import { Country } from '../country';
import { States } from '../states';


@Component({
    selector: 'editPlatform',
    templateUrl: './editplatform.component.html',
    styleUrls: ['./editplatform.component.less']
})
export class EditPlatformComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
    country: Array<Country>;
    states: Array<States>;
    errorMessage: string;
    platformList: Platform[];
    platformDetails: Platform[];
    success: string;
    error: string;
    showMessage = false;
    submitMessage: string;
    successMsg = false;
    selectCountry: string = 'US';
    selectStates: string = '001';
    updatedMsg: boolean = false;
    returnVal: JSON;
    selectedPlatform: string = "0";
    failureMsg = "";
    isFailure = false;
    platformNameResp = JSON;
    isNamenotAvailable = false;
    pName: string;
    selectPlName: boolean = false;

    constructor(private _fb: FormBuilder, private _editplatformService: EditPlatformService, private _platformListService: ManageUserService, private _addplatformService: AddPlatformService) { } // form builder simplify form initialization

    ngOnInit() {
        this.myForm = this._fb.group({
            orgName: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            platformName: ['', Validators.compose([Validators.required, this.validateDefaultProduct])],
            phoneNumber: ['', [<any>Validators.required]],
            addressLine1: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            addressLine2: [''],
            city: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z][a-zA-Z0-9 \-]*")]],
            zip: ['', [<any>Validators.required, Validators.minLength(5), Validators.maxLength(5)]]
            //country: [''],
            //states: ['']
        });

        this.getPlatformList();
        this.getCountryList();
        this.getStatesList("US");


    }

    validateDefaultProduct(fieldControl: FormControl) {
        if (fieldControl != undefined && fieldControl != null) {
            return fieldControl.value == '0' ? { defaultError: "error" } : null;
        }
        else {
            return null;
        }
    }

    checkPlatformName() {
        let input = this.myForm.controls['orgName'].value;
        if ((this.pName.toLowerCase() === input.toLowerCase() && input !== '') || input === null)
            this.isNamenotAvailable = false;
        if (this.pName.toLowerCase() !== input.toLowerCase() && input !== '' && input !== null) {
            this._addplatformService.checkPlatformName(input).subscribe(
                prodName => {
                    this.platformNameResp = prodName;
                    console.log(this.platformNameResp);
                    if (this.platformNameResp['result'] == "exists") {
                        this.isNamenotAvailable = true;
                    }
                    else {
                        this.isNamenotAvailable = false;
                    }
                },
                error => { console.log(error) }

            )
        }
    }
    removeUpdateMsg() {
        this.updatedMsg = false;
    }

    getPlatformList() {
        this._platformListService.getPlatforms().subscribe(
            platformDetails => this.platformList = platformDetails,
            error => { console.log(error) });

    }
    onChange(orgId) {
        if (orgId == "0") {
            this.myForm.reset();
            this.selectedPlatform = "0";
            this.selectPlName = false;
            this.selectCountry = 'US';
            this.getStatesList("US");
            this.selectStates = '001';
            this.isFailure = false;
        }
        else {
            this._editplatformService.getPlatformDetails(orgId).subscribe(
                UserDetails => this.platformDetails = UserDetails,
                error => { console.log(error) },
                () => {
                    if(this.platformDetails !== [] && this.platformDetails !== undefined){
                     this.isFailure = false;
                     this.updatePlatformDetails();
                    }
                    else{
                    this.failureMsg = "Sorry, Communication error with Gateway, please come back later."
                    this.isFailure = true;
                    }
                 }
            );
            this.showMessage = false;
        }
        this.isNamenotAvailable = false;
    }

    updatePlatformDetails() {
        this.myForm.controls['orgName'].patchValue(this.platformDetails[0].orgName);
        this.myForm.controls['phoneNumber'].patchValue(this.platformDetails[0].phoneNumber);
        this.myForm.controls['addressLine1'].patchValue(this.platformDetails[0].addressLine1);
        this.myForm.controls['addressLine2'].patchValue(this.platformDetails[0].addressLine2);
        this.myForm.controls['zip'].patchValue(this.platformDetails[0].zip.toString().trim());
        this.myForm.controls['city'].patchValue(this.platformDetails[0].city.trim());
        this.getStatesList(this.platformDetails[0].country); //country = countryCode
        this.selectCountry = this.platformDetails[0].country;
        this.pName = this.platformDetails[0].orgName;
        this.selectPlName = true;

    }

    getCountryList() {
        this._addplatformService.getCountryList().subscribe(
            country => this.country = country,
            error => { console.log(error) });
    }
    getStatesList(countryCode) {
        this._addplatformService.getStatesList(countryCode).subscribe(
            states => this.states = states,
            error => { console.log(error) },
            () => {

                this.selectStates = this.platformDetails != undefined ? this.platformDetails[0].state : '001'
            }
        );
    }

    save(model: any, isValid: boolean) {
        if (this.isNamenotAvailable) {
            return false;
        }
        let input = new Object;
        input["orgName"] = model.orgName;
        input["addressLine1"] = model.addressLine1;
        input["addressLine2"] = model.addressLine2;
        //input["country"] = model.country;
        input["country"] = this.selectCountry;
        //input["state"] = model.states;
        input["state"] = this.selectStates;
        input["phoneNumber"] = model.phoneNumber;
        input["city"] = model.city;
        input["zip"] = model.zip;
        input["orgId"] = +this.platformDetails[0].organizationId;

        let action = "U";

        this._addplatformService.submitPlatform(input, action).subscribe(
            adminPl => {
                this.returnVal = adminPl;

                if (this.returnVal['statusCode'] == 200) {
                    this.updatedMsg = true;
                    this.isFailure = false;
                    this.submitted = true;
                    this.pName = model.orgName;
                }
                else {
                    this.failureMsg = "Failed to update Platform. Please try after some time.";
                    this.isFailure = true;
                    this.updatedMsg = false;
                }
            },
            error => { console.log(error) },
            () => { console.log(this.returnVal) }
        )

    }
}