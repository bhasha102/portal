import { Pipe, PipeTransform } from '@angular/core';
import { UserList } from './user-list';

@Pipe({
    name: 'filter',
    pure: false
})
export class ManageUserFilterPipe implements PipeTransform {
    transform(list: UserList[], nameFilter: string): any {
        let tempStatus: Array<string> = ["ACTIVE", "INACTIVE"];
        if (nameFilter === undefined) {
            return list;
        }

        return list.filter(function (qt) {
            if (tempStatus[0].toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1 && qt.status.toLowerCase().indexOf('y') !== -1)
                return qt.status.toLowerCase().includes('y');
            if (tempStatus[1].toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1 && qt.status.toLowerCase().indexOf('n') !== -1)
                return qt.status.toLowerCase().includes('n');
            if (qt.fName.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt.fName.toLowerCase().includes(nameFilter.toLowerCase());
            if (qt.lName.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt.lName.toLowerCase().includes(nameFilter.toLowerCase());
            if (qt.userId.toString().indexOf(nameFilter) !== -1)
                return qt.userId.toString().toLowerCase().includes(nameFilter.toLowerCase());
            if (qt.emailAddress.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt.emailAddress.toLowerCase().includes(nameFilter.toLowerCase());
        })
    }
}