import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AdminAddProductService } from './addproduct.service';
import { EditPlatformService } from '../../platform/edit-platform/editplatform.service';
import { ManageUserService } from '../../platform/manageuser.service';
import { Platform } from '../../platform/platform';
import { ApigeeProduct } from './apigee-product';
import {ModelLocator} from '../../../../app/modelLocator';

@Component({
    selector: 'adminaddproduct',
    templateUrl: './addproduct.component.html',
    styleUrls: ['./addproduct.component.less']
})

export class AdminProductComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
    errorMessage: string;
    showModal: boolean = false;
    loaded: boolean = false;
    imageLoaded: boolean = false;
    imageSrc: string = '';
    file: Blob;
    returnVal: JSON;
    apigeeList: JSON;
    textValue = [];
    saveList: Array<string> = [];
    isApigeeProd: string = "nonapigee";
    showtext: boolean = true;
    showmultiple: boolean;
    selectOption: string = "0";
    select: any;
    platformDetails: Platform[];
    newApigeeList: Array<ApigeeProduct>;
    selectCategory: string = 'nonapigee';
    selectcategory = [{ name: "Non-Apigee Product", code: "nonapigee" }, { name: "Link Apigee Product", code: "realtime" }];
    isSuccess: boolean = false;
    isFailure: boolean = false;
    prodCategory: string = "PRIVATE";
    failureMsg: string = "";
    isInvalidFile = false;
    isEmptyFile = true;
    productNameResp = JSON;
    apigeeListResp = JSON;
    isNamenotAvailable = false;
    fr: FileReader = new FileReader();

    _model = ModelLocator.getInstance();

    constructor(private _fb: FormBuilder, private _addproductService: AdminAddProductService, private _addplatformService: EditPlatformService, private _manageUserService: ManageUserService) { }

    ngOnInit() {
        this.myForm = this._fb.group({
            prodDescription: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][\na-zA-Z0-9 \-]*")]],
            prodName: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9\-][a-zA-Z0-9 \-]*")]],
            adminProduct: ['', Validators.compose([Validators.required, this.validateDefaultProduct])],
            prodLocation: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            fileUpload: [''],
            chooseCategory: [''],
            prodCategory: ['']
        });
        this.platformDetails = [];
        this.getAddPlatform();
        this.getApigeeList();
        this.select = "Non-Apigee Product";

    }

    validateDefaultProduct(fieldControl: FormControl) {
        if (fieldControl != undefined && fieldControl != null) {
            return fieldControl.value == '0' ? { defaultError: "error" } : null;
        }
        else {
            return null;
        }
    }
    getApigeeList() {

        if (this._model.int_details == null || this._model.int_details == undefined) {

        this._manageUserService.getApigeeList().subscribe(
            apigeeList => {
                this.apigeeListResp = apigeeList;
                if (this.apigeeListResp == null || this.apigeeListResp['oAuthMap'] == null) {
                    this.failureMsg = "Sorry, Communication error with Gateway, please come back later for adding products.";
                    this.isFailure = true;
                    this.isSuccess = false;
                }
                else {
                    this.apigeeList = this.apigeeListResp['oAuthMap'];
                    this.isFailure = false;
                }
                console.log(this.apigeeList);
            },
            error => { console.log(error) },
            () => { this.convertApigeeList(this.apigeeList) }
        );
        }
        else{
            this.apigeeList = (this._model.int_details['oAuthMap']);
            this.convertApigeeList(this.apigeeList);
        }
    }

    isApigeeProduct(apigeeProd) {
        if ("realtime" == apigeeProd) {
            this.showModal = true;
            this.showtext = false;
            this.showmultiple = true;
        }
        else {
            this.selectCategory = "nonapigee";
            this.showModal = false;
            this.showtext = true;
            this.showmultiple = false;
        }
    }

    removeUpdateMsg() {
        this.isSuccess = false;
        this.isFailure = false;
    }

    checkProductName() {
        let input = this.myForm.controls['prodName'].value;
        this._addproductService.checkProdctName(input).subscribe(
            prodName => {
                this.productNameResp = prodName;
                if (this.productNameResp['result'] == "exists") {
                    this.isNamenotAvailable = true;
                }
                else {
                    this.isNamenotAvailable = false;
                }
            },
            error => { console.log(error) }

        )
    }

    getAddPlatform() {
        this._manageUserService.getPlatforms().subscribe(
            UserDetails => this.platformDetails = UserDetails,
            error => { console.log(error) }
        );
    }
    handleInputChange(e) {
        this.file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        if (this.file == undefined) {
            this.isEmptyFile = true;
            return;
        }
        else {
            this.isEmptyFile = false;
        }
        let ext = this.file['name'].split('.');
        if (ext[ext.length - 1] != 'json') {
            this.isInvalidFile = true;
        }
        else {
            this.isInvalidFile = false;
            this.fr.readAsBinaryString(this.file); 
            console.log(this.fr); 

        }
    }

    convertApigeeList(list) {
        this.newApigeeList = [];
        for (var key in list) {
            var value = list[key];

            let tempObj: ApigeeProduct = { name: "", author: "", state: false };
            tempObj['name'] = key;
            tempObj['author'] = value;
            tempObj['state'] = false;
            this.newApigeeList.push(tempObj);
        }
    }

    save(formValue, isValid) {
        var input = new Object();
        if (this.isNamenotAvailable) {
            return false;
        }
        if (this.isInvalidFile || this.isEmptyFile) {
            isValid = false;
            this.isInvalidFile = true;
            return false;
        }
        var input1=new Object();

        input1['product_desc']= formValue.prodDescription;
        input1['product_name']= formValue.prodName;
        input1['orgId']=+formValue.adminProduct;
        input1['product_loc']= formValue.prodLocation;
        input1['category']= formValue.prodCategory;
        input['products']=input1;
        input['fileString']=this.fr.result;
        if (formValue.chooseCategory == "nonapigee") {
            input['nonApigeeProducts']= "NonApigeeProduct";
        }
        else {
            input["apigeeproducts"]= this.saveList;
        }

        
        formValue.uploadFile = this.file;
       // input.append('fileupload', formValue.uploadFile);

        this._addproductService.submitproduct(input).subscribe(
            adminPl => {
                this.returnVal = adminPl;
                console.log(this.returnVal);
                if (this.returnVal['status'] == "pass") {
                    console.log("Success");
                    this.isSuccess = true;
                    this.isFailure = false;
                    this.submitted = true;
                    this.myForm.reset();
                    this.selectCategory = 'nonapigee';
                    this.prodCategory = "PRIVATE";
                    this.selectOption = "0";
                    this.showModal = false;
                    this.showtext = true;
                    this.showmultiple = false;
                }
                else if (this.returnVal['status'] == "EMPTY") {
                    this.failureMsg = "JSON file is empty.Kindly upload valid JSON file.";
                    console.log("Failure");
                    this.isFailure = true;
                    this.isSuccess = false;
                }
                else if (this.returnVal['status'] == "Invalid_Json") {
                    this.failureMsg = "JSON file is invalid.Kindly upload valid JSON file.";
                    console.log("Failure");
                    this.isFailure = true;
                    this.isSuccess = false;
                }
                else {
                    this.failureMsg = "Failed to add Product. Please try after some time.";
                    console.log("Failure");
                    this.isFailure = true;
                    this.isSuccess = false;
                }
            }
            ,
            error => { console.log(error) },
            () => { console.log(this.returnVal) }
        )


    }


    displaySelect() {

        let j = 0;
        this.textValue = [];
        this.saveList = [];
        for (let i = 0; i < this.newApigeeList.length; i++) {
            if (this.newApigeeList[i].state) {
                this.textValue[j] = this.newApigeeList[i].name;
                this.saveList[j] = this.newApigeeList[i].name + "^" + this.newApigeeList[i].author;
                j++;
            }
        }

        if (j === 0) {
            this.isApigeeProd = "nonapigee";
            this.selectCategory = "nonapigee";
            this.showtext = true;
            this.showmultiple = false;
        }
        else {
            this.isApigeeProd = "realtime";
            this.selectCategory = "realtime";
            this.showtext = false;
            this.showmultiple = true;
        }
        this.showModal = false;
    }
    close() {
        this.isApigeeProd = "nonapigee";
        this.selectCategory = "nonapigee";
        this.showtext = true;
        this.showmultiple = false;
        this.showModal = false;
    }

    closeModal(event) {
        if (event.target.className == "modal") {
            this.showModal = false;
            this.showtext = true;
            this.showmultiple = false;
            this.selectCategory = "nonapigee";
        }
    }
}

