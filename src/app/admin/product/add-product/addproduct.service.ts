import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AdminAddProductService {
  private baseUrl1: string = "http://localhost:8085/dev-portal-intra/addProdDetails";//"./app/admin/product/add-product/admin-product-apigee.json";
  private baseUrl3: string = "http://localhost:8085/dev-portal-intra/getProductNames";
  responseObj: any;
  headers = new Headers();
  constructor(private http: Http) {
  }
  
  checkProdctName(input): Observable<JSON> {
    let params = new URLSearchParams();
    params.set('prodName', input);
    return this.http.post(this.baseUrl3, params,{ withCredentials:true })
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }

  submitproduct(formData: Object): Observable<JSON> {
    this.headers=new Headers();
    this.headers.append('Content-Type', 'application/json');
    //let jsonData = JSON.stringify(formData);
    return this.http.post(this.baseUrl1, formData,{ headers: this.headers,withCredentials:true }) // ...using post request
      .map((res: Response) => res.json())
      .do(data => {
        this.responseObj = data;
      }) // ...and calling .json() on the response to return data

      .catch((error: any) => Observable.throw(error || 'Server error')); //...errors if any
  }



}