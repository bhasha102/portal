import { Injectable } from '@angular/core';
import { AdminProduct } from './adminproduct';
import { AdminProductSelect } from './adminproductselect';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AdminEditProductService {
    private baseUrl: string = "http://localhost:8085/dev-portal-intra/getProdList";
    private baseUrl3: string = "http://localhost:8085/dev-portal-intra/editProdDetails";
    private baseUrl1: string = "http://localhost:8085/dev-portal-intra/getProdDetail";
    headers = new Headers();

    constructor(private http: Http) {
    }

    getAdminProductDetails(prodId: string): Observable<AdminProduct> {
        let params = new URLSearchParams();
        params.set('prodId', prodId);
        return this.http.post(this.baseUrl1, params, { withCredentials: true })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
    }
    getAdminProductList(): Observable<AdminProductSelect[]> {
        return this.http.get(this.baseUrl, { withCredentials: true })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
    }


    saveEditProductDetails(formData: Object): Observable<JSON> {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        return this.http.post(this.baseUrl3, formData, { headers: this.headers, withCredentials: true }) // ...using post request
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error || 'Server error')); //...errors if any
    }
}