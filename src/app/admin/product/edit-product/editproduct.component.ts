import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AdminProduct } from './adminproduct';
import { AdminEditProductService } from './editproduct.service';
import { AdminAddProductService } from '../add-product/addproduct.service';
import { Platform } from '../../platform/platform';
import { EditPlatformService } from '../../platform/edit-platform/editplatform.service';
import { ManageUserService } from '../../platform/manageuser.service';
import { AdminProductSelect } from './adminproductselect';
@Component({
    selector: 'editProduct',
    templateUrl: './editproduct.component.html',
    styleUrls: ['./editproduct.component.less']
})

export class AdminEditProductComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
    errorMessage: string;
    editPRO: AdminProduct;
    showModal: boolean = false;
    loaded: boolean = false;
    imageLoaded: boolean = false;
    imageSrc: string = '';
    file: Blob;
    returnVal: JSON;
    textValue = [];
    saveList: Array<string> = [];
    platformDetails: Platform[];
    showtext: boolean = true;
    showmultiple: boolean;
    selectOption: any;
    select: any;
    listPRO: AdminProductSelect[];
    prodCategory: string = "PRIVATE";
    fileUpdated: string;
    apProdList: Array<string> = [];
    isApigeeProd: string = "nonapigee";
    updatedPlatformId: string;
    selectPlatform: string = "0";
    selectCategory: string = 'nonapigee';
    updatedMsg: boolean = false;
    category = [{ name: "Non-Apigee Product", code: "nonapigee" }, { name: "Link Apigee Product", code: "realtime" }];
    isError: boolean = false;
    errorMsg: string;
    selectedProduct: string = "0";
    selectedApigeeProd: Array<string>;
    productNameResp = JSON;
    isNamenotAvailable = false;
    pName: string = '';
    selectProName: boolean = false;
    fr: FileReader = new FileReader();
    constructor(private _fb: FormBuilder, private _editproductService: AdminEditProductService, private _addplatformService: EditPlatformService, private _manageUserService: ManageUserService, private _addProductService: AdminAddProductService) { }

    ngOnInit() {
        this.myForm = this._fb.group({
            prodDescription: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][\na-zA-Z0-9 \-]*")]],
            prodName: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9\-][a-zA-Z0-9 \-]*")]],
            editProduct: ['', Validators.compose([Validators.required, this.validateDefaultProduct])],
            prodLocation: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            fileUpload: [{ value: '', disabled: true }],
            chooseCategory: [{ value: '', disabled: true }],
            platform: [{ value: '', disabled: true }, Validators.compose([Validators.required, this.validateDefaultProduct])],
            prodCategory: [{ value: '', disabled: true }]
        });

        this.getDetails();
        this.select = "Non-Apigee Product";

    }
    validateDefaultProduct(fieldControl: FormControl) {
        if (fieldControl.value != undefined && fieldControl.value != null) {
            return fieldControl.value === '0' ? { defaultError: "error" } : null;
        }
        else {
            return null;
        }
    }

    checkProductName() {
        let input = this.myForm.controls['prodName'].value;
        if ((this.pName.toLowerCase() === input.toLowerCase() && input !== '') || input === null)
            this.isNamenotAvailable = false;
        if (this.pName.toLowerCase() !== input.toLowerCase() && input !== '' && input !== null) {
            this._addProductService.checkProdctName(input).subscribe(
                prodName => {
                    this.productNameResp = prodName;
                    if (this.productNameResp['result'] == "exists") {
                        this.isNamenotAvailable = true;
                    }
                    else {
                        this.isNamenotAvailable = false;
                    }
                },
                error => { console.log(error) }

            )
        }
    }

    isApigeeProduct(apigeeProd) {
        if ("realtime" == apigeeProd) {
            this.selectCategory = this.category[1].code;
            this.showmultiple = true;
            this.showtext = false;
        }
        else {
            this.selectCategory = this.category[0].code;
            this.showtext = true;
            this.showmultiple = false;
        }
    }

    getDetails() {
        this._manageUserService.getPlatforms().subscribe(
            UserDetails => this.platformDetails = UserDetails,
            error => { console.log(error) }
        );

        this._editproductService.getAdminProductList().subscribe(
            adminPRO => this.listPRO = adminPRO,
            error => { console.log(error) });

    }
    onChange(prodId) {
        if (prodId == "0") {
            this.myForm.get("prodName").reset();
            this.myForm.get("prodDescription").reset();
            this.myForm.get("prodLocation").reset();
            this.myForm.get("platform").reset();
            this.myForm.get("fileUpload").reset();
            this.showtext = true;
            this.showmultiple = false;
            this.selectedProduct = "0";
            this.selectPlatform = "0";
            this.fileUpdated = "";
            this.myForm.get("platform").disable();
            this.myForm.get("prodCategory").disable();
            this.myForm.get("fileUpload").disable();
            this.myForm.get("chooseCategory").disable();
            this.prodCategory = "PRIVATE";
            this.selectCategory = "nonapigee";
            this.isError = false;
        }
        else {
            this._editproductService.getAdminProductDetails(prodId).subscribe(
                adminPRO => {
                    this.editPRO = adminPRO;
                },
                error => { console.log(error) },
                () => {
                    if (this.editPRO !== null && this.editPRO !== undefined) {
                        this.updateProductDetails();
                        this.isError = false;
                    }
                    else {
                        this.errorMsg = "Sorry, Communication error with Gateway, please come back later."
                        this.isError = true;
                    }

                }
            );
        }
        this.isNamenotAvailable = false;
    }

    updateProductDetails() {
        this.myForm.controls['prodName'].patchValue(this.editPRO.prodName);
        this.myForm.controls['prodDescription'].patchValue(this.editPRO.prodDs);
        this.myForm.controls['prodLocation'].patchValue(this.editPRO.prodLocTx);
        if (this.editPRO.apProdList[0] === 'NonApigeeProduct') {
            this.showtext = true;
            this.showmultiple = false;
            this.isApigeeProd = "nonapigee";
            this.selectCategory = this.category[0].code;
            this.isError = false;
        }
        else {
            this.showmultiple = true;
            this.showtext = false;
            this.apProdList = this.editPRO.apProdList;
            if(this.apProdList !== [] && this.apProdList !== undefined ){ 
            this.selectedApigeeProd = this.apProdList;
            this.isError = false;
            }
            else{
                this.errorMsg = "No Products available temporarily."
                this.isError = true;
            }
            this.isApigeeProd = "realtime";
            this.selectCategory = this.category[1].code;
        }
        this.prodCategory = this.editPRO.pvtCtgyId.trim();
        this.fileUpdated = this.editPRO.prodName + ".json";
        console.log("id", this.editPRO.orgId.toString());
        console.log("id1", this.editPRO.orgId);
        if (this.editPRO.orgId.toString() === "noOrg")
            this.selectPlatform = "0";
        else
            this.selectPlatform = this.editPRO.orgId.toString();
        this.pName = this.editPRO.prodName;
        this.selectProName = true;
        this.myForm.get("platform").enable();
        this.myForm.get("prodCategory").enable();
        this.myForm.get("fileUpload").enable();
        this.myForm.get("chooseCategory").enable();
    }

    handleInputChange(e) {
        this.file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

        let ext = this.file['name'].split('.');
        // if (ext[ext.length - 1] != 'json') {
        //     this.isInvalidFile = true;
        // }
        //else {
           // this.isInvalidFile = false;

            this.fr.onload = function (eve) { 
                /* let array = new Uint8Array(this.file.toString()), 
                 binaryString = String.fromCharCode.apply(null, array);*/ 
                //console.log(this.fr.result); 
            } 
            this.fr.readAsBinaryString(this.file); 
            //console.log(this.fr); 

        //}
    }

    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    }

    save(formValue, isValid) {
        if (this.isNamenotAvailable) {
            return false;
        }
        var input = new Object();
        var input1=new Object();
        input1['product_ID']= this.editPRO.prodId;
        input1['product_desc']= formValue.prodDescription;
        input1['product_name']= formValue.prodName;
        input1['orgId']= +formValue.platform.toString();
        input1['product_loc']= formValue.prodLocation;
        input1['category']= formValue.prodCategory;
        input['products']=input1;
        input['prevProdName']= this.editPRO.prodName;

        if (formValue.chooseCategory == "nonapigee") {
            input['nonApigeeProducts']= "NonApigeeProduct";
        }
        else {
            input["apigeeproducts"]= this.selectedApigeeProd;
        }

        
       // formValue.uploadFile = this.file;
        input['fileString']=this.fr.result;
        this._editproductService.saveEditProductDetails(input).subscribe(
            adminPl => {
                this.returnVal = adminPl
                console.log(this.returnVal);
                if (this.returnVal[0]['prodId'] != 0) {
                    if (this.returnVal[0]['product_desc'] != null) {
                        this.updatedMsg = false;
                        this.isError = true;
                        this.errorMsg = "JSON file is invalid.Kindly upload valid JSON file."
                    }
                    else {
                        this.updatedMsg = true;
                        this.isError = false;
                        this.submitted = true;
                        this.pName = formValue.prodName;
                    }

                }
                else {
                    this.updatedMsg = false;
                    this.isError = true;
                    this.errorMsg = "Failed to update Product. Kindly try after some time."
                }
            },
            error => { console.log(error) },
            () => { console.log(this.returnVal) })

    }
    removeUpdateMsg() {
        this.updatedMsg = false;
        this.isError = false
    }

}
