import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { DeleteProductService } from './delete-product.service';
import { ProdList } from './delete-product';
@Component({

    selector: 'deleteproduct',
    templateUrl: './delete-product.component.html',
    styleUrls: ['./delete-product.component.less']
})
export class DeleteProductComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
    errorMessage: string;
    product: ProdList[];
    updatedMsg: boolean = false;
    errorMsg: boolean = false;
    deleteProduct: string = "0";
    response: any;
    errorString: string = "Please select a Product.";
    errorClass: boolean = false;
    constructor(private _fb: FormBuilder, private _delproductService: DeleteProductService) { }
    ngOnInit() {
        this.myForm = this._fb.group({
            deleteProduct: ['']
        });
        this.getProductsList();
    }

    getProductsList() {
        this._delproductService.getProducts().subscribe(
            productDetails => this.product = productDetails,
            error => { console.log(error) });
    }
    onChange() {
        if (this.deleteProduct != undefined || this.deleteProduct === "0") {
            this.updatedMsg = false;
            this.errorMsg = false;
            this.errorClass = false;
        }
    }

    deleteRowForm() {

        if (this.deleteProduct === undefined || this.deleteProduct === "0") {
            this.errorString = "Please select a Product.";
            this.errorMsg = true;
            this.updatedMsg = false;
            this.errorClass = true;
        }
        else {
            let productID = this.deleteProduct['product_ID'];
            this._delproductService.deleteProduct(productID, this.deleteProduct['product_name']).subscribe(
                adminPl => {
                    this.response = adminPl;
                    console.log(this.response);
                    this.updatedMsg = true;
                    if (this.response['result'] == "Success") {
                        this.updatedMsg = true;
                        this.errorMsg = false;
                        this.errorClass = false;
                        this.product = this.product.filter(function (qt) {
                            if (qt.product_ID !== productID)
                                return qt;
                        })
                        this.deleteProduct = "0";
                    }
                    else {
                        this.errorString = "Error in deleting the product.Please try after sometime.";
                        this.updatedMsg = false;
                        this.errorMsg = true;
                    }

                },
                error => { console.log(error) }
            )
        }
    }
}