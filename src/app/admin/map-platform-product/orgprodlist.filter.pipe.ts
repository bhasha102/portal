import { Pipe, PipeTransform } from '@angular/core';
import { MapPlatformProduct } from './mapplatformproduct';

@Pipe({
    name: 'orgfilter',
    pure: false
})
export class OrgProdListFilterPipe implements PipeTransform {

    transform(item: MapPlatformProduct[], productnameFilter: string): any {
        if (productnameFilter === undefined) {
            return item;
        }
        return item.filter(function (qt) {
            if (qt.product_name.toLowerCase().indexOf(productnameFilter.toLowerCase()) !== -1)
                return qt.product_name.toLowerCase().includes(productnameFilter.toLowerCase());
        })
    }
}