import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Platform } from "../../admin/platform/platform";

@Injectable()
export class CreateAppServices
{

private mybaseUrl2: string = "./app/view-user-apps/createapp/productList.json";
private CREATE_APP_URL : string = "./app/view-user-apps/createapp/createApp.json";
private GET_USER_INFO_URL : string = "./app/view-user-apps/createapp/userInfo.json";
private GET_PLATFORM_LIST_URL : string = "./app/admin/platform/admin.json";
private GET_PLATFORM_PRODUCTS_URL : string = "./app/view-user-apps/createapp/fetchPlatformProducts.json";

constructor(private http : Http)
{
}

getPlatforms():Observable<Platform[]>
{
       
  return this.http.get(this.GET_PLATFORM_LIST_URL)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
     
}
  getPlatformProductList():Observable<any>
  {
    return this.http.get(this.GET_PLATFORM_PRODUCTS_URL)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
  }

  getProductList():Observable<Object[]>
  {
    return this.http.get(this.mybaseUrl2)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
  }
  
  createApp(app):Observable<any> {
    // TODO :: instead of get make use of post method with the "app"" data
    return this.http.get(this.CREATE_APP_URL)
    .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getUserInfo():Observable<any>
  {
    return this.http.get(this.GET_USER_INFO_URL)
    .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

}