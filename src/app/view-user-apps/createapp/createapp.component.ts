import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { CreateAppServices } from './createapp.service';
import { CreateApp } from './createapp';
import { Platform } from "../../admin/platform/platform";

@Component({
    selector: 'app-view-user-apps',
    templateUrl: './createapp.component.html',
    styleUrls: ['./createapp.component.less']
})

export class CreateAppComponent implements OnInit {
    public myForm: FormGroup;

    platforms: Platform[];
    selectedPlatform: any;
    platformProductMap: any;
    products: any[];
    selectedProducts: string[] = [];

    userInfo: Object;

    appCreated: boolean;

    createAppResponceMessage: string;

    // --------------------

    constructor(private _fb: FormBuilder, private _addproductService: CreateAppServices) { }

    ngOnInit() {
        this.myForm = this._fb.group(
            {
                applicationName: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
                descriptionName: ['', [<any>Validators.required, Validators.pattern("[a-zA-Z][a-zA-Z0-9 \-]*")]],
                callbackName: ['', [<any>Validators.required, Validators.pattern("^((https?|ftp)://)+([A-Za-z]+\\.)?[A-Za-z0-9-]+(\\.[a-zA-Z]{1,4}){1,2}(/.*\\?.*)?(/?[a-zA-Z0-9]*)*$")]],
                choosePlatform: [''],
                envName: [''],
                firstName: [''],
                lastName: [''],
                emailName: [''],
                userName: ['']
            });

        this._addproductService.getPlatforms().subscribe(
            platforms => this.onGetPlatforms(platforms, null),
            error => this.onGetPlatforms(null, error)
        );

        this._addproductService.getPlatformProductList().subscribe(
            result => this.onGetPlatformProductList(result, null),
            error => this.onGetPlatformProductList(null, error)
        );

        this._addproductService.getUserInfo().subscribe(
            userInfo => this.userInfo = userInfo,
            error => { console.log(error) }
        );
    }

    onGetPlatforms(result, error = null) {
        if (result) {
            this.platforms = result;
        }
        else {
            console.log(error);
        }
    }

    onGetPlatformProductList(result, error) {
        if (result) {
            this.platformProductMap = result;
        }
        else {
            console.log(error);
        }
    }
    onPlatformChange(platform) {
        this.products = this.getProductsByPlatformId(platform);
        this.selectedProducts = [];
        console.log("selected platform", platform);
        console.log('products', this.products);
    }

    getProductsByPlatformId(platformId): any[] {
        let products: any[] = this.platformProductMap[platformId];
        if (products) {
            let keyValue: string;
            let map: string[];
            for (let i = 0; i < products.length; i++) {

                keyValue = products[i];
                map = keyValue.split('_');
                products[i] = { id: map[0], name: map[1] }
            }
        }
        return products;
    }

    setProductSelected(selectElement) {
        this.selectedProducts = [];
        for (var i = 0; i < selectElement.options.length; i++) {
            var optionElement = selectElement.options[i];
            if (optionElement.selected) {
                this.selectedProducts.push(optionElement.value);
            }
        }
    }
    save(formValue, isValid) {

        if (this.selectedProducts.length <= 0) {
            return;
        }

        let app: Object = {
            appDetails: {
                environment: "SandBox",//formValue.envName,
                orgID: formValue.choosePlatform,
                appName: formValue.applicationName,
                callBackURL: formValue.callbackName,
                appDesc: formValue.descriptionName
            },
            productsIds: this.selectedProducts
        }
        console.log('create app from data', app);
        console.log('isValid: ' + isValid);

        this._addproductService.createApp(app).subscribe(
            result => this.onSaveResult(result, null),
            error => this.onSaveResult(null, error)
        );
    }

    onSaveResult(result, error: Error) {
        // console.log(result);
        this.appCreated = true;
        if (result) {
            if (result.result == "Success") {
               this.createAppResponceMessage = "Your app has been created successfully!";
            }
            else /* if (result.result == "error")*/ {
                this.createAppResponceMessage = "Unable to create your app. try again later";
            }
            this.myForm.reset();
            this.products = null;
            this.selectedProducts = [];
        }
        else {
            this.createAppResponceMessage = "Unable to create your app. try again later<br>" + error.message;
            console.error(error);
        }
    }
}

