export interface AppVo
{
    
    appId: string;//1381",
    appName: string;//My-App-Test-op1",
    appDesc: string;
    createdBy: string;
    callBackURL: string;
    clientId: string;
    clientSecret: string;
    environment: string;
    mappedProducts: string[];
    dateCreated:string;
}