import { Pipe, PipeTransform } from '@angular/core';
import { AppVo } from './app.vo';

@Pipe({
  name: 'userAppListFilter'
})
export class UserAppListFilterPipe implements PipeTransform {

  transform(items: AppVo[], args?: any): any {
    //console.log(args);
    if(args) {
      return items.filter(item => (item.appName.toLowerCase().indexOf(args) !== -1));
    }
    else {
      return items;
    }
  }

}