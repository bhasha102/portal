export interface Product{
    product_name: string,
    product_desc: string,
    dateCreated: string,
    product_ID : string, // for public products
    prodId : number, // for your "private" products
    orgId : number,
    orgName : string,
    roleName : string,
    roleId : number
}