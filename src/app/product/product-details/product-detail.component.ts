import { Component, EventEmitter, Input, OnChanges, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Product } from '../product';
import { ProductDetailService } from './product-detail.service';
import { SanitizeHtml } from './sanitizehtml.pipe';


@Component({
    selector: 'product-container',
    templateUrl: './product-detail.component.html',
    styleUrls: ['./product-detail.component.less', '../../../assets/axp-dev-portal/css/common.css']
})

export class ProductDetailComponent implements OnInit {
    sub: any;
    documentId: string;
    documentName: string;
    documentOrgId: number;
    documentRoleName: string;
    productsDetails: any[];
    responseObj: JSON;
    leftNav: JSON;
    documentation: JSON = JSON.parse('[]');
    lastModifiedFile: string;
    responseTxt: string;
    highlightedDiv: number;
    isPublic: boolean;
    docData: string;
    showModal: boolean = false;
    private timer;
    ticks = 0;
    isDocLoaded = false;
    listOfFiles = new Object();
    selectArrayFiles: Array<string> = [];
    constructor(private route: ActivatedRoute, private _productDetailService: ProductDetailService, private router: Router) {
        this.isPublic = route.snapshot.data[0]['isPublic'];
        console.log("Public : " + this.isPublic);
    }

    ngOnInit() {

        this.sub = this.route.params.subscribe(params => {
            this.documentId = params['id'];            // + converts string to number
            this.documentName = params['name'];
            if (!this.isPublic) {
                if (params['orgId']) {
                    this.documentOrgId = +params['orgId'];
                }

                if (params['roleName']) {
                    this.documentRoleName = params['roleName'];
                }
            }
        });

        if (this.isPublic) {
            this.getPublicDetails();
        }
        else {
            this.getUserDetails();
        }
        //window['CKEDITOR']['inline']( 'prodAPIDoc',{customConfig : '../../../assets/axp-dev-portal/ckeditor/config.js'} );      
    }

    getPublicDetails() {
        this._productDetailService.getPublicProductDetails(this.documentId, this.documentName).subscribe(
            response => {
                this.isDocLoaded = true;
                this.responseObj = response;
                this.documentation = this.responseObj['apiDocumentationData'];
                this.leftNav = this.responseObj['leftNavList'];
                this.lastModifiedFile = this.responseObj['lastModifiedFile'];
                this.listOfFiles = this.responseObj['listOfFiles'];
                for (let key in this.listOfFiles) {
                    console.log("key : " + key + " value : " + this.listOfFiles[key]);
                    this.selectArrayFiles.push(this.listOfFiles[key]);
                }
            },
            error => { console.log(error) }
        );
    }

    getUserDetails() {
        this._productDetailService.getUserProductDetails(this.documentId, this.documentName, this.documentRoleName, this.documentOrgId).subscribe(
            response => {
                this.responseObj = response;
                this.documentation = this.responseObj['apiDocumentationData'];
                this.leftNav = this.responseObj['leftNavList'];
                this.lastModifiedFile = this.responseObj['lastModifiedFile'];
                this.listOfFiles = this.responseObj['listOfFiles'];
                /* if(window['CKEDITOR'].instances['prodAPIDoc'] == undefined)
                 {
                     window['CKEDITOR']['inline']( 'prodAPIDoc',{customConfig : '../../../assets/axp-dev-portal/ckeditor/config.js'} );
                 }*/
            },
            error => { console.log(error) }
        );
    }

    loadDocument() {
        console.log(this.lastModifiedFile);
        this._productDetailService.loadDocument(this.lastModifiedFile).subscribe(
            response => {
                this.responseObj = response;
                this.documentation = this.responseObj['apiDocumentation'];
                this.leftNav = this.responseObj['leftNav'];
            },
            error => { console.log(error) }
        );
    }

    focusFunction() {
        console.log(window['CKEDITOR'].instances['prodAPIDoc']);
        if (window['CKEDITOR'].instances['prodAPIDoc'] == undefined) {
            window['CKEDITOR']['inline']('prodAPIDoc', { customConfig: '../../../assets/axp-dev-portal/ckeditor/config.js' });
        }
    }

    isActiveClass(index) {
        if (index == 0) {
            return "active";
        }
        return "";
    }

    addActiveClass(newValue: number) {
        if (this.highlightedDiv === newValue) {
            this.highlightedDiv = -1;
        }
        else {
            this.highlightedDiv = newValue;
        }
    }

    /* focusOutFunction(event)
     {
         console.log(event.target.id);
         let data = window['CKEDITOR'].instances['prodAPIDoc'].getData();
         //console.log(data);
         //console.log(window['CKEDITOR'].instances['prodAPIDoc']);
    }*/

    updateCurrentDoc(action) {

        this.docData = window['CKEDITOR'].instances['prodAPIDoc'] == undefined ? this.documentation : window['CKEDITOR'].instances['prodAPIDoc'].getData();
        this._productDetailService.updateDocumentation(this.lastModifiedFile, this.documentName, this.documentId, this.docData, action).subscribe(
            response => {
                this.responseObj = response;
                let tempObj = {};
                let isBetaVersion = false;
                let isPublishBetaVersion = false;
                let isPublishedCurrentVersion = false;
                let isPreviousVersion = false;

                if (action == 'publishBeta') {
                    let tempJSON: Object = new Object();
                    let index = 0;
                    this.selectArrayFiles.forEach(key => {

                        if (key == this.documentName + "_BetaVersion.json") {
                            this.selectArrayFiles.splice(index);
                            console.log("key : " + key);
                            console.log("index : " + index);
                        }
                        if (key == this.documentName + "_PublishedBetaVersion.json") {
                            isPublishBetaVersion = true;
                        }
                        index++;
                    });
                    if (!isPublishBetaVersion) {
                        this.selectArrayFiles.push(this.responseObj['retFileName']);
                        console.log("new beta publish file added : " + this.selectArrayFiles);
                        console.log(this.selectArrayFiles);
                    }
                }

                if (action == 'create') {
                    if ("fileBetaPresent" == this.responseObj['retFileName']) {
                        alert("Please published file to  Beta version then create new File");
                    } else {
                        /*for (let key in this.listOfFiles) {
                            console.log("key : " + key + " value : " + this.listOfFiles[key]);
                            if (key == this.documentName + "_BetaVersion.json") {
                                isBetaVersion = true;
                            }
                        }*/
                        this.selectArrayFiles.forEach(key => {
                            if (key == this.documentName + "_BetaVersion.json") {
                                isBetaVersion = true;
                            }
                        });
                        if (!isBetaVersion) {
                            this.selectArrayFiles.push(this.responseObj['retFileName']);
                            console.log("new beta publish file added : " + this.selectArrayFiles);
                            console.log(this.selectArrayFiles);
                        }
                    }
                }

                if (action == 'publishCurrent') {
                    if ("errorPublish" == this.responseObj['retFileName']) {
                        alert("Error in Publishing File. Please try again");
                    } else {
                        this.selectArrayFiles.forEach(key => {
                            if (key == this.documentName + "_PublishedCurrentVersion.json") {
                                isPublishedCurrentVersion = true;
                            }
                            if (key == this.documentName + "_PreviousVersion.json") {
                                isPreviousVersion = true;
                            }
                        });
                        if (!isPublishedCurrentVersion) {
                            this.selectArrayFiles.push(this.responseObj['retFileName']);
                            console.log("new beta publish file added : " + this.selectArrayFiles);
                            console.log(this.selectArrayFiles);
                        }
                        if (!isPreviousVersion) {
                            this.selectArrayFiles.push(this.documentName + "_PreviousVersion.json");
                            console.log("new previous publish file added : " + this.selectArrayFiles);
                            console.log(this.selectArrayFiles);
                        }
                    }
                }
                this.documentation = this.responseObj['apiDocumentation'];
                this.leftNav = this.responseObj['leftNav'];
                if ("fileBetaPresent" !== this.responseObj['retFileName'] && "errorPublish" !== this.responseObj['retFileName']) {
                    this.lastModifiedFile = this.responseObj['retFileName'];
                    this.successMessage(true);
                }
                console.log("selectArrayFiles : ");
                console.log(this.selectArrayFiles);
                console.log("FINAL lastModifiedFile : ");
                console.log(this.lastModifiedFile);
            },
            error => { console.log(error) }
        )

    }

    //Showing success message based on timer
    successMessage(isvisible) {
        this.timer = Observable.interval(1000).take(3);
        this.timer.subscribe(t => this.tickerFunc(t));
        this.showModal = isvisible;
    }

    tickerFunc(tick) {
        this.ticks = tick
        if (this.ticks >= 2) {
            this.showModal = false;
            this.ticks = 0;
        }
        return false;
    }

    navigateApiReference() {
        if (this.isPublic) {
            this.router.navigate(['/apiReference', this.documentId, this.documentName]);
        }
        else {
            this.router.navigate(['/apiReference', this.documentId, this.documentName, this.documentRoleName, this.documentOrgId]);
        }
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
        if (window['CKEDITOR'].instances['prodAPIDoc'] != undefined) {
            window['CKEDITOR'].instances['prodAPIDoc'].destroy(true);
        }
    }
}
