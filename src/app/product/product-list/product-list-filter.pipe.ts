import { Pipe, PipeTransform } from '@angular/core';
import { Product } from '../product';

@Pipe({
    name: 'productFilter',
})
export class ProductFilterPipe implements PipeTransform {

    transform(list: Product[], nameFilter: string, arg1, arg2, arg3, arg4): any {

        if (nameFilter === undefined) {
            return list;
        }

        return list.filter(function (qt) {
            if (qt[arg1].toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt[arg1].toLowerCase().includes(nameFilter.toLowerCase());
            if (qt[arg2].toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt[arg2].toLowerCase().includes(nameFilter.toLowerCase());
            if (qt[arg3].toString().indexOf(nameFilter) !== -1)
                return qt[arg3].toString().toLowerCase().includes(nameFilter.toLowerCase());
            if (qt[arg4].toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
                return qt[arg4].toLowerCase().includes(nameFilter.toLowerCase());
        })
    }
}