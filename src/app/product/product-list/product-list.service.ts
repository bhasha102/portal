import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Product} from '../product';
import {JsonpModule} from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ProductService{
  //private baseUrl: string = 'http://localhost:8085/dev-portal-intra/'; //"./app/product/product-list/productService.json";//'http://localhost:8085/dev-portal-intra/dummyProducts';
  private baseUrl: string = 'http://localhost:8085/dev-portal-intra/getPublicProducts/'; //"./app/product/product-list/productService.json";//'http://localhost:8085/dev-portal-intra/dummyProducts';
  private baseUrl2: string = 'http://localhost:8085/dev-portal-intra/getPrivateProducts/'; //"./app/product/product-list/productService.json";//'http://localhost:8085/dev-portal-intra/dummyProducts';
 
  constructor(private http : Http){
  }

  getPublicProductList():Observable<Product[]>{
	const headers = new Headers();


    return this.http.get(this.baseUrl,{ headers: headers,withCredentials:true })
    // ...and calling .json() on the response to return data
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw('Server error'))
  }

  getUserProductList():Observable<Product[]>{
	const headers = new Headers();


    return this.http.get(this.baseUrl2,{ headers: headers,withCredentials:true })
    // ...and calling .json() on the response to return data
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw('Server error'))
  }

  // Add a new product
    getProductDetails (body: Object): Observable<Product[]> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option

        return this.http.post(this.baseUrl, body, options) // ...using post request
                         .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }


}